//
// Created by szhao on 2/4/20.
//

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

#define MAX_VOLUME 5

typedef struct pool_t pool_t;
struct pool_t {
    int volume;
    pthread_mutex_t lock;
    pthread_cond_t can_fill;
    pthread_cond_t can_drill;
};

void *produce(void *arg) {

//    sleep(2);

    pool_t *p = (pool_t *) arg;
    while (1) {
        pthread_mutex_lock(&(p->lock));

        while (p->volume == MAX_VOLUME) {
            pthread_cond_wait(&(p->can_fill), &(p->lock));
        }

        p->volume += 1;
        printf("producing 1 unit, volume = %d\n", p->volume);

        pthread_cond_signal(&(p->can_drill));

        pthread_mutex_unlock(&(p->lock));
    }
}

void *consume(void *arg) {
    sleep(2);

    pool_t *p = (pool_t *) arg;
    while (1) {
        pthread_mutex_lock(&(p->lock));

        while (p->volume == 0) {
            pthread_cond_wait(&(p->can_drill), &(p->lock));
        }

        p->volume -= 1;
        printf("consuming 1 unit, volume = %d\n", p->volume);

//        pthread_cond_broadcast(&(p->can_fill));
        pthread_cond_signal(&(p->can_fill));
        pthread_mutex_unlock(&(p->lock));
    }
}

int main(int argc, char *argv[]) {
    pthread_t p, c;

    pool_t pool = {
            .volume = 5,
            .lock = PTHREAD_MUTEX_INITIALIZER,
            .can_fill = PTHREAD_COND_INITIALIZER,
            .can_drill = PTHREAD_COND_INITIALIZER
    };

    pthread_create(&p, NULL, produce, &pool);
    pthread_create(&c, NULL, consume, &pool);

    pthread_join(p, NULL);
    pthread_join(c, NULL);

}