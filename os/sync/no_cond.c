//
// Created by szhao on 2/3/20.
//

#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>

static int available = 0;
static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

void *thread_fun(void *arg) {
    int loops = *((int *) arg);

    for (int i = 0; i < loops; i++) {
        sleep(1);
        pthread_mutex_lock(&mtx);
        available++;
        pthread_mutex_unlock(&mtx);
    }
    return NULL;
}

int main(int argc, char *argv[]) {
    pthread_t tid[5];
    int products[5] = {100, 300, 200, 160, 220};
    for (int i = 0; i < 5; i++) {
        pthread_create(&tid[i], NULL, thread_fun, &products[i]);
    }
    int totalRequired = 100 + 300 + 200 + 160 + 220;
    int consumed = 0;

    time_t t = time(NULL);

    for( ; ; ) {

        pthread_mutex_lock(&mtx);
        while (available > 0) {
            consumed++;
            available--;
            printf("time = %ld: consumed = %d\n", (long)(time(NULL) - t), consumed);

        }
        pthread_mutex_unlock(&mtx);
        if (consumed >= totalRequired) {
            break;
        }
    }

    for (int j = 0; j < 5 ; ++j) {
        pthread_join(tid[j], NULL);
    }


}