//
// Created by szhao on 2/5/20.
//

#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <stdio.h>

typedef struct skier_t skier_t;
struct skier_t {
    int id;
    sem_t *sem;
};
void *ski(void *arg) {
    skier_t *s = (skier_t *) arg;
    sem_wait(s->sem);
    printf("skier %d on the car\n", s->id);
    sleep(s->id % 3 + 1);
    sem_post(s->sem);
    printf("skier %d off the car\n", s->id);
    return NULL;
}

int main(int argc, char *argv[]) {
    sem_unlink("car");

    sem_t *car = sem_open("car",  O_CREAT | O_EXCL, 0x0777, 5);

    pthread_t skiers[15];

    skier_t args[15];
    for (int i = 0; i < 15; i++) {
        args[i].id = i;
        args[i].sem = car;
    }

    for (int i = 0; i < 15; i++) {
        pthread_create(&skiers[i], NULL, ski, &args[i]);
    }

    for (int i = 0; i < 15; i++) {
        pthread_join(skiers[i], NULL);
    }

    sem_unlink("car");
}