//
// Created by szhao on 2/3/20.
//

#include <pthread.h>
#include <stdio.h>

static int glob = 0;
static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER; // mutual exclusive

void *thread_fun(void *arg) {
    int loops = *((int *) arg);

    for (int i = 0; i < loops; i++) {
        pthread_mutex_lock(&mtx);

        int loc = glob;
        loc += 1;
        glob = loc;

        pthread_mutex_unlock(&mtx);

    }
    return NULL;
}

int main(int argc, char *argv[]) {
    pthread_t t1, t2;
    int loops = 1000000;
    pthread_create(&t1, NULL, thread_fun, &loops);
    pthread_create(&t2, NULL, thread_fun, &loops);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    printf("glob = %d\n", glob);
}