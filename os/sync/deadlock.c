//
// Created by szhao on 2/3/20.
//

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

static pthread_mutex_t ma = PTHREAD_MUTEX_INITIALIZER; // mutual exclusive
static pthread_mutex_t mb = PTHREAD_MUTEX_INITIALIZER; 

void *thread_fun(void *arg) {
    char *id = (char *) arg;
    
    pthread_mutex_lock(&ma);
    
    printf("thread %s acquired lock A, sleeping... \n", id);
    sleep(1);
    printf("thread %s trying to acquire lock B...\n", id);
    
    pthread_mutex_lock(&mb);
    
    printf("thread %s entered critical section\n", id);
    
    pthread_mutex_unlock(&mb);
    pthread_mutex_unlock(&ma);
    
    return NULL;
}

void *another_thread_func(void *arg) {
    char *id = (char *) arg;
    pthread_mutex_lock(&mb);
    printf("thread %s acquired lock B, sleeping... \n", id);
    sleep(1);
    printf("thread %s trying to acquire lock A...\n", id);
    pthread_mutex_lock(&ma);

    printf("thread %s entered critical section\n", id);

    pthread_mutex_unlock(&ma);
    pthread_mutex_unlock(&mb);

    return NULL;  
}

int main(int argc, char *argv[]) {
    pthread_t t1, t2;
    pthread_create(&t1, NULL, thread_fun, "abc-123");
    pthread_create(&t2, NULL, another_thread_func, "xyz-678");

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    
}