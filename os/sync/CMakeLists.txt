cmake_minimum_required(VERSION 3.14)
project(os C)

set(CMAKE_C_STANDARD 99)

add_executable(thread_incr thread_incr.c)
target_link_libraries(thread_incr ${CMAKE_THREAD_LIBS_INIT})

add_executable(mutex mutex.c)
target_link_libraries(mutex ${CMAKE_THREAD_LIBS_INIT})

add_executable(no_cond no_cond.c)
target_link_libraries(no_cond ${CMAKE_THREAD_LIBS_INIT})

add_executable(deadlock deadlock.c)
target_link_libraries(deadlock ${CMAKE_THREAD_LIBS_INIT})

add_executable(producer_consumer prod_cons.c)
target_link_libraries(producer_consumer ${CMAKE_THREAD_LIBS_INIT})

add_executable(semaphore semaphore.c)
target_link_libraries(semaphore ${CMAKE_THREAD_LIBS_INIT})

add_executable(sem-01 sem-01.c)
target_link_libraries(sem-01 ${CMAKE_THREAD_LIBS_INIT})