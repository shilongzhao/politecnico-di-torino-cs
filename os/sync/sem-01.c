//
// Created by szhao on 1/31/21.
//

#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>

sem_t sem;

int count = 0;
void *incr(void *arg) {
    for (int i = 0; i < 10000; i++) {
        sem_wait(&sem);
        count += 1;
        sem_post(&sem);
    }
}

int main() {
    pthread_t p1, p2;
    sem_init(&sem, 0, 1);
    pthread_create(&p1, NULL, incr, NULL);
    pthread_create(&p2, NULL, incr, NULL);
    pthread_join(p1, NULL);
    pthread_join(p2, NULL);
    sem_destroy(&sem);

    printf("value of count is %d\n", count);
}