//
// Created by szhao on 2/3/20.
//

#include <pthread.h>
#include <stdio.h>

static int global = 0;

void *incr_func(void *arg) {
    int loops = * ((int *) arg);

    int local;
    for (int i = 0; i < loops; i++) {
        local = global; // global += 1;
        local += 1;
        global = local;
    }

    return NULL;
}

int main(int argc, char *argv[]) {
    pthread_t t1, t2;

    int loops = 10000;
    pthread_create(&t1, NULL, incr_func, &loops);
    pthread_create(&t2, NULL, incr_func, &loops);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    printf("global var = %d\n", global);
}