//
// Created by szhao on 1/30/20.
//

#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <stdio.h>

void handler(int sig) {
    printf("Ouch! PID %d received signal %d\n", getpid(), sig);
}
int main(int argc, char *argv[]) {
    
    printf("parent starting...PID = %d\n", getpid());
    signal(SIGINT, handler);

    int pid = fork();
    if (pid) {
        sleep(1);
        kill(pid, SIGINT);
        wait(NULL);
        printf("parent process finished\n");
    } else {
        printf("child process continue ... PID = %d\n", getpid());
        sleep(10); // receiving a signal will wake up the process!
        printf("child process finished\n");
    }
}