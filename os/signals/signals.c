//
// Created by szhao on 1/30/20.
//



#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


void handler(int sig) {
    printf("Ouch! received signal %d\n", sig);
}

void another_handler(int sig) {
    printf("Ciao! received signal %d\n", sig);
}


int main(int argc, char *argv[]) {
    // $ kill -2 PID, each time will print out an `Ouch`
    signal(SIGINT, handler);

    signal(SIGUSR1, another_handler);
    
    printf("pid = %d\n", getpid());

    for (int j = 0; ; j++) {
        printf("loop %d...\n", j);
        sleep(3);
    }
}