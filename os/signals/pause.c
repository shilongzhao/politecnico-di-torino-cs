//
// Created by szhao on 1/30/20.
//

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
void signal_handler(int sig) {
    printf("Ouch, received signal %d\n", sig);
}

int main(int argc, char *argv[]) {
    signal(SIGALRM, signal_handler);
    printf("PID = %d\n", getpid());
    // pause until receive the SIGALRM, kill -14 PID
    pause();
    printf("process resumed!\n");
    exit(0);
}