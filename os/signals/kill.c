//
// Created by szhao on 1/30/20.
//

#include <signal.h>
#include <stdio.h>
#include <unistd.h>

void handler(int sig) {
    printf("Ouch, received signal %d\n", sig);
}

void handler2(int sig) {
    printf("Ciao, received signal %d\n", sig);    
}

int main(int argc, char *argv[]) {
    int sig = 2;
    signal(sig, handler);
    signal(SIGUSR1, handler2);

    // send signal to a process (itself here)
    kill(getpid(), sig);
    kill(getpid(), SIGUSR1);

    return 0;
}