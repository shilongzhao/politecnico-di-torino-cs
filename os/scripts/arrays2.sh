#!/bin/bash

declare -A v # without this declare it does not work

v[alex]=22
name=bob
v[$name]=33

echo "array size ${#v[*]}"

for i in ${!v[*]} ; do 
  echo "name is $i, age is ${v[$i]}"
done

# all array elements ${a[*]}
# array size ${#a[*]}
# array index element ${a[2]}
# array indices ${!a[*]}
