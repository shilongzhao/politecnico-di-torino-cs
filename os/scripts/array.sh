#!/bin/bash


for i in {0..5} ; do 
  echo "input number $i:"
  read arr[$i]
done

echo "array size: ${#arr[*]}"

echo "array content: " ${arr[*]}

echo "array 2 = ${arr[2]}"

sum=0
for i in ${arr[*]} ; do 
 let sum=sum+$i
done

echo "sum = $sum"

sum2=0
for i in ${!arr[*]} ; do 
 let sum2=sum2+${arr[$i]}
done

echo "sum2 = $sum2" 
