#!/bin/bash

echo "input a name: "
read name
if [ -f $name ]; then 
  echo "$name is a file"
elif [ -d $name ]; then
  echo "$name is a directory"
else 
  echo "$name is neither file nor directory!"
fi


