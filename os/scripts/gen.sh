#!/bin/bash

if [ $# -lt 2 ] ; then 
  read n1
  read n2
else 
  n1=$1
  n2=$2
fi

echo "n1=$n1, n2=$n2"

i=0
n=0
while [ $i -lt $n1 ]; do 
  j=0
  while [ $j -lt $n2 ]; do
   echo -n "$n "
   let n=n+1
   let j=j+1
  done
  echo
  let i=i+1 
done
