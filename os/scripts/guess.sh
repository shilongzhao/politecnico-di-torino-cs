#!/bin/bash

# guess passwords, max 3 times

echo "input password:"
read g
count=0
while [ $g != "top_secret" ] ; do 
  let count=count+1

  if [ $count -eq 3 ] ; then 
    echo "exit, too many tries"
    break
  fi 

  echo "wrong guess, guess again:"
  read g
done
