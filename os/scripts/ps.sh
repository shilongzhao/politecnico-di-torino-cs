#!/bin/bash


declare -A pu #pid,user

declare -A pp #ppid.children

{
  read
  while read line; do 
    uid=$(echo $line | cut -d " " -f 1)
    pid=$(echo $line | cut -d " " -f 2)
    ppid=$(echo $line | cut -d " " -f 3)
    pu[$pid]=$uid
    pp[$ppid]="${pp[$ppid]} $pid"
  done

} < $1

for pid in ${!pu[*]}; do
  echo "process $pid has user ${pu[$pid]}, children ${pp[$pid]}"
done
