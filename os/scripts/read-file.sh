#!/bin/bash

# ./script.sh fname
# create a new file copy_fname

filename="copy_$1"

rm -f $filename

while read i ; do
  echo "LINE: $i" >> $filename
done < $1
