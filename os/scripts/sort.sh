#!/usr/bin/env bash

# comments
echo '$0_$0' # single quotes preserves literal value

echo "sorting file..."
cat ./words.txt | sort

echo "received command line param: $1"

echo "sorting file $1 with reverse order..."
cat $1 | sort -r