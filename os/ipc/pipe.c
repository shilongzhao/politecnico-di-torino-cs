//
// Created by szhao on 2/1/20.
//


#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define BUF_SIZE 5

int main(int argc, char *argv[]) {
    int fd[2] = {};
    char buf[BUF_SIZE];

    printf("fd0 = %d, fd1 = %d before pipe created\n", fd[0], fd[1]);
    if (pipe(fd) == -1) {
        return -1;
    }
    printf("fd0 = %d, fd1 = %d AFTER pipe created\n", fd[0], fd[1]);

    if (fork() == 0) {

        // getpid(), getppid();
        close(fd[1]); // write end is unused by child
        for (;;) {
            int n = read(fd[0], buf, BUF_SIZE);
            printf("\nread %d characters\n", n);
            if (n == 0) {
                break;
            }
            printf("\nread from pipe: \n");
            write(STDOUT_FILENO, buf, n); // printf()
        }
        close(fd[0]);
        exit(EXIT_SUCCESS);
    }
    else {
        close(fd[0]);
        write(fd[1], argv[1], strlen(argv[1]));
        close(fd[1]);
        wait(NULL);
        exit(0);
    }
}