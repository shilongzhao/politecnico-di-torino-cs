//
// Created by szhao on 1/14/20.
//

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {

    printf("starting program .... \n");
    int pid = fork();
    if (pid != 0) {
        printf("parent process %d continue working ... \n", getpid());
        wait(NULL);
        printf("child %d terminated", pid);
    }
    else {
//        char *args[3] = {"command-line", "-a", "-l"};
//        execvp("ls", args);
        execl("/bin/ls", "-a", "-l", "/home/szhao", NULL);
    }


}