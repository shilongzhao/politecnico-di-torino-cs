//
// Created by szhao on 1/30/20.
//

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    printf("parent %d starts ... \n", getpid());

    int f = fork();

    if (f == 0) {
        printf("child %d sleeping ...\n", getpid());

        sleep(10);

        printf("child %d wake up!\n", getpid());
    }
    else {

        printf("parent waiting ...\n");

        wait(NULL);

        printf("parent can terminate now ... \n");
    }
}