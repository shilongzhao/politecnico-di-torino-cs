//
// Created by szhao on 1/14/20.
//

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

    printf("Hello, this is process PID = %d\n", getpid());

    int i = fork();
    if(i == 0) {
        printf("I am child, my PID is %d\n", getpid());
    }
    else {
       printf("I am father, my child PID is %d, my PID is %d\n", i, getpid());
    }

    printf("BOTH parent and child will execute this (PID = %d)\n", getpid());
}
