//
// Created by szhao on 2/1/20.
//

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void *thread_func(void *arg) {
    char *s = (char *) arg;
    printf("thread %ld process %d executing %s\n", pthread_self(), getpid(), arg);
    return (void *) strlen(s);
}

int main(int argc, char *argv[]) {
    pthread_t t1;

    char *str = "HELLO world!\n";
    pthread_create(&t1, NULL, thread_func, str);

    void *res;
    pthread_join(t1, &res);

    printf("thread returned string length %ld to process %d\n", (long) res, getpid());

    exit(EXIT_SUCCESS);
}