//
// Created by szhao on 2/1/20.
//

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

typedef struct arr_t arr_t;
struct arr_t {
    int *start;
    int size;
};

typedef struct stats_t stats_t;
struct stats_t {
    int max;
    int min;
    int sum;
};

void *statistics(void *arg) {

    arr_t *array = (arr_t *) arg;

    int max = INT_MIN, min = INT_MAX, sum = 0;
    for (int i = 0; i < array->size; i++) {
        int v = (array->start)[i];
        if (v > max) max = v;
        if (v < min) min = v;
        sum += v;
    }

    stats_t *s = malloc(sizeof(struct stats_t));
    s->max = max;
    s->min = min;
    s->sum = sum;

    return s;

}
int main(int argc, char *argv[]) {
    pthread_t t2;

    int a[10] = {1,2,3,4,5,6,7,8,9,0};
    arr_t arr = {
            .start = a,
            .size = 10
    };

    pthread_create(&t2, NULL, statistics, &arr);

    stats_t *res;
    pthread_join(t2, (void **) &res);

    printf("sum = %d, max = %d, min = %d", res->sum, res->max, res->min);
}