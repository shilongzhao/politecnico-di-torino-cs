//
// Created by szhao on 1/5/20.
//

#include <fcntl.h>
#include <unistd.h>
#include <string.h>


#include <stdio.h>

int main(int argc, char *argv[]) {

    printf("o wronly = %d\n, o append = %d\n, WR|APP = %d\n", O_WRONLY, O_APPEND, O_WRONLY|O_APPEND);

    int fd = open("/home/szhao/a.txt", O_WRONLY | O_APPEND); // bitwise operator, &, |, ^, ~

     char s[100] = "last line!\n";

    write(fd, s, strlen(s) + 1);

    close(fd);
}