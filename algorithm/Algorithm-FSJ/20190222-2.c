//
// Created by zhaos on 6/16/2019.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct list1_t list1_t ;
typedef struct list2_t list2_t ;

struct list1_t {
    int key ;
    int count ;
    list1_t *next ;
};

struct list2_t {
    int key;
    list2_t *next ;
};

void insert_ordered(list2_t **h, int v) {
    // TODO: ordered insert
}

void insert(list2_t **h2  , list1_t *h1){
    int k = h1 ->count ;
    for(int i =  0 ; i < k ; i ++){
//        list2_t *temp2 = malloc(1*sizeof(list2_t));
//        temp2 ->key = h1->key ;
//        temp2 ->next = *h2 ;
//        *h2 = temp2 ;
        insert_ordered(h2, h1->key);
    }
}

list2_t *list_expand(list1_t *p1){
    list2_t *h2 = NULL ;
    for(list1_t *h1 = p1 ; h1 != NULL ; h1 = h1 -> next){
        insert(&h2 , h1) ;
    }
    return h2 ;
}

int main() {
    list1_t a = {3, 5, NULL};
    list1_t b = {4, 3, NULL};
    list1_t c = {2, 1, NULL};
    list1_t d = {6, 2, NULL};
    a.next = &b;
    b.next = &c;
    c.next = &d;
    d.next = NULL;

    list2_t *r = list_expand(&a);

    for (list2_t *h = r; h != NULL; h = h->next) {
        printf("%d \n", h->key);
    }
}