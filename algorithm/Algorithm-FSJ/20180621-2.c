//
// Created by zhaos on 6/16/2019.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *left;
    node_t *right;
};

int max(int a, int b) {
    if (a > b) return a;
    return b;
}

int height(node_t *root) {
    if (root == NULL) {
        return -1;
    }
    return max( height(root->left), height(root->right) ) + 1;
}

/**
 * 经过root节点的最大路径的长度
 * @param root
 * @return
 */
int max_diameter(node_t *root) {
    return 2 + height(root->left) + height(root->right);
}

void iterate(node_t *root, int *max) {
    if (root == NULL) {
        return;
    }

    int d = max_diameter(root);
    if (d > *max) {
        *max = d;
        printf("find max diameter: %d, root %d \n", d, root->k);
    }

    iterate(root->left, max);
    iterate(root->right, max);
}

int tree_diameter(node_t *tree) {
    int max = 0;
    iterate(tree, &max);
    return max;
}

void preorder(node_t *root) {
    if (root == NULL) return;
    printf("%d ", root->k);
    preorder(root->left);
    preorder(root->right);
}

int main() {
    node_t t30,t19,t21,t45,t38,t36,t31,t67,t63,t69;
    t30.k=30;
    t19.k=19;
    t21.k=21;
    t45.k=45;
    t38.k=38;
    t36.k=36;
    t31.k=31;
    t67.k=67;
    t63.k=63;
    t69.k=69;
    t30.left=&t19;
    t30.right=&t45;
    t19.left=NULL;
    t19.right=NULL;
    t21.left=NULL;
    t21.right=NULL;
    t45.left=&t38;
    t45.right=&t67;
    t38.left=&t36;
    t38.right=NULL;
    t36.left=&t31 ;
    t36.right=NULL;
    t31.left=NULL;
    t31.right=NULL;
    t67.left=&t63;
    t67.right=&t69;
    t63.left=NULL;
    t63.right=NULL;
    t69.left=&t21;
    t69.right=NULL;

    preorder(&t30);

    int r = tree_diameter(&t30);
    printf("final result = %d\n", r);
}