//
// Created by zhaos on 1/21/2019.
//

#include <stdio.h>
int main() {
    int income;
    scanf("%d", &income);

    double tax;
    if (0 < income && income <= 1000) {
        tax = income * 0.2;
    }
    else if (income <= 1500) {
        tax = 1000 * 0.2 + (income - 1000) * 0.25;
    }
    else if (income <= 2000) {
        tax = 1000 * 0.2 + 500 * 0.25 + (income - 1500) * 0.3;
    }
    else {
        tax = 1000 * 0.2 + 500 * 0.25 + 500 * 0.3 + (income - 2000) * 0.4;
    }

    printf("tax = %f\n", tax);

}