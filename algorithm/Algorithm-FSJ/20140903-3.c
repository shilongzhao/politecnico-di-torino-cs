//
// Created by zhaos on 2/18/2019.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    char *name;
    char *surname;
    int grade;
    node_t *sibling;
    node_t *first_child;
};
void preorder(node_t *root) {
    if (root == NULL) return;
    printf("%s %s %d\n", root->name, root->surname, root->grade);
    preorder(root->sibling);
    preorder(root->first_child);
}