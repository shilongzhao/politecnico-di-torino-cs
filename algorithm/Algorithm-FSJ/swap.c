//
// Created by zhaos on 2/9/2019.
//
#include <stdio.h>

void add_one(int *x) {
    *x = *x + 1;
}

int add_one2(int x) {
    return x + 1;
}

int factorial(int x) {
    int r = 1;
    for (int i = 1; i <= x; i++) {
        r = r * i;
    }
    return r;
}

int main() {
    int v = 123;
    add_one(&v);
    v = add_one2(v);
    printf("x = %d\n", v);
    
    int n = 10;
    int r = factorial(n);
}