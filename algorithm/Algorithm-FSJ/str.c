//
// Created by zhaos on 1/25/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char s1[20] = "Appollo!";
    char s2[20] = "Apple19";

    printf("s1 compare s2 = %d\n", strcmp(s1, s2));
    strcat(s1, s2);
    printf("%s, %s\n", s1, s2);
    strcpy(s1, s2);
    printf("%s, %s\n", s1, s2);

    char s3[30] = "hello? good morning!";
    int a[26] = {}; // a[0] - 'a', a[1] - 'b', a[2] - 'c', ..., a[25] - 'z'
    for (int i = 0; i < strlen(s3); i++) {
        char c = s3[i];
        if (c >= 'a' && c <= 'z') {
            int x = c - 'a';
            a[x] = a[x] + 1;
        }
    }
    for (int i = 0; i < 26; i++) {
        printf("%c - %d\n", i + 'a', a[i]);
    }


}