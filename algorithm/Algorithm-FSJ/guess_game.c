//
// Created by zhaos on 1/21/2019.
//

#include <stdio.h>
int main() {
    int secret = 111;
    int guess;
    scanf("%d", &guess);
    while (guess != secret) {
        if (guess > secret) {
            printf("too big!\n");
        }
        else {
            printf("too small!\n");
        }
        scanf("%d", &guess);
    }

    printf("you are right! secret is %d\n", secret);
}