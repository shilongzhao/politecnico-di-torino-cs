//
// Created by zhaos on 2/14/2019.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *next;
};


void print_list(node_t *h) {
    for ( ; h != NULL; h = h->next) {
        printf("%d\n", h->k);
    }
}

void print_list2(node_t *h) {
    for (node_t *p = h; p != NULL; p = p->next) {
        printf("%d\n", p->k);
    }
}

void print_list_r(node_t *h) {
    if (h == NULL) {
        return;
    }
    printf("%d\n", h->k);
    print_list_r(h->next);
}

node_t *find(node_t *h, int x) {
    for (node_t *p = h; p != NULL; p = p->next) {
        if (p->k == x) {
            return p;
        }
    }
    return NULL;
}

void print_list_rr(node_t *h) {
    if (h == NULL) {
        return;
    }
    print_list_rr(h->next);
    printf("%d\n", h->k);

}

// insert


int main() {
    node_t n1 = {3, NULL};
    node_t n2 = {5, NULL};
    node_t n3 = {1, NULL};
    node_t n4 = {4, NULL};

    n1.next = &n2;
    n2.next = &n3;
    n3.next = &n4;
    print_list_rr(&n1);
}
