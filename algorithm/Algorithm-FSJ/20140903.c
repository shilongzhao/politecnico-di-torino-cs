//
// Created by zhaos on 2/17/2019.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct node_t node_t;
struct node_t {
    char *str;
    node_t *next;
};

node_t *splitStr(char *str) {
    node_t *head = NULL;
    char *q = str;
    char *p = strchr(str, '.');
    while (p != NULL) {
        *p = '\0';
        printf("inserting substring %s\n", q);

        node_t *t = malloc(sizeof(node_t));
        t->str = strdup(q); // malloc + strcpy
        t->next = head;
        head = t;

        q = p + 1;
        p = strchr(q, '.');
    }
    node_t *t = malloc(sizeof(node_t));
    t->str = strdup(q); // malloc + strcpy
    t->next = head;
    return t;
}
int main() {
//    char str[100] = "a.bb.ccc.dddd.eeee.fffff";
//    printf("=== looking for . in string %s === \n", str);
//
//    char *q = str;
//    char *p = strchr(str, '.');
//
//    while (p != NULL) { // found a '.', pointed by p
//        *p = '\0'; // replace . with '\0'
//        printf("find substring %s\n", q); // print string from q to \0
//        q = p + 1;
//        p = strchr(q, '.');
//    }
//    printf("find substring %s\n", q);

    char str[100] = "a.bb.ccc.dddd.eeee.fffff";
    node_t *h = splitStr(str);
    for (node_t *p = h; p != NULL; p = p->next) {
        printf("%s\n", p->str);
    }
}