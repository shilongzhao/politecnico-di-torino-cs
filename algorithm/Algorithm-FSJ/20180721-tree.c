//
// Created by zhaos on 2/18/2019.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *left;
    node_t *right;
};
int max(int a, int b) {
    if (a > b) return a;
    return b;
}
int height(node_t *root, int *max_diameter) {
    if (root == NULL) {
        return -1;
    }
    int h1 = height(root->left, max_diameter);
    int h2 = height(root->right, max_diameter);

    int diameter = h1 + h2 + 2;

    if (diameter > *max_diameter) {
        *max_diameter = diameter;
    }

    return 1 + max(h1, h2);
}