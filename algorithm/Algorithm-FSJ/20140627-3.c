//
// Created by zhaos on 2/16/2019.
//
#include <stdlib.h>
#include <stdio.h>
typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *prev;
    node_t *next;
};
void listInsert (node_t **left, node_t **right, int key, int leftRight) {

    node_t *p = (node_t *) malloc(sizeof(node_t));
    if (p == NULL) {
        fprintf(stderr, "malloc fails\n");
        return;
    }
    p->k = key;
    p->next = p->prev = NULL;

    if (*left == NULL && *right == NULL) {
        *left = p;
        *right = p;
        return;
    }

    if (leftRight == 0) {
        p->next = *left;
        (*left)->prev = p;
        *left = p;
        return;
    }
    else {
        p->prev = *right;
        (*right)->next = p;
        *right = p;
    }
}

void listDisplay (node_t *left, node_t *right, int leftRight) {
    if (leftRight == 0) {
        for (node_t *p = left; p != NULL; p = p->next) {
            fprintf(stdout, "%d \n", p->k); // printf
        }
    }
    else {
        for (node_t *q = right; q != NULL; q = q->prev) {
            fprintf(stdout, "%d \n", q->k);
        }
    }
}
int main() {
    node_t *left = NULL;
    node_t *right = NULL;
    listInsert(&left, &right, 1, 0);
    listInsert(&left, &right, 2, 0);
    listInsert(&left, &right, 3, 0);
    listInsert(&left, &right, 4, 0);
    listInsert(&left, &right, 5, 0);
    listInsert(&left, &right, 6, 1);
    listInsert(&left, &right, 7, 1);
    listInsert(&left, &right, 8, 1);

//    listDisplay(left, right, 0);
    listDisplay(left, right, 1);


}