//
// Created by zhaos on 2/13/2019.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
typedef int Integer;
typedef struct student student;
struct student {
    Integer  id;
    char *name;
    char *surname;
    int  *grade;
    int  n;
};
//
//typedef struct student2 student2;
//struct student2 {
//    int id;
//    char name[10];
//    char surname[10];
//    int grade[10];
//};

void student_info(student s) {
    printf("FUNCTION: address of s is %p\n", &s);
    printf("FUNCTION: <name> address is %p\n", (void *) s.name);

    int sum = 0;
    for (int i = 0; i < s.n; i++) {
        sum += s.grade[i];
    }
    printf("Student[%d, %s %s, %f]\n", s.id, s.name, s.surname, sum/(double) s.n);
}

void student_info2(student *s) {
    int sum = 0;
    for (int i = 0; i < s->n; i++) {
        sum = sum + s->grade[i];
    }
    printf("Student[%d, %s %s, %.2f]\n", s->id, s->name, s->surname, sum/(double)s->n);
}
int main() {
    student s = {0, NULL, NULL, NULL, 0};

    printf("memory size of a student = %d\n", sizeof(s));

    s.name = strdup("Alex");

    s.surname = strdup("Halloween");

    printf("student s name is %s\n", s.name);
    printf("student s surname is %s\n", s.surname);
    s.name[0] = 'a';
    printf("student s name is %s\n", s.name);
    printf("memory size of student s = %d\n", sizeof(s));

    s.grade = malloc(sizeof(int) * 2);
    s.grade[0] = 20;
    s.grade[1] = 24;
    s.n = 2;

    printf("MAIN: address of s is %p\n", &s);
    printf("MAIN: <name> address is %p\n", (void *) s.name);

    student_info2(&s);


}