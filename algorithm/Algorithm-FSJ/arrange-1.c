//
// Created by zhaos on 2/19/2019.
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void gen(char *s, char *r, int d, int n) {
    if (d == n) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        r[d] = s[i];
        gen(s, r, d + 1, n);
        r[d] = '\0';
    }
}


void gen_nonrepeat(char *s, char *r, int d, int n, int *used) {
    if (d == n) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        if (used[i] == 0) {
            r[d] = s[i];
            used[i] = 1;
            gen_nonrepeat(s, r, d + 1, n, used);
            used[i] = 0;
            r[d] = '\0';
        }
    }
}

void gen_repeat2(char *s, char *r, int d, int n, int *used) {
    if (d == n) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        if (used[i] < 2) {
            r[d] = s[i];
            used[i] = used[i] + 1;
            gen_repeat2(s, r, d + 1, n, used);
            used[i] = used[i] - 1;
            r[d] = '\0';
        }
    }
}

void combo(char *s, char *r, int d, int n, int lastPick) {
    if (d == n) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }

    for (int i = lastPick + 1; i < strlen(s); i++) {
        r[d] = s[i];
        combo(s, r, d + 1, n, i);
        r[d] = '\0';
    }

}
int main() {
    char s[6] = "ABCD";
    char r[5] = "";
//    gen(s, r, 0, 3);
    int used[4] = {};
    gen_repeat2(s, r, 0, 3, used);
}