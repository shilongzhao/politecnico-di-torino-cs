//
// Created by zhaos on 2/17/2019.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
struct node {
    char *s;
    struct node *left;
    struct node *right;
};
int treeIsomorph (struct node *t1, struct node *t2) {
    if (t1 == NULL && t2 == NULL) {
        return 1;
    }
    else if (t1 != NULL && t2 != NULL) {
        int r = treeIsomorph(t1->left, t2->left)
                && treeIsomorph(t1->right, t2->right)
                && (strcmp(t1->s, t2->s) == 0);
        return r;
    }
    else {
        return 0;
    }
}

int main() {

}