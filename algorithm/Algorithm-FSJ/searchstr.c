//
// Created by zhaos on 2/10/2019.
//

// 20140903

#include <stdio.h>
#include <string.h>

void searchStr(char *str, int *start, int *length) {
    int st = 0, len = 0;
    int i = 0;
    while (i < strlen(str)) {
        if (str[i] == str[i+1]) {
            len = len + 1;
            i++;
        }
        else {
            if (len > *length) {
                *length = len;
                *start = st;
            }
            i++;
            len = 1;
            st = i;
        }
    }
}

int main() {
    char str[100] = "aabbbccdddddeee";
    int start = 0, length = 0;
    searchStr(str, &start, &length);
    printf("starts from %d, length %d\n", start, length);
}