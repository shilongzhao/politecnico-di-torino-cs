//
// Created by zhaos on 2/19/2019.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int all_included(char *s, char *r) {
    for (int i = 0; i < strlen(s); i++) {
        if (strchr(r, s[i]) == NULL) {
            return 0;
        }
    }
    return 1;
}

void gen(char *s, char *r, int d, int N) {
    if (d == N) {
        r[d] = '\0';
        if (all_included(s, r)) {
            printf("%s\n",r);
        }
        return;
    }
    
    for (int i = 0; i < strlen(s); i++) {
        r[d] = s[i];
        gen(s, r, d + 1, N);
        r[d] = '\0';
    }
}

int main() {
    int N;
    scanf("%d", &N);
    char *r = malloc(sizeof(char) * (N  + 1));
    gen("AEIOU", r, 0, N);
}