//
// Created by zhaos on 1/25/2019.
//

#include <stdio.h>
int main() {
    int m;
    scanf("%d", &m);
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            if (i + j < m - 1) {
                printf(" ");
            }
            else {
                printf("*");
            }
        }
        printf("\n");
    }
}