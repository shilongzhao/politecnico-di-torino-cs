//
// Created by zhaos on 2/17/2019.
//
#include <stdlib.h>
#include <stdio.h>
int max_len(int *v, int n) {
    int count = 0;
    int max = 0;
    for (int i = 0; i < n; i++) {
        if (v[i] != 0) {
            count++;
            if (count > max) {
                max = count;
            }
        }
        else {

            count = 0;
        }
    }
    return max;
}
int contains_zero(int *v, int start, int count) {
    for (int i = start; i < start + count; i++) {
        if (v[i] == 0) {
            return 1;
        }
    }
    return 0;
}
void print_subarray(int *v, int start, int count) {
    for (int i = start; i < start + count; i++) {
        printf("%d ", v[i]);
    }
    printf("\n");
}
void subvet_write(int *v, int n) {
    int count = max_len(v, n);
    for (int i = 0; i <= n - count; i++) {
        if (!contains_zero(v, i, count)) {
            print_subarray(v, i, count);
        }
    }
}

int main() {
    int v[20] = {1,0,2,3,0,4,5,6,7,0,8,9,10,11,0,1,2,3,4,5};

//    printf("max subarray len %d\n", max_len(v, 20));
    subvet_write(v, 20);

}