//
// Created by zhaos on 2/17/2019.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *left;
    node_t *right;
};

void preorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%d\n", root->k);
    preorder(root->left);
    preorder(root->right);
}

void inorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    inorder(root->left);
    printf("%d\n", root->k);
    inorder(root->right);
}

void postorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    postorder(root->left);
    postorder(root->right);
    printf("%d\n", root->k);
}

// BST
void insert(node_t **root, int v) {
    if (*root == NULL) {
        node_t *t = malloc(sizeof(node_t));
        t->left = t->right = NULL;
        t->k = v;
        *root = t;
        return;
    }

    if (v == (*root)->k) {
        return;
    }
    else if (v < (*root)->k) {
        insert(&((*root)->left), v);
    }
    else {
        insert(&((*root)->right), v);
    }
}

int main() {
    node_t n1 = {1, NULL, NULL};
    node_t n2 = {2, NULL, NULL};
    node_t n3 = {3, NULL, NULL};
    node_t n4 = {4, NULL, NULL};
    node_t n5 = {5, NULL, NULL};
    node_t n6 = {6, NULL, NULL};
    node_t n7 = {7, NULL, NULL};
    node_t n8 = {8, NULL, NULL};
    node_t n9 = {9, NULL, NULL};
    node_t n10 = {10, NULL, NULL};

    n1.left = &n2;
    n1.right = &n3;

    n2.left = &n4;
    n2.right = &n5;

    n3.left = &n6;
    n3.right = &n7;

    n4.left = &n8;
    n4.right = &n9;

    n5.left = &n10;

    printf("preorder:\n");
    preorder(&n1);
    printf("inorder:\n");
    inorder(&n1);

    node_t *root = NULL;
    insert(&root, 8);
    insert(&root, 5);
    insert(&root, 12);
    insert(&root, 3);
    insert(&root, 7);
    insert(&root, 10);
    insert(&root, 15);
    insert(&root, 2);
    insert(&root, 4);
    insert(&root, 6);
    insert(&root, 9);
    insert(&root, 11);

    printf("==== inorder BST =====\n");
    inorder(root);

}