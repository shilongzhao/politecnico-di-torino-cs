//
// Created by zhaos on 2/17/2019.
//
#include <stdlib.h>
#include <stdio.h>
struct node {
    int k;
    struct node *left;
    struct node *right;
};
struct node *treeMirror(struct node *root) {
    if (root == NULL) {
        return NULL;
    }
    struct node *t = root->left;
    root->left = root->right;
    root->right = t;
    treeMirror(root->left);
    treeMirror(root->right);
}
// 20150202
int main() {

}