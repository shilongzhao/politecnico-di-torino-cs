//
// Created by zhaos on 2/17/2019.
//

// TODO: runtime error
#include <stdlib.h>
#include <stdio.h>

typedef struct interval_t interval_t;
struct interval_t {
    int *v;
    int start;
    int end;
    interval_t *next;
};
void print_interval(int *v, int start, int end) {
    for (int i = start; i <= end; i++) {
        printf("%d ", v[i]);
    }
    printf("\n");
}
void print_intervals(interval_t *h) {
    while (h != NULL) {
        print_interval(h->v, h->start, h->end);
        h = h->next;
    }
}

void subarray_write(int *v, int n) {
    interval_t **intervals = malloc(sizeof(interval_t *) * (n + 1));
    for (int i = 0; i < n + 1; i++) {
        intervals[i] = NULL;
    }

    int i = 0;
    int j = 0;
    while (i < n && j < n) {
        while (v[i] == 0) i++;
        j = i + 1;
        while (v[j] != 0 && j < n) j++;

        interval_t *t = malloc(sizeof(interval_t));
        t->v = v;
        t->start = i;
        t->end = j - 1;

        t->next = intervals[j - i];
        intervals[j - i] = t;
    }

    for (int p = n; p >= 0; p--) {
        if (intervals[p] != NULL) {
            print_intervals(intervals[p]);
            break;
        }
    }
}

int main() {
    int v[20] = {1,0,2,3,0,4,5,6,7,0,8,9,10,11,0,1,2,3,4,5};

//    printf("max subarray len %d\n", max_len(v, 20));
    subarray_write(v, 20);

}

