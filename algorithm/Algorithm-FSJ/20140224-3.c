//
// Created by zhaos on 2/17/2019.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    int c;
    node_t *next;
};
typedef struct matr_t matr_t;
struct matr_t {
    int nr;
    int nc;
    node_t **p;
};
void matInsert(matr_t *mat, int r, int c, float val) {
    node_t *t = malloc(sizeof(node_t));
    t->v = val;
    t->c = c;
    node_t **arr = mat->p;
    t->next = arr[r];
    arr[r] = t;
}

void print_list(node_t *h) {
    for (node_t *p = h; p != NULL; p = p->next) {
        printf("v = %d, col = %d\n", p->v, p->c);
    }
}

void print_matrix(matr_t *mat) {
    for (int i = 0; i < mat->nr; i++) {
        printf("Row %d: \n", i);
        print_list((mat->p)[i]);
    }
}
int main() {
    matr_t *mat = malloc(sizeof(matr_t));
    mat->nr = 3;
    mat->nc = 3;
    mat->p = malloc(sizeof(node_t *) * (mat->nr));
    for (int i = 0; i < mat->nr; i++) {
        (mat->p)[i] = NULL;
    }

    matInsert(mat, 0, 0, 11);
    matInsert(mat, 0, 2, 22);
    matInsert(mat, 1, 1, 33);
    matInsert(mat, 2, 0, 44);


    print_matrix(mat);

}