//
// Created by zhaos on 2/10/2019.
//

#include <stdio.h>

int sum(int n) {
    if (n == 0)
        return 0;
    return sum(n - 1) + n;
}

int fib(int n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fib(n - 1) + fib(n - 2);
}

int fibonacci(int n, int N, int acc1, int acc2) {
    if (n == N) {
        return acc1 + acc2;
    }
    return fibonacci(n + 1, N, acc2, acc1 + acc2);
}

int fibonacci2(int n, int acc1, int acc2) {
    if (n == 0) return acc1;
    if (n == 1) return acc2;
    return fibonacci2(n - 1, acc2, acc1 + acc2);
}

int gcd(int a, int b) {
    if (b == 0) return a;
    return gcd(b, a % b);
}

int main() {
    int r = sum(3);
    printf("sum = %d\n", r);

    printf("fib(10) = %d\n", fib(11));

    printf("fibonacci(10) = %d\n", fibonacci(2, 11, 0, 1));

    printf("fibonacci2(10) = %d\n", fibonacci2(11, 0, 1));

    printf("gcd 18, 48 = %d\n", gcd(64, 72));
}