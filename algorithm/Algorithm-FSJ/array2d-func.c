//
// Created by zhaos on 2/7/2019.
//
#include <stdio.h>
#include <stdlib.h>

int sum_array2d_v1(int **m, int r, int c) {
    int sum = 0;
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            sum += m[i][j];
        }
    }
    return sum;
}

int sum_array2d_v2(int *m[], int r, int c) {
    int sum = 0;
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            sum += m[i][j];
        }
    }
    return sum;
}

//
int sum_array2d_v3(int m[][4], int r) {
    int sum = 0;
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < 4; j++) {
            sum += m[i][j];
        }
    }
    return sum;
}
void all_size_n_submatrix(int **m, int r, int c, int n) {

}

int submatrix(int **m1, int r1, int c1, int **m2, int r2, int c2) {
    return 0;
}
//int sum_array2d_wrong(int m[][], int r, int c) {
//    return 0;
//}
int main() {
    int a[3][4] = {{1,2,3,4},
                   {3,4,5,6},
                   {5,6,7,8}};

    int *m[3] = {NULL, NULL, NULL};
    int m1[4] = {1,2,3,4};
    int m2[4] = {3,4,5,6};
    int m3[4] = {5,6,7,8};

    m[0] = &m1[0];
    m[1] = m2;
    m[2] = &m3[0];


    printf("%d\n", sum_array2d_v1(m, 3, 4));
}