//
// Created by zhaos on 1/21/2019.
//
#include <stdio.h>
#include <math.h>
void print_matrix(int m);

/**
 * calculate x^y
 * @param x positive int
 * @param y positive int
 * @return
 */
int power(int x, int y) {
    int r = 1;
    for (int i = 0; i < y; i++) {
        r = r * x;
    }
    return r;
}

int factorial(int n) {
    int r = 1;
    for (int i = n; i > 0; i--) {
        r = r * i;
    }
    return r;
}

int main() {
    int x;
//    scanf("%d", &x);
//    print_matrix(x);

    double r = sqrt(1.2);
    printf("%f\n", r);

    double y = pow(1.2, 3);
    printf("%f\n", y);

    int p = power(2, 4);
    printf("%d\n", p);

    int q = factorial(5);
    printf("factorial(5) = %d\n", q);
}

void print_matrix(int m) {
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < m; j++) {
            printf("*");
        }
        printf("\n");
    }
}