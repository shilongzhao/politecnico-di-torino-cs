//
// Created by zhaos on 2/21/2019.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct list1_t list1_t;
typedef struct list2_t list2_t;
struct list1_t {
    int k;
    list1_t *next;
};

struct list2_t {
    int count;
    list2_t *next;
    list1_t *node;
};

void insert(list2_t **pList2, int k);

list2_t *find(list2_t *pList2, int k);

list2_t *list_compress(list1_t *p1) {
    list2_t *h2 = NULL;

    for (list1_t *h1 = p1; h1 != NULL; h1 = h1->next) {
        insert(&h2, h1->k);
    }
    return h2;
}

void insert(list2_t **pList2, int k) {
    list2_t *t = find(*pList2, k);
    if (t != NULL) {
        t->count += 1;
        return;
    }

    list1_t *tmp1 = malloc(sizeof(list1_t));
    tmp1->k = k;
    tmp1->next = NULL;

    list2_t *tmp = malloc(sizeof(list2_t));
    tmp->count = 1;
    tmp->node = tmp1;
    tmp->next = *pList2;

    *pList2 = tmp;
}

list2_t *find(list2_t *pList2, int k) {
    for (list2_t *h = pList2; h != NULL; h = h->next) {
        if (h->node->k == k) {
            return h;
        }
    }
    return NULL;
}

int main() {
    list1_t l1 = {1, NULL};
    list1_t l2 = {1, &l1};
    list1_t l3 = {1, &l2};
    list1_t l4 = {2, &l3};
    list1_t l5 = {3, &l4};
    list1_t l6 = {1, &l5};
    list1_t l7 = {2, &l6};
    list1_t l8 = {3, &l7};
    list1_t l9 = {3, &l8};
    list1_t l10 = {1, &l9};
    list1_t l11 = {1, &l10};
    list1_t l12 = {1, &l11};

    list2_t *h2 = list_compress(&l12);
    for (list2_t *p = h2; p != NULL; p = p->next) {
        printf("%d count = %d\n", p->node->k, p->count);
    }
}