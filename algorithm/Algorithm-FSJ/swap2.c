//
// Created by zhaos on 2/9/2019.
//
#include <stdio.h>
void swap(int *a, int *b) {
    int i = *a;
    *a = *b;
    *b = i;
}
int main() {
    int a = 1, b = 2;
    swap(&a, &b);
    printf("a = %d ,b = %d\n", a, b);
}