//
// Created by zhaos on 2/18/2019.
//

#include <stdlib.h>
#include <stdio.h>
#define N 5

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *left;
    node_t *right;
};

void printLevel(node_t *root, int l) {
    if (root == NULL) return;
    if (l == 0) {
        printf("%d ", root->k);
        return;
    }

    printLevel(root->left, l - 1);
    printLevel(root->right, l - 1);
}

void printLevel2(node_t *root, int depth, int l) {
    if (root == NULL) return;
    if (depth == l) {
        printf("%d ", root->k);
        return;
    }
    printLevel2(root->left, depth + 1, l);
    printLevel2(root->right, depth + 1, l);
}

void printLevelByLevel(node_t *root, int l1, int l2) {
    for (int i = l1; i <= l2; i++) {
        printLevel(root, i);
        printf("\n");
    }
}

// =========== Nary Tree ============

typedef struct node node;
struct node {
    int k;
    node *child[N];
};

void printLevelNaryTree(node *root, int l) {
    if (root == NULL) return;
    if (l == 0) {
        printf("%d ", root->k);
        return;
    }

    for (int i = 0; i < N; i++) {
        printLevelNaryTree(root->child[i], l - 1);
    }
}

void printLevelByLevelNaryTree(node *root, int l1, int l2) {
    for (int i = l1; i <= l2; i++) {
        printLevelNaryTree(root, i);
    }
}


int main() {
    node_t n1 = {1, NULL, NULL};
    node_t n2 = {2, NULL, NULL};
    node_t n3 = {3, NULL, NULL};
    node_t n4 = {4, NULL, NULL};
    node_t n5 = {5, NULL, NULL};
    node_t n6 = {6, NULL, NULL};
    node_t n7 = {7, NULL, NULL};
    node_t n8 = {8, NULL, NULL};
    node_t n9 = {9, NULL, NULL};
    node_t n10= {10, NULL, NULL};

    n1.left = &n2;
    n1.right = &n3;

    n2.left = &n4;
    n2.right = &n5;

    n3.right = &n6;

    n4.left = &n7;
    n4.right = &n8;

    n6.left = &n9;
    n6.right = &n10;

//    printLevel(&n1, 3);

    printLevel2(&n1, 0, 3);
}