//
// Created by zhaos on 2/16/2019.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *next;
};

void print_list(node_t *h) {
    for (node_t *p = h; p != NULL; p = p->next) {
        printf("%d\n", p->k);
    }
}

void insert(node_t **h, int v) {
    node_t *t = malloc(sizeof(node_t));
    t->k = v;
    t->next = *h;
    *h = t;
}

node_t *insert2(node_t *h, int v) {
    node_t *t = malloc(sizeof(node_t));
    t->k = v;
    t->next = h;
    return t;
}

void delete(node_t **h, int v) {
    if ((*h)->k == v) {
        *h = (*h)->next;
        return;
    }

    for (node_t *p = *h; p->next != NULL; p = p->next) {
        if (p->next->k == v) {
            node_t *q = p->next;
            p->next = q->next;
            free(q);
        }
    }
}

int main() {
    node_t *head = NULL;
    insert(&head, 1);
    insert(&head, 2);
    insert(&head, 3);
    insert(&head, 4);

    head = insert2(head, 5);
    head = insert2(head, 6);

    delete(&head, 6);
    delete(&head, 3);

    print_list(head);
}