//
// Created by zhaos on 1/28/2019.
//
#include <stdio.h>
int main() {
    int a[11] = {1,2,3,4,5,1,2,3,4,5,6};
    int count = 1;
    int max = 1;
    for (int i = 0; i < 10; i++) {
        if (a[i + 1] > a[i]) {
            count++;
        }
        else {
            count = 1;
        }
        if (count > max) {
            max = count;
        }
    }
    printf("%d", max);
}