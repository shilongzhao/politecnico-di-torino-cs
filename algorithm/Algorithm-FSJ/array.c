//
// Created by zhaos on 1/25/2019.
//
#include <stdio.h>
#include <stdlib.h>

int main() {
    printf("sizeof int is %d bytes\n", sizeof(int));

    int a[10] = {7, 5, 3, 6, 2, 8, 11, 9, 6, 4};
    for (int i = 0; i < 10; i++) {
        printf("%d\n", a[i]);
    }

    // min
    int min = a[0];
    for (int i = 1; i < 10; i++) {
        if (a[i] < min) {
            min = a[i];
        }
    }
    printf("min value is %d\n", min);

    // sum
    int sum = 0;
    for (int i = 0; i < 10; i++) {
        sum += a[i];
    }
    printf("sum of array is %d\n", sum);

    // revert

    for (int i = 0, j = 9; i < j; i++, j--) {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
    for (int i = 0; i < 10; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");

    // TODO: homework, max length of increasing sequence


}
