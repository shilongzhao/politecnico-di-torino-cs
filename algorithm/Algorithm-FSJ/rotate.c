//
// Created by zhaos on 2/10/2019.
//

// 20140224
#include <stdlib.h>
#include <stdio.h>

/**
 *
 * @param v  array
 * @param n  array length
 * @param p  num shift
 */
void rotate(int *v, int n, int p) {
    if (p > 0) {
        int *aux = malloc(sizeof(int) * p);
        for (int i = n - p, j = 0; i < n && j < p; i++, j++) {
            aux[j] = v[i];
        }
        for (int i = n - p - 1; i >= 0; i--) {
            v[p + i] = v[i];
        }
        for (int i = 0; i < p; i++) {
            v[i] = aux[i];
        }
        free(aux);
    }
    else {
        // TODO: homework
    }
}

int main() {
    int v[9] = {1,2,3,4,5,6,7,8,9};
    rotate(v, 9, 3);
    for (int i = 0; i < 9; i++) {
        printf("%d ", v[i]);
    }
}