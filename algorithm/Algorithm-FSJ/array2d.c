//
// Created by zhaos on 2/2/2019.
//

#include <stdio.h>
int main() {
    int a[3][4] = {
            {1,2,3,4},
            {3,2,0,5},
            {4,6,2,4}
    };

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    int sum = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            sum += a[i][j];
        }
    }
    printf("sum matrix = %d\n", sum);

    /// sum of each row
    for (int i = 0; i < 3; i++) {
        int sum = 0;
        for (int j = 0; j < 4; j++) {
            sum += a[i][j];
        }
        printf("sum of row %d is %d\n",i, sum);
    }

    /// sum of each column
    for (int j = 0; j < 4; j++) {
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            sum += a[i][j];
        }
        printf("sum of column %d is %d\n", j, sum);
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            int b = 1; // b == 1 : a[i][j] < any number around
            // check
            for (int p = i - 1; p <= i + 1; p++) {
                for (int  q = j - 1; q <= j + 1; q++) {
                    if (p == i && q == j) {
                        continue;
                    }
                    if (p >= 0 && p < 3 && q >= 0 && q < 4) {
                        if (a[p][q] <= a[i][j]) {
                            b = 0;
                        }
                    }
                }
            }
            if (b == 1) {
                printf(" lower point (%d, %d)\n", i, j);
            }
        }
    }



}