//
// Created by zhaos on 1/28/2019.
//

#include <stdio.h>
#include <string.h>
#include <ctype.h>

int array_sum(int *a, int n) {
    int r = 0;
    for (int i = 0; i < n; i++) {
        r += a[i];
    }
    return r;
}

void to_upper(char *a) {
    for (int i = 0; i < strlen(a); i++) {
        if (islower(a[i])) {
            a[i] = toupper(a[i]);
        }
    }
}
int main() {
    int x = 1;
    // &
    printf("value of x = %d, address of x is %p\n", x, &x);
    int *p = &x;
    printf("value of p = %p, address of p = %p\n", p, &p);

    printf("variable value pointed by p is %d\n", *p);

    *p = 2;
    printf("x = %d\n", x);

    double y = 1.1;
    double *q = &y;

    char c = 'c';
    char *pc = &c;

    char **p2 = &pc;
    char ***p3 = &p2;


    // array and pointers
    int arr[10] = {1,2,3};
    printf("value of arr = %p\n", arr);
    printf("address of a[0], %p\n", &arr[0]);
    printf("address of a[1], %p\n", &arr[1]);
    printf("address of a[2], %p\n", &arr[2]);

    printf("arr + 1, %p\n", arr + 1);
    printf("arr + 2, %p\n", arr + 2);

    for (int i = 0; i < 5; i++) {
        printf("%d\n", arr[i]);
    }

    for (int i = 0; i < 5; i++) {
        printf("%d\n", *(arr + i));
    }

    char s[10] = "abc123!";
    to_upper(s);
    printf("After to upper = %s\n", s);
}