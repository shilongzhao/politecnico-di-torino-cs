#include <stdlib.h>
#include <string.h>
#include <stdio.h>
/**
 * iterate through vertices and mark with IDs
 * @param m     adjacency matrix
 * @param n     matrix size
 * @param v     vertex
 * @param id    current component id
 * @param ids   array of component ids
 */
void dfs(int **m, int n, int v, int id, int *ids) {
    ids[v] = id;
    for (int i = 0; i < n; i++) {
        if (m[v][i] == 1 && !ids[i]) {
            dfs(m, n, i, id, ids);
        }
    }
}

void print_ids(int *ids, int n) {
    for (int i = 0; i < n; i++) {
        printf("|%d ", i);
    }
    printf("\n");
    for (int i = 0; i < n; i++)
        printf(" - ");
    printf("\n");
    for (int i = 0; i < n; i++) {
        printf("|%d ", ids[i]);
    }
}
// return 1 if able to partition, 0 otherwise;
// imagine it as a graph of n nodes
// then m is adjacency matrix,
// algorithm to get strongly connected components
// and test if there is one component has more than k nodes
int partition(int n, int k, int **m) {
    int *ids = malloc(sizeof(int) * n);
    memset(ids, 0, sizeof(int) * n);
    int id = 1;
    for (int i = 0; i < n; i++) {
        if (ids[i]) continue;
        dfs(m, n, i, id, ids);
        id++;
    }
    print_ids(ids, n);
    return 0;
}
void test1() {
    printf("\n---------test1--------\n");
    int **m = malloc(sizeof(int *));
    *m = malloc(sizeof(int));
    m[0][0] = 1;
    partition(1, 0, m);
    free(*m);
    free(m);
}
void test2() {
    printf("\n---------test2--------\n");
    int N = 2;
    int **m = malloc(sizeof(int *) * N);
    for (int i = 0; i < N; i++) {
        m[i] = malloc(sizeof(int) * N);
    }
    m[0][1] = 1;
    partition(N,0,m);
    for (int i = 0; i < N; i++) {
        free(m[i]);
    }
    free(m);
}
// all in one components
void test3() {
    printf("\n---------test3--------\n");
    int t[5][5] = {
            {0,1,0,0,0},
            {1,0,1,0,0},
            {0,1,0,1,0},
            {0,0,1,0,1},
            {0,0,0,1,0}
    };
    int N = 5;
    int **m = malloc(sizeof(int *) * N);
    for (int i = 0; i < N; i++) {
        m[i] = malloc(sizeof(int) * N);
        for (int j = 0; j < N; j++) {
            m[i][j] = t[i][j];
        }
    }
    partition(N,0,m);
    for (int i = 0; i < N; i++) {
        free(m[i]);
    }
    free(m);
}
// separate components
void test4() {
    printf("\n---------test4--------\n");
    int t[5][5] = {
            {0,1,1,0,0},
            {1,0,0,0,0},
            {1,0,0,0,0},
            {0,0,0,0,1},
            {0,0,0,1,0}
    };
    int N = 5;
    int **m = malloc(sizeof(int *) * N);
    for (int i = 0; i < N; i++) {
        m[i] = malloc(sizeof(int) * N);
        for (int j = 0; j < N; j++) {
            m[i][j] = t[i][j];
        }
    }
    partition(N,0,m);
    for (int i = 0; i < N; i++) {
        free(m[i]);
    }
    free(m);
}
// all single node components
void test5() {
    printf("\n---------test5--------\n");

    int N = 5;
    int **m = malloc(sizeof(int *) * N);
    for (int i = 0; i < N; i++) {
        m[i] = malloc(sizeof(int) * N);
        for (int j = 0; j < N; j++) {
            m[i][j] = 0;
        }
    }
    partition(N,0,m);
    for (int i = 0; i < N; i++) {
        free(m[i]);
    }
    free(m);
}
int main(int argc, char *argv[]) {
    test1();
    test2();
    test3();
    test4();
    test5();
}

