#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
void copySubMat(int **m1, int r1, int c1, int **m2, int r2, int c2, int x, int y) {
    if (x >= r1 || y >= c1) return;
    int x2 = r1 - 1;
    if (x2 > x + r2 - 1) x2 = x + r2 -1;
    int y2 = c1 - 1;
    if (y2 > y + c2 - 1) y2 = y + c2 - 1;

    for (int i = x; i <= x2; i++) {
        for (int j = y; j <= y2; j++) {
            m1[i][j] = m2[i-x][j-y];
        }
    }
}

int **init_m1() {
    int **m1 = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        m1[i] = malloc(sizeof(int *) * 3);
        for (int j = 0; j < 3; j++) {
            m1[i][j] = i + j;
        }
    }
    return m1;
}

void free_m(int **m, int r) {
    for (int i = 0; i < r; i++) {
        free(m[i]);
    }
    free(m);
}


int **init_m2() {
    int **m2 = malloc(sizeof(int *) * 2);
    for (int i = 0; i < 2; i++) {
        m2[i] = malloc(sizeof(int) * 2);
        memset(m2[i], 0, sizeof(int) * 2);
    }
    return m2;
}
/* m2 inside m1 */
void test1() {
    int **m1 = init_m1();
    int **m2 = init_m2();
    copySubMat(m1, 3, 3, m2, 2, 2, 0, 0);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d ", m1[i][j]);
            if (i <= 1 && j <= 1)
                assert(m1[i][j] == 0);
            else
                assert(m1[i][j] == i + j);
        }
        printf("\n");
    }
    printf("test1 success\n");
    free_m(m1,3);
    free_m(m2,2);
}
/* m2 cuts both right and bottom corner */
void test2() {
    int **m1 = init_m1();
    int **m2 = init_m2();
    copySubMat(m1, 3, 3, m2, 2, 2, 2, 2);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d ", m1[i][j]);
            if (i == 2 && j == 2)
                assert(m1[i][j] == 0);
            else
                assert(m1[i][j] == i + j);
        }
        printf("\n");
    }
    printf("test2 success\n");
    free_m(m1,3);
    free_m(m2,2);
}
/* m2 cuts right boarder only */
void test3() {
    int **m1 = init_m1();
    int **m2 = init_m2();
    copySubMat(m1, 3, 3, m2, 2, 2, 0, 2);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d ", m1[i][j]);
            if (i < 2 && j == 2)
                assert(m1[i][j] == 0);
            else
                assert(m1[i][j] == i + j);
        }
        printf("\n");
    }
    printf("test3 success\n");
    free_m(m1,3);
    free_m(m2,2);
}

/* m2 cuts bottom boarder only */
void test4() {
    int **m1 = init_m1();
    int **m2 = init_m2();
    copySubMat(m1, 3, 3, m2, 2, 2, 2, 0);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d ", m1[i][j]);
            if (i == 2 && j < 2)
                assert(m1[i][j] == 0);
            else
                assert(m1[i][j] == i + j);
        }
        printf("\n");
    }
    printf("test4 success\n");
    free_m(m1,3);
    free_m(m2,2);

}
int main(int argc, char **argv) {
    test1();
    test2();
    test3();
    test4();
}


