#include <stdio.h>
#include <stdlib.h>
typedef struct element element_t;
struct element {
    int k;
    element_t *fc, *fs; // first child, first sister
};

typedef struct node_t node_t;
struct node_t {
    int k;
    int c; // number of direct child
    node_t *p, *fc, *fs; // parent, first child, first sister
};


node_t *process(struct element *root, node_t *parent) {
    if (root == NULL) {
        return NULL;
    }

    node_t *t = malloc(sizeof(node_t));
    t->k = root->k;
    t->p = parent;
    t->c = 0;
    if (parent != NULL) parent->c++;

    t->fs = process(root->fs, parent);
    t->fc = process(root->fc, t);

    return t;
}

void processTree(struct element *root, node_t **node) {
    *node = process(root, NULL);
}

void print_node(node_t *node) {
    if (node == NULL) return;
    printf("(k = %d, p = %d, c = %d)\n",
           node->k, node->p == NULL? 0: node->p->k, node->c);
    print_node(node->fc);
    print_node(node->fs);

}

void test1() {
    printf("----------test1----------\n");
    element_t *elements[2];
    elements[1] = malloc(sizeof(element_t));
    elements[1]->fc = NULL;
    elements[1]->fs = NULL;
    elements[1]->k = 1;
    node_t *node = NULL;
    processTree(elements[1],&node);
    print_node(node);
}

void test2() {
    printf("----------test2----------\n");
    element_t *elements[3];
    for (int i = 1; i < 3; i++) {
        elements[i] = malloc(sizeof(element_t));
        elements[i]->fc = NULL;
        elements[i]->fs = NULL;
        elements[i]->k = i;
    }
    elements[1]->fc = elements[2];
    node_t *node = NULL;

    processTree(elements[1],&node);
    print_node(node);
}

void test3() {
    printf("----------test3----------\n");
    element_t *elements[4];
    for (int i = 1; i < 4; i++) {
        elements[i] = malloc(sizeof(element_t));
        elements[i]->fc = NULL;
        elements[i]->fs = NULL;
        elements[i]->k = i;
    }
    elements[1]->fc = elements[2];
    elements[2]->fs = elements[3];

    node_t *node = NULL;

    processTree(elements[1],&node);
    print_node(node);
}

void test4() {
    printf("----------test4----------\n");
    element_t *elements[5];
    for (int i = 1; i < 5; i++) {
        elements[i] = malloc(sizeof(element_t));
        elements[i]->fc = NULL;
        elements[i]->fs = NULL;
        elements[i]->k = i;
    }
    elements[1]->fc = elements[2];
    elements[2]->fc = elements[3];
    elements[3]->fc = elements[4];

    node_t *node = NULL;

    processTree(elements[1],&node);
    print_node(node);
}
void test5() {
    printf("----------test5----------\n");
    element_t *elements[6];
    for (int i = 1; i < 6; i++) {
        elements[i] = malloc(sizeof(element_t));
        elements[i]->fc = NULL;
        elements[i]->fs = NULL;
        elements[i]->k = i;
    }
    elements[1]->fc = elements[2];
    elements[2]->fc = elements[3];
    elements[2]->fs = elements[4];
    elements[4]->fc = elements[5];

    node_t *node = NULL;

    processTree(elements[1],&node);
    print_node(node);
}

int main(int argc, char *argv[]) {
    test1();
    test2();
    test3();
    test4();
    test5();
    // more tests to do
}