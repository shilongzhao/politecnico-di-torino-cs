
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// 检查是否全为1
int all_on(int **m, int n) {
    int i, j;
    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            if (m[i][j] == 0)
                return 0;
    return 1;
}
// 更新第 r 行的状态, 如果原先是0则变为1,反之亦然
void updateRow(int **m, int n, int r) {
    int i;
    for (i = 0; i < n; i++)
        m[r][i] = (m[r][i] + 1) % 2;
}
// 更新第 c 列的状态
void updateCol(int **m, int n, int c) {
    int i;
    for (i = 0; i < n; i++)
        m[i][c] = (m[i][c] + 1) % 2;
}
/**
 * 这是一个典型的排列问题
 * m 待处理矩阵, 大小为n*n
 * n 矩阵的大小
 * selected 选择的行，列
 * next 下一次要从哪一个开始选择
 * d 已经一共选择了多少行和列
 */
void play(int **m, int n, int *selected, int next, int d) {
    int i;
    // 如果全为1, 打印结果
    if (all_on(m, n)) {
        printf("--------------\n");
        for (i = 0; i < d; i++) // selected 里面 0 到 n-1 代表第 0 到 n - 1 行, n -- 2n - 1 代表的是 第 0 到 n-1 列
            if (selected[i] >= n) // 如果是列
                printf("col %d ", selected[i] % n); // 打印多少列
            else printf("row %d ", selected[i]); // 否则打印多少行
        printf("\n");
        return;
    }
    // 否则, 继续递归所有可能
    for (i = next; i < 2 * n; i++) {
        selected[d] = i;
        if (i < n) updateRow(m, n, i); else updateCol(m,n,i%n);
        play(m, n, selected, i+1, d+1);
        if (i < n) updateRow(m, n, i); else updateCol(m, n, i%n);
    }
}
// 题目要求的函数格式, 必须调用递归函数
void matrixSwap(int **m, int n) {
    int *selected = malloc(sizeof(int) * 2 * n);
    memset(selected, 0, sizeof(int) * 2 * n);
    play(m, n, selected, 0, 0);
}
// 主函数
int main(int argc, char *argv[]) {
    int t[3][3] = {
            {0,1,0},
            {1,0,1},
            {0,1,0}
    };
    // 分配内存并且初始化矩阵
    int **m = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        m[i] = malloc(sizeof(int) * 3);
        for (int j = 0; j < 3; j++) {
            m[i][j] = t[i][j];
        }
    }
    // 调用函数
    matrixSwap(m, 3);
}
