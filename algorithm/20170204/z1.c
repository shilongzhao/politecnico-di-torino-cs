//
//

#include <stdlib.h>
#include <stdio.h>
int partSum(int **mat,int top,int bottom,int left,int right,int n)
{
    int i,j,k,addiation;

    k=1;
    addiation = 0;

    for(i= top;i < bottom ;i++)
    {
        for(j = left;j < right; j++)
        {
            if( i >= top+k && j>= left+k && i < bottom-k && j < right-k)
            {
                continue;
            }
            k ++;
            addiation+= mat[i][j];
        }
    }

    return addiation;
}

void frameSum(int **mat, int n, int **vet)
{
    int size_vet,i,top,bottom,left,right,*result;

    top = 0;bottom = n;left = 0;right = n;
    size_vet = (n-1)/2+1;

    result = malloc(size_vet *sizeof(int));

//    for(i=0;i<size_vet;i++)
//    {
//        result[i] = partSum(mat,top,bottom,left,right,n);
//        top++; bottom--; left++; right--;
//    }
    int t = 0;
    int c = 0;
    for (i = 0; i < size_vet; i++) {
        c = partSum(mat, top, bottom, left, right, n);
        t = partSum(mat, top+1, bottom - 1, left + 1, right - 1, n);
        result[i] = c - t;
        top++; bottom--; left++; right--;
    }

    *vet = result;
    return;
}

int main(int argc, char **argv) {
    int t[5][5] = {
            {2,2,2,2,2},
            {2,1,1,1,2},
            {2,1,3,1,2},
            {2,1,1,1,2},
            {2,2,2,2,2}
    };
    int **m = malloc(sizeof(int *) * 5);
    for (int i = 0; i < 5; i++) {
        m[i] = malloc(sizeof(int) * 5);
        for (int j = 0; j < 5; j++) {
            m[i][j] = t[i][j];
        }
    }
    int *vet = NULL;
    frameSum(m, 5, &vet);
    for (int i = 0; i <= 2; i++)
        printf("%d ", vet[i]);
}