#include <stdio.h>
#include <stdlib.h>

typedef struct node node;
struct node
{
    int key;
    struct node *left;
    struct node *right;
};

struct node *treeMirror(struct node *root);
struct node *buildtree(struct node *root);
void printTree(struct node *final);   

int main(int argc, char **argv) {
    struct node *root,*final;

    // rewrite main function here

    return 0;
}


struct node *treeMirror(struct node *root)
{
    struct node *curr;

    curr = buildtree(root);

    return curr;
}


struct node *buildtree(struct node *root)
{
    if (root == NULL)
        return NULL;

    struct node *t = root->left;
    root->left = root->right;
    root->right = t;

    buildtree(root->left);
    buildtree(root->right);

    return root;
}

void printTree(struct node *final) // preorder
{
    if(final == NULL)
    {
        return;
    }
    printf("%d  ",final->key);
    printTree(final->left);
    printTree(final->right);
    return;
}



