
#include <stdlib.h>

// sum of ring f
/**
 * 计算矩阵mat中第f圈所有数的和
 * @param mat   matrix
 * @param n     matrix size
 * @param f     ring index
 * @return
 */
int sumFrame(int **mat, int n, int f) {
    int r = 0;
    // 计算第f行所有数的和
    int i;
    for ( i = f; i < n -f; i++) {
        r += mat[f][i];
    }
    // 加上第n-f-1列的数
    for ( i = f + 1; i < n -f; i++) {
        r += mat[i][n-f-1];
    }
    // 加上第n-f-1行的数
    for (i = n - f - 2; i >= f; i--) {
        r += mat[n-f-1][i];
    }
    // 加上第f列的数
    for ( i = n - f - 2; i > f; i--) {
        r += mat[i][f];
    }
    // 返回结果
    return r;
}
/**
 * 计算mat中每一圈的所有数的和，将第i圈的所有数的和放到
 ＊ 数组*vet的第i个位置
 * @param mat   square matrix
 * @param n     matrix size, must be odd
 * @param vet   return result
 */
void frameSum(int **mat, int n, int **vet) {
    // 分配内存
    *vet = malloc(sizeof(int) * (n/2 + 1));
    int i;
    for (i = 0; i <= n/2; i++) {
        // 计算第i圈的和，放到相应的位置
        (*vet)[i] = sumFrame(mat, n, i);
    }
}

// 主函数
int main(int argc, char **argv) {
    int t[5][5] = {
            {2,2,2,2,2},
            {2,1,1,1,2},
            {2,1,3,1,2},
            {2,1,1,1,2},
            {2,2,2,2,2}
    };
    // 分配内存
    int **m = malloc(sizeof(int *) * 5);
    for (int i = 0; i < 5; i++) {
        m[i] = malloc(sizeof(int) * 5);
        for (int j = 0; j < 5; j++) {
            m[i][j] = t[i][j];
        }
    }
    int *vet = NULL;
    
    frameSum(m, 5, &vet);
}
