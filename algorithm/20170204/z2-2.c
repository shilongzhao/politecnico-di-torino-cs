

#include <stdlib.h>
#include <stdio.h>

typedef struct node node;
struct node
{
    int key;
    struct node *left;
    struct node *right;
};

struct node *buildtree(struct node *root,struct node **curr)
{
    if(root  == NULL)
    {
        return NULL;
    }
    *curr = malloc(sizeof(struct node));

    (*curr)->key = root->key;
    buildtree(root->left,&((*curr)->right));
    buildtree(root->right,&((*curr)->left));

    return *curr;

}

struct node *treeMirror(struct node *root)
{
    struct node *curr;

    curr = buildtree(root,&curr);

    return curr;
}

void printTree(struct node *final) // preorder
{
    if(final == NULL)
    {
        return;
    }
    printf("%d  ",final->key);
    printTree(final->left);
    printTree(final->right);
    return;
}

int main(int argc, char **argv) {
    node *ns[10];
    for (int i = 9; i >= 0; i--) {
        ns[i] = malloc(sizeof(node));
        ns[i]->key = i;

        ns[i]->left = 2*i + 1 < 10 ? ns[2*i + 1]: NULL;

        ns[i]->right = 2*i + 2 < 10 ?  ns[2*i + 2]: NULL;
    }

    printTree(ns[0]);

    node *mirrored = treeMirror(ns[0]);
    printTree(mirrored);
}