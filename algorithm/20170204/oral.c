#include <string.h>
#include <stdlib.h>
#include <stdio.h>
//
// insert into an initial-character indexed bi-linked list
//
typedef struct element_t e_t;
struct element_t {
    char    *name;
    int     ival;
    float   fval;
    e_t     *left, *right;
};
void insert(e_t **v, char *name, int ival, float fval) {
    e_t *t = malloc(sizeof(e_t));
    t->name = strdup(name);
    t->ival = ival;
    t->fval = fval;
    t->left = NULL;
    t->right = NULL;
    int index = name[0] - 'A';
    e_t *h = v[index];

    if (h == NULL) {
        v[index] = t;
        return;
    }

    while(strcmp(h->name, name) < 0) {
        if (h->right == NULL) {
            h->right = t;
            t->left = h;
            return;
        }
        h = h->right;
    }

    t->right = h;
    t->left = h->left;
    h->left = t;
    t->left->right = t;
    return;
}

int main(int argc, char *argv[]) {
    e_t *v[26] = {};

    insert(v, "AAA", 1, 1.0);
    insert(v, "AAC", 2, 1.2);
    insert(v, "AAB", 3, 1.1);
    insert(v, "AAE", 3, 1.1);
    insert(v, "AAF", 3, 1.1);
    insert(v, "AAD", 3, 1.1);

    for(e_t *p = v[0]; p != NULL; p = p->right) {
        printf("%s %d %0.1f\n", p->name, p->ival, p->fval);
    }
}
