
#include <stdlib.h>
#include <stdio.h>
typedef struct node node;
struct node {
    int val;
    node *left, *right;
};
struct node *treeMirror(struct node *root) {
    if (root == NULL) return NULL;
    node *t = malloc(sizeof(struct node));
    t->val = root->val;
    t->left = treeMirror(root->right);
    t->right = treeMirror(root->left);
    return t;
}

void preorder(node *root) {
    if (root == NULL) return;
    printf("%d\n", root->val);
    preorder(root->left);
    preorder(root->right);
}


struct node *buildtree(struct node *root)
{
    if (root == NULL)
        return NULL;
    
    struct node *t = root->left;
    root->left = root->right;
    root->right = t;
    
    buildtree(root->left);
    buildtree(root->right);
    
    return root;
}


struct node *treeMirror2(struct node *root)
{
    struct node *curr;
    
    curr = buildtree(root);
    
    return curr;
}


int main(int argc, char **argv) {
    node *ns[10];
    for (int i = 9; i >= 0; i--) {
        ns[i] = malloc(sizeof(node));
        ns[i]->val = i;

        ns[i]->left = 2*i + 1 < 10 ? ns[2*i + 1]: NULL;

        ns[i]->right = 2*i + 2 < 10 ?  ns[2*i + 2]: NULL;
    }

    preorder(ns[0]);

    node *mirrored = treeMirror(ns[0]);
    preorder(mirrored);
}
