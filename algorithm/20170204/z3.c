
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
void matrixSwap(int **mat,int n,int num,int next_i,int next_j,int *best,int *result_r,int *result_c, int num_r, int num_c);
int check(int **mat,int n);
void row_change(int **mat,int col,int n);
void col_change(int **mat,int row,int n);

void print_mat(int **m, int n)
{
    int i, j;
    for (i = 0;i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("%d ", m[i][j]);
        }
        printf("\n");
    }
}

void updateRow(int **m, int n, int r) {
    int i;
    for (i = 0; i < n; i++)
        m[r][i] = (m[r][i] + 1) % 2;
}
void updateCol(int **m, int n, int c) {
    int i;
    for (i = 0; i < n; i++)
        m[i][c] = (m[i][c] + 1) % 2;
}

int main(int argc, char *argv[]) {
    int t[3][3] = {
            {0,1,0},
            {1,0,1},
            {0,1,0}
    };
    int **m = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        m[i] = malloc(sizeof(int) * 3);
        for (int j = 0; j < 3; j++) {
            m[i][j] = t[i][j];
        }
    }
    int best = INT_MAX;
    int result_r[3] = {};
    int result_c[3] = {};
    matrixSwap(m, 3, 0, 0, 0, &best, result_r, result_c, 0, 0);
}

void matrixSwap(int **mat,int n,int num,int next_i,int next_j,int *best,int *result_r,int *result_c, int num_r, int num_c)
{
    int i,j;



        if(check(mat,n))
        {
            if(num < *best)
            {
                *best = num;
                printf("the number of row is:\n");
                for(i=0;i<num_r;i++)
                {
                    printf("%d    ",result_r[i]);
                }
                printf("\n");
                printf("the number of column is:\n");
                for(i=0;i<num_c;i++)
                {
                    printf("%d    ",result_c[i]);
                }
                printf("\n");
            }
        }
//    for(i=next_i ; i < n ; i++)
//    {
//        for(j = next_j ; j< n; j++)
//        {
//            row_change(mat, j,n);
//            matrixSwap(mat,n,depth++,next_i,next_j++, best,result_r,result_c);
//        }
//        col_change(mat,i,n);
//        matrixSwap(mat,n,depth++,next_i++,next_j,best, result_r,result_c);
//    }
    for(i= next_i ; i < n ; i++)
    {
//        col_change(mat,i,n);
        updateRow(mat, n, i);
        result_r[num_r] = i;
        num_r++;
        matrixSwap(mat,n,num++,i+1,next_j,best, result_r, result_c, num_r, num_c);
        num_r--;
//        col_change(mat, i, n);
        updateRow(mat, n, i);
    }

    for(j = next_j ; j< n; j++)
    {
//        row_change(mat, j,n);
        updateCol(mat, n, j);
        result_c[num_c] = j;
        num_c++;
        matrixSwap(mat,n,num++,next_i,j+1, best, result_r, result_c, num_r, num_c);
        num_c--;
//        col_change(mat, j, n);
        updateCol(mat, n, j);
    }
}

int check(int **mat,int n)
{
    int i,j;

    for(i = 0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            if(mat[i][j] == 0)
            {
                return 0;
            }
        }
    }
    return 1;
}
//error here
void row_change(int **mat,int col,int n) //change the number of each row with the same column
{
    int m;

    for(m=0; m < n;m++)
    {
        if(mat[m][col] == 1)
        {
            mat[m][col] = 0;
        }
        else if(mat[m][col] == 0)
        {
            mat[m][col] = 1;
        }
    }
    return;
}
// error here
void col_change(int **mat,int row,int n)//change the number of each column with the same row
{
    int m;

    for(m = 0;m<n;m++)
    {
        if(mat[row][m] == 0)
        {
            mat[row][m] = 1;
        }
        else if(mat[row][m] == 1)
        {
//            mat[row][m] == 0;
            mat[row][m] = 0;
        }
    }
    return;
}











