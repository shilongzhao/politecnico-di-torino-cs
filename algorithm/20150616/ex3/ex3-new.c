#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 30+1

typedef struct product{
    char name[MAX];
    int price;
    struct product *next;
} product_t;


typedef struct producer{
    char name[MAX];
    product_t *products;
    struct producer *next;
} producer_t;


void seek(char *producername,char *productname,producer_t *producer_node, int *found){
    if (*found == 1) return;

    if(producer_node==NULL && *found==0){
        printf("\t no such product %s %s!\n", producername, productname);
        return;
    }

    if (producer_node == NULL) return;

    product_t *tmp2;
    tmp2=producer_node->products;

    while(tmp2!=NULL){
        if(strcmp(producer_node->name,producername)==0&&strcmp(tmp2->name,productname)==0){
            printf("\t%s of %s's price is %d\n",tmp2->name,producer_node->name,tmp2->price);
            *found=1;
        }
        tmp2=tmp2->next;
    }

    seek(producername,productname,producer_node->next, found);
}


int main(int argc,char *argv[])
{

    char *producers[3]={"audi","bmw","fiat"};
    char *fiat_products[2] = {"f1","f2"};
    char *audi_products[2] = {"a1", "a2"};
    char *bmw_products[2] = {"b1", "b2"};
    char **all_products[3] = { audi_products, bmw_products, fiat_products};

    producer_t *producer_list = NULL;

    int count = 0;
    for(int i = 0; i < 3; i++) {
        producer_t *tmp = malloc(sizeof(producer_t));
        strcpy(tmp->name, producers[i]);

        tmp->next = producer_list;
        tmp->products = NULL;
        producer_list = tmp;

        for(int j = 0; j < 2; j++) {
            product_t *tmp2 = malloc(sizeof(product_t));
            strcpy(tmp2->name, all_products[i][j]);
            tmp2->price = count++;
            tmp2->next = tmp->products;
            tmp->products = tmp2;
        }
    }
    // print producers and products;
    for(producer_t *p = producer_list; p != NULL; p = p->next) {
        printf("producer %s:\n", p->name);
        for(product_t *t = p->products; t != NULL; t = t->next) {
            printf("\t%s $$ %d\n", t->name, t->price);
        }
    }


    for(int i = 0; i < 3; i++) {
        for (int j = 0; j < 6; j++) {
            int found = 0;
            printf("seeking %s %s\n", producers[i], all_products[j%3][j%2]);
               seek(producers[i], all_products[j%3][j%2], producer_list, &found);
        }
    }




    return 0;
}
