#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MAXLEN 31

typedef struct producer_t_ producer_t;
typedef struct product_t_ product_t;

struct producer_t_ {
    char name[MAXLEN];
    product_t *products;
    producer_t *next;
};

struct product_t_ {
    double val;
    char name[MAXLEN];
    product_t *next;
};


void search(producer_t* producer, char *producer_name, char *product_name, int *flag) {
    // flag == 1 means the product has been found in previous producers' products
    if (*flag == 1) return;

    if (producer == NULL && *flag == 0) {
        fprintf(stderr, "product not found!\n");
        return;
    }

    for (product_t *p = producer->products; p != NULL; p = p->next) {
        if ((strcmp(producer->name, producer_name) == 0) &&
                strcmp(p->name, product_name) == 0) {
            printf("price of %s %s is %lf\n", producer_name, product_name, p->val);
            *flag = 1;
            return;
        }
    }

    search(producer->next, producer_name, product_name, flag);

}