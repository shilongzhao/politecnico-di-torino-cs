
#include <stdlib.h>
#include <stdio.h>

typedef struct node node_t;

struct node{
    int dup;
    int key;
    node_t *left;
    node_t *right;
};

void doubleTree(struct node *root) {

    if (root == NULL)
        return;

    printf("node %d \n", root->key);

    if (root->dup != 1) {
        node_t *dup = malloc(sizeof(node_t));
        root->dup = 1;
        dup->dup = 1;
        dup->key = root->key;
        dup->left = root->left;
        dup->right = NULL;
        root->left = dup;
    }

    doubleTree(root->left);
    doubleTree(root->right);
}

void preorder(node_t *root) {
    if(root == NULL) {
        return;
    }

    printf("preordering %d\n", root->key);
    preorder(root->left);
    preorder(root->right);

}
int main(int argc, char *argv[]) {
    node_t *nodes[4];
    nodes[0] = NULL;
    for (int i = 1; i <=3; i++) {
        nodes[i] = malloc(sizeof(node_t));
        nodes[i]->key = i;
        nodes[i]->left = NULL;
        nodes[i]->right = NULL;
        nodes[i]->dup = 0;
    }

    nodes[1]->left = nodes[2];
    nodes[1]->right = nodes[3];

    doubleTree(nodes[1]);
    preorder(nodes[1]);
}
