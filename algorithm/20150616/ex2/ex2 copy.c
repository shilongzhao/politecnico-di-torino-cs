
#include <stdlib.h>
#include <stdio.h>

typedef struct node node_t;

struct node{
    int key;
    node_t *left;
    node_t *right;
};

void doubleTree(struct node *root) {
    if (root == NULL) return;

    doubleTree(root->left);
    doubleTree(root->right);

    node_t *tmp = malloc(sizeof(node_t));
    tmp->key = root->key;
    tmp->right = NULL;
    tmp->left = root->left;
    root->left = tmp;

    return;
}

void postorder(node_t *root) {
    if (root == NULL) return;

    postorder(root->left);
    postorder(root->right);
    printf("%d\n", root->key);
}

void preorder(node_t *root) {
    if(root == NULL) {
        return;
    }

    printf("preordering %d\n", root->key);
    preorder(root->left);
    preorder(root->right);

}
int main(int argc, char *argv[]) {
    node_t *nodes[4];
    nodes[0] = NULL;
    for (int i = 1; i <=3; i++) {
        nodes[i] = malloc(sizeof(node_t));
        nodes[i]->key = i;
        nodes[i]->left = NULL;
        nodes[i]->right = NULL;
    }

    nodes[1]->left = nodes[2];
    nodes[1]->right = nodes[3];

    doubleTree(nodes[1]);
    preorder(nodes[1]);
}
