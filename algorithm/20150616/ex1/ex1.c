#include <stdlib.h>
#include <stdio.h>

void mult(int *v1, int *v2, int n, int **pv) {
    *pv = malloc(2*n*sizeof(int));
    int *tmp = *pv;
    for (int i = 0; i < 2*n; i++) {
        tmp[i] = 0;
    }
    for(int i = 0; i < n; i ++) {
        int carry = 0;
        for (int j = 0; j < n; j ++) {
            tmp[i+j] += v1[i] * v2[j] + carry;
            carry =tmp[i+j]/10;
            tmp[i+j] = tmp[i+j] % 10;
        }
        tmp[i+n] += carry;
    }
}

int main(int argc, char *argv[]) {
    int a[3] = {2, 3, 0}; // int 32
    int b[3] = {3, 4, 2}; // int 243

    int *pv = NULL;
    mult(a, b, 3, &pv);
    for (int i = 5; i >= 0; i--) {
        printf("%d", pv[i]);
    }
}