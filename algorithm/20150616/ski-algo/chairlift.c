#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAXLEN 100
#define DEBUG

typedef struct skier_t_ skier_t;
typedef struct chairlift_t_ chairlift_t;


skier_t *skiers = NULL;
chairlift_t *chairlifts = NULL;

// a list of skiers for each chairlift is not implemented
// "the number of times a skier used a chairlift" can be maintained
// in chairlift list for each skier.
// this is not quite memory-efficient, but needs less structures.
struct chairlift_t_ {
    char *id;
    int  interval;
    int  last_auth;
    int  count;     // number of times a skier has used this lift
    chairlift_t *next;
};



// skiers are maintained as a BST, for logarithmic complexity
struct skier_t_ {
    int id;
    skier_t *left;
    skier_t *right;
    // each skier has a list of chairlifts;
    chairlift_t *chairlifts;
};

chairlift_t *readFile(char *filename, int *n_chairlift) {
    *n_chairlift = 0;
    chairlift_t *head = NULL;
    FILE *fp = fopen(filename, "r");
    if (!fp) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }
    char id[MAXLEN];
    int  interval;
    while(fscanf(fp, "%s %d", id, &interval) == 2) {
        chairlift_t *tmp = (chairlift_t *) malloc(sizeof(chairlift_t));
        tmp->interval = interval;
        tmp->id = strdup(id);
        tmp->last_auth = tmp->count = 0;
        tmp->next = head;
        head = tmp;
        *n_chairlift += 1;
    }
    fclose(fp);
    return head;
}

skier_t *insert_skier(skier_t *root, int cardId) {
    // insert into an empty tree
    if (root == NULL) {
        skier_t *t = malloc(sizeof(skier_t));
        t->left = t->right = NULL;
        t->id = cardId;
        t->chairlifts = NULL;
        return t;
    }
    if (cardId == root->id) {
        return root;
    }
    if (cardId < root->id) {
         root->left = insert_skier(root->left, cardId);
    }
    if (cardId > root->id) {
        root->right = insert_skier(root->right, cardId);
    }

    return root;
}

skier_t *search_skier(skier_t *root, int cardId) {
    if (root == NULL)
        return NULL;

    if (root->id == cardId)
        return root;
    else if (cardId < root->id) {
        return search_skier(root->left, cardId);
    }
    else {
        return search_skier(root->right, cardId);
    }
}

void preoder_skiers(skier_t *root) {
    if (root == NULL)
        return;

    printf("preordering %d\n", root->id);
    preoder_skiers(root->left);
    preoder_skiers(root->right);
}

chairlift_t *search_lift(skier_t *skier, char *chairliftId) {
    for(chairlift_t *c = skier->chairlifts; c != NULL; c = c->next) {
        if (strcmp(c->id, chairliftId) == 0) {
            return c;
        }
    }

    chairlift_t *tmp = malloc(sizeof(chairlift_t));
    tmp->id = strdup(chairliftId);
    tmp->count = 0;
    tmp->last_auth = 0;
    for (chairlift_t *c = chairlifts; c != NULL; c = c->next) {
        if (strcmp(c->id, chairliftId) == 0) {
            tmp->interval = c->interval;
        }
    }

    tmp->next = skier->chairlifts;
    skier->chairlifts = tmp;

    return tmp;
}

int authorize(int cardId, char *chairliftId, int time) {
    skiers = insert_skier(skiers, cardId);
    skier_t *this= search_skier(skiers, cardId);

    chairlift_t *lift = search_lift(this, chairliftId);

    if (time - lift->last_auth >= lift->interval ) {
        lift->last_auth = time;
        lift->count += 1;
        printf("Hi %d, this is your %d time in %s ", cardId, lift->count, lift->id);
        return 1;
    }

    return 0;
}

int main(int argc, char *argv[]) {

    int n_chairlift;
    char chairliftId[MAXLEN];
    int  cardId;
    int  current_time;
    chairlifts = readFile(argv[1], &n_chairlift);

    #ifdef DEBUG
    for(chairlift_t *t = chairlifts; t != NULL; t = t->next) {
        printf("%s %d\n", t->id, t->interval);
    }
    #endif
    int n_skier = 0;
    char *skier_filenames[3] = {"skier2.txt","skier1.txt" ,"skier3.txt"};
    while(n_skier < 3) {
        FILE *fp = fopen(skier_filenames[n_skier], "r");
        if (!fp) {
            fprintf(stderr, "skier file %s open failed\n",skier_filenames[n_skier]);
            exit(-1);
        }
        while(fscanf(fp, "%s %d %d",chairliftId, &cardId, &current_time) == 3) {
            #ifdef DEBUG
            printf("%s -- %d -- %d\n", chairliftId, cardId, current_time);
            #endif
            if (authorize(cardId, chairliftId, current_time)) {
                printf("welcome & have fun\n");
            }
            else {
                printf("Hi %d! you ski in worm hole & time travel!\n", cardId);
            }
        }
        fclose(fp);
        n_skier += 1;
    }

    #ifdef DEBUG
    preoder_skiers(skiers);
    #endif
    return 0;
}