//
// Created by zhaos on 2/16/2019.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int vowels(char *s, int start, int n) {
    int count = 0;
    for (int i = start; i < start + n; i++) {
        if (strchr("AEIOUaeiou", s[i]) != NULL) {
            count++;
        }
    }
    return count;
}
void print_substr(char *s, int start, int n) {
    for (int i = start; i < start + n; i++) {
        printf("%c", s[i]);
    }
    printf("\n");
}
int string_count(char *s, int n) {
    for (int i = 0; i <= strlen(s) - n; i++) {
        if (vowels(s, i, n) >= 2) {
            print_substr(s, i, n);
        }
    }
}
int main() {
    char s[20] = "ForExample";
    string_count(s, 5);
}
