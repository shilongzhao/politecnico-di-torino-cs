//
// Created by zhaos on 1/26/2019.
//

#include <stdio.h>
#include <stdlib.h>
typedef struct node_t node_t;
struct node_t {
    int k ;
    node_t *left;
    node_t *right;
    int heightLeft;
    int heightRight;
};

int max(int a, int b) {
    if (a > b)
        return a;
    else
        return b;
}

int height(node_t *root) {
    if (root == NULL) {
        return -1;
    }
    else {
        root->heightLeft = height(root->left) + 1;
        root->heightRight = height(root->right) + 1;
        return max(root->heightLeft, root->heightRight);
    }
}

void helper(node_t *r, int *max) {
    if (r == NULL) {
        return;
    }
    if (*max < r->heightRight + r->heightLeft) {
        *max = r->heightLeft + r->heightRight;
    }
    helper(r->left, max);
    helper(r->right, max);
}
int tree_diameter(node_t *r) {
    int max = 0;
    helper(r, &max);
    printf("max diameter = %d\n", max);
    return max;
}

void print_height(node_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%d: %d %d\n", root->k, root->heightLeft, root->heightRight);
    print_height(root->left);
    print_height(root->right);
}
int main() {
    node_t n1 = {1, NULL, NULL, -1, -1};
    node_t n2 = {2, NULL, NULL, -1, -1};
    node_t n3 = {3, NULL, NULL, -1, -1};
    node_t n4 = {4, NULL, NULL, -1, -1};
    node_t n5 = {5, NULL, NULL, -1, -1};
    node_t n6 = {6, NULL, NULL, -1, -1};
    node_t n7 = {7, NULL, NULL, -1, -1};

    n1.left = &n2;
    n1.right = &n3;

    n2.left = &n4;
    n2.right = &n5;

    n4.left = &n6;

//    n5.right = &n7;

    height(&n1);

    print_height(&n1);

    tree_diameter(&n1);
}