//
// Created by zhaos on 1/26/2019.
//
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
int alpha_order(char *r) {
    int i = 0;
    while (i < strlen(r) - 1) {
        if (r[i] < r[i+1]) {
            i++;
        }
        else {
            return 0;
        }
    }
    return 1;
}

void combo(char *s, int k, char *r, int nr, int p) {
    if (k == 0) {
        r[nr] = '\0';
        if (alpha_order(r)) {
            printf("%s\n", r);
            return;
        }
    }
    for (int i = p + 1; i < strlen(s); i++) {
        r[nr] = s[i];
        combo(s, k - 1, r, nr + 1, i);
    }
}

void subsequences(char *s, int k) {
    char *r = malloc((k + 1) * sizeof(char));
    combo(s, k, r, 0, -1);
}

int main() {
    char s[5] = "ABCD";
    subsequences(s, 3);
}