
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_L_NAME   20
#define MAX_L_ROW  2048

/* structure declaration */
typedef struct node_t {
    char          *name;
    struct node_t *left;
    struct node_t *right;
} node_s;

/* function prototypes */
node_s *load_tree(char *file_name);
node_s *find_node_r(node_s *root1, char *name);
int visit_r(node_s *root1, node_s *root2);
void order_sons(node_s *root);
void destroy_tree_r(node_s *root);
FILE *util_fopen(char *name, char *access);
char *util_strdup(char *src);
void *util_malloc(int size);

/*
 * main program
 */
int main(int argc,char *argv[])
{
  node_s *root_1, *root_2;

  root_1 = load_tree(argv[1]);
  root_2 = load_tree(argv[2]);
  /*
     have a look in visit_r and remind:
     EXIT_SUCCESS=0
     EXIT_FAILURE=1
  */
  if (!visit_r(root_1, root_2)) {
    printf("Equivalent trees\n");
  } else {
    printf("NOT equivalent trees\n");
  }

  destroy_tree_r(root_1);
  destroy_tree_r(root_2);

  return EXIT_SUCCESS;
}

/*
 * visit and compare trees
 */
int visit_r(node_s *root1, node_s *root2)
{

  if (root1==NULL && root2==NULL) {
    return EXIT_SUCCESS;
  }

  if ((root1!=NULL && root2!=NULL) &&
       strcmp(root1->name, root2->name)==0) {
    order_sons(root1);
    order_sons(root2);
    if (!visit_r(root1->left, root2->left)) {
      return visit_r(root1->right, root2->right);
    }
  }

  return EXIT_FAILURE;
}

/*
 * order sons
 */
void order_sons(node_s *root)
{
  node_s *tmp;

  if ((root->left!=NULL && root->right!=NULL &&
       strcmp(root->left->name, root->right->name)>0) ||
       (root->left==NULL && root->right!=NULL)) {
    tmp = root->left;
    root->left = root->right;
    root->right = tmp;
  }

  return;
}

/*
 * search parent
 */
node_s *find_node_r(node_s *root, char *name)
{
  node_s *tmp = NULL;

  if (root != NULL) {
    if (strcmp(root->name, name) == 0) {
      tmp = root;
    } else if ((tmp = find_node_r(root->left, name)) == NULL) {
      tmp = find_node_r(root->right, name);
    }
  }

  return tmp;
}

/*
 * load file
 */
node_s *load_tree(char *file_name)
{
  node_s *root, *tmp;
  FILE *fp;

  char row[MAX_L_ROW];
  char name[MAX_L_NAME+1], leftName[MAX_L_NAME+1];
  char rightName[MAX_L_NAME+1];
  int n;

  root = NULL;
  fp = util_fopen(file_name, "r");

  while (fgets(row, MAX_L_ROW, fp) != NULL) {
    sscanf(row, "%s %s %n", name, leftName, &n);

    /* first node: it must be initialized */
    if (root == NULL) {
      tmp = root = (node_s *)util_malloc(sizeof(node_s));
      tmp->left = tmp->right = NULL;
      tmp->name = util_strdup(name);
    } else {
      /* other nodes: parent is already inserted */
      tmp = find_node_r(root, name);
    }
      /* left child */
      tmp->left = (node_s *)util_malloc(sizeof(node_s));
      tmp->left->name = util_strdup(leftName);
      tmp->left->left = tmp->left->right = NULL;
      /* right child */
      if (sscanf(row+n, "%s", rightName) != EOF) {
        tmp->right = (node_s *)util_malloc(sizeof(node_s));
        tmp->right->name = util_strdup(rightName);
        tmp->right->left = tmp->right->right = NULL;
      } else {
        tmp->right = NULL;
      }
    }

  fclose(fp);

  return root;
}

/*
 * free memory
 */
void destroy_tree_r(node_s *root)
{
  node_s *s,*t;
  t = root;

  if (t == NULL) {
    return;
  } else {
    s=t;
    destroy_tree_r(t->left);
    destroy_tree_r(t->right);
    free(s->name);
    free(s);
  }

}

/*
 * open file and return the file pointer
 */
FILE *util_fopen(char *name, char *access)
{
  FILE *fp = fopen(name, access);

  if(fp == NULL) {
    printf("Error opening file %s!\n", name);
    exit(EXIT_FAILURE);
  }

  return fp;
}

/*
 * string: dynamic allocation
 */
char *util_strdup(char *src)
{
  char *dst = strdup(src);

  if (dst == NULL) {
    printf("Memory allocation error!\n");
    exit(EXIT_FAILURE);
  }

  return dst;
}

/*
 * dynamic allocation
 */
void *util_malloc(int size)
{
  void *ptr = malloc(size);

  if (ptr == NULL) {
    printf("Memory allocation error!\n");
    exit(EXIT_FAILURE);
  }

  return ptr;
}




