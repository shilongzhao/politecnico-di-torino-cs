
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX 51
#define N 32

/* structure declaration */
typedef struct node_t {
    int           node_num;
    char          *key_1;
    char          *key_2;
    int           child_1;
    int           child_2;
    int           child_3;
    struct node_t *left;
    struct node_t *middle;
    struct node_t *right;
}node_s;

/* function prototypes */
node_s *create(FILE *);
node_s *add_node_r(node_s *, node_s *);
void destroy_tree_r(node_s *);
void find_node_r (node_s *, char *, int, int *);
FILE *util_fopen(char *, char *);
char *util_strdup(char *);
void *util_malloc(int);

/*
 * main program
 */
int main(int argc, char*argv[])
{
  FILE *fp;
  node_s *keys;
  int end, found, depth;
  char cmd[N];

  keys = NULL;

  if(argc != 2) {
    fprintf(stderr, "Error: missing parameter\n");
    exit(EXIT_FAILURE);
  }

  fp = util_fopen(argv[1], "r");
  keys = create(fp);
  fclose(fp);

  end = EXIT_SUCCESS;

  while (!end) {
    printf("key/end: ");
    scanf("%s", cmd);
    if (strcmp(cmd, "end") == 0) {
      destroy_tree_r(keys);
      end = EXIT_FAILURE;
    } else {
      found = EXIT_FAILURE;
      depth = 1;
      find_node_r(keys, cmd, depth, &found);
    }
  }

  return EXIT_SUCCESS;
}

/*
 * create tree
 */
node_s *create(FILE *fp)
{
  node_s *root, *curr;
  int num;
  char key_1[MAX];
  char key_2[MAX];
  int node_num;
  int child_1, child_2, child_3;

  root = NULL;
  while(!feof(fp)) {
    num = fscanf(fp, "%d %s %s %d %d %d", &node_num, key_1, key_2,
                                          &child_1, &child_2, &child_3);
    if(num == 6) {
      curr = (node_s *)util_malloc(sizeof(node_s));
      curr->node_num = node_num;
      curr->key_1 = util_strdup(key_1);
      curr->key_2 = util_strdup(key_2);
      curr->child_1 = child_1;
      curr->child_2 = child_2;
      curr->child_3 = child_3;
      curr->left = NULL;
      curr->middle = NULL;
      curr->right = NULL;
      root = add_node_r(root, curr);
    }
  }

  return root;
}

/*
 * add a node to the tree
 */
node_s *add_node_r(node_s *root, node_s *node)
{
  if(root == NULL) {
    return node;
  }

  if (strcmp(node->key_1, root->key_1)<0 &&
      strcmp(node->key_2, root->key_1)<0) {

    root->left = add_node_r(root->left, node);

  } else if (strcmp(node->key_1, root->key_2)>0 &&
             strcmp(node->key_2, root->key_2)>0) {

    root->right = add_node_r(root->right, node);
  } else {
    root->middle = add_node_r(root->middle, node);
  }

  return root;
}

/*
 * search a node, calculate depth and print path
 */
void find_node_r (node_s *root, char *key, int depth, int *found)
{
  if(root == NULL) {
    printf("Not found\n");
    return;
  } else {
    if (strcmp(key, root->key_2) > 0) {
      find_node_r(root->right, key, depth+1, found);
    }
    if (strcmp(root->key_1, key) > 0) {
      find_node_r(root->left, key, depth+1, found);
    }
    if (strcmp(root->key_1, key)<0 && strcmp(root->key_2, key)>0) {
      find_node_r(root->middle, key, depth+1, found);
    }
    if (strcmp(root->key_1, key)==0 || strcmp(root->key_2, key)==0) {
      printf("Depth: %d\n", depth);
      printf("%s - %s\n", root->key_1, root->key_2);
      *found = EXIT_SUCCESS;
      return;
    }

    if (*found == EXIT_SUCCESS) {
      printf("%s - %s\n", root->key_1, root->key_2);
    }
  }

}

/*
 * free memory
 */
void destroy_tree_r(node_s *root)
{
  node_s *s, *t;
  t = root;

  if (t == NULL) {
    return;
  } else {
    s = t;
    destroy_tree_r(t->left);
    destroy_tree_r(t->middle);
    destroy_tree_r(t->right);
    free(s->key_1);
    free(s->key_2);
    free(s);
  }

}

/*
 * open file and return the file pointer
 */
FILE *util_fopen(char *name, char *access)
{
  FILE *fp = fopen(name, access);

  if(fp == NULL) {
    printf("Error opening file %s!\n", name);
    exit(EXIT_FAILURE);
  }

  return fp;
}

/*
 * string: dynamic allocation
 */
char *util_strdup(char *src)
{
  char *dst = strdup(src);

  if (dst == NULL) {
    printf("Memory allocation error!\n");
    exit(EXIT_FAILURE);
  }

  return dst;
}

/*
 * dynamic allocation
 */
void *util_malloc(int size)
{
  void *ptr = malloc(size);

  if (ptr == NULL) {
    printf("Memory allocation error!\n");
    exit(EXIT_FAILURE);
  }

  return ptr;
}
