
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define M 20+1

/* structure declaration */
typedef struct node_t{
  char          *name;
  int           n_children;
  struct node_t **children;
} node_s;

/* function prototypes */
void read_file(node_s **head, char *fileName);
FILE *util_fopen(char *name, char *access);
node_s * create_node(void);
node_s * search_node_r(char *name, node_s *head);
int level_r(node_s *head);
void free_tree_r(node_s *head);
char *util_strdup(char *src);
void *util_malloc(int size);
FILE *util_fopen(char *name, char *access);

/*
 * main program
 */
int main(int argc, char *argv[])
{
  FILE *fp;
  node_s *head = NULL;

  if (argc != 2) {
    fprintf(stderr, "Missing parameters\n");
    exit(EXIT_FAILURE);
  }

  read_file(&head, argv[1]);
  level_r(head);

  free_tree_r(head);

  return EXIT_SUCCESS;
}

/*
 * read file and create tree
 */
void read_file(node_s **head, char *fileName)
{
  node_s *temp;
  int i, n;
  char name[M], child[M];
  FILE *fp;

  fp = util_fopen(fileName, "r");
  while (fscanf(fp, "%s %d", name, &n) != EOF) {
    if (*head == NULL) {
      temp = *head = create_node();
      temp->name = util_strdup(name);
      } else {
      temp = search_node_r(name, *head);
      }
      temp->n_children = n;
      temp->children = (node_s **)malloc(n*sizeof(node_s *));
      if (temp->children == NULL) {
        fprintf(stderr, "Dynamic allocation error\n");
        exit(EXIT_FAILURE);
      }

      for (i=0; i<n; i++) {
        fscanf(fp, "%s", child);
        temp->children[i] = create_node();
        temp->children[i]->name = util_strdup(child);
      }
  }
  fclose(fp);

}

/*
 * level nodes
 */
int level_r(node_s *head)
{
  int i, h, high;

  h = -1;

  if (head == NULL) {
    return -1;
  }

   /* if node is a leaf...*/
  if (head->n_children == 0) {
    printf("%s=0 ", head->name);
  }
  /*calculate height for each node */
  for (i=0; i<head->n_children; i++) {
    high = level_r(head->children[i]);
    if (h < high) {
      h = high;
      printf("%s=%d ", head->name, h+1);
    }
  }

  return h+1;
}

/*
 * search node
 */
node_s *search_node_r(char name[M], node_s *head)
{
  node_s *temp = NULL;
  int i;

  if (head != NULL) {
    if (strcmp(name, head->name) == 0) {
      temp = head;
      } else {
      for (i=0; i<head->n_children && temp==NULL; i++) {
         temp = search_node_r(name, head->children[i]);
      }
    }
  }

  return temp;
}

/*
 * create a node
 */
node_s * create_node(void)
{
  node_s *q;

  q = (node_s *)util_malloc(sizeof(node_s));
  q->n_children = 0;
  q->children = NULL;

  return q;
}

/*
 * free memory
 */
void free_tree_r(node_s *head)
{
  int i;

  if (head == NULL) {
    return;
  }

  for (i=0; i<head->n_children; i++) {
    free_tree_r(head->children[i]);
  }

  free(head->name);
  free(head);

}

/*
 * open file and return the file pointer
 */
FILE *util_fopen(char *name, char *access)
{
  FILE *fp = fopen(name, access);

  if(fp == NULL) {
    printf("Error opening file %s!\n", name);
    exit(EXIT_FAILURE);
  }

  return fp;
}

/*
 * string: dynamic allocation
 */
char *util_strdup(char *src)
{
  char *dst = strdup(src);

  if (dst == NULL) {
    printf("Memory allocation error!\n");
    exit(EXIT_FAILURE);
  }

  return dst;
}

/*
 * dynamic allocation
 */
void *util_malloc(int size)
{
  void *ptr = malloc(size);

  if (ptr == NULL) {
    printf("Memory allocation error!\n");
    exit(EXIT_FAILURE);
  }

  return ptr;
}
