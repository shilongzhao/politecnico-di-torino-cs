#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int val;
    char leftRel;
    char upRel;
    char rowFlag;
    char colFlag;
} cell_t;

cell_t **load_futoshiki(char *, int *, int *, int *);
int check_relations(cell_t **, int, int);
void print_futoshiki(cell_t **, int);
int solve_futoshiki(cell_t **, int, int);

int main (int argc, char *argv[])
{
    int i, n, valid, complete;
    cell_t **scheme;

    scheme = load_futoshiki(argv[1], &n, &complete, &valid);

    if (complete) {
        printf("Complete scheme.\n");
        if (valid) {
            printf("Valid solution.\n");
        } else {
            printf("Invalid solution.\n");
        }
    } else {
        printf("Incomplete scheme.\n");
        if (valid) {
            valid = solve_futoshiki(scheme, n, 0);
        }
        // now valid is the return value from solve_futoshiki
        if (valid) {
            printf("Solution:\n");
            print_futoshiki(scheme, n);
        } else {
            printf("A solution does not exist.\n");
        }
    }


    for (i=0; i<n; i++) {
        free(scheme[i]);
    }
    free(scheme);
    return EXIT_SUCCESS;
}

cell_t **load_futoshiki(char *name, int *dim, int *complete, int *valid)
{
    cell_t **scheme;
    int n, i, j, val;
    char sep[10];
    FILE *fp;

    fp = fopen(name, "r");
    fscanf(fp, "%d", &n);
    scheme = (cell_t **)malloc(n*sizeof(cell_t *));

    for (i=0; i<n; i++) {
        scheme[i] = (cell_t *)malloc(n*sizeof(cell_t));
        for (j=0; j<n; j++) {
            scheme[i][j].val = 0;
            scheme[i][j].leftRel = '|';
            scheme[i][j].upRel = '-';
            scheme[i][j].rowFlag = 0;
            scheme[i][j].colFlag = 0;
        }
    }

    *dim = n;
    *complete = 1;
    *valid = 1;
    for (i=0; i<n; i++) {
        if (i > 0) { // scheme[i][j] have an upRelation from the second line
            for (j=0; j<n; j++) {
                fscanf(fp, "%s", sep);
                scheme[i][j].upRel = sep[0];
                if (j < n-1) { // jump over the useless relation
                    fscanf(fp, "%*s");
                }
            }
        }

        for (j=0; j<n; j++) {
            if (j > 0) { // scheme[i][j] has a leftRelation from the second column
                fscanf(fp, "%s", sep);
                scheme[i][j].leftRel = sep[0];
            }
            fscanf(fp, "%d", &val); // scan value
            scheme[i][j].val = val;
            if (val == 0) {
                *complete = 0;
            } else if (val<0 || val>n) {
                *valid = 0;
            } else {
                if (scheme[i][val-1].rowFlag++) { // scheme[i][val-1].rowFlag  plus 1, meaning that in row i, val has bee taken
                    *valid = 0;
                }
                if (scheme[val-1][j].colFlag++) { // schme[val-1][j].colFlag plus 1, meaning that in column j, val has been taken
                    *valid = 0;
                }
                if (check_relations(scheme, i, j) == 0) {
                    *valid = 0;
                }
            }
        }
    }
    fclose(fp);

    return scheme;
}


// only check relation, not conflicts in a row nor column  ???
int check_relations(cell_t **scheme, int row, int col)
{
    int val=scheme[row][col].val;
    char sep;

    if (col>0 && scheme[row][col-1].val!=0) {
        sep = scheme[row][col].leftRel;
        if (sep=='<' && scheme[row][col-1].val>val) {
            return 0;
        }
        if (sep=='>' && scheme[row][col-1].val<val) {
            return 0;
        }
    }

    if (row>0 && scheme[row-1][col].val!=0) {
        sep = scheme[row][col].upRel;
        if (sep=='^' && scheme[row-1][col].val>val) {
            return 0;
        }
        if (sep=='v' && scheme[row-1][col].val<val) {
            return 0;
        }
    }

    return 1;
}

// n: dimension of matrix
// step: equivalent to which cell we are solving
int solve_futoshiki(cell_t **scheme, int n, int step)
{
    int i, j, k, rowUsed, colUsed;

    i = step / n;
    j = step % n;

    if (step >= n*n) {
        return 1;
    }

    if (scheme[i][j].val) { // if value has been set
        if (check_relations(scheme, i, j)) {
            return solve_futoshiki(scheme, n, step+1); // solve next scheme
        } else {
            return 0;
        }
    }

    for (k=1; k<=n; k++) {
        scheme[i][j].val = k;
        rowUsed = scheme[i][k-1].rowFlag;
        scheme[i][k-1].rowFlag++;
        colUsed = scheme[k-1][j].colFlag++;
        if (rowUsed==0 && colUsed==0 && check_relations(scheme, i, j)==1) {
            if (solve_futoshiki(scheme, n, step+1)) {
                return 1;
            }
        }
        //reset, prepare for the next expand branch
        scheme[i][j].val = 0;
        scheme[i][k-1].rowFlag--;
        scheme[k-1][j].colFlag--;
    }
    return 0;
}

void print_futoshiki(cell_t **scheme, int n)
{
    int i, j;

    for (i=0; i<n; i++) {
        if (i > 0) {
            for (j=0; j<n; j++) {
                printf("%c", scheme[i][j].upRel);
                if (j < n-1) {
                    printf(" - ");
                }
            }
            printf("\n");
        }
        for (j=0; j<n; j++) {
            if (j > 0) {
                printf(" %c ", scheme[i][j].leftRel);
            }
            printf("%d", scheme[i][j].val);
        }
        printf("\n");
    }
}