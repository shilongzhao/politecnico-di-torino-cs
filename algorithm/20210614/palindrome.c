
#include<stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

// check if a string is palindrome
int is_palindrome(char *str);
void print_palindromes(char **strings, int N);
void recur(char **strings, int N, char *result, int *used, int depth);

void palindrome(char *name) {
    FILE *fp = fopen(name, "r");
    int N;
    // read lines number
    fscanf(fp, "%d\n", &N);
    // storage for all lines
    char **strings = malloc(sizeof(char *) * N);
    // CHANGED to 12 since max string len is 10, there could be \n, and also \0
    char t[12];
    int i = 0;
    // CHANGE from fscanf to fgets since there may be white space in the same line
    while(fgets(t, 12, fp) != NULL) {
        // CHANGED search position of newline and replace it with \0
        char *npos = strchr(t, '\n');
        if (npos != NULL) {
            *npos = '\0';
        }
        // read file and save the line to strings[i]
        strings[i++] = strdup(t);
    }
    print_palindromes(strings, N);
}

int is_palindrome(char *str) {
    int i = -1, j = strlen(str);
    while (i < j) {
        while (str[++i] == ' ');
        while (str[--j] == ' ');
        char ci = tolower(str[i]);
        char cj = tolower(str[j]);
        if (ci != cj) return 0;
    }
    return 1;
}

void recur(char **strings, int N, char *result, int *used, int depth) {
    // end of recursion
    if (strlen(result)> 0 && is_palindrome(result)) {
        printf("%s\n",result);
        // CHANGE: removed `return` statement because a palindrome string can still go down in the recursion tree
//        return;
    }
    // already tried with all strings
    if (depth == N) return;
    for (int i = 0; i < N; i++) {
        if (!used[i]) {
            used[i] = 1;
            // remember the point to do backtracking
            int len = strlen(result);
            // concat the string
            strcat(result, strings[i]);
            recur(strings, N, result, used, depth + 1);
            // backtracking
            result[len] = '\0';
            used[i] = 0;
        }
    }
}
void print_palindromes(char **strings, int N) {
    // reserve space for a palindrome that contains all strings in the file
    char *result = malloc(sizeof(char) * N * 10 + 1);
    result[0] = '\0';
    // remember if a string has been used or not
    int *used = calloc(N, sizeof(int));
    recur(strings, N, result, used, 0);
}

int main() {
    palindrome("palindrome.txt");
}