
#include <stdlib.h>
#include <string.h>
#include <printf.h>

typedef struct producer_s producer_t;
typedef struct product_s product_t;

struct producer_s {
    char *name;
    producer_t *next;
    product_t *right;
};

struct product_s {
    char *id;
    float price;
    product_t *next;
};

producer_t *find_producer(producer_t *head, char *producer_name) {
    producer_t *h = head;
    while (h != NULL) {
        if (strcmp(h->name, producer_name) == 0) {
            return h;
        }
        h = h->next;
    }
    return NULL;
}

product_t *find_product(product_t *head, char *id) {
    product_t *h = head;
    while (h != NULL) {
        if (strcmp(h->id, id) == 0) {
            return h;
        }
        h = h->next;
    }
    return NULL;
}

product_t *create_product(char *id, float price) {
    product_t *pdt = malloc(sizeof(product_t));
    pdt->id = strdup(id);
    pdt->price = price;
    pdt->next = NULL;
    return pdt;
}

void insert(producer_t **head, char *name, char *id, float price) {
    producer_t *p = find_producer(*head, name);
    if (p == NULL) {
        producer_t *pdr = malloc(sizeof(producer_t));
        pdr->name = strdup(name);
        product_t *pdt = create_product(id, price);
        pdr->right = pdt;
        pdr->next = *head;
        *head = pdr;
        return;
    }

    product_t *found = find_product(p->right, id);
    if (found == NULL) {
        product_t *pdt = create_product(id, price);
        pdt->next = p->right;
        p->right = pdt;
        return;
    }
    found->price = price;
}


void print_producers(producer_t *head) {
    while (head != NULL) {
        printf("%s: ", head->name);
        product_t *pdt = head->right;
        while (pdt != NULL) {
            printf(" (%s %.1f) ", pdt->id, pdt->price);
            pdt = pdt->next;
        }
        printf("\n");
        head = head->next;
    }
}

int main() {
    producer_t *producers = NULL;

    printf("=== inserting Alfa Romeo Spider ====\n");
    insert(&producers, "Alfa Romeo", "Spider", 55000.0f);

    printf("=== inserting FIAT 500 ====\n");
    insert(&producers, "FIAT", "500", 23000.0f);

    printf("=== inserting Citroen C4 ====\n");
    insert(&producers, "Citroen", "C4", 33000.0f);

    printf("=== inserting Alfa Romeo Stelvio ====\n");
    insert(&producers, "Alfa Romeo", "Stelvio", 50000.0f);

    printf("=== inserting FIAT Tipo ====\n");
    insert(&producers, "FIAT", "Tipo", 34000.0f);

    printf("=== inserting Renault Clio ====\n");
    insert(&producers, "Renault", "Clio", 25000.0f);

    printf("=== inserting Alfa Romeo Giulia ====\n");
    insert(&producers, "Alfa Romeo", "Giulia", 45000.0f);

    printf("=== updating Alfa Romeo Spider ====\n");
    insert(&producers, "Alfa Romeo", "Spider", 65000.0f);

    printf("=== updating FIAT 500 ====\n");
    insert(&producers, "FIAT", "500", 21000.0f);

    print_producers(producers);

}