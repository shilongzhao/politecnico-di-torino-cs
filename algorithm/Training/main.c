/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#define DEBUG
#define P 4
#define MAXLEN 100

// N = number of exercises per phase
int N, MAX_CAL;
// num of all exercises in total
int NUM_EX;

//max number of exercises per phase, global variable

typedef struct exercise_t_ {
    char *name;
    int cal;
    int used;
    int phases[P+1];
} exercise_t;

typedef struct solution_t_ {
    int num; // how many exercises are now in
    int cal;
    exercise_t **sol;
} solution_t;

exercise_t **read_file(char *filename, int *count);

solution_t *solution_t_init(solution_t *solution);

solution_t *compare_solution(solution_t *c, solution_t *b);


 void find_solution(exercise_t **exercises, int p,
                   solution_t *current, solution_t *best, int index);

/**
 * print best solution for phase p
 *
 * @param   s   best solution
 * @param   p   phase p
 */
void print_solution(solution_t *s, int p);



int main(int argc,char *argv[]) {

    exercise_t **exercises = NULL;



    //read all exercises into an array
    exercises = read_file(argv[3], &NUM_EX);

    N = atoi(argv[1]);
    MAX_CAL = atoi(argv[2]);

    solution_t *current = malloc(sizeof(solution_t));

    solution_t *best = malloc(sizeof(solution_t));

    for (int p = 1 ; p <= P; p ++) {
        // every loop it is needed to reinitialize structures
        current = solution_t_init(current);
        best = solution_t_init(best);

        find_solution(exercises, p, current, best, 0);
        for(int i = 0; best->sol[i] != NULL; i++) {
            best->sol[i]->used = 1;
        }
        print_solution(best, p);
    }

    return 0;
}

void print_solution(solution_t *s, int p) {
    printf("Phase %d: \n", p);
    for (int i = 0; i < s->num; i++) {
        printf("%20s%10d\n", s->sol[i]->name, s->sol[i]->cal);
    }
    return;
}

void find_solution(exercise_t **exercises, int p,
                   solution_t *current, solution_t *best, int index) {

    if (index >= NUM_EX || current->num == N) {
        compare_solution(current, best);
        return;
    }

    if (exercises[index]->phases[p] != 1 ||
        exercises[index]->used ||
        (exercises[index]->cal + current->cal > MAX_CAL) ) {
        find_solution(exercises, p, current, best, index + 1);
    }
    else {

        // do recursion when exercise[i] is used;
        exercises[index]->used = 1;
        current->sol[current->num] = exercises[index];
        current->num += 1;
        current->cal += exercises[index]->cal;

        find_solution(exercises, p, current, best,index+1);

        // do recursion when exercise[i] is not used, restoration first;
        current->cal -= exercises[index]->cal;
        current->num -= 1;
        current->sol[current->num] = NULL;
        exercises[index]->used = 0;
        find_solution(exercises, p, current, best,index+1);
    }

}

solution_t *compare_solution(solution_t *c, solution_t *b) {
    if (abs(c->cal - 800) < abs(b->cal - 800)) {
        b->num = c->num;
        b->cal = c->cal;
        for (int i = 0; c->sol[i] != NULL; i++) {
            b->sol[i] = c->sol[i];
        }
    }
    return b;
}

solution_t *solution_t_init(solution_t *solution) {
    solution->num = 0;
    solution->cal = 0;
    solution->sol = malloc((N+1) * sizeof(exercise_t *));
    for (int i = 0; i <= N; i++)
        solution->sol[i] = NULL;
    return solution;
}

exercise_t **read_file(char *filename, int *count) {
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    fscanf(fp, "%d", count);
    exercise_t **array = (exercise_t **)malloc((*count+1) * sizeof(exercise_t *));
    char name[MAXLEN];
    int  cal;

    int id= 0;
    while(fscanf(fp, "%s %d %*d", name, &cal) == 2) {
        array[id] = malloc(sizeof(exercise_t));
        array[id]->name = strdup(name);
        array[id]->cal  = cal;
        array[id]->used = 0;

        for (int i =0; i <=P; i++)
            array[id]->phases[i] = 0;

        int p;
        while(fscanf(fp, "%d", &p) == 1) {
            array[id]->phases[p] = 1;
        }
        id++;
    }
    array[id] = NULL;
#ifdef DEBUG
    for (int i = 0; array[i] != NULL; i++) {
        printf("%s %d %d %d %d %d\n", array[i]->name,
               array[i]->cal, array[i]->phases[1],
               array[i]->phases[2], array[i]->phases[3],
               array[i]->phases[4]);
    }

#endif
    return array;
}