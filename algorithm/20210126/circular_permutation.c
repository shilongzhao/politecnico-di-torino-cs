
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int is_same(char *s, char *t, int n) {
    int i = 0;
    while (s[i] != t[0]) {
        i++;
    }
    for (int p = i, q = 0; q < n; p = (p + 1) % n, q = q + 1) {
        if (s[p] != t[q]) return 0;
    }
    return 1;
}

int contains(char **s, char *t, int rows, int n) {
    for (int i = 0; i < rows; i++) {
        if (is_same(s[i], t, n)) return 1;
    }
    return 0;
}

void recur(char *str, char **perms, int *count, int rows, int n, char *s, int d, int *used) {
    if (d == n && !contains(perms, s, *count, n)) {
        perms[*count] = strdup(s);
        (*count) += 1;
        return;
    }
    for (int i = 0; i < n; i++) {
        if (!used[i]) {
            s[d] = str[i];
            used[i] = 1;
            recur(str, perms, count, rows, n, s, d + 1, used);
            s[d] = '\0';
            used[i] = 0;
        }
    }
}

void circularPermutation(char *str) {
    int n = strlen(str);
    int rows = 1;
    for (int i = 1; i < n; i++) {
        rows = rows * i;
    }

    char **perms = malloc(sizeof(char *) * rows);
    int count = 0;
    char *res = calloc(n + 1, sizeof(char));
    int *used = calloc(n, sizeof(int));

    recur(str, perms, &count, rows, n, res, 0, used);

    for (int i = 0; i < rows; i++) {
        printf("%s\n", perms[i]);
    }

    free(used);
    free(res);
    for (int i = 0; i < rows; i++)
        free(perms[i]);
    free(perms);
}

int main() {
    circularPermutation("ABCD");
}