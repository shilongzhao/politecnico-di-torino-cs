#ifndef EX4_201509_H
#define EX4_201509_H

typedef struct route_t route_t;

struct route_t {
    int dist; // distance is simplified as hop
    int weight;
};

void search(route_t *best, route_t *cur, int **mat, int **visited, int N, int x, int y);

#endif // 201509_EX4_H
