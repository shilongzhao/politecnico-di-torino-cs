#include <stdlib.h>
#include "ex3.h"
#include <string.h>
int treeIsomorph(node_t *t1, node_t *t2) {
    // both empty, return true;
    // recursion reaches bottom, return true;
    if (t1 == NULL && t2 == NULL)
        return 1;

    // in case only one is null, return false
    if(t1 != NULL && t2 == NULL)
        return 0;
    if (t1 == NULL && t2 != NULL)
        return 0;

    if (strcmp(t1->key, t2->key) == 0) {
            // recursion, return true only if BOTH left child and right child are isomorphic
            return treeIsomorph(t1->left, t2->left) && treeIsomorph(t1->right, t2->right);
    }
    else
        return 0;

}
