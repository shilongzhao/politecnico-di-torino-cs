#ifndef EX3_201509_H
#define EX3_201509_H

typedef struct node node_t;

struct node {
    char *key;
    node_t *left;
    node_t *right;
};

int treeIsomorph(node_t *t1, node_t *t2);

#endif //201509_EX3_H
