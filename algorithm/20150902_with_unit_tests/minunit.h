//
// Created by Shilong on 03/09/15.
//
#undef NDEBUG

#ifndef MINUNIT_H
#define MINUNIT_H
#include <stdio.h>
#include "dbg.h"
#include <stdlib.h>

#define mu_suite_start() char *message = NULL;
#define mu_assert(test, message) if (!(test)) { log_err(message); return message; }

#define mu_run_test(test) do {debug("\n\n-------%s", #test); message = test(); tests_run += 1; if (message) return message;} while(0)

#define RUN_TESTS(name) int main(int argc, char *argv[]) { \
    argc = 1; \
    debug("----- Running: %s\n", argv[0]); \
    printf("----- Running: %s\n", argv[0]); \
    char *result = name(); \
    if (result != 0) { \
        printf("FAILED: %s\n", result); \
    } \
    else { \
        fflush(stdout); \
        printf("ALL TESTS PASSED\n"); \
    } \
    printf("Tests run: %d\n", tests_run); \
    exit(result != 0); \
}

int tests_run;

#endif //MINUNIT_H
