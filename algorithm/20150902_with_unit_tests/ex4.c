#include <stdio.h>
#include <stdlib.h>
#include "ex4.h"
#include "util.h"

void search(route_t *best, route_t *cur, int **mat, int **visited, int N, int x, int y) {

    if (x == N-1 && y == N-1) {
        if (cur->weight > best->weight) {
            best->weight = cur->weight;
            best->dist = cur->dist;
        }

        if (cur->weight == best->weight && cur->dist < best->dist) {
            best->dist = cur->dist;
        }

        return;
    }

    for (int next_x = x - 1; next_x <= x + 1; next_x ++) {
        for (int next_y = y - 1; next_y <= y + 1; next_y ++) {

            if (next_x == x && next_y == y) continue;

            if (!legal_array_range(next_x, N) || !legal_array_range(next_y, N) ||
              visited[next_x][next_y]) continue;

            visited[next_x][next_y] = 1;
            cur->dist += 1;
            cur->weight += mat[next_x][next_y];

            search(best, cur, mat, visited, N, next_x, next_y);

            cur->dist -= 1;
            cur->weight -= mat[next_x][next_y];
            visited[next_x][next_y] = 0;
        }
    }
}
