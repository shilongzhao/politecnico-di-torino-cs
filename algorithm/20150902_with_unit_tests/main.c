#include "ex2.h"
#include "minunit.h"
#include "util.h"
#include "ex3.h"
#include "ex4.h"

char *test_ex2() {

  int N = 4;
  int **matrix;
  matrix = calloc(N, sizeof(int *));

  for (int i = 0; i < N; i++) {
    matrix[i] = calloc(N, sizeof(int));
  }

  // TODO: initialization ?
  int mat[4][4] = {{0,3,4,0}, {1,0,6,6}, {1,3,9,0}, {0,0,3,1}};
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      matrix[i][j] = mat[i][j];
    }
  }

  mu_assert(max_diag(matrix, N) == 16, "max diagonal value should be 16!\n");

  for (int i = 0; i < N;  i++) {
    free(matrix[i]);
  }

  free(matrix);
  return NULL;
}

char *test_ex3() {
  struct node *t1, *t2;
  int tests_passed = 0;
  //in case both are NULL
  t1 = NULL;
  t2 = NULL;
  mu_assert(treeIsomorph(t1, t2) == 1, "trees should be isomorphic for two empty trees!");

  // in case only t2 is null;
  t2 = NULL;
  t1 = malloc(sizeof(struct node));
  t1->key = strdup("xx");
  t1->left = t1->right = NULL;
  mu_assert(treeIsomorph(t1, t2) == 0, "empty tree should not be isomorphic for non empty tree!");

  t2 = malloc(sizeof(struct node));
  t2->key = strdup(t1->key);
  t2->left = t2->right = NULL;
  mu_assert(treeIsomorph(t1, t2) == 1, "two single node trees with same key should be isomorphic")

	return NULL;
}

char *test_ex4() {
  int N = 3;

  int mat[3][3] = {{1,2,-3}, {9,-9,7}, {0,1,4}};
  int **matrix = calloc(N, sizeof(int *));
  for (int i = 0; i < N; i++) {
    matrix[i] = calloc(N, sizeof(int));
    for (int j = 0; j < N; j++) {
      matrix[i][j] = mat[i][j];
    }
  }

  route_t *best = (route_t *)malloc(sizeof(route_t));
  route_t *current = malloc(sizeof(route_t));


  // test search2()
  int **visited = calloc(N, sizeof(int *));
  for (int i = 0; i < N; i++) {
    visited[i] = calloc(N, sizeof(int));
    for (int j = 0; j < N; j++) {
      visited[i][j] = 0;
    }
  }

  best->dist = best->weight =  0;

  current->weight = matrix[0][0];
  visited[0][0] = 1;
  current->dist = 0;
  search(best, current, matrix, visited, N, 0, 0);
  mu_assert(best->weight == 24, "best weight should be 24.");
  mu_assert(best->dist == 5, "shortest distance should be 5.");

  free(best);
  free(current);
  for (int i = 0; i < N; i++) {
    free(visited[i]);
    free(matrix[i]);
  }
  free(visited);
  free(matrix);

  return NULL;
}
char *all_tests() {

  mu_suite_start();

  mu_run_test(test_ex2);
  mu_run_test(test_ex3);
  mu_run_test(test_ex4);

  return NULL;
}

RUN_TESTS(all_tests)
