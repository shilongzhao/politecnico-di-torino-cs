/*
 *  StQ 02.09.2015
 *  A&P Exam
 *  Solution of exercize #1
 *
 *  Pay Attention: No check on malloc and other issues are performed
 *                 NEED to introduce them ...
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG 0

int play (int **, int **, int **, int, int, int);
int check (int **, int **, int **, int, int);
void constraintPrint (int **, int);
void matPrint (FILE *, int **, int, int);

int main (
  int argc,
  char *argv[]
  )
{
  FILE *fpR, *fpW;
  int **matR, **matC, **nonogram;
  int i, j, n, nR, nC;

  fpR = fopen (argv[1], "r");
  fpW = fopen (argv[2], "w");
  if (fpR==NULL || fpW==NULL) {
    fprintf (stdout, "Error Opening Files.\n");
    exit (1);
  }

  fscanf (fpR, "%d", &nR);
  matR = (int **) malloc (nR * sizeof (int *));
  for (i=0; i<nR; i++) {
    fscanf (fpR, "%d", &n);
    matR[i] = (int *) malloc ((n+1) * sizeof (int));
    matR[i][0] = n;
    for (j=1; j<=n; j++) {
      fscanf (fpR, "%d", &matR[i][j]);
    }
  }

  fscanf (fpR, "%d", &nC);
  matC = (int **) malloc (nC * sizeof (int *));
  for (i=0; i<nC; i++) {
    fscanf (fpR, "%d", &n);
    matC[i] = (int *) malloc ((n+1) * sizeof (int));
    matC[i][0] = n;
    for (j=1; j<=n; j++) {
      fscanf (fpR, "%d", &matC[i][j]);
    }
  }

  fclose (fpR);

  constraintPrint (matR, nR);
  constraintPrint (matC, nC);

  nonogram = (int **) malloc (nR * sizeof (int *));
  for (i=0; i<nR; i++) {
    nonogram[i] = (int *) malloc (nC * sizeof (int));
  }

  for (i=0; i<nR; i++) {
    for (j=0; j<nC; j++) {
      nonogram[i][j] = 0;
    }
  }

  if (play (nonogram, matR, matC, nR, nC, 0) == 1) {
    fprintf (stdout, "Found Solution:\n");
    matPrint (stdout, nonogram, nR, nC);
    matPrint (fpW, nonogram, nR, nC);
  }

  for (i=0; i<nR; i++) {
    free (matR[i]);
  }
  free (matR);
  for (i=0; i<nC; i++) {
    free (matC[i]);
  }
  free (matC);
  for (i=0; i<nR; i++) {
    free (nonogram[i]);
  }
  free (nonogram);

  fclose (fpW);

  return (0);
}

void constraintPrint (
  int **mat,
  int n
  )
{
  int r, c;

  fprintf (stdout, "%d\n", n);
  for (r=0; r<n; r++) {
    for (c=0; c<=mat[r][0]; c++) {
      fprintf (stdout, "%d ", mat[r][c]);
    }
    fprintf (stdout, "\n");
  }
  
  return;
}

void matPrint (
  FILE *fpW,
  int **mat,
  int nR,
  int nC
  )
{
  int r, c;

  for (r=0; r<nR; r++) {
    for (c=0; c<nC; c++) {
      fprintf (fpW, "%d ", mat[r][c]);
    }
    fprintf (fpW, "\n");
  }
  
  return;
}

int play (
  int **nonogram,
  int **matR,
  int **matC,
  int nR,
  int nC,
  int level
  )
{
  int r, c;

  if (level == (nR*nC)) {
    return (check (nonogram, matR, matC, nR, nC));
  }

  /* From recursion level to matrix indexes */
  r = (int) (level / nC);
  c = (int) (level % nC);

  /* Try all possible combinations of white/clack cells */
  nonogram[r][c] = 0;
  if (play (nonogram, matR, matC, nR, nC, level+1) == 1) {
    return (1);
  }
  nonogram[r][c] = 1;
  if (play (nonogram, matR, matC, nR, nC, level+1) == 1) {
    return (1);
  }
  
  return (0);
}

int check (
  int **nonogram,
  int **matR,
  int **matC,
  int nR,
  int nC
  )
{
  int r, c, n, l;

#if DEBUG
  static int checkN = 0;

  fprintf (stdout, "NONOGRAMS %d\n", checkN++);

  for (r=0; r<nR; r++) {
    for (c=0; c<nC; c++) {
      fprintf (stdout, "%d ", nonogram[r][c]);
    }
    fprintf (stdout, "\n");
  }
#endif

  /* Row Check */
  for (r=0;r<nR; r++) {
    n = 0;
    l = 0;

    for (c=0; c<nC; c++) {
      if (nonogram[r][c]==1) {
        l++; if (l==1)n++;
      } else {
        if (l!=0) {
          if (n>matR[r][0] || l!=matR[r][n]) {
            return (0);
          } else {
            l = 0;
          }
        }
      }
    }

    if (n!=matR[r][0])
      return (0);
    if (l!=0 && l!=matR[r][n])
      return (0);
  }

  /* Column Check */
  for (c=0; c<nC; c++) {
    n = 0;
    l = 0;

    for (r=0; r<nR; r++) {
      if (nonogram[r][c]==1) {
        l++; if (l==1)n++;
      } else {
        if (l!=0) {
          if (n>matC[c][0] || l!=matC[c][n]) {
            return (0);
          } else {
            l = 0;
          }
        }
      }
    }

    if (n!=matC[c][0])
      return (0);
    if (l!=0 && l!=matC[c][n])
      return (0);
  }

  return (1);
}
