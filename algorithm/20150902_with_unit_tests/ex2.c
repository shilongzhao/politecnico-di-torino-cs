#include <stdio.h>
#include "util.h"
#include "ex2.h"

// auxiliary function: the sum of all diagonal
// & inverse diagonal values at mat[x][y]
int sum_diagonal(int **mat, int N, int x, int y) {
    int sum = 0;
    for (int i = -N; i < N; i++) {
        // diagonal
        if (legal_array_range(i+x, N) && legal_array_range(i+y, N)){
            sum += mat[i+x][i+y];
        }
        //reverse diagonal
        if (legal_array_range(i+x, N) && legal_array_range(-i+y, N)) {
            sum += mat[i+x][-i+y];
        }
    }
    return sum;
}

int max_diag(int **mat, int N) {

    int best_val = 0;
    int best_x = 0;
    int best_y = 0;

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (mat[i][j])
                continue;

            int this_sum = sum_diagonal(mat, N, i, j);

            if (this_sum > best_val) {
                best_val = this_sum;
                best_x = i;
                best_y = j;
            }
        }
    }
    return best_val;
}
