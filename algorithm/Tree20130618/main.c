/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */
//
// exam 2013 7

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 4
#define M 20+1

typedef struct node {
    char *name;
    int n_children;
    int level;
    struct node **children;
} node_t;


node_t * create_node(void);
node_t *search_node_r(char name[M], node_t *head);
void visit (node_t *root,int level,int l);
void readfile(node_t **root,char *name1);
void visitLevelByLevel (node_t *root,int l1,int l2);
void printpath(node_t *root,char *str,char **strbest);

int main(int argc, char *argv[])
{
    node_t *root=NULL;
    char *strbest=NULL;

    if(argc!=2){
        fprintf(stderr,"missing parameters");
        exit(1);
    }
    readfile(&root,argv[1]);
    visitLevelByLevel(root,0,3);
    printpath(root,"",&strbest);
    fprintf (stdout, "max id:\n   %s\n", strbest);

    return 0;
}

void readfile(node_t **root,char *name1){
    node_t *tmp;
    FILE *fp;
    char name[M],child[M];
    int n,i;

    fp=fopen(name1,"r");
    if(fp==NULL){
        fprintf(stderr,"error opening");
        exit(1);
    }
    while (fscanf(fp, "%s %d", name, &n) != EOF){
        if(*root==NULL){
            tmp = *root = create_node();
            tmp->name = strdup(name);
        }else{
            tmp=search_node_r(name,*root);
        }
        tmp->n_children=n;
        tmp->children=(node_t **)malloc(n*sizeof(node_t *));
        if(tmp->children==NULL){
            fprintf(stderr,"error allocating");
            exit(1);
        }
        for (i=0; i<n; i++) {
            fscanf(fp, "%s", child);
            tmp->children[i] = create_node();
            tmp->children[i]->name = strdup(child);
        }
    }
    fclose(fp);
}

node_t * create_node(void)
{
    node_t *q;

    q = (node_t *)malloc(sizeof(node_t));
    if(q==NULL){
        fprintf(stderr,"error allacating");
        exit(1);
    }
    q->n_children = 0;
    q->level = (-1);
    q->children = NULL;

    return q;
}

node_t *search_node_r(char name[M], node_t *head)
{
    node_t *temp = NULL;
    int i;

    if (head != NULL) {
        if (strcmp(name, head->name) == 0) {
            temp = head;
        } else {
            for (i=0; i<head->n_children && temp==NULL; i++) {
                temp = search_node_r(name, head->children[i]);
            }
        }
    }
    return temp;
}

void
visitLevelByLevel (
        node_t *root,
        int l1,
        int l2
)
{
    int i;
    // when i = l1, print all nodes in level l1
    // ....
    // when i = l2 , print all nodes in level l2
    for (i=l1; i<=l2; i++) {
        fprintf (stdout, "Level %d: ", i);
        visit (root, i, 0);
        fprintf (stdout, "\n");
    }

    return;
}

void
visit (
        node_t *root,
        int level,
        int l
)
{
    int i;

    if (root==NULL)
        return;

    if (l == level) {
        fprintf (stdout, "%s ", root->name);
        return;
    }

    for (i=0; i<root->n_children; i++) {

        visit (root->children[i], level, l+1);

    }
    return;
}

void printpath(node_t *root,char *str,char **strbest){
    int i;
    char *tmp;

    if(root==NULL){
        return;
    }

    // in the medium levels, concatenate the current node name to the path string
    tmp=(char *)malloc(((strlen(str))+strlen(root->name))*sizeof(char));
    sprintf(tmp,"%s%s",str,root->name);
    // when reach the last level, compare if the string is longer than the best string
    if(root->children==NULL){
        if(*strbest==NULL||strlen(tmp)>strlen(*strbest)){
            *strbest=strdup(tmp);

        }
    }
    //expansion, for every child do the recursion
    for(i=0;i<root->n_children;i++){
        printpath(root->children[i],tmp,strbest);
    }
    return;


}
