#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define NV 5
#define NH 3

#define DEBUG

int values[NV] = {15, 23, 10, 33, 7};

typedef struct {
	int v;
	int a[NV];
} solution_t;

solution_t *calculate(solution_t *sol) {

	int total[NH];
	for (int i = 0; i < NV; i++) {
		total[sol->a[i]] += values[i];	
	}

	for (int i = 0; i < NH; i++) 
		for (int j = i+1; j < NH; j++) 
			sol->v += abs(total[i] - total[j]);
	return sol;
}

solution_t *min_solution(solution_t **solutions, int n) {
	solution_t *min = solutions[0]; 
	for (int i = 0; i < n; i++) {
			if (solutions[i]->v < min->v) { 
//				solution_t *tmp = min;
				min = solutions[i];
//				free(tmp);
			}
//			else 
//				free(solutions[i]);
	}
	return min;
}

solution_t *assign(solution_t *this, int depth, int heir) {
	
	this->a[depth] = heir;
	if (depth == 5) {
		return calculate(this);	
	}

	solution_t *branches[NH];
	for (int i = 0; i < NH; i++) {
		branches[i] = malloc(sizeof(solution_t));
		branches[i]->v = 0;
		for (int j = 0; j < NV; j++) {
			branches[i]->a[j] = this->a[j];
		}
		branches[i] = assign(branches[i], depth+1, i);
	}
	
	return min_solution(branches, NH);
}

int main() {

	solution_t *sol = malloc(sizeof(solution_t));
	sol = assign(sol, 0, 0);
	printf("%d\n", sol->v);
	for (int i = 0; i < NV; i++)
		printf("%d ",sol->a[i]);
	printf("\n");

#ifdef DEBUG
	sol = assign(sol, 0, 1);
	printf("%d\n", sol->v);
	for (int i = 0; i < NV; i++)
		printf("%d ",sol->a[i]);
	printf("\n");


	sol = assign(sol, 0, 2);
	printf("%d\n", sol->v);
	for (int i = 0; i < NV; i++)
		printf("%d ",sol->a[i]);
	printf("\n");
#endif

}
