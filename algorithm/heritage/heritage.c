/**
* WARNING: this program is incomplete
*/
#include  	<stdio.h>
#include  	<stdlib.h>
#include  	<ctype.h>
#include 	<math.h>

#define DEBUG

typedef struct {
	int n_heirs;
	int vcount;
	int result;
	int *values;
	int assignment[vcount];
} solution_t;

int *read_file(char *fname, int  *values, int *vcount) {
	FILE *fp = fopen(fname, "r");

	fscanf(fp, "%d", vcount);
	values = malloc(*vcount * sizeof(int));
	for (int i = 0; i < *vcount; i++) {
		fscanf(fp, "%*c%d", &values[i]);
	}
#ifdef DEBUG
	fprintf(stdout, "%d heritages: \n", *vcount);
	for (int i = 0; i < *vcount; i++) {
		fprintf(stdout, "%d ", values[i]);
	}
	fprintf(stdout,"\n");
#endif
	return values;
}

/**
* 
* @param	sol		a solution fully assigned, result uncalculated yet
* @return 			sol, with result calculated
*/
solution_t *calculate(solution_t *sol) {
	int total[n_heirs];
	for (int i = 0; i < v_count; i++) {
		total[sol->assignment[i]] += values[i];
	}

	sol->result = 0;
	for (int i = 0; i < n_heirs; i++) 
		for (int j = i+1; j < n_heirs; j++)
			sol->result += abs(total[i] - total[j]);
	
	return sol;
}

/**
* helper function, make a new solution with identical contents as the input solution
*/
solution_t *copy(solution_t *sol) {
	solution_t *tmp = malloc(sizeof(solution_t));
	tmp->n_heirs = sol->n_heirs;
	tmp->vcount = sol->vcount;
	tmp->result = 0;
	tmp->values = sol->values;
	for(int i = 0; i < sol->vcount; i++) {
		tmp->assignment[i] = tmp->assignment[i]
	}
	return tmp;
}

/**
* @param 	curr	the current incomplete solution to be expanded
* @param	heir	to whom the property to be assigned
* @param	prop	the property to allocate
* @return			new solution, with one more known assignment
*/
solution_t *expand(solution_t *curr, int heir, int property) {
	
}
solution_t *assign( int depth, int *values, const int v_count, int n_heirs, int *solution) {
	int results[n_heirs];


	if (depth == v_count) 
		return calculate(values, v_count, heirs, solution);

	for (int i = 0; i < n_heirs; i++) {
		
	}
	int known_min = min(results, n_heirs);
	solution[depth-1] = know_min;
}


int main(int argc, char *argv[]) {
	int *values; 
	const int v_count;
	const int n_heirs;

	n_heirs = atoi(argv[2]);

	values = read_file(argv[1], values, &v_count);
	

#ifdef DEBUG
	printf("%d, %d\n",values[0], values[v_count-1]);
#endif

}
