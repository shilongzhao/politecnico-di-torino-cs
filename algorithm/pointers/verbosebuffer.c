#include <stdio.h>
#include <string.h>

int main(void) {
	char *buf_ptr = "good luck & have fun!";
	
	printf("Address of buf_ptr: %p\n", &buf_ptr);
	printf("Content of buf_ptr: %p\n", buf_ptr);
	printf("Size of buf_ptr: %lu\n", sizeof(buf_ptr));
	printf("Size of buffer which buf_ptr's pointing to: %lu\n", strlen(buf_ptr) + 1);
	printf("Content of buffer which buf_ptr's pointing to: %s\n", buf_ptr);

	return 0;
}
