//
// Created by zhaos on 12/16/2018.
//

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

// strcmp, strcat, strdup, strcpy

int main() {
    char a[10] = "abc";
    char b[10] = "def";

    int r = strcmp(a, b); //ASCII
    printf("%d\n", r);

    char c[10] = "mn";
    char d[10] = "pqr";
    strcat(c, d);
    printf("c = %s, d = %s\n", c, d);

    char e[10] = "xymnpq";
    char f[10] = "abcd";
    strcpy(e, f);
    printf("e = %s, f = %s\n", e, f);

    char *t = NULL;
    printf("value of t = %p\n", t);
    char g[10] = "abcxyz";
    t = strdup(g); // duplicate
    printf("value of t after strdup = %p\n", t);
    printf("t = %s, g = %s\n", t, g);


    // equivalent to strdup
    char *p = NULL;
    char h[10] = "pqrstu";
    p = malloc(sizeof(char) * (1 + strlen(h)));
    strcpy(p, h);
    printf("p = %s, h = %s\n", p, h);
}
