//
// Created by zhaos on 12/16/2018.
//

#include <stdio.h>
#include <stdlib.h>
// WRONG!
//int sum1(int a[2][], int c) {
//
//}

/**
 * can only handle arrays with 3 columns
 * cannot deal with arrays having 2,4,5,6... columns
 */
//int sum2(int a[][3], int r) {
//    return 0;
//}

int sum3(int *a[], int r, int c) {
    int sum = 0;
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            sum += a[i][j];
        }
    }
    return sum;
}

int sum4(int **a, int r, int c) {
    int sum = 0;
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            sum += a[i][j];
        }
    }
    return sum;
}

// WRONG! array in memory
//int sum3(int a[][], int r, int c) {
//    return 0;
//}

int main() {
    int a[2][3] = {{1,2,3}, {4,5,6}};

    int *b[2];
    for (int i = 0; i < 2; i++) {
        b[i] = malloc(sizeof(int) * 3);
    }

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 3; j++) {
            b[i][j] = a[i][j];
        }
    }
    int r = sum3(b, 2, 3);
     printf("r = %d\n", r);
}