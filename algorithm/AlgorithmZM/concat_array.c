//
// Created by zhaos on 12/16/2018.
//

#include <stdlib.h>
#include <stdio.h>
/**
 * Given array a, size na and array b, size nb
 * return a new array with (na + nb) elements
 * a = {1,2,3}, na = 3;
 * b = {4,5,6,7}, nb = 4;
 *
 * return an array of {1,2,3,4,5,6,7}
 *
 * hints: malloc
 * @param a
 * @param na
 * @param b
 * @param nb
 * @return the initial address of allocated memory
 */
int *concat(int *a, int na, int *b, int nb) {
    int *r = malloc(sizeof(int) * (na + nb));
    for (int i = 0; i < na; i++) {
        r[i] = a[i];
    }

    for (int i = na, j = 0; j < nb; j++, i++) {
        r[i] = b[j];
    }
    return r;
}

int main() {
    int a[3] = {1,2,3};
    int b[4] = {5,6,7,8};
    int *r = concat(a, 3, b, 4);
    
    
    printf("address a = %p, address b = %p, address r = %p\n", a, b, r);
    
    for (int i = 0; i < 7; i++) {
        printf("%d\n", r[i]);
    }
}