//
// Created by zhaos on 2/10/2019.
//

#include <stdio.h>
#include <string.h>
void combination(char *s, char *r, int d, int n, int lastPick) {
    if (d == n) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }

    for (int i = lastPick + 1; i < strlen(s); i++) {
        r[d] = s[i];
        combination(s, r, d + 1, n, i);
        r[d] = '\0';
    }
}

int main() {
    char s[6] = "ABCDE";
    char r[4] = "";
    combination(s, r, 0, 3, -1);
}