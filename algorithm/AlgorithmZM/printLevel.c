//
// Created by zhaos on 1/24/2019.
//
#define N 100
#include <stdio.h>
#include <stdlib.h>
typedef struct element node_t;
struct element {
    int v;
    element *child[N];
};
void printLevel(element *root, int x, int l) {
    if (root == NULL) {
        return;
    }
    if (x == l) {
        printf("%d ", root->v);
        return;
    }
    for (int i = 0; i < N; i++) {
        printLevel(root->child[i], x + 1, l);
    }
}

void printLevelByLevel(element *root, int l1, int l2) {
    for (int i = l1; i <= l2; i++) {
        printLevel(root, 0, i);
    }
}

int main() {

}