//
// Created by zhaos on 12/13/2018.
//

#include <stdio.h>
#include <stdlib.h>



int main() {
    float q[100] = {3,2,1,7,8,9};
    printf("sizeof float = %d\n", sizeof(float));
    printf("q = %p\n", q);
    printf("q[0] address = %p\n", &q[0]);
    printf("q[1] address = %p\n", &q[1]);
    printf("q + 1 = %p\n", q + 1);
//
//    printf("q[0] = %f\n", *q);
//    printf("q[1] = %f\n", *(q + 1));
//    printf("q[2] = %f\n", *(q + 2));

    for (int i = 0; i < 10; i++) {
        printf("%f\n", *(q + i)); // q[i]
    }

    float *p = malloc(sizeof(float) * 10);
    for (int i = 0; i < 10; i++) {
        *(p + i) = i; // p[i] = i;
    }
}