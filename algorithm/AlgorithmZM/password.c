//
// Created by zhaos on 2/10/2019.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char letters[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
char digits[11] = "0123456789";

void gen_repeat_m( char *r, int d, int *used, int m) {
    if (d == 5) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }

    if (d < 3) {
        for (int i = 0; i < 26; i++) {
            if (used[i] < m) {
                r[d] = letters[i];
                used[i] = used[i] + 1;
                gen_repeat_m(r, d + 1, used, m);
                used[i] = used[i] - 1;
                r[d] = '\0';
            }
        }
    }
    else {
        for (int i = 0; i < 10; i++) {
            if (used[i + 25] < m) {
                r[d] = digits[i];
                used[i+25] = used[i+25] + 1;
                gen_repeat_m(r, d+1, used, m);
                used[i+25] = used[i+25] - 1;
                r[d] = '\0';
            }
        }
    }
}

int main() {
    char result[6] = "";
    int used[36] = {};
    gen_repeat_m(result, 0, used, 2);
}
