//
// Created by zhaos on 1/13/2019.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct element node_t;
struct element {
    int w;
    element *next;
};

void orderInsert(element **h, int v) {
    element *t = malloc(sizeof(element));
    t->w = v;
    t->next = NULL;
    if (*h == NULL) {
        *h = t;
        return;
    }

    if (t->w < (*h)->w) {
        t->next = *h;
        *h = t;
        return;
    }

    element *p;
    for (p = *h; p->next != NULL; p = p->next) {
        if (t->w < p->next->w) {
            t->next = p->next;
            p->next = t;
            break;
        }
    }

    if (p->next == NULL) {
        p->next = t;
    }
    return;
}

void print_list_r(element *h) {
    if (h == NULL) return;
    printf("%d\n", h->w);
    print_list_r(h->next);
}


int main() {
    element *h = NULL;
    orderInsert(&h, 5);
    orderInsert(&h, 8);
    orderInsert(&h, 6);
    orderInsert(&h, 4);
    orderInsert(&h, 2);
    orderInsert(&h, 10);
    orderInsert(&h, 13);
    print_list_r(h);
}