//
// Created by zhaos on 2/8/2019.
//
#include <stdio.h>
#include <string.h>
void gen(char *s, char *r, int d, int N, int *used) {
    if (d == N) {
        r[d] = '\0';
        printf("%s\n", r);
//        printf("used: ");
//        for (int i = 0; i < strlen(s); i++) {
//            printf("%d ", used[i]);
//        }
//        printf("\n");
        return;
    }

    for (int i = 0; i <strlen(s); i++) {
        if (used[i] == 0) {
            used[i] = 1;
            r[d] = s[i];
            gen(s, r, d + 1, N, used);
            r[d] = '\0';
            used[i] = 0;
        }
    }
}

/**
 *
 * @param s  candidate chars
 * @param r  result
 * @param d  depth, (current number of chars in result)
 * @param N  target string length
 * @param used  used times
 * @param m  every char can repeat m times
 */
void gen_repeat_m(char *s, char *r, int d, int N, int *used, int m) {
    if (d == N) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }

    for (int i = 0; i < strlen(s); i++) {
        if (used[i] < m) {
            used[i] = used[i] + 1;
            r[d] = s[i];
            gen_repeat_m(s, r, d + 1, N, used, m);
            r[d] = '\0';
            used[i] = used[i] - 1;
        }
    }
}

int main() {
//    char s[5] = "ABCD";
//    char r[4] = "";
//    int used[4] = {};
//    gen(s, r, 0, 3, used);

    char s1[6] = "ABCDE";
    char r1[6] = "";
    int used1[5] = {};
    gen_repeat_m(s1, r1, 0, 5, used1, 2);
}