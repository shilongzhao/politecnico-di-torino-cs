//
// Created by zhaos on 2/12/2019.
//

#include <stdio.h>
#include <stdlib.h>

int covers_all(int m[][9], int n, int *r, int k) {
    int covered[9] = {};
    for (int i = 0; i < k; i++) {
        int row = r[i];
        for (int j = 0; m[row][j] != 0; j++) {
            int x = m[row][j];
            covered[x] = 1;
        }
    }

    for (int i = 1; i < 9; i++) {
        if (covered[i] == 0) {
            return 0;
        }
    }

    return 1;
}

void combo(int m[][9], int *r, int depth, int k, int lastPick, int n) {
    if (depth == k) {
       if (covers_all(m, n, r, k)) {
           for (int i = 0; i < k; i++) {
               printf("%d ", r[i]);
           }
           printf("\n");
           return;
       }
    }

    for (int i = lastPick + 1; i < n; i++) {
        r[depth] = i;
        combo(m, r, depth + 1, k, i, n);
    }
}

void cover(int m[][9], int n, int k) {
    int *r = malloc(sizeof(int) * n);
    combo(m, r, 0, k, -1, n);
    free(r);
}
int main() {
    int m[5][9] = {
            {1,2,3},
            {2,3,7,8},
            {7,8},
            {3,4},
            {4,5,6}
    };
    cover(m, 5, 3);
}