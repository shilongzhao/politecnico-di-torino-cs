//
// Created by zhaos on 1/15/2019.
//
#include <stdlib.h>
#include <stdio.h>
typedef struct element node_t;
struct element {
    int k;
    element *prev;
    element *next;
};
void listInsert(element **left, element **right, int key, int leftRight) {
    element *t = malloc(sizeof(element));
    t->k = key;
    t->prev = NULL;
    t->next = NULL;
    if (*left == NULL && *right == NULL) {
        *left = t;
        *right = t;
        return;
    }
    if (leftRight == 0) {
        t->next = *left;
        (*left)->prev = t;
        *left = t;
    }
    else {
        t->prev = *right;
        (*right)->next = t;
        *right = t;
    }
}

void listDisplay(element *left, element *right, int leftRight) {
    if (leftRight == 0) {
        for (element *p = left; p != NULL; p = p->next) {
            printf("%d ", p->k);
        }
        printf("\n");
    }
    else {
        for (element *p = right; p != NULL; p = p->prev) {
            printf("%d ", p->k);
        }
        printf("\n");
    }
}

int main() {
    element *l = NULL;
    element *r = NULL;
    listInsert(&l, &r, 0, 0);
    listInsert(&l, &r, 1, 0);
    listInsert(&l, &r, 2, 0);
    listInsert(&l, &r, 3, 0);
    listInsert(&l, &r, 4, 0);
    listInsert(&l, &r, 5, 1);
    listInsert(&l, &r, 6, 1);
    listInsert(&l, &r, 7, 1);
    listInsert(&l, &r, 8, 1);
    listInsert(&l, &r, 9, 1);

    listDisplay(l, r, 0);
}


