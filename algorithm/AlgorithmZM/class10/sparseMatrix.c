//
// Created by zhaos on 1/15/2019.
//
#include <stdio.h>
#include <stdlib.h>

typedef struct cell_t cell_t;
typedef struct row_t row_t;
typedef struct mat_t mat_t;

struct cell_t {
    int c;
    double val;
    cell_t *next;
};

struct row_t {
    int r;
    cell_t *right;
    row_t *next;
};

struct mat_t {
    int row;
    int column;
    row_t *firstRow;
};

row_t *findRow(row_t *head, int r) {
    for (row_t *p = head; p != NULL; p = p->next) {
        if (p->r == r) {
            return p;
        }
    }
    return NULL;
}

cell_t *findCol(cell_t *head, int c) {
    for (cell_t *p = head; p != NULL; p = p->next) {
        if (p->c == c) {
            return p;
        }
    }
    return NULL;
}

void matWrite(mat_t *M, float value, int r, int c) {
    row_t *pr = findRow(M->firstRow, r);
    if (pr != NULL) {
        // row r exists
        cell_t *pc = findCol(pr->right, c);
        if (pc != NULL) {
            // col c exists
            pc->val = value;
        }
        else {
            // col c not exists
            cell_t *tc = malloc(sizeof(cell_t));
            tc->c = c;
            tc->val = value;
            tc->next = pr->right;
            pr->right = tc;
        }
    }
    else {
        cell_t *tc = malloc(sizeof(cell_t));
        tc->c = c;
        tc->val = value;
        tc->next = NULL;

        row_t *tr = malloc(sizeof(row_t));
        tr->r = r;
        tr->right = tc;
        tr->next = M->firstRow;
        M->firstRow = tr;
    }
}