//
// Created by zhaos on 1/20/2019.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct element node_t;
struct element {
    int w;
    element *left;
    element *right;
};

void preorder(element *root) {
    if (root == NULL) {
        return;
    }
    printf("%d ", root->w);
    preorder(root->left);
    preorder(root->right);
}

void inorder(element *root) {
    if (root == NULL) {
        return;
    }
    inorder(root->left);
    printf("%d ", root->w);
    inorder(root->right);
}

void postorder(element *root) {
    if (root == NULL) {
        return;
    }
    postorder(root->left);
    postorder(root->right);
    printf("%d ", root->w);
}

void insert(element **root, int v) {
    if (*root == NULL) {
        element *t = malloc(sizeof(element));
        t->w = v;
        t->left = NULL;
        t->right = NULL;
        *root = t;
        return;
    }
    element *p = *root;
    if (v < p->w) {
        insert(&(p->left), v);
    }
    else if (v > p->w) {
        insert(&(p->right), v);
    }
}

void preorderWithDepth(element *root, int x) {
    if (root == NULL) {
        return;
    }
    printf("%d - %d\n", root->w, x);
    preorderWithDepth(root->left, x + 1);
    preorderWithDepth(root->right, x + 1);
}


void printLevelHelper(element *root, int x, int l) {
    if (root == NULL) {
        return;
    }
    if (x == l) {
        printf("%d - %d\n", root->w, x);
        return;
    }
    printLevelHelper(root->left, x + 1, l);
    printLevelHelper(root->right, x + 1, l);
}


void printLevel(element *root, int l) {
    printLevelHelper(root, 0, l);
}


int main() {
    element *r = NULL;
    insert(&r, 5);
    insert(&r, 2);
    insert(&r, 4);
    insert(&r, 8);
    insert(&r, 6);
    insert(&r, 1);
    insert(&r, 9);

    inorder(r);

    printf("\n===========\n");
    preorderWithDepth(r, 0);
    printf("\n===========\n");
    printLevel(r, 1);
}