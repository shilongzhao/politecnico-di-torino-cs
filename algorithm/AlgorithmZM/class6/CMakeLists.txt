cmake_minimum_required(VERSION 3.13)
project(AlgorithmZM C)

set(CMAKE_C_STANDARD 99)

add_executable(20160614T1 20160614T1.c)

add_executable(student student.c)