//
// Created by zhaos on 12/21/2018.
//
#include <stdio.h>

typedef struct student_t student_t;
struct student_t {
    int id;
    char *name;
    char *surname;
    int year;
    int month;
    int day;
};

int main() {
    int a = 0;
    student_t s1 = {
            .id = 1,
            .name = "Alex",
            .surname = "Hello",
            .year = 1998,
            .month = 12,
            .day = 20
    };

    student_t s2 = {
            .id = 2,
            .name = "bob",
            .surname = "Caaa",
            .year = 1990,
            .month = 11,
            .day = 11
    };

    printf("%d \n", s1.id);
    printf("%s \n", s1.name);

    printf("%s \n", s2.surname);
}