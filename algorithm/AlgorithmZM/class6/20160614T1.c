//
// Created by zhaos on 12/21/2018.
//

#include <stdio.h>
#include <stdlib.h>

int isMax(int **m, int r, int c, int i, int j);

void matMax(int **m, int r, int c) {
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            if (isMax(m, r, c, i, j)) {
                printf("(%d, %d)\n", i, j);
            }
        }
    }
}

int isMax(int **m, int r, int c, int i, int j) {
    for (int p = i - 1; p <= i + 1; p++) {
        for (int q = j -1; q <= j + 1; q++) {
            if (p >= 0 && p < r && q >= 0 && q < c) {
                if (p == i && q == j) {
                    continue;
                }
                else {
                    if (m[p][q] >= m[i][j]) {
                        return 0;
                    }
                }
            }
        }
    }
    return 1;
}

int main() {
    int a[3][4] = {
            {5,2,3,1},
            {3,1,3,4},
            {4,0,5,2}
    };

    int *b[3];
    for (int i = 0; i < 3; i++) {
        b[i] = malloc(sizeof(int) * 4);
    }

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            b[i][j] = a[i][j];
        }
    }

    matMax(b, 3, 4);
}