//
// Created by zhaos on 2/20/2019.
//
#include <stdio.h>
#include <stdlib.h>

int in_range(int *v, int from, int to, int c1, int c2) {
    int sum = 0;
    for (int i = from; i <= to; i++) {
        sum += v[i];
    }
    if (sum >= c1 && sum <= c2) {
        return 1;
    }
    else {
        return 0;
    }
}

void print_sub(int *v, int from, int to) {
    for (int i = from; i <= to; i++) {
        printf("%d ", v[i]);
    }
    printf("\n");
}
void range_sum(int *v, int n, int c1, int c2) {
    for (int i = 0; i < n; i++) {
        for (int j = i; j < n; j++) {
            if (in_range(v, i, j, c1, c2)) {
                print_sub(v, i, j);
            }
        }
    }
}
// --------------------------------------------- //
#define N 5
typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *child[N];
};
int max_height(node_t *root) {
    if (root == NULL) {
        return 0;
    }

    int max = 0;
    for (int i = 0; i < N; i++) {
        if (max_height(root->child[i]) > max) {
            max = max_height(root->child[i]);
        }
    }
    return max + 1;
}

void iterate(node_t *root, int depth, int *count) {
    if (root == NULL) {
        return;
    }
    count[depth] += 1;
    for (int i = 0; i < N; i++) {
        iterate(root->child[i], depth + 1, count);
    }
}

void tree_max_level(node_t *root) {
    int h = max_height(root);
    printf("tree height is %d\n", h);
    int *count = malloc(sizeof(int) * h);
    for (int i = 0; i < h; i++) {
        count[i] = 0;
    }
    iterate(root, 0, count);

    int level = -1, max_count = 0;
    for (int i = 0; i < h; i++) {
        printf("level %d has %d nodes\n", i, count[i]);
        if (count[i] > max_count) {
            level = i;
            max_count = count[i];
        }
    }
    printf("level %d has max number of nodes %d\n", level, max_count);
}

int apply_op(int *v, int n, char *s) {
    int result = v[0];
    for (int i = 1; i < n; i++) {
        if (s[i - 1] == '+') {
            result += v[i];
        }
        else if (s[i - 1] == '-') {
            result -= v[i];
        }
        else {
            result = result * v[i];
        }
    }
    return result;
}
void print_exp(int *v, int n, char *s) {
    printf("%d ", v[0]);
    for (int i = 1; i < n; i++) {
        printf("%c ", s[i - 1]);
        printf("%d ", v[i]);
    }
    printf("\n");
}
void gen(int *v, int n, int result, char *r, int nr) {
    if (nr == n - 1){
        r[nr] = '\0';
        if (apply_op(v, n, r) == result) {
            print_exp(v, n, r);
        }
        return;
    }

    char s[4] = "+-*";
    for (int i = 0; i < 3; i++) {
        r[nr] = s[i];
        gen(v, n, result, r, nr + 1);
    }
}

void calculator(int *v, int n, int result) {
    char *r = malloc(sizeof(char) * n);
    gen(v, n, result, r, 0);
}

int main() {
    int v[10] = {0, 5, 10, 15, 20, 25, 30, 35, 40, 50};
    range_sum(v, 10, 20, 30);

    node_t n1 = {1, {}};
    node_t n2 = {2, {}};
    node_t n3 = {3, {}};
    node_t n4 = {4, {}};
    node_t n5 = {5, {}};
    node_t n6 = {6, {}};
    node_t n7 = {7, {}};
    node_t n8 = {8, {}};
    node_t n9 = {9, {}};

    n1.child[0] = &n2;
    n1.child[1] = &n3;
    n1.child[2] = &n4;
    n1.child[4] = &n5;

    n2.child[1] = &n6;
    n6.child[2] = &n9;

    n3.child[4] = &n7;
    n5.child[3] = &n8;

    tree_max_level(&n1);

    int v1[5] = {55, 3, 45, 33, 25};
    calculator(v1, 5, 404);
}