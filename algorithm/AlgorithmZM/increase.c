//
// Created by zhaos on 12/13/2018.
//
#include <stdio.h>

void increase1(int a) {
    a = a + 1;
    printf("increase1 a = %d, address of a is = %p\n", a, &a);
}

void increase2(int *pa) {
    *pa = *pa + 1;
    printf("increase2 pa = %p, *pa = %d\n", pa, *pa);
}

int main() {
    int a = 1;
    increase1(a);
    printf("main a = %d, address a = %p\n", a, &a); // a = 1

    increase2(&a); // int *pa = &a;
    printf("main a = %d, address a = %p\n", a, &a); // a = 2
}