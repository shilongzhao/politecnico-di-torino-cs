//
// Created by zhaos on 12/13/2018.
//

#include <stdio.h>

void swap(int *a, int *b) {
    int c = *a; // 1
    *a = *b; // rvalue, lvalue
    *b = c;
}

int main() {
    int a = 1, b = 2;

    swap(&a, &b);

    printf("a = %d, b = %d\n", a, b);
}