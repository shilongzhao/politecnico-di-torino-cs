//
// Created by zhaos on 1/4/2019.
//

#include <stdio.h>
#include <stdlib.h>
typedef struct element node_t;
struct element {
    int k;
    element *next;
};

element *insert(element *h, int v) {
    element *t = malloc(sizeof(element));
    t->k = v;
    t->next = h;
    return t;
}

void insert2(element **h, int v) {
    element *t = malloc(sizeof(element));
    t->k = v;
    t->next = *h;
    *h = t;
}

void print_list_r(element *h) {
    if (h == NULL) return;
    printf("%d\n", h->k);
    print_list_r(h->next);
}

int main() {
    element *list = NULL;
    
    insert2(&list, 1);
    insert2(&list, 2);
    insert2(&list, 3);
    
    print_list_r(list);
}