#include <stdio.h>


int max3(int a, int b, int c) {
    if (a > b && a > c) {
        return a;
    }
    else if (b > a && b > c) {
        return b;
    }
    else {
        return c;
    }
}

/**
 * *****
 * ****
 * ***
 * **
 * *
 * @param s
 */
void print_reversed_triangle(int s) {
    for (int i = 0; i < s; i++) {
        for (int j = 0; j < s - i; j++) {
            printf("*");
        }
        printf("\n");
    }
}

int main() {
    print_reversed_triangle(3);

    print_reversed_triangle(5);

    print_reversed_triangle(10);

    return 0;
}