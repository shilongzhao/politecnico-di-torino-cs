//
// Created by zhaos on 12/16/2018.
//
#include <limits.h>
int sum(int **mat, int i, int j, int n) {
    int s = 0;
    for (int p = i; p < i + n; p++) {
        for (int q = j; q < j + n; q++) {
            s = s + mat[p][q];
        }
    }
    return s;
}
int subMatMax(int **mat, int r, int c, int n) {
    int max = INT_MIN;
    for (int i = 0; i <= r - n; i++) {
        for (int j = 0; j <= c - n; j++) {
            int s = sum(mat, i, j, n);
            if (s > max) {
                max = s;
            }
        }
    }
    return max;
}