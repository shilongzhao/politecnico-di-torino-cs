cmake_minimum_required(VERSION 3.13)
project(AlgorithmZM C)

set(CMAKE_C_STANDARD 99)

add_executable(max_submatrix max_submatrix.c)