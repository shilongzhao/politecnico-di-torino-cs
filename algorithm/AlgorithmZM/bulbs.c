//
// Created by zhaos on 2/10/2019.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int op_ok(int **m, int ns, int nb, int *r, int d) {
    int *bulbs = malloc(sizeof(int) * nb);
    memset(bulbs, 0, sizeof(int) * nb);

    for (int i = 0; i < d; i++) {
        int s = r[i];
        for (int j = 0; j < nb; j++) {
            bulbs[j] = bulbs[j] ^ m[s][j];
        }
    }

    for (int i = 0; i < nb; i++) {
        if (bulbs[i] == 0) {
            return 0;
        }
    }
    return 1;
}
/**
 *
 * @param m  control matrix
 * @param ns num switches, m rows
 * @param nb num bulbs, m columns
 * @param r  switches selected
 * @param d  number of switches selected, size of r
 */
void combo(int **m, int ns, int nb, int *r, int d, int lastPick) {
    if (op_ok(m, ns, nb, r, d) == 1) {
        for (int i = 0; i < d; i++) {
            printf("%d ", r[i]);
        }
        printf("\n");
        return;
    }

    for (int i = lastPick + 1; i < ns; i++) {
        r[d] = i;
        combo(m, ns, nb, r, d + 1, i);
    }
}

int main() {
    int s0[5] = {1,1,0,0,1};
    int s1[5] = {1,0,1,0,1};
    int s2[5] = {0,1,1,1,0};
    int s3[5] = {1,0,0,1,0};
    int *m[4] = {s0, s1, s2, s3};
    int r[4] = {};

    combo(m, 4, 5, r, 0, -1);
}