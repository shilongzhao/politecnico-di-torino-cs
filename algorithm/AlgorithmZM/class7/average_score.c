//
// Created by zhaos on 12/22/2018.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct student_t student_t;
struct student_t {
    int id;
    char *name;
    double score;
};
double average_grade1(student_t *students, int n) { //double average_grade1(student_t students[], int n) {
    double sum = 0;
    for (int i = 0; i < n; i++) {
        sum = sum + students[i].score;
    }
    return sum/n;
}
double average_grade2(student_t **students, int n) { //double average_grade2(student_t *students[], int n) {
    double sum = 0;
    for (int i = 0; i < n; i++) {
        sum = sum + students[i]->score;
    }
    return sum/n;
}

int main() {
    student_t students1[10]; // array of structs, 160B
    student_t *students2[10]; // array of pointer, 40B

    printf("sizeof student = %d\n", sizeof(student_t));
    printf("sizeof student* = %d\n", sizeof(student_t *));


    students1[0].id = 1;
    students1[0].name = strdup("Alex");
    students1[0].score = 22;

    students2[0] = malloc(sizeof(student_t));
    students2[0]->id = 2;
    students2[0]->name = strdup("Bob");
    students2[0]->score = 23;

//    student_t *ps = malloc(sizeof(student_t));
////    (*ps).id = 1;
////    (*ps).name = strdup("Bob");
//    ps->id = 1;
//    ps->name = strdup("Bob");
}