//
// Created by zhaos on 12/22/2018.
//
#include <stdio.h>
#include <stdlib.h>
typedef struct element node_t;
struct element {
    int k;
    element *next;
};


void print_list(element *head) {
//    for ( ;head != NULL ; head = head->next ) {
//        printf("%d\n", head->k);
//    }
    for (element *t = head; t != NULL; t = t->next) {
        printf("%d\n", t->k);
    }
}

void print_list_r(element *h) {
    if (h == NULL) return;
    printf("%d\n", h->k);
    print_list_r(h->next);
}

/**
 * TODO: print list h in reverse order recursively
 * @param h
 */
void printListReverseRecursive(element *h) {
    if (h == NULL) return;
    printListReverseRecursive(h->next);
    printf("%d\n", h->k);
}

int main() {
    element t1 = {.k = 1, .next = NULL};

    element t2 = {.k = 2, .next = NULL};

    element t3 = {.k = 3, .next = NULL};

    t1.next = &t2;
    t2.next = &t3;
    t3.next = NULL;

    print_list_r(&t1);

    printf("=========\n");
    printListReverseRecursive(&t1);
}