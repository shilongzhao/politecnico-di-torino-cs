//
// Created by zhaos on 1/30/2019.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct element element;

struct element {
    char *s;
    int numChild;
    element *parent;
    element *sibling;
    element *child;
};

void processTree(struct element *root, struct element *p) {
    if (root == NULL) {
        return;
    }

    root->parent = p;

    if (p != NULL) {
        p->numChild = p->numChild + 1;
    }

    processTree(root->sibling, p);
    processTree(root->child, root);
}

void print_tree(element *root) {
    if (root == NULL) {
        return;
    }
    if (root->parent == NULL) {
        printf("%s: num_child = %d, parent = %s\n", root->s, root->numChild, "NULL");
    }
    else {
        printf("%s: num_child = %d, parent = %s\n", root->s, root->numChild, root->parent->s);
    }
    print_tree(root->sibling);
    print_tree(root->child);
}

int main() {
    element a = {"a", 0, NULL, NULL, NULL};
    element b = {"b", 0, NULL, NULL, NULL};
    element c = {"c", 0, NULL, NULL, NULL};
    element d = {"d", 0, NULL, NULL, NULL};
    element e = {"e", 0, NULL, NULL, NULL};
    element f = {"f", 0, NULL, NULL, NULL};
    element g = {"g", 0, NULL, NULL, NULL};

    a.child = &b;

    b.sibling = &c;
    c.sibling = &d;

    b.child = &e;
    e.sibling = &f;

    e.child = &g;

    processTree(&a, NULL);

    print_tree(&a);

}