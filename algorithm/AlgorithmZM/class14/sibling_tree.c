//
// Created by zhaos on 1/30/2019.
// exam 20140903

#include <stdlib.h>
#include <stdio.h>

typedef struct element node_t;

struct element {
    char *s;
    element *child;
    element *sibling;
};

void print_tree(element *root) {
    if (root == NULL) {
        return;
    }
    printf("%s\n", root->s);

    print_tree(root->child);
    print_tree(root->sibling);
}

