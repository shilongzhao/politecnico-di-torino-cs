//
// Created by zhaos on 1/27/2019.
//

#include <stdio.h>

typedef struct element node_t;

struct element {
    int k;
    element *left;
    element *right;
};

/**
 * find the lowest common ancestor of p and q, p < q
 * @param root  root tree
 * @param p
 * @param q
 * @return  lowest common ancestor
 */
element *lca(element *root, int p, int q) {
    if (root->k <= q && root->k >= p) {
        return root;
    }
    else if (root->k < p) {
        return lca(root->right, p, q);
    }
    else {
        return lca(root->left, p, q);
    }
}
/**
 * distance from root to k
 * @param root
 * @param k
 * @return
 */
int dist(element *root, int k) {
    if (k == root->k) {
        return 0;
    }
    else if (k < root->k) {
        return 1 + dist(root->left, k);
    }
    else {
        return 1 + dist(root->right, k);
    }
}
/**
 * 20160909
 * @param root
 * @param k1
 * @param k2
 * @return
 */
int distance(element *root, int k1, int k2) {
    element *l = lca(root, k1, k2);
    int d1 = dist(l, k1);
    int d2 = dist(l, k2);
    return d1 + d2;
}