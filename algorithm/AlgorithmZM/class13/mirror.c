//
// Created by zhaos on 1/27/2019.
//

#include <stdlib.h>
struct node {
    int k;
    struct node *left;
    struct node *right;
};

struct node *treeMirror(struct node *root) {
    if (root == NULL) {
        return root;
    }
    struct node *tmp = root->left;
    root->left = root->right;
    root->right = tmp;

    treeMirror(root->left);
    treeMirror(root->right);

    return root;
}