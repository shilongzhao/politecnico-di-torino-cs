//
// Created by zhaos on 1/27/2019.
//

#include <stdlib.h>
#include <string.h>
struct element {
    char *str;
    struct element *left;
    struct element *right;
};
int treeIsomorph(struct element *t1, struct element *t2) {
    if (t1 == NULL && t2 == NULL) {
        return 1;
    }
    else if (t1 == NULL && t2 != NULL) {
        return 0;
    }
    else if (t1 != NULL && t2 == NULL){
        return 0;
    }
    else {
        return strcmp(t1->str, t2->str) == 0 &&
                treeIsomorph(t1->left, t2->left) &&
                treeIsomorph(t1->right, t2->right);
    }
}