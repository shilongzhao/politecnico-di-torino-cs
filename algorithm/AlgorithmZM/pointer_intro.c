//
// Created by zhaos on 12/8/2018.
//
#include <stdio.h>
int main() {
    int x = 5;
    printf("the value of x is %d, address of x is %p\n", x, &x);
    char c = 'm';
    printf("the value of c is %c, address of x is %p\n", c, &c);

    int *pi;
    printf("size of pi is %d\n", sizeof(pi));
    pi = &x;
    printf("value of pi is %p\n, address of pi is %p\n", pi, &pi);

    int **ppi = &pi;
    printf("value of ppi is %p, address of ppi is %p\n", ppi, &ppi);

    printf("value of pi is %p\n", *ppi); // dereference
    printf("value of x is %d\n", *pi); // dereference
}
