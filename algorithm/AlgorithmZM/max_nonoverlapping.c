//
// Created by zhaos on 2/15/2019.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int overlap(int **m, int i, int j) {
    if (m[i][1] <= m[j][0] || m[j][1] <= m[i][0]) {
        return 0;
    }
    else {
        return 1;
    }
}

int overlap_array(int **m, int *r, int rs, int i) {
    for (int k = 0; k < rs; k++) {
        if ( overlap(m, i, r[k]) == 1) {
            return 1;
        }
    }
    return 0;
}

int intervals_len(int **m, int *r, int rs) {
    int len = 0;
    for (int i = 0; i < rs; i++) {
        len = len + (m[r[i]][1] - m[r[i]][0]);
    }
    return len;
}


void select(int **m, int n, int *r, int rs, int *max, int *v, int lastPick) {
    if (intervals_len(m, r, rs) > *max) {
        *max = intervals_len(m, r, rs);
        memset(v, 0, sizeof(int) * n);
        for (int i = 0; i < rs; i++) {
            v[r[i]] = 1;
        }
    }
    for (int i = lastPick + 1; i < n; i++) {
        if (overlap_array(m, r, rs, i) == 0) {
            r[rs] = i;
            select(m, n, r, rs + 1, max, v, i);
        }
    }
}
void activity_selection(int **m, int  *v, int n) {
    int max = 0;
    int *r = malloc(sizeof(int) * n);
    memset(r, 0, sizeof(int) * n);
    select(m, n, r, 0, &max, v, -1);
    printf("max intervals length = %d\n", max);
    for (int i = 0; i < n; i++) {
        printf("%d ", v[i]);
    }
    printf("\n");
}
int main() {
    int m0[2] = {1,4};
    int m1[2] = {2,3};
    int m2[2] = {4,6};
    int m3[2] = {5,9};
    int m4[2] = {8,9};
    int *m[5] = {m0, m1, m2, m3, m4};

    int v[5] = {};
    activity_selection(m, v, 5);
}