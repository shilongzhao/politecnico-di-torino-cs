//
// Created by zhaos on 2/2/2019.
//

#include <stdio.h>
#include <string.h>
/**
 *
 * @param s  candidate chars
 * @param r  result
 * @param depth  result length
 * @param N  target length
 */
void generate(char *s, char *r, int depth, int N) {
    if (depth == N) {
        r[depth] = '\0';
        printf("%s\n", r);
        return;
    }

    for (int i = 0; i < strlen(s); i++) {
        r[depth] = s[i];
        generate(s, r, depth + 1, N);
    }
}
/**
 *
 * @param v  array
 * @param n  size of array
 * @param r  state
 * @param depth
 * @param N  target length
 */
void gen(int *v, int n, int *r, int depth, int N) {
    if (depth == N) {
        for (int i = 0; i < N; i++) {
            printf("%d ", r[i]);
        }
        printf("\n");
        return;
    }

    for (int i = 0; i < n; i++) {
        r[depth] = v[i];
        gen(v, n, r, depth + 1, N);
    }
}

int main() {
    char s[5] = "abcd";
    char r[4] = "";
    generate(s, r, 0, 3);

    int a[6] = {0,1,2,3,4,5};
    int b[4] = {};
    gen(a, 6, b, 0, 4);
}