//
// Created by zhaos on 2/2/2019.
//
#include <stdio.h>
#include <stdlib.h>

typedef struct node_t node_t;

struct node_t {
    int k;
    node_t *left;
    node_t *right;
    int heightLeft;
    int heightRight;
};

int max(int a, int b) {
    if (a > b) {
        return a;
    }
    else {
        return b;
    }
}
int height(node_t *root) {
    if (root->left == NULL) {
        root->heightLeft = 0;
    }
    else {
        root->heightLeft = height(root->left) + 1;
    }

    if (root->right == NULL) {
        root->heightRight = 0;
    }
    else {
        root->heightRight = height(root->right) + 1;
    }
    return max(root->heightLeft, root->heightRight);
}

void max_diameter(node_t *root, int *max) {
    if (root == NULL) {
        return;
    }
    if (*max < root->heightLeft + root->heightRight) {
        *max = root->heightLeft + root->heightRight;
    }
    max_diameter(root->left, max);
    max_diameter(root->right, max);
}

void print_tree(node_t *root) {
    if (root == NULL) return;
    printf("%d: left = %d, right = %d\n", root->k, root->heightLeft, root->heightRight);
    print_tree(root->left);
    print_tree(root->right);
}

int tree_diameter(node_t *r) {
    height(r);
    int max = 0;
    max_diameter(r, &max);
    return max;
}

int main() {
    node_t n1 = {1, NULL, NULL, 0, 0};
    node_t n2 = {2, NULL, NULL, 0, 0};
    node_t n3 = {3, NULL, NULL, 0, 0};
    node_t n4 = {4, NULL, NULL, 0, 0};
    node_t n5 = {5, NULL, NULL, 0, 0};
    node_t n6 = {6, NULL, NULL, 0, 0};
    node_t n7 = {7, NULL, NULL, 0, 0};
    node_t n8 = {8, NULL, NULL, 0, 0};
    node_t n9 = {9, NULL, NULL, 0, 0};
    node_t n10 = {10, NULL, NULL, 0, 0};

    n1.left = &n2, n1.right = &n3;
    n2.left = &n4, n2.right = &n5;
    n4.left = &n6;
    n5.left = &n8;

    n6.left = &n7, n6.right = &n9;

    n8.left = &n10;

    int r = tree_diameter(&n1);
    printf("diameter = %d\n", r);
    print_tree(&n1);
}