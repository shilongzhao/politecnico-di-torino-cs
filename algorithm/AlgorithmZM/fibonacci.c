//
// Created by zhaos on 12/8/2018.
//
#include <stdio.h>

// low efficiency
int fib(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    else {
        return fib(n-1) + fib(n-2);
    }
}

int jc(int n) {
    if (n == 0) {
        return 1;
    }
    else {
        return n * jc(n - 1);
    }
}

int main() {
    for (int i = 0; i < 10; i++) {
        printf("%d\n", jc(i));
    }


}