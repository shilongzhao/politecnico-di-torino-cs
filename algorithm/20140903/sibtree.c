/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


/**
 *  Tree represented by sibling and first child
 *  03 Sep 2014 Exam
 */

#include <stdio.h>
#include <stdlib.h>

#define  DEBUG

typedef struct  sibtree_node_t_ sibtree_node;

struct sibtree_node_t_ {
    char name;
    sibtree_node *child; // first child of this node
    sibtree_node *sibling; // a sibling list of this node
};
sibtree_node *search(sibtree_node *node, char c);
void preorder_sibtree(sibtree_node *node);
/**
 * tree structure
 *              A0                           A
 *             /                           / | \
 *            B1---C2---D3                B  C  D
 *           /         /         ===     /     / \
 *          E4         F5---H7          E      F  H
 *         /                           /
 *        G6                           G
 */
int main(int argc, char *argv[]) {
    sibtree_node **nodes = malloc(8 * sizeof(sibtree_node *));
    for (int i = 0; i < 8; i++) {
        nodes[i] = malloc(sizeof(sibtree_node));
        nodes[i]->name = (i+'A');
        nodes[i]->child = NULL;
        nodes[i]->sibling = NULL;
    }

    nodes[0]->child = nodes[1];

    nodes[1]->child = nodes[4];
    nodes[1]->sibling = nodes[2];

    nodes[2]->sibling = nodes[3];

    nodes[3]->child = nodes[5];

    nodes[4]->child = nodes[6];

    nodes[5]->sibling = nodes[7];


	sibtree_node *B = search(nodes[0], 'B');
    preorder_sibtree(B);

	
}

sibtree_node *search(sibtree_node *root, char c) {
	sibtree_node *result = NULL;
	if (root->name == c) return root;
	if (root == NULL ) return NULL;

	result =  search(root->child, c);
	if (result == NULL) 
		result = search(root->sibling, c);
	
	return result;
	

}
void preorder_sibtree(sibtree_node *node) {
    if (node == NULL) return;

    printf("%c\n", node->name);
    preorder_sibtree(node->child);
    preorder_sibtree(node->sibling);
}

