#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    char *string;
    struct node *next;
}node_t;

node_t *splitStr(char *str);
node_t *insert(node_t *p,node_t *list);
void printlist(node_t *head);


int main(int argc, const char * argv[])
{
    char *str="a.bb.ccc.dddd.eeeee.fffff";
    node_t *result;
    
    result=splitStr(str);
    printlist(result);
    return 0;
}




node_t *splitStr(char *str)
{
    node_t *list;
    char *p,*q,c;
    
    p=str;
    q=str;
    while(1)
    {
        while (*q != '.'  && *q != '\0')
        {
            q++;
        }
        c=*q;
        *q='\0';
        node_t *n = malloc(sizeof(node_t));
        n->string = strdup(p);
        
        if(c !='\0')
        {
            p=q+1;
            q++;
        }
        else
        {
            break;
        }
    }
    
    return list;
}

node_t *insert(node_t *n,node_t *list)
{
    
    if(list == NULL)
    {
        list=n;
        list->next=NULL;
        return list;
    }
    else
    {
        n->next=list;
        list=n;
        return list;
    }
}

void printlist(node_t *head)
{
    if(head==NULL)
    {
        return;
    }
    printf("%s",head->string);
    printf(" -> ");
    printlist(head->next);
}

