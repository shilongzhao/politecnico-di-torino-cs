/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct node_t_ node_t;
struct node_t_ {
    char *name;
    node_t *next;
};

node_t *insert(node_t *head, char *str) {
    node_t *tmp = malloc(sizeof(node_t));
    tmp->name = strdup(str);// or tmp->name = str
    tmp->next = head;
    head = tmp;

    return head;
}
/**
 * one can change content in an array, but not a string
 * if in main function, the paramter str is declared as 
 * char *str, then this will be wrong; 
 * SO char *str and char str[] are DIFFERENT!!!
 * http://c-faq.com/strangeprob/strlitnomod.html
 */
node_t *splitStr2(char *str) {
    node_t *list_head = NULL;
    int start = 0;
    int i=0;
    while (str[i] != '\0') {
        if ( str[i] == '.' ) {
            str[i] = '\0';
            list_head = insert(list_head, &str[start]);
            i += 1;
            start = i;
        }
        else
            i++;
    }
    list_head=insert(list_head, &str[start]);
    return list_head;

}

node_t *splitString2(char *s) {
    node_t *h = NULL;
    char r[10] = {};// TODO: dynamically allocate r or copy s and use \0???
    int j = 0;
    for (int i = 0; i <= strlen(s); i++) {
        if (s[i] == '.' || s[i] == '\0') {
            r[j] = '\0';
            // strdup
            insert2(&h, r);
//            printf("%s\n", r);
            j = 0;
        } else {
            r[j] = s[i];
            j++;
        }
    }
    return h;
}
//node_t *splitStr(char *str) {
//    node_t *list_head = NULL;
//    char *pch = strtok(str, ".");
//    while (pch != NULL) {
//        list_head = insert(list_head, pch);
//        pch = strtok(NULL, ".");
//    }
//
//    return list_head;
//}

int main() {
    char str[100] = "a.bb.ccc.eeee.fffff";
    node_t *list = splitStr2(str);

    for (node_t *n = list; n != NULL; n = n->next) {
        printf("%s\n", n->name);
    }
}











