/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>

void searchStr(char *str, int *start, int *length) {
    *start = 0;
    *length = 1;
    int len = 1;
    int st = 0;
    int i = 0;
    while (str[i] != '\0') {
        if (str[i] == str[i+1] ) {
            len += 1;
        }
        else {
            if (len > *length) {
                *length = len;
                *start  = st;
            }
            len = 1;
            st = i+1;
        }
        i++;
    }

    return;
}

int main() {
    char a[30] = "abbcccdddddeeeeee";
    int start = 0;
    int length = 0;

    searchStr(a, &start, &length);

    printf("%d %d\n", start, length);

    return 0;
}
