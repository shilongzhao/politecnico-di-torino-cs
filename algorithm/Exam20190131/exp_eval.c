#include <stdio.h>
#include <stdlib.h>
int eval(const int *v, int n, const char *ops) {
    int z = v[0];
    for (int i = 1; i < n; i++) {
        switch (ops[i-1]) {
            case '+':
                z += v[i];
                break;
            case '-':
                z -= v[i];
                break;
            case '*':
                z *= v[i];
                break;
            default:
                break;
        }
    }
    return z;
}
void print_exp(int *v, int n, char *ops) {
    printf("%d", v[0]);
    for (int i = 1; i < n; i++) {
        printf("%c%d", ops[i-1], v[i]);
    }
    printf("\n");
}
void gen(int *v, int n, char *ops, int num_op, int result) {
    if (num_op == n -1) {
        if (eval(v, n, ops) == result) {
            print_exp(v, n, ops);
        }
        return;
    }
    char s[4] = "+-*";
    for (int i = 0; i < 3; i++) {
        ops[num_op] = s[i];
        gen(v, n, ops, num_op + 1, result);
    }
}
void calculator(int *v, int n, int result) {
    char *ops = malloc(sizeof(char) * n); // operator sequence
    gen(v, n, ops, 0, result);
    free(ops);
}
int main() {
    int v[5] = {55,3,45,33,25};
    calculator(v, 5, 404);
    return 0;
}