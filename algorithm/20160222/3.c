
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#define R 4
#define C 5
typedef struct mat_t mat_t;
struct mat_t {
    int **m;
    int r;
    int c;
};

typedef struct sol_t sol_t;
struct sol_t {
    int *best;
    int best_len;
};

/*
 * check if all bulbs are turned on
 * @param m     input matrix
 * @param r     matrix rows count
 * @param c     matrix columns count
 * @param selected    array of seleted switches' indices
 * @param len       number of selected switches
 */
int bulbs_all_on(mat_t *mat, int *selected, int len) {
    int *state = malloc(sizeof(int) * mat->c);
    memset(state, 0, sizeof(int) * mat->c);
    
    for (int i = 0; i < len; i++) {
        int s = selected[i];
        if (s >= mat->r) return 0; // error;
        for (int j = 0; j < mat->c; j++) {
            state[j] += (mat->m)[s][j];
        }
    }
    for (int i = 0; i < mat->c; i++) {
        if (state[i] % 2 == 0) {
            free(state);
            return 0;
        }
    }
    free(state);
    return 1;
}

void play(mat_t *m, int *selected, sol_t *sol, int depth, int next) {
    if (bulbs_all_on(m, selected, depth) && depth < sol->best_len) {
        sol->best_len = depth;
        for (int i = 0; i < depth; i++) {
            (sol->best)[i] = selected[i];
        }
        return;
    }
    for (int i = next; i < m->r; i++) {
        selected[depth] = i;
        play(m, selected, sol, depth + 1, i + 1);
    }
}

void switcher(int **m, int r, int c) {
    mat_t *matrix = malloc(sizeof(mat_t));
    matrix->m = m;
    matrix->r = r;
    matrix->c = c;
    
    sol_t *sol = malloc(sizeof(sol_t));
    sol->best = malloc(sizeof(int) * r);
    sol->best_len = INT_MAX;
    
    int *selected = malloc(sizeof(int) * r);
    
    play(matrix, selected, sol, 0, 0);
    
    if (sol->best_len == INT_MAX) {
        printf("solution NOT exist!\n");
        return;
    }
    for (int i= 0; i < sol->best_len; i++) {
        printf("S%d ", (sol->best)[i]);
    }
    printf("\n");
    
    free(selected);
    free(sol);
    free(matrix);
    
    return;
}
/*
 * turn a switch 2*n times = 0 time
 * 2n + 1 times = 1 time
 * turn ABC = ACB = BCA = ...
 * so it's a combination problem ..
 */

int main(int argc, char **argv) {
    int test[R][C] =
    {
        {1, 0, 0, 0, 0},
        {0, 1, 0, 0, 0},
        {0, 0, 1, 0, 0},
        {0, 0, 0, 0, 1}
    };
    int **m = malloc(sizeof(int *) * R);
    for (int i = 0; i < R; i++) {
        m[i] = malloc(sizeof(int) * C);
        for (int j = 0; j < C; j++) {
            m[i][j] = test[i][j];
        }
    }
    
    switcher(m, R, C);
    
    for (int i = 0; i < R; i++)
        free(m[i]);
    
    free(m);
}
