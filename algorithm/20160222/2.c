#include <stdio.h>
#include <stdlib.h>
typedef struct list_t list_t;

struct list_t {
	int val;
	list_t *next;
};

/**
 * (*tail) cannot be null in this example code
 * and do NOT set (*tail)->next = null in this code
 */
void insertTail(list_t **tail, list_t *p) {
        (*tail)->next = p;
        *tail = p;
		return;
}

/**
 * original list is distroyed
 */
void split(list_t **p, int threshold, list_t **p1, list_t **p2) {
	if (*p == NULL) {
		return;
	}
    list_t *h = *p;
    list_t *p1t = NULL, *p2t = NULL;
    
	while (h != NULL) {
		list_t *nextp = h->next;

		if (h->val >= threshold) {
            if (*p1 == NULL) {
                *p1 = h;
                p1t = h;
            }
			else
                insertTail(&p1t, h);
		}
        else {
            if (*p2 == NULL) {
                *p2 = h;
                p2t = h;
            }
			else insertTail(&p2t, h);
        }

		h = nextp;
	}
    if (p1t != NULL) p1t->next = NULL;
    if (p2t != NULL) p2t->next = NULL;
    return;
}

void listPrint(list_t *p) {
    if (p == NULL) {
        printf("\n");
        return;
    }
    printf("%d ", p->val);
    listPrint(p->next);
}

void insertHead(list_t **head, list_t *n) {
    n->next = *head;
    *head = n;
}

int main() {
    int n[8] = {7, 8, 25, 2, 9, -5, 10, 37};
    list_t *p = NULL, *p1 = NULL, *p2 = NULL;
    int i;
    list_t *t;
    for (i = 0; i < 8; i++) {
        t = malloc(sizeof(list_t));
        t->val = n[i];
        t->next = NULL;
        insertHead(&p, t);
    }
    listPrint(p);
    split(&p, 18, &p1, &p2);
    listPrint(p1);
    listPrint(p2);
}   
