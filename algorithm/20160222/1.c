#include <stdio.h>

int m[10][10] = {};
int area(FILE *fp) {
	int x1, y1, x2, y2;
	int i, j;

	while(fscanf(fp, "%d %d %d %d",&x1, &y1, &x2, &y2) == 4) {
		for (i = x1; i < x2; i++) {
			for (j = y1; j < y2; j++) {
				m[i][j] = 1;
			}
		}
	}
	int re = 0;
	for (i = 0; i < 10; i++) {
		for (j = 0; j < 10; j++) {
			re = re + m[i][j];
		}
	}

	return re;
}

int main() {
	FILE *fp = fopen("input.txt", "r");
	int a = area(fp);
	printf("area is %d\n", a);
}
