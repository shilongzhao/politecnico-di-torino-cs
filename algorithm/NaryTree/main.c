/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */
/*
 *  Examination 18 July 2013
 *  Solution by StQ
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define M 20+1

typedef struct node_t {
    char *name;
    int n_children;
    int level;
    struct node_t **children;
} NODE;

/* Those are library Data Structures ... */
typedef struct stack_t {
    NODE **array;
    int index;
    int size;
} STACK;

typedef struct queue_t {
    NODE **array;
    int head;
    int tail;
    int num;
    int size;
} QUEUE;
/* ... end library Data Structures */

void read_file (NODE **, char *);
FILE *util_fopen(char *, char *);
NODE * create_node (void);
NODE * search_node_r (char *, NODE *);

void f1 (NODE *);
void f2 (NODE *, char *, char **, int);

/* Library prototipes */
QUEUE *QUEUEinit (int);
int QUEUEempty (QUEUE *);
int QUEUEenqueue (QUEUE *, NODE *);
int QUEUEdequeue (QUEUE *, NODE **);
void QUEUEdestroy (QUEUE *);
STACK *STACKinit (int);
int STACKempty (STACK *);
int STACKpush (STACK *, NODE *);
int STACKpop (STACK *, NODE **);
void STACKdestroy (STACK *);

void free_tree_r (NODE *);
char *util_strdup (char *);
void *util_malloc (int);
FILE *util_fopen (char *, char *);

int main(int argc, char *argv[])
{
    NODE *head = NULL;
    char *strBest = NULL;

    if (argc != 2) {
        fprintf (stderr, "Missing parameters\n");
        exit (EXIT_FAILURE);
    }

    read_file (&head, argv[1]);
    fprintf (stdout, "f1:\n");
    f1 (head);
    f2 (head, "", &strBest, 0);
    fprintf (stdout, "f2:\n   %s\n", strBest);

    free_tree_r(head);

    return EXIT_SUCCESS;
}

void read_file (NODE **head, char *fileName)
{
    NODE *temp;
    int i, n;
    char name[M], child[M];
    FILE *fp;

    fp = util_fopen(fileName, "r");
    while (fscanf(fp, "%s %d", name, &n) != EOF) {
        if (*head == NULL) {
            temp = *head = create_node();
            temp->name = util_strdup(name);
        } else {
            temp = search_node_r(name, *head);
        }
        temp->n_children = n;
        temp->children = (NODE **)malloc(n*sizeof(NODE *));
        if (temp->children == NULL) {
            fprintf(stderr, "Dynamic allocation error\n");
            exit(EXIT_FAILURE);
        }

        for (i=0; i<n; i++) {
            fscanf(fp, "%s", child);
            temp->children[i] = create_node();
            temp->children[i]->name = util_strdup(child);
        }
    }
    fclose(fp);

}

void f1 (NODE *head)
{
    NODE *p;
    QUEUE *q;
    STACK *s;
    int i;

    q = QUEUEinit (100);
    s = STACKinit (100);
    head->level = 0;
    QUEUEenqueue (q, head);
    while (QUEUEempty (q) == 0) {
        QUEUEdequeue (q, &p);
        for (i=0; i<p->n_children; i++) {
            p->children[i]->level = p->level+1;
            QUEUEenqueue (q, p->children[i]);
        }
        STACKpush (s, p);
    }

    /* Generate Output */
    while (STACKempty(s)==0) {
        STACKpop (s, &p);
        fprintf (stdout, "   %d %s\n", p->level, p->name);
    }

    QUEUEdestroy (q);
    STACKdestroy (s);

    return;
}

void f2 (NODE *head, char *str, char **strBest, int level)
{
    int i;
    char *tmp;

    if (head == NULL) {
        return;
    }

    /* To be precise I should add spaces to separate strings ... ! */
    tmp = util_malloc ((strlen (str) + strlen (head->name)) * sizeof (char));
    sprintf (tmp, "%s%s", str, head->name);

    if (head->n_children == 0) {
        if (*strBest==NULL || strlen(tmp)>strlen(*strBest)) {
            free (*strBest);
            *strBest = util_strdup (tmp);
        }
    }

    for (i=0; i<head->n_children; i++) {
        f2 (head->children[i], tmp, strBest, level+1);
    }

    free (tmp);

    return;
}

void free_tree_r(NODE *head)
{
    int i;

    if (head == NULL) {
        return;
    }

    for (i=0; i<head->n_children; i++) {
        free_tree_r(head->children[i]);
    }

    free(head->name);
    free(head);

}

/*
 *  LIBRARY Functions !!!
 *  To same space and time I use a QUEUE and a STACK on a dynamic array
 *  (they can be generalized to lists)
 */

NODE * create_node(void)
{
    NODE *q;

    q = (NODE *)util_malloc(sizeof(NODE));
    q->n_children = 0;
    q->level = (-1);
    q->children = NULL;

    return q;
}

NODE *search_node_r(char name[M], NODE *head)
{
    NODE *temp = NULL;
    int i;

    if (head != NULL) {
        if (strcmp(name, head->name) == 0) {
            temp = head;
        } else {
            for (i=0; i<head->n_children && temp==NULL; i++) {
                temp = search_node_r(name, head->children[i]);
            }
        }
    }

    return temp;
}

QUEUE *QUEUEinit (int size)
{
    QUEUE *qp;

    qp = (QUEUE *) util_malloc (sizeof (QUEUE));
    qp->size = size;
    qp->head = qp->tail = qp->num = 0;
    qp->array = (NODE **) util_malloc (size * sizeof (NODE *));

    return qp;
}

int QUEUEenqueue (QUEUE *qp, NODE *data)
{
    if (qp==NULL || qp->num>=qp->size) {
        return 0;
    }

    qp->array[qp->tail] = data;
    qp->tail = (qp->tail+1) % (qp->size);
    qp->num++;
    return 1;
}

int QUEUEdequeue (QUEUE *qp, NODE **data_ptr)
{
    if (qp==NULL || qp->num<=0) {
        return 0;
    }

    *data_ptr = qp->array[qp->head];
    qp->head = (qp->head+1) % (qp->size);
    qp->num--;

    return 1;
}

int QUEUEempty (QUEUE *qp)
{
    if (qp==NULL || qp->num<=0) {
        return 1;
    }

    return 0;
}

void QUEUEdestroy (QUEUE *qp)
{
    free (qp->array);
    free (qp);

    return;
}

STACK *STACKinit (int size)
{
    STACK *sp;

    sp = (STACK *) util_malloc (sizeof (STACK));
    sp->size = size;
    sp->index = 0;
    sp->array = (NODE **) util_malloc (size * sizeof (NODE *));

    return sp;
}

int STACKempty (STACK *sp)
{
    if (sp==NULL || sp->index<=0) {
        return 1;
    }

    return 0;
}

int STACKpush (STACK *sp, NODE *data)
{
    if (sp==NULL || sp->index>=sp->size) {
        return 0;
    }

    sp->array[sp->index++] = data;
    return 1;
}

int STACKpop (STACK *sp, NODE **data_ptr)
{
    if (sp==NULL || sp->index<=0) {
        return 0;
    }

    *data_ptr = sp->array[--sp->index];

    return 1;
}

void STACKdestroy (STACK *sp)
{
    free (sp->array);
    free (sp);

    return;
}

FILE *util_fopen(char *name, char *access)
{
    FILE *fp = fopen(name, access);

    if(fp == NULL) {
        printf("Error opening file %s!\n", name);
        exit(EXIT_FAILURE);
    }

    return fp;
}

char *util_strdup(char *src)
{
    char *dst = strdup(src);

    if (dst == NULL) {
        printf("Memory allocation error!\n");
        exit(EXIT_FAILURE);
    }

    return dst;
}

void *util_malloc(int size)
{
    void *ptr = malloc(size);

    if (ptr == NULL) {
        printf("Memory allocation error!\n");
        exit(EXIT_FAILURE);
    }

    return ptr;
}