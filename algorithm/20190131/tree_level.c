//
// Created by zhaos on 2/3/2019.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *child;
    node_t *sibling;
};
/**
 * since we do not know the number of levels
 * the array may need to be resized
 * @param array
 * @param size
 */
void double_array_size(int **array, int *size) {
    int new_size = 2 * (*size);
    int *array2 = malloc(new_size * sizeof(int));
    memset(array2, 0, new_size * sizeof(int));
    for (int i = 0; i < *size; i++) {
        array2[i] = (*array)[i];
    }
    free(*array);
    *array = array2;
    *size = new_size;
}

void iterate(node_t *root, int level, int **counters, int *size) {
    if (root == NULL) return;

    if (level >= *size) {
        double_array_size(counters, size);
    }
    (*counters)[level] += 1;
    iterate(root->sibling, level, counters, size);
    iterate(root->child, level + 1, counters, size);
}

void tree_max_level(node_t *root) {
    int size = 1;
    int *counters = malloc(sizeof(int) * size);
    memset(counters, 0, sizeof(int) * size);

    iterate(root, 0, &counters, &size);

    for (int i = 0; i < size; i++) {
        printf("%d ", counters[i]);
    }
    int max_nodes = 0, max_level = 0;
    for (int i = 0; i < size; i++) {
        if (counters[i] > max_nodes) {
            max_level = i;
            max_nodes = counters[i];
        }
    }
    printf("\nmax level is %d with %d nodes \n", max_level, max_nodes);
}

int main(int argc, char *argv[]) {
    node_t n1 = {1, NULL, NULL};
    node_t n2 = {2, NULL, NULL};
    node_t n3 = {3, NULL, NULL};
    node_t n4 = {4, NULL, NULL};
    node_t n5 = {5, NULL, NULL};
    node_t n6 = {6, NULL, NULL};
    node_t n7 = {7, NULL, NULL};
    node_t n8 = {8, NULL, NULL};
    
    n1.child = &n2;
    n2.sibling = &n3;
    n3.sibling = &n4;
    n4.sibling = &n5;
    
    n5.child = &n6;
    n6.sibling = &n7;
    n7.sibling = &n8;
    
    tree_max_level(&n1);
}