//
// Created by Shilong Zhao on 30/01/2018.
//

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int max_sum = 0;
int *max_v = NULL;
int intersect(int **m, int *v, int n, int p) {
    for (int i = 0; i < n; i++) {
        if (v[i] == 1) {
            int s1 = m[i][0];
            int f1 = m[i][1];

            int s2 = m[p][0];
            int f2 = m[p][1];

            if (!(s1 > f2 || s2 > f1)) {
                return 1;
            }
        }
    }

    return 0;
}

void recursion(int **m, int *v, int n, int last_pick) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            if (v[i]) {
                sum += m[i][1] - m[i][0];
            }
        }
        if (sum > max_sum) {
            max_sum = sum;
            for (int i = 0; i < n; i++) {
                max_v[i] = v[i];
            }
        }
    for (int i = last_pick + 1; i < n; i++) {
        if (!intersect(m, v, n, i)) {
            v[i] = 1;
            recursion(m, v, n, i);
            v[i] = 0;
        }
    }
}

void activity_selection(int **m, int *v, int n) {
    max_v = malloc(sizeof(int) * n);
    memset(max_v, 0, sizeof(int)*n);
    recursion(m, v, n, -1);
}

int main() {
    int mat[6][2] = {
            {1,4},
            {3,5},
            {4,6},
            {5,9},
            {10,11},
            {8,20}
    };

    int **m = malloc(sizeof(int *) * 6);
    for (int i = 0; i < 6; i++) {
        m[i] = malloc(sizeof(int) * 2);
        for (int j = 0; j < 2; j++) {
            m[i][j] = mat[i][j];
        }
    }

    int v[6] = {};

    activity_selection(m, v, 6);

    for (int i = 0; i < 6; i++) {
        printf("%d ", max_v[i]);
    }
}
