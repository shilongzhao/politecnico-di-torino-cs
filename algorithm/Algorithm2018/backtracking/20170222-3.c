//
// Created by Shilong Zhao on 17/01/2018.
//

#include <stdlib.h>
#include <string.h>
#include <printf.h>
#define  R 5
void dfs(int **mat, int n, int start, int *visited, int gn) {
    visited[start] = gn;
    for (int i = 0; i < n; i++) {
        if (mat[start][i] == 1 && visited[i] == 0 && i != start) {
            dfs(mat, n, i, visited, gn);
        }
    }
}
int partition(int n, int k, int **mat) {
    int *t = malloc(sizeof(int) * n); // t[i]-> node i's sub-graph number, if t[i] == 0, unvisited
    memset(t, 0, sizeof(int) * n);

    int gn = 0; // sub-graph number
    for (int i = 0; i < n; i++) {
        if (t[i] != 0) {
            continue;
        }
        else {
            gn++;
            dfs(mat, n, i,  t, gn);
        }
    }

    int *gc = malloc(sizeof(int) * (gn + 1)); // gc[i] -> sub-graph i's num of nodes
    memset(gc, 0, sizeof(int) * (gn + 1));
    for (int i = 0; i < n; i++) {
        gc[t[i]] += 1;
    }
    //-------
    for (int i = 0; i < n; i++) {
        printf("%d ", t[i]);
    }
    printf("\n");
    for (int i = 1; i <= gn; i++) {
        printf("%d ", gc[i]);
    }
    printf("\n");
    //------

    for (int i = 1; i <= gn; i++) {
        if (n - gc[i] >= k || gc[i] >= k) {
            for (int j = 0; j < n; j++) {
                if (t[j] == i) {
                    printf("%d ", j);
                }
            }
            return 1;
        }
    }
    return 0;
}


int main() {
    int test[R][R] =
            {
                    {1, 0, 0, 1, 1},
                    {0, 1, 1, 0, 0},
                    {0, 1, 1, 0, 0},
                    {1, 0, 0, 1, 1},
                    {1, 0, 0, 1, 1}
            };
    int **m = malloc(sizeof(int *) * R);
    for (int i = 0; i < R; i++) {
        m[i] = malloc(sizeof(int) * R);
        for (int j = 0; j < R; j++) {
            m[i][j] = test[i][j];
        }
    }

    partition(R, 3, m);
}