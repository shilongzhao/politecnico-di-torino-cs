//
// Created by Shilong Zhao on 16/01/2018.
//

#include <stdio.h>
#include <stdlib.h>
//#define M 3

int **read(char *, int *);
void matrix_write(int **, int);
void matrix_swap(int **, int);
int matrix_swap_r(int, int **, int *, int, int, int);
int check(int **, int, int *, int);

void matrix_swap(int **mat, int n){
    int *sol;
    //int i;
    //int count = 0;
    int k;
    int N = 2 * n;
    int flag = 0;

    sol = malloc(sizeof(int) * N);
    if(sol == NULL){
        fprintf(stderr, "Memory allocation error!\n");
        exit(EXIT_FAILURE);
    }

    for(k = 1; k <= N && flag == 0; k++){
        if(matrix_swap_r(0, mat, sol, 0, k, N)){
            flag = 1;//Ã¯≥ˆ—≠ª∑£¨≤ª’“¡À£®k“—æ≠’“µΩ¡À“ª∏ˆ◊Ó–°÷µ£¨¬˙◊„Œ“√«µƒ"shortest"£©
        }
    }
    if(flag == 0){
        fprintf(stdout, "No solution found\n");
    }

    free(sol);

    return;
}

int matrix_swap_r(int pos, int **mat, int *sol, int start, int k, int n){
    int i;
    //int count = 0;

    if(pos == k){
        //check
        if(check(mat, n, sol, k)){
            fprintf(stdout,"Solution:\n");
            for(i = 0; i < k; i++){
                if(sol[i] < (n/2)){
                    fprintf(stdout, "Row = %d ", sol[i]);
                }
                else{
                    fprintf(stdout,"Column = %d ", sol[i] - (n/2));
                }
            }
            fprintf(stdout,"\n");
            return 1;
        }
        else{
            return 0;
        }
    }

    for(i = start; i < n; i++){
        sol[pos] = i;
        if(matrix_swap_r(pos + 1, mat, sol, i + 1, k, n)){
            return 1;
        }
    }

    return 0;

}

int main(int argc, char *argv[])
{
    int i;
    int n;
    int **mat = read(argv[1], &n);

    fprintf(stdout,"Initial Matrix:\n");
    matrix_write(mat, n);

    matrix_swap(mat, n);

    for(i = 0; i < n; i++){
        free(mat[i]);
    }
    free(mat);

    return 0;
}

int **read(char *name, int *n){
    int i,j;
    int **mat;
    FILE *fp;

    fp = fopen(name, "r");
    if(fp == NULL){
        fprintf(stderr, "File open error!\n");
        exit(EXIT_FAILURE);
    }

    fscanf(fp, "%d", n);// n is a pointer here
    mat = malloc(sizeof(int *) * (*n));

    for(i = 0; i < (*n); i++){
        mat[i] = malloc(sizeof(int) * (*n));
        for(j = 0; j < (*n); j++){
            fscanf(fp, "%d", &mat[i][j]);
        }
    }
    fclose(fp);

    return mat;
}

void matrix_write(int **mat, int n){
    int i, j;

    for(i = 0; i < n; i++){
        for(j =  0; j < n; j++){
            fprintf(stdout,"%d", mat[i][j]);
        }
        fprintf(stdout,"\n");
    }

    return;
}

int check(int **mat, int n, int *sol, int k){
    int i, j;
    int **copy_mat;
    int all_one = 1;

    copy_mat = malloc(sizeof(int *) * n);
    if(copy_mat == NULL){
        fprintf(stderr, "Memory allocation error!\n");
        exit(EXIT_FAILURE);
    }//¥¥Ω®“ª∏ˆæÿ’Û->––

    for(i = 0;i < n; i++){
        copy_mat[i] = malloc(sizeof(int) * n);
        if(copy_mat[i] == NULL){
            fprintf(stderr, "Memory allocation error!\n");
            exit(EXIT_FAILURE);
        }
        for(j = 0; j < n; j++){
            copy_mat[i][j] = mat[i][j];
        }
    }//Ω´æÿ’ÛøΩ±¥π˝¿¥

    for(i = 0; i < k; i++){
        if(sol[i] < (n/2)){//row
            for(j = 0; j < (n/2);j++){
                copy_mat[sol[i]][j] = (copy_mat[sol[i]][j]+1) % 2;
            }
        }
        else{
            for(j = 0; j < (n/2); j++){
                copy_mat[j][sol[i]-(n/2)] = (copy_mat[j][sol[i]-(n/2)] + 1) % 2;
            }
        }
    }

    for(i = 0; i < (n/2) ; i++){
        for(j = 0; j < (n/2) ; j++){
            if(!copy_mat[i][j]){
                return 0;
            }
        }
    }

    return 1;
}













