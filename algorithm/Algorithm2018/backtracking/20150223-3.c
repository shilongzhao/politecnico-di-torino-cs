//
// Created by Shilong Zhao on 13/01/2018.
//

#include <printf.h>
#include <memory.h>

int check(char *s, int n) {
    int c[36] = {};
    for (int i = 0; i < strlen(s); i++) {
        int x;
        if (s[i] >= '0' && s[i] <= '9') {
            x = s[i] - '0';
            c[x]++;
        }
        else {
            x = s[i] - 'A' + 10;
            c[x]++;
        }
        if (c[x] > n) {
            return 0;
        }
    }
    return 1;
}

void generate(char *str, int h, int n) {
    if (h == 5) {
        str[5] = '\0';
        if (check(str, n)) {
            printf("%s\n", str);
        }
        return;
    }

    if (h < 3) {
        for (int i = 0; i < 26; i++) {
            str[h] = (char) ('A' + i);
            generate(str, h + 1, n);
            str[h] = '\0';
        }
    }
    else {
        for (int i = 0; i < 10; i++) {
            str[h] = (char) ('0' + i);
            generate(str, h + 1, n);
            str[h] = '\0';
        }
    }
}

int main() {
    char str[6] = "";
    generate(str, 0, 1);
}