//
// Created by Shilong Zhao on 13/01/2018.
//

#include <printf.h>
#include <string.h>

void generate(char **set, int n, char *str, int h) {
    if (h == n) {
        str[h] = '\0';
        printf("%s\n", str);
        return;
    }

    for (int i = 0; i < strlen(set[h]); i++) {
        str[h] = set[h][i];
        generate(set, n, str, h+1);
        str[h] = '\0';
    }

}

int main() {
    char *set[3] = {"A", "xyz", "123"};
    char str[4] = "";
    generate(set, 3, str, 0);
}