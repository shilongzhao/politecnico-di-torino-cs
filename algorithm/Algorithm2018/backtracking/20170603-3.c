//
// Created by Shilong Zhao on 23/01/2018.
//

#include <stdlib.h>
#include <printf.h>

int *result = NULL;
int *smallest = NULL;
int *longest = NULL;
int rs = 0, ss = INT32_MAX, ls = 0;



void combo(int **mat, int r, int c, int k, int last_i, int last_j) {
    if (last_i == r - 1 && last_j == c - 1 && k != 0) {
        return;
    }
    if (k == 0) {
        if (rs < ss) {
            ss = rs;
            for (int i = 0; i < rs; i++) {
                smallest[i] = result[i];
            }
        }
        if (rs > ls) {
            ls = rs;
            for (int i = 0; i < rs; i++) {
                longest[i] = result[i];
            }
        }
        return;
    }

    for (int i = last_i + last_j/(c-1); i < r; i++) {
        for (int j = (last_j + 1) % c; j < c; j++) {
            int v = mat[i][j];
            result[rs] = v;
            rs++;
            combo(mat, r, c, k - v, i, j);
            rs--;
        }
    }
}


void balance(int **mat, int r, int c, int k) {
    result = malloc(sizeof(int) * r * c);
    smallest = malloc(sizeof(int) * r * c);
    longest = malloc(sizeof(int) * r * c);

    combo(mat, r, c, k, -1, c - 1);

    for (int i = 0; i < ss; i++) {
        printf("%d ", smallest[i]);
    }
    printf("\n");
    for (int i = 0; i < ls; i++) {
        printf("%d ", longest[i]);
    }
    printf("\n");
}

int main() {
    int m[3][4] = {
            {2,3,1,2},
            {6,4,2,5},
            {2,4,3,2}
    };
    int **mat = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        mat[i] = malloc(sizeof(int) * 4);
        for (int j = 0; j < 4; j++) {
            mat[i][j] = m[i][j];
        }
    }

    balance(mat, 3, 4, 10);
}