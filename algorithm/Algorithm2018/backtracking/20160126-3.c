//
// Created by Shilong Zhao on 16/01/2018.
//

#include <printf.h>
#include <memory.h>
#include <stdlib.h>

int split(int total, int *set, int n, int sum, int *result, int rs) {
    if (sum == total) {
        for (int i = 0; i < rs; i++) {
            printf("%d ", result[i]);
        }
        printf("\n");
        return 1;
    }
    if (sum > total) {
        return 0;
    }

    for (int i = 0; i < n; i++) {
        result[rs] = set[i];
        split(total, set, n, sum + set[i], result, rs + 1);
    }
}

int split_str(char *str, int *set, int n, int sum, int *result, int rs) {
    int total = (int) strlen(str);
    if (sum == total) {
        for (int i = 0; i < rs; i++) {
            printf("%d ", result[i]);
        }
        printf("\n");
        return 1;
    }
    if (sum > total) {
        return 0;
    }

    for (int i = 0; i < n; i++) {
        result[rs] = set[i];
        split(total, set, n, sum + set[i], result, rs + 1);
    }
}

int split2(int total, int *set, int n, int *result, int rs) {
    if (0 == total) {
        for (int i = 0; i < rs; i++) {
            printf("%d ", result[i]);
        }
        printf("\n");
        return 1;
    }
    if (0 > total) {
        return 0;
    }

    for (int i = 0; i < n; i++) {
        result[rs] = set[i];
        split2(total - set[i], set, n, result, rs + 1);
    }
}


int main() {
    int set[3] = {2,3,4};
    int *result = malloc(sizeof(int) * 10);
    split2(10, set, 3, result, 0);
    free(result);

    printf("%d\n", (int)sizeof(long long));
}