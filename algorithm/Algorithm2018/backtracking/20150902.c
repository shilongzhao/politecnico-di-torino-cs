#include <printf.h>
#include <stdlib.h>

//
// Created by Shilong Zhao on 13/01/2018.
//
typedef struct route_t route_t;

struct route_t {
    int x;
    int y;
    route_t *next;
};

route_t *best_path;
int best_path_sum;
int best_path_len;



int deadend(int **mat, int m, int n, int p, int q, int **visited) {
    for (int i = p - 1; i <= p + 1; i++) {
        for (int j = q - 1; j <= q + 1; j++) {
            if (i  == p && j == q) {
                continue;
            }
            if (i < 0 || i >= m || j < 0 || j >= n) {
                continue;
            }
            if (!visited[i][j]) {
                return 0;
            }
        }
    }
    return 1;
}

route_t *insert(route_t *h, int i, int j) {
    route_t *t = malloc(sizeof(route_t));
    t->x = i;
    t->y = j;
    t->next = h;
    return t;
}

route_t *traceback(route_t *h) {
    route_t *t = h;
    h = h->next;
    free(t);
    return h;
}

route_t *copy(route_t *p) {
    if (p == NULL) {
        return NULL;
    }

    route_t *t = malloc(sizeof(route_t));
    t->x = p->x;
    t->y = p->y;
    t->next = copy(p->next);
    return t;
}

void route(int **mat, int m, int n, int **visited, int p, int q, route_t *path, int len, int sum) {
    if (p == m - 1 && q == n - 1) {
        // sum path, compare, take shortest
        if ((sum > best_path_sum) || (sum == best_path_sum && len < best_path_len)) {
            //TODO: free old best path;
            best_path_len = len;
            best_path_sum = sum;
            best_path = copy(path);
        }
        return;
    }

    if (deadend(mat, m, n, p, q, visited)) {
        return ;
    }

    for (int i = p - 1; i <= p + 1; i++) {
        for (int j = q - 1; j <= q + 1; j++) {
            if (i == p && j == q)
                continue;
            if (i < 0 || i >= m || j < 0 || j >= n) {
                continue;
            }
            if (visited[i][j])
                continue;
            visited[i][j] = 1;
            path = insert(path, i, j);
            route(mat, m, n, visited, i, j, path, len+1, sum + mat[i][j]);
            path = traceback(path);
            visited[i][j] = 0;
        }
    }
}

int main() {
    best_path = NULL;
    best_path_sum = INT32_MIN;
    best_path_len = INT32_MAX;

    int **mat;
    int m;
    int n;
    int **visited;

    int mm[3][3] = {{1,2,-3}, {9,-9,7},{0,1,4}};
    mat = malloc(sizeof(int *) * 3);
    visited = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        mat[i] = malloc(sizeof(int) * 3);
        visited[i] = malloc(sizeof(int) * 3);

        for (int j = 0; j < 3; j++){
            mat[i][j] = mm[i][j];
            visited[i][j] = 0;
        }
    }
    m = n = 3;


    route(mat, m, n, visited, 0, 0, NULL, 0, 0);

    route_t *t = best_path;
    while(t != NULL) {
        printf("(%d, %d) ", t->x, t->y);
        t = t->next;
    }

}
