#include <printf.h>
#include <stdlib.h>

//
// Created by Shilong Zhao on 16/01/2018.
//
#define R 4
#define C 5
int all_on(int *bulbs, int n) {
    for (int i = 0; i < n; i++) {
        if (bulbs[i] == 0) {
            return 0;
        }
    }
    return 1;
}

void press(int **mat, int *bulbs, int nb, int s) {
    for (int i = 0; i < nb; i++) {
        bulbs[i] = bulbs[i] ^ mat[s][i];
    }
}

void play(int **mat, int ns, int *bulbs, int nb, int *result, int rs, int last_pick) {
    if (all_on(bulbs, nb)) {
        for (int i = 0; i < rs; i++) {
            printf("%d ", result[i]);
        }
        printf("\n");
        return;
    }

    for (int i = last_pick + 1; i < ns; i++) {
        result[rs] = i;
        press(mat, bulbs, nb, i);
        play(mat, ns, bulbs, nb, result, rs + 1, i);
        press(mat, bulbs, nb, i);
    }
}

int main() {
    int test[R][C] =
            {
                    {1, 0, 0, 0, 0},
                    {0, 1, 1, 1, 1},
                    {1, 1, 1, 0, 0},
                    {1, 0, 0, 1, 1}
            };
    int **m = malloc(sizeof(int *) * R);
    for (int i = 0; i < R; i++) {
        m[i] = malloc(sizeof(int) * C);
        for (int j = 0; j < C; j++) {
            m[i][j] = test[i][j];
        }
    }

    int bulbs[C] = {};
    int result[R] = {};
    play(m, R, bulbs, C, result, 0, -1);
}