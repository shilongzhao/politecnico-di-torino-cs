//
// Created by Shilong Zhao on 12/01/2018.
//

#include <stdlib.h>
#include <printf.h>

void press(int **mat, int n, int s) {
    if (s < n) {
        for (int i = 0; i < n; i++) {
            mat[s][i] ^= 1;
        }
    }
    else {
        for (int i = 0; i < n; i++) {
            mat[i][s - n] ^= 1;
        }
    }
}

int all_on(int **mat, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (mat[i][j] == 0) {
                return 0;
            }
        }
    }
    return 1;
}

void play(int **mat, int n, int best[], int *b, int current[], int c, int last_pick) {

    if (last_pick == 2*n - 1) {
        return;
    }

    if (all_on(mat, n)) {
        if (c < *b){
            *b = c;
            for (int i = 0; i < c; i++) {
                best[i] = current[i];
                printf("%d ", current[i]);
            }
            printf("\n");
        }
        return;
    }

    for (int i = last_pick + 1; i < 2 * n; i++) {
        current[c] = i;
        press(mat, n, i);
        play(mat, n, best, b, current, c + 1, i);
        press(mat, n, i);
    }
}

int main() {
    int **m;
    int mat[3][3] = {{0,1,0}, {1,0,1}, {0,1,0}};
    m = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        m[i] = malloc(sizeof(int) * 3);
        for (int j = 0; j < 3; j++) {
            m[i][j] = mat[i][j];
        }
    }
    int best[6] = {};
    int b = 6;
    int current[6] = {};
    int c = 0;
    play(m, 3, best, &b, current, 0, -1);
}
