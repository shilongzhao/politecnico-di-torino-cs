#include <printf.h>
#include <stdlib.h>
#include <string.h>

//
// Created by Shilong Zhao on 22/01/2018.
//
int check(int m[][9], int *result, int k) {
    int appeared[9] = {}; // appeared[i] number i included
    for (int i = 0; i < k; i++) {
        int r = result[i];
        for (int j = 0; m[r][j] != 0; j++){
            int v = m[r][j];
            appeared[v] = 1;
        }
    }
    for (int i = 1; i <= 8; i++) {
        if (appeared[i] == 0) {
            return 0;
        }
    }
    return 1;
}

void play(int m[][9], int n, int k, int *result, int d, int last_pick) {
    if (last_pick == n - 1 && d < k) {
       return;
    }
    if (d == k) {
        if (check(m, result, k)) {
            for (int i = 0; i < k; i++) {
                printf("%d ", result[i]);
            }
            printf("\n");
        }
        return;
    }

    for (int i = last_pick + 1; i < n; i++) {
        result[d] = i;
        play(m, n, k, result, d + 1, i);
    }
}

void cover(int m[][9], int n, int k) {
    int *result = malloc(sizeof(int) * k);
    memset(result, 0, sizeof(int) * k);
    play(m, n, k, result, 0, -1);
}

int main() {
    int m[5][9] = {
            {1,2,3},
            {2,3,8},
            {7,8},
            {3,4},
            {4,5,6,7}
    };
    cover(m, 5, 3);
    return 0;
}