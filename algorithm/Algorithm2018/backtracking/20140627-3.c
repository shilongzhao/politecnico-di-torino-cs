#include <stddef.h>
#include <memory.h>
#include <stdlib.h>
#include <printf.h>

//
// Created by Shilong Zhao on 23/01/2018.
//
int num = 0;
void printMenu(char *menu[], int m) {
    printf("menu_%d: ", num);
    for (int i = 0; i < m; i++) {
        printf("%s ", menu[i]);
    }
    printf("\n");
}
void recursion(char **data[], int n, char *menu[], int m) {
    if (m == n) {
        num++;
        printMenu(menu, m);
        return;
    }
    char **dishes = data[m];
    for (int i = 0; dishes[i] != NULL; i++) {
        menu[m] = strdup(dishes[i]);
        recursion(data, n, menu, m + 1);
    }
}
void buildMenu (char **data[], int n) {
    char **menu = malloc(sizeof(char *) * n);
    recursion(data, n, menu, 0);
}

int main(int argc, char *argv[]) {
    // initialize data
    char *primo[5] = { "duck_salad",
                       "scotch_egg",
                       "soupe_and_bread",
                       "baby_squid",
                       NULL
    };

    char *secondo[4] = { "rabbit_and_bacon",
                         "fish_and_chips",
                         "roat_lamp",
                         NULL
    };

    char *contorno[4] = { "gateaux_opera",
                          "ice_cream",
                          "cheese_cake",
                          NULL
    };

    char ***data = (char ***) malloc(3 * sizeof (char **));
    data[0] = primo;
    data[1] = secondo;
    data[2] = contorno;

    buildMenu(data, 3);
}