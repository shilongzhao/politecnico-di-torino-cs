#include <stddef.h>
#include <limits.h>
#include <stdlib.h>
#include <printf.h>

//
// Created by Shilong Zhao on 27/01/2018.
//
int *best = NULL;

int bs = INT_MAX;
void jump(int *vet, int n, int p, int *result, int rs) {

    if (p == n - 1) {
        result[rs++] = p;
        if (rs < bs) {
            for (int i = 0; i < rs; i++) {
                best[i] = result[i];
                bs = rs;
            }
        }
        return;
    }

    for (int i = 1; i <= vet[p]; i++) {
        result[rs] = p;
        if (p + i <= n - 1) {
            jump(vet, n, p + i, result, rs + 1);
        }
    }
}

int main() {
    int vet[7] = {3,1,4,3,1,1,2};
    best = malloc(sizeof(int) * 7);
    int *result = malloc(sizeof(int) * 7);
    jump(vet, 7, 0, result, 0);

    for (int i = 0; i < bs; i++) {
        printf("%d ", best[i]);
    }
}