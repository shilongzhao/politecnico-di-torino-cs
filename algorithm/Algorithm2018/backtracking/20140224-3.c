//
// Created by Shilong Zhao on 12/01/2018.
//

#include <printf.h>

#define N 5
#define M 6

char str[N+1] = "AEIOU";
int count = 0;
int check(char *s) {
    int r[N] = {};
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            if (s[i] == str[j]) {
                r[j] = 1;
            }
        }
    }
    for (int i = 0; i < N; i++) {
        if (r[i] == 0) {
            return 0;
        }
    }
    return 1;
}
void recursion(char *result, int n) {
    if (n == M ) {
        if (check(result)) {
            count++;
            result[M] = '\0';
            printf("%s\n", result);
        }
        return;
    }

    for (int i = 0; i < N; i++) {
        result[n] = str[i];
        recursion(result, n + 1);
        result[n] = '\0';
    }
}

int main() {
    char result[M+1]="";

    recursion(result, 0);

    printf("%d \n", count);
}