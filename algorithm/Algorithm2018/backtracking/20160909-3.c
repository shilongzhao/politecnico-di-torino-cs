//
// Created by Shilong Zhao on 22/01/2018.
//


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
void print_array(char *array_name, int *a, int n) {
    printf("%s: ", array_name);
    for (int  i = 0; i < n; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void combo(int *books, int nb, int ng, int k,  int *selected, int *result, int rs, int last_pick) {
//    print_array("books", books, nb);
//    print_array("selected", selected, ng);
    if (last_pick == nb - 1 && rs < k) {
        return;
    }
    if (rs == k) {
        for (int i = 0; i < rs; i++) {
            printf("%d ", result[i]);
        }
        printf("\n");
        return;
    }

    for (int i = last_pick + 1; i < nb; i++) {
        int g = books[i];
        if (!selected[g]) {
            result[rs] = i;
            selected[g] = 1;
            combo(books, nb, ng, k, selected, result, rs + 1, i);
            selected[g] = 0;
        }
    }
}

int birthday (int *vet, int n, int m, int k) {
    int *selected = malloc(sizeof(int) * (m + 1));
    memset(selected, 0, sizeof(int) *(m + 1));
    int *result = malloc(sizeof(int) * k);
    memset(selected, 0, sizeof(int) * k );
    combo(vet, n, m, k, selected, result, 0, -1);
}

int main() {
    int vet[5] = {2,1,1,4,3};
    birthday(vet, 5, 4, 4);
}