//
// Created by ZhaoShilong on 13/02/2018.
//

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

int min_port;

int num_ports_used(int N, int L, int *space) {
    int r = 0;
    for (int i = 0; i < N; i++) {
        if (space[i] != L) {
            r++;
        }
    }
    return r;
}
/**
 * 1. Given a ship s, its length is l[s], find a port i with space[i] >= l[s] and
 * place the ship at port i.
 * 2. Continue place ship s + 1
 * @param s     ship id which needs to be placed
 * @param m     total number of ships
 * @param l     lengths of ships
 * @param N     total number of ports
 * @param space remaining spaces of ports
 * @param pos   positions of ships
 */
void place(int s, int m, int *l, int N, int L, int *space, int *pos) {
    if (s == m) { // if all ships are placed
        if (num_ports_used(N, L, space) < min_port) {
            min_port = num_ports_used(N, L, space);
        }
        for (int i = 0; i < m; i++) {
            printf("ship %d port = %d \n", i, pos[i]);
        }
        printf("---------\n");
        return;
    }
    // TODO: better solution? avoid repeating recursion on duplicate situation
    for (int i = 0; i < N; i++) {
        if (space[i] >= l[s]) { // remaining space at port i is greater or equal to length of ship s
            pos[s] = i;
            space[i] = space[i] - l[s];
            place(s+1, m, l, N, L, space, pos);
	    place[i] = space[i] + l[s]
         }
    }
}

void ship_to_port(int N, int L, int m, int *l) {
    int *pos = malloc(sizeof(int) * m); //  port of ship i = pos[i]
    int *space = malloc(sizeof(int) * N); // remaining space in port i = space[i]
    for (int i = 0; i < N; i++) {
        space[i] = L;
    }

    place(0, m, l, N, L, space, pos);
}

int main() {
    int len[4] = {6, 5, 5, 4};
    min_port = INT_MAX;
    ship_to_port(3, 10, 4, len);
    printf("minimum ports used = %d\n", min_port);
}