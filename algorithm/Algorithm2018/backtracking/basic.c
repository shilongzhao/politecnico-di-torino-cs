//
// Created by Shilong Zhao on 11/01/2018.
//

#include <printf.h>

char str[6] = "ABCDE";
int  used[6] = {};
void arrange(char result[], int n) {
    if (n == 3) {
        result[3] = '\0';
        printf("%s\n", result);
        return;
    }
    for (int i = 0; i < 5; i++) {
        if (used[i] == 0) {
            result[n] = str[i];
            used[i] = 1;

            arrange(result, n + 1);

            result[n] = '\0';
            used[i] = 0;
        }
    }
}


void combination(char *result, int n, int last_pick) {
    if (n == 3) {
        result[3] = '\0';
        printf("%s\n", result);
        return;
    }
    for (int i = last_pick + 1; i < 5; i++) {
        result[n] = str[i];
        combination(result, n + 1, i);
        result[n] = '\0';
    }
}

int main() {
    char result[4] = "";
    combination(result, 0, -1);
}



