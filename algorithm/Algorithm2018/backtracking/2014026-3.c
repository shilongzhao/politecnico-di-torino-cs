//
// Created by Shilong Zhao on 24/01/2018.
//

#include <limits.h>
#include <stdlib.h>
#include <printf.h>

int min_diff = INT_MAX;

int diff(int *a, int n) {
    int max = INT_MIN;
    int min = INT_MAX;
    int balance = 0;
    for (int i = 0; i < n; i++) {
        balance += a[i];
        if (balance > max) {
            max = balance;
        }

        if (balance < min) {
            min = balance;
        }
    }

    return max - min;
}
void balance(int *vet, int vs, int *result, int rs, int *best, int *used) {
    if (rs == vs) {
        for (int i = 0; i < vs; i++) {
            printf("%d ", result[i]);
        }
        printf(": diff=%d\n", diff(result, vs));

        if (diff(result, vs) < min_diff) {
            min_diff = diff(result, vs);
            for (int i = 0; i < vs; i++) {
                best[i] = result[i];
            }
        }
        return;
    }

    for (int i = 0; i < vs; i++) {
        if (!used[i]) {
            used[i] = 1;
            result[rs] = vet[i];
            balance(vet, vs, result, rs + 1, best, used);
            used[i] = 0;
        }
    }
}

int main() {
    int vet[4] = { -5, 10, -10, 7};
    int *result = malloc(sizeof(int) * 4);
    int *best = malloc(sizeof(int) * 4);
    int *used = calloc(4, sizeof(int));
    balance(vet, 4, result, 0, best, used);
    for (int i = 0; i < 4; i++) {
        printf("%d ", best[i]);
    }
}