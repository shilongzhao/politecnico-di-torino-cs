//
// Created by Shilong Zhao on 04/01/2018.
//

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <printf.h>

typedef struct list_t list_t;
struct list_t {
    char *name;
    char *surname;
    list_t *next;
};

int compare(list_t *t, list_t *n) {
    if (t == NULL) {
        return 1;
    }
    if (strcmp(t->surname, n->surname) != 0) {
        return strcmp(t->surname, n->surname);
    }
    return strcmp(t->name, n->name);
}
int orderInsert(list_t **list, char *surname, char *name) {
    list_t *t = malloc(sizeof(list_t));
    t->name = malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(t->name, name);
    t->surname = strdup(surname);
    t->next = NULL;

    if (compare(*list, t) == 0) {
        return 0;
    }
    if (compare(*list, t) > 0) {
        t->next = (*list);
        *list = t;
        return 1;
    }

    for (list_t *p = *list; p != NULL; p = p->next) {
        if (compare(p->next, t) > 0) {
            t->next = p->next;
            p->next = t;
            return 1;
        }
        else if (compare(p->next, t)  == 0) {
            return 0;
        }
    }

}

int main() {
    list_t *h = NULL;
    orderInsert(&h, "wu", "tianjian");
    orderInsert(&h, "gao", "nan");
    orderInsert(&h, "he", "nan");
    orderInsert(&h, "zhao", "shilong");

    for (list_t *t = h; h != NULL; h = h->next) {
        printf("%s %s \n", h->surname, h->name);
    }
}