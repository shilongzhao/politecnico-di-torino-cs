//
// Created by Shilong Zhao on 05/01/2018.
//

#include <stddef.h>
#include <stdlib.h>
#include <printf.h>

typedef struct list_t list_t;
struct list_t {
    int k;
    list_t *next;
};


void insert(list_t **head, int i) {
    list_t *t = malloc(sizeof(list_t));
    t->k = i;
    t->next = *head;
    *head = t;
}


void print_list(list_t *head) {
    if (head == NULL) {
        return;
    }
    printf("%d ", head->k);
    print_list(head->next);
}

void split (list_t **p, int threshold, list_t **p1, list_t **p2) {
    list_t *t1 = NULL;
    list_t *t2 = NULL;

    for ( ; *p != NULL; *p = (*p)->next) {
        if ((*p)->k < threshold) {
            if (*p1 == NULL) {
                *p1 = *p;
                t1 = *p;
            }
            else {
                t1->next = *p;
                t1 = *p;
            }
        }
        else {
            if (*p2 == NULL) {
                *p2 = *p;
                t2 = *p;
            }
            else {
                t2->next = *p;
                t2 = *p;
            }
        }
    }
    if (t1 != NULL) {
        t1->next = NULL;
    }
    if (t2 != NULL) {
        t2->next = NULL;
    }
}

int main() {
    list_t *p = NULL;
    list_t *p1 = NULL;
    list_t *p2 = NULL;

    insert(&p, 37);
    insert(&p, 10);
    insert(&p, -5);
    insert(&p, 9);
    insert(&p, 2);
    insert(&p, 25);
    insert(&p, 8);
    insert(&p, 7);

    split(&p, 18, &p1, &p2);

    print_list(p1);
    print_list(p2);
}



