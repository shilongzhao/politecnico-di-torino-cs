//
// Created by Shilong Zhao on 05/01/2018.
//

#include <stdlib.h>
#include <printf.h>
#include <string.h>

#define NR 4
#define NC 4

typedef struct list_t list_t;
typedef struct matr_t matr_t;

struct list_t {
    int c;
    float v;
    list_t *next;
};

struct matr_t {
    int nr;
    int nc;
    list_t *arr[NR];
};

list_t *insert(list_t *head, list_t *t) {
    t->next = head;
    return t;
}

void matInsert (matr_t *mat, int r, int c, float val) {
    list_t *t = malloc(sizeof(list_t));
    t->v = val;
    t->c = c;
    t->next = NULL;
//    list_t *h  = mat->arr[r];
//    h = insert(h, t);
     mat->arr[r] = insert(mat->arr[r], t);
}

void print_list(list_t *head) {
    for (list_t *t = head; t != NULL; t = t->next) {
        printf("%f ", t->v);
    }
}

int main() {
    matr_t *mat = malloc(sizeof(matr_t));
    for (int i = 0; i < NR; i++) {
        mat->arr[i] = NULL;
    }
    mat->nr = NR;
    mat->nc = NC;
    matInsert(mat, 0, 1, 0.5);
    matInsert(mat, 0, 3, 0.2);
    print_list(mat->arr[0]);
}