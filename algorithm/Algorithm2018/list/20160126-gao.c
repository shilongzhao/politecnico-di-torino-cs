#include <stdio.h>
#include <stdlib.h>
#define R 4
#define C 5

typedef struct col_s{
    int col;
    float value;
    struct col_s *next;
}col_t;

typedef struct row_s{
    int r;
    col_t *col_next;
    struct row_s *next;
}row_t;

typedef struct matr_s {
    int row;
    int column;
    row_t *row_list;
} matr_t;

row_t *insertR(row_t **head, int r) {
    row_t *t = malloc(sizeof(row_t));
    t->r = r;
    t->col_next = NULL;
    t->next = NULL;

    if (*head == NULL || (*head)->r > r) {
        t->next = *head;
        *head = t;
        return t;
    }
    if ((*head)->r == r) {
        return *head;
    }
    row_t *p;
    for (p = *head; p->next != NULL; p = p->next) {
        if (p->next->r == r) {
            return p->next;
        }

        if (p->next->r > r) {
            t->next = p->next;
            p->next = t;
            return t;
        }
    }
    if (p->next == NULL) {
        p->next = t;
        return t;
    }
    return t;
}

col_t *insertC(col_t **head, int c, float value) {
    col_t *t = malloc(sizeof(col_t));
    t->col = c;
    t->next = NULL;
    t->value = value;
    if (*head == NULL || (*head)->col > c) {
        t->next = *head;
        *head = t;
        return t;
    }
    if ((*head)->col == c) {
        (*head)->value = value;
        return *head;
    }
    col_t *p;
    for (p = *head; p->next != NULL; p = p->next) {
        if (p->next->col == c) {
            p->next->value = value;
            return p->next;
        }
        if (p->next->col > c) {
            t->next = p->next;
            p->next = t;
            return t;
        }
    }
    if (p->next == NULL) {
        p->next = t;
        return t;
    }
    return t;
}

void mat_insert(matr_t *mat, int r, int c, float value){
    row_t *pr = insertR(&(mat->row_list), r);
    insertC(&(pr->col_next), c, value);
}

void print_list(matr_t *head){
    row_t *phead;
    for(phead = head->row_list; phead != NULL; phead = phead->next){
        printf("row %d:\n", phead->r);
        col_t *pphead;
        for(pphead = phead->col_next; pphead != NULL; pphead = pphead->next){
            printf("column %d, val= %.2f\n", pphead->col, pphead->value);
        }
    }
}
int main()
{
    matr_t *head = malloc(sizeof(matr_t));
    head->row = 4;
    head->column = 5;
    head->row_list = NULL;

    mat_insert(head, 2, 2, 4.8);
    mat_insert(head, 0, 3, 3.2);
    mat_insert(head, 2, 2, 5);
    mat_insert(head, 2, 3, 6);
    mat_insert(head, 2, 0, 1);
    mat_insert(head, 3, 2, -1.8);
    mat_insert(head, 2, 4, 2.5);

    print_list(head);


    return 0;
}
