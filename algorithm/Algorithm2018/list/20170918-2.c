//
// Created by Shilong Zhao on 24/01/2018.
//

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <printf.h>

typedef struct list1_t list1_t;
typedef struct list2_t list2_t;

struct list1_t {
    char *fn;
    list1_t *nf;
    list2_t *np;
};
struct list2_t {
    char *pn;
    list1_t *nf;
    list2_t *np;
};

void insert(list2_t **head, char *pname, char *fname) {
    list2_t *prod = NULL;
    for (list2_t *t = *head; t != NULL; t = t->np) {
        if (strcmp(pname, t->pn) == 0) {
            prod = t;
            break;
        }
    }

    if (prod == NULL) {
        prod = malloc(sizeof(list2_t));
        prod->pn = strdup(pname);
        prod->nf = NULL;

        prod->np = *head;
        *head = prod;
    }

    list1_t *farmer = malloc(sizeof(list1_t));
    farmer->fn = strdup(fname);
    farmer->np = NULL;

    farmer->nf = prod->nf;
    prod->nf = farmer;

}

void insert1(list1_t **head, char *fname, char *pname) {
    list1_t *farmer = NULL;
    for (list1_t *t = *head; t != NULL; t = t->nf) {
        if (strcmp(fname, t->fn) == 0) {
            farmer = t;
            break;
        }
    }
    if (farmer == NULL) {
        farmer = malloc(sizeof(list1_t));
        farmer->fn = strdup(fname);
        farmer->np = NULL;

        farmer->nf = *head;
        *head = farmer;
    }
    list2_t *prod = malloc(sizeof(list2_t));
    prod->pn = strdup(pname);
    prod->nf = NULL;

    prod->np = farmer->np;
    farmer->np = prod;
}

void list_of_list_invert(list1_t *head1, list2_t **head2) {
    if (head1 == NULL) {
        *head2 = NULL;
        return;
    }
    for (list1_t *f = head1; f != NULL; f = f->nf) {
        for (list2_t *p = f->np; p != NULL; p = p->np) {
            insert(head2, p->pn, f->fn);
        }
    }
}

void print_list1(list1_t *head1) {
    for (list1_t *f = head1; f != NULL; f = f->nf) {
        printf("%s: ", f->fn);
        for (list2_t *p = f->np; p != NULL; p = p->np) {
            printf("%s ", p->pn);
        }
        printf("\n");
    }
}

void print_list2(list2_t *head) {
    for (list2_t *p = head; p != NULL; p = p->np) {
        printf("%s: ", p->pn);
        for (list1_t *f = p->nf; f != NULL; f = f->nf) {
            printf("%s ", f->fn);
        }
        printf("\n");
    }
}
int main() {
    list1_t *head1 = NULL;
    list2_t *head2 = NULL;
    insert1(&head1, "farmer1", "product2");
    insert1(&head1, "farmer1", "product4");
    insert1(&head1, "farmer1", "product5");
    insert1(&head1, "farmer2", "product3");
    insert1(&head1, "farmer2", "product4");
    insert1(&head1, "farmer2", "product2");
    insert1(&head1, "farmer3", "product2");
    insert1(&head1, "farmer3", "product5");
    print_list1(head1);
    list_of_list_invert(head1, &head2);
    print_list2(head2);
}
