//
// Created by Shilong Zhao on 05/01/2018.
//

#include <stddef.h>
#include <stdlib.h>
#include <printf.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *prev;
    node_t *next;
};

void listInsert(node_t **left, node_t **right, int key, int insertRight) {
    node_t *t = malloc(sizeof(node_t));
    t->k = key;
    t->prev = t->next = NULL;
    if (*left == NULL && *right == NULL) {
        *left = *right = t;
        return;
    }

    if (insertRight) {
        t->prev = *right;
        (*right)->next = t;
        *right = t;
        return;
    }
    else {
        t->next = *left;
        (*left)->prev = t;
        *left = t;
        return;
    }
}

void listDisplay (node_t *left, node_t *right, int rightToLeft) {
    if (rightToLeft) {
        for (node_t *t = right; t != NULL; t = t->prev) {
            printf("%d ", t->k);
        }
    }
    else {
        for (node_t *t = left; t != NULL; t = t->next) {
            printf("%d ", t->k);
        }
    }
}
int main() {
    node_t *left = NULL;
    node_t *right = NULL;
    listInsert(&left, &right, 0, 1);
    listInsert(&left, &right, 1, 1);
    listInsert(&left, &right, 2, 1);
    listInsert(&left, &right, 3, 1);
    listInsert(&left, &right, 0, 0);
    listInsert(&left, &right, 1, 0);
    listInsert(&left, &right, 2, 0);
    listInsert(&left, &right, 3, 0);
    listDisplay(left, right, 0);

}

