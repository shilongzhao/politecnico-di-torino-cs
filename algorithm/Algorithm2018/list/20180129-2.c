//
// Created by Shilong Zhao on 30/01/2018.
//

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct list_t list_t;
struct list_t {
    int k;
    list_t *next;
};

void order_insert(list_t *p, int k) {
    for (; p->next != NULL; p = p->next) {
        if (p->next->k > k) {
            list_t *t = malloc(sizeof(list_t));
            t->k = k;
            t->next = p->next;
            p->next = t;
            return;
        }
        else if (p->next->k == k) {
            return;
        }
        else {
            continue;
        }
    }
}

void list_complete(list_t *p) {
    while (p->next != NULL) {
        if (p->next->k - p->k != 1) {
            list_t *t = malloc(sizeof(list_t));
            t->k = p->k + 1;
            t->next = p->next;
            p->next = t;
        }
        p = p->next;
    }
}

void printlist(list_t *l) {
    while (l != NULL) {
        printf("%d ", l->k);
        l = l->next;
    }
    printf("\n");
}

int main() {

    list_t t10 = {10, NULL};
    list_t t7 = {7, &t10};
    list_t t4 = {4, &t7};
    list_complete(&t4);
    printlist(&t4);

}