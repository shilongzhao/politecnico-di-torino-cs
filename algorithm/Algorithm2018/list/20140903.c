//
// Created by Shilong Zhao on 05/01/2018.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node_t node_t;
struct node_t {
    char *str;
    node_t *next;
};

void insert(node_t **head, char *str) {
    // malloc
    node_t *n = malloc(sizeof(node_t));

    if (*head == NULL ||strlen((*head)->str) > strlen(str)) {

    }

    node_t *t;
    for (t = *head; t->next != NULL; t = t->next) {
        if (strlen(t->next->str) > strlen(str)) {
            n->next = t->next;
            t->next = n;
        }
    }
    if (t->next == NULL) {
        t->next = n;
    }

}

node_t *splitStr (char *str) {
    node_t *h = NULL;
    char *token = strtok(str, ".");
    while (token != NULL) {
        node_t *t = malloc(sizeof(node_t));
        t->next = NULL;
        t->str = strdup(token);
        t->next = h;
        h = t;

        token = strtok(NULL, ".");
    }
    printf("str = %s\n", str);
    return h;
}

int main() {
    char str[100] = "a.bb.ccc.dddd.eeeee.fffff";
    node_t *head = splitStr(str);
    for (node_t *h = head; h != NULL; h = h->next) {
        printf("%s ", h->str);
    }
}