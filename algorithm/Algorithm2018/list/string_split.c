//
// Created by Shilong Zhao on 27/01/2018.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node_s{
    char *str;
    struct node_s *next;
}node_t;

void insert(node_t **head, char *token){
    node_t *temp;
    temp = malloc(sizeof(node_t) * 1);
    temp->next = NULL;
    temp->str = strdup(token);

    if((*head) == NULL || strlen(temp->str) < strlen((*head)->str)){
        temp->next = (*head);
        (*head) = temp;
        return;
    }

    node_t *np;
    for(np = (*head); np->next != NULL; np = np->next){
        if(strlen(np->next ->str) > strlen(temp->str)){
            temp->next = np->next;
            np->next = temp;
        }
    }
    if(np->next == NULL){
        np->next = temp;
    }
}

node_t *string_split(char *str){
    node_t *head = NULL;
    char *token = strtok(str, ".");
    while(token != NULL){
        insert(&head, token);

        token = strtok(NULL, ".");
    }

    return head;
}
void print_list(node_t *head){
    node_t *n;
    for(n = head; n != NULL; n = n->next){
        printf("%s\n", n->str);
    }
}

int main()
{
    char str[] = "a.bb.ccc.dddd.eeeee.fffff";
    node_t *phead = NULL;
    phead = string_split(str);

    print_list(phead);

    return 0;
}
