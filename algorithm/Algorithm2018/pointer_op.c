//
// Created by ZhaoShilong on 29/12/2017.
//

#include <printf.h>

int sum_array(int *a, int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum += *(a + i);
    }
    return sum;
}

int main() {
    int a[5] = {1,2,3,4,5};
    printf("%d", sum_array(&a[0], 5));
    printf("%d", sum_array(a, 5));
}

