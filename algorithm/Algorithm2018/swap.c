#include <printf.h>

//
// Created by ZhaoShilong on 29/12/2017.
//
void swap(int *x, int *y) {
    printf("swap: x = %p, y = %p\n", x, y);
    int c;
    c = *x;
    *x = *y;
    *y = c;
}

int main() {

    int x = 1, y = 2;
    printf("main: &x = %p, &y = %p\n", &x, &y);
    swap(&x, &y);
    printf("x = %d, y = %d", x, y);

}