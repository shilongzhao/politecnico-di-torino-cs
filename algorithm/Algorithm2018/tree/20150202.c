
#include <printf.h>
#include <string.h>

#define N 2
typedef struct node_t node_t;
struct node_t {
    int key;
    node_t *child[N];
};

void visitLevel(node_t *root, int level, int k) {
    if (root == NULL) {
        return;
    }
    if (k == level) {
        printf("%d ", root->key);
        return;
    }
    for (int i = 0; i < N; i++) {
        visitLevel(root->child[i], level, k+1);
    }
}

void visitLevelByLevel (struct node_t *root, int l1, int l2) {
    for (int i = l1; i <= l2; i++) {
        visitLevel(root, i, 0);
        printf("\n");
    }
}

int main() {
    node_t nodes[11];
    memset(nodes, 0, sizeof(node_t) * 11);
    for (int i = 0; i < 11; i++) {
        nodes[i].key = i;
    }
    for (int i = 0; 2*i + 2 < 11; i++) {
        nodes[i].child[0] = &nodes[2*i + 1]; // 0 - 1,2, 1->3,4, 2->5,6, 3->7,8, 4->9, 10
        nodes[i].child[1] = &nodes[2*i + 2];
    }
    visitLevelByLevel(&nodes[0], 2, 3);

}