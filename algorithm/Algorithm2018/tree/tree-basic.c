//
// Created by Shilong Zhao on 06/01/2018.
//

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct tree_t tree_t;
struct tree_t {
    int k;
    tree_t *left;
    tree_t *right;
};

// insert, search/print, delete

void preorder(tree_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%d ", root->k);
    preorder(root->left);
    preorder(root->right);
}

void inorder(tree_t *root) {
    if (root == NULL) {
        return;
    }
    inorder(root->left);
    printf("%d ", root->k);
    inorder(root->right);

}

void postorder(tree_t *root) {
    if (root == NULL) {
        return;
    }
    postorder(root->left);
    postorder(root->right);
    printf("%d ", root->k);
}

// Binary Search Tree (BST) insert
tree_t *insert(tree_t *root, int k) {

    if (root == NULL) {
        tree_t *t = malloc(sizeof(tree_t));
        t->k = k;
        t->left = t->right = NULL;
        return t;
    }

    if (k == root->k) {
        return root;
    }

    if (k > root->k) {
        root->right = insert(root->right, k);
    }
    else {
        root->left = insert(root->left, k);
    }
    return root;
}

void insert2(tree_t **root, int k) {
    if (*root == NULL) {
        tree_t *t = malloc(sizeof(tree_t));
        t->k = k;
        t->left = t->right = NULL;
        *root = t;
        return;
    }
    if (k == (*root)->k) {
        return;
    }

    if (k > (*root)->k) {
        insert2(&((*root)->right), k);
    }
    else {
        insert2(&((*root)->left), k);
    }
}

int main() {
    tree_t *root = NULL;
    insert2(&root, 8);
    insert2(&root, 10);
    insert2(&root, 12);
    insert2(&root, 5);
    insert2(&root, 3);
    insert2(&root, 6);

    preorder(root);
}