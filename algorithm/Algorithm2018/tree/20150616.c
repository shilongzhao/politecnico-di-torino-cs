#include <stdlib.h>
#include <printf.h>

//
// Created by Shilong Zhao on 08/01/2018.
//
typedef struct node node;
struct node {
    int k;
    node *left;
    node *right;
};

void doubleTree (struct node *root) {
    if (root == NULL) {
        return;
    }
    doubleTree(root->left);
    doubleTree(root->right);

    node *t = malloc(sizeof(node));
    t->k = root->k;
    t->left = root->left;
    t->right = NULL;
    root->left = t;
}

void doubleTreeInorder(node *root) {
    if (root == NULL) {
        return;
    }
    doubleTreeInorder(root->left);

    node *t = malloc(sizeof(node));
    t->k = root->k;
    t->right = NULL;
    t->left = root->left;
    root->left = t;

    doubleTreeInorder(root->right);
}
void postorder(node *root) {
    if (root == NULL) {
        return;
    }

    postorder(root->left);
    postorder(root->right);

    printf("%d ", root->k);
}
int main() {
    node *nodes[3];
    for (int i = 0; i < 3; i++) {
        nodes[i] = malloc(sizeof(node));
        nodes[i]->left = nodes[i]->right = NULL;
        nodes[i]->k = i + 1;
    }
    nodes[1]->left = nodes[0];
    nodes[1]->right = nodes[2];
    doubleTreeInorder(nodes[1]);
    postorder(nodes[1]);
}