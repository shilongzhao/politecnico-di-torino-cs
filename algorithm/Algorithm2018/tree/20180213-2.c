#include <stdlib.h>
#include <stdio.h>
typedef struct node node;
struct node {
    int k;
    node *left;
    node *right;
};

int isomorph(struct node *t1, struct node *t2) {
    if (t1 == NULL && t2 == NULL) {
        return 1;
    }
    if (t1 != NULL && t2 == NULL) {
        return 0;
    }

    if (t1 == NULL && t2 != NULL) {
        return 0;
    }

    return (t1->k == t2->k
            && isomorph(t1->left, t2->left)
            && isomorph(t1->right, t2->right));
}

int subtree_check(node *t1, node *t2) {
    if (isomorph(t1, t2)) {
        return 1;
    }
    return subtree_check(t1, t2->left)
           || subtree_check(t1, t2->right);
}
