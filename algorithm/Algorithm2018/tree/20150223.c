//
// Created by Shilong Zhao on 06/01/2018.
//
#include <stdio.h>
typedef struct node node;

struct node {
    int key;
    struct node *left;
    struct node *right;
};

void printPath (struct node *root, int h, int l, node *path[]) {
    if (l >= h || root == NULL) {
        return;
    }
    path[l] = root;
    if (root->left == NULL && root->right == NULL) {
        for (int i = 0; i <= l; i++) {
            printf("%d ", path[i]->key);
        }
        printf("\n");
        return;
    }

    printPath(root->left, h, l + 1, path);
    printPath(root->right, h, l + 1, path);
}

int main() {
    node nodes[11];
    for (int i = 0; i <= 10; i++) {
        nodes[i].key = i;
        nodes[i].left = NULL;
        nodes[i].right = NULL;
    }
    for (int i = 0; i <= 4; i++) {
        nodes[i].left = &nodes[2*i + 1];
//        nodes[i].right = &nodes[2*i + 2];
    }
        node *path[4] = {};
    printPath(&nodes[0], 4, 0, path);
}