//
// Created by Shilong Zhao on 09/01/2018.
//

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <printf.h>

typedef struct bst_t bst_t;
typedef struct list1_t list1_t;
typedef struct list2_t list2_t;
struct bst_t {
    char *model;
    list1_t *dealers;

    bst_t *left;
    bst_t *right;
};

struct list1_t {
    char *dealerName;
    float price;
    list1_t *next;
};

struct list2_t {
    char *model;
    char *dealerName;
    float minPrice;
    list2_t *next;
};

list1_t *min(list1_t *head) {
    list1_t *m = head;
    for (list1_t *t = head; t != NULL; t = t->next) {
        if (t->price < m->price) {
            m  = t;
        }
    }
    return m;
}
void bst2list(bst_t *bst, list2_t **p) {
    if (bst == NULL) {
        return;
    }
    bst2list(bst->right, p);
    list1_t *m = min(bst->dealers);
    if (m != NULL) {
        list2_t *tmp = malloc(sizeof(list2_t));
        tmp->dealerName = strdup(m->dealerName);
        tmp->minPrice = m->price;
        tmp->model = strdup(bst->model);

        tmp->next = *p;
        *p = tmp;
    }
    bst2list(bst->left, p);
}

int main() {
    list1_t fiat_a = {"dealerA", 1110, NULL};

    list1_t fiat_b = {"dealerB", 1109, &fiat_a};

    list1_t fiat_c = {"dealerC", 1101, &fiat_b};

    list1_t *m = min(&fiat_c);

    if ((strcmp(m->dealerName, "dealerB") == 0) && (m->price == 1109)) {

        printf("[OK] min\n");

    }

    list1_t volk_a = {"dealer_V", 11111, NULL};

    bst_t volk = {"VOLKSWAGEN", &volk_a, NULL, NULL};



    list1_t bmw_a = {"dealer_B", 22222, NULL};

    bst_t bmw = {"BMW", &bmw_a, NULL, NULL};



    bst_t fiat = { .dealers = &fiat_c, .model = "FIAT", .left = &bmw, .right = &volk};

    list2_t *p = NULL;

    bst2list(&fiat, &p);

    while(p != NULL) {

        printf("%s %s %.2f\n", p->model, p->dealerName, p->minPrice);

        p = p->next;

    }

}