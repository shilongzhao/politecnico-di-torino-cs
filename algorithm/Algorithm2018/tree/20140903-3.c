//
// Created by Shilong Zhao on 09/01/2018.
//

#include <stddef.h>
#include <printf.h>

typedef struct node_t node_t;

struct node_t {
    char *name;
    char *surname;
    int grade;
    node_t *first_child;
    node_t *sibling;
};

void iterate(node_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%s %s %d", root->name, root->surname, root->grade);
    iterate(root->first_child);
    iterate(root->sibling);
}