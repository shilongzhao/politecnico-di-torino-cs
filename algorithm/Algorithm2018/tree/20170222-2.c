//
// Created by Shilong Zhao on 09/01/2018.
//

#include <stddef.h>
#include <stdlib.h>
#include <printf.h>

typedef struct element element;
struct element {
    int k;
    element *fc;
    element *fs;
};

typedef struct node node;
struct node {
    int k;
    int nc;
    node *p, *fc, *fs;
};

void processTree(struct element *root, struct node **r, struct node *p) {
    if (root == NULL) {
        *r = NULL;
        return;
    }
    *r = malloc(sizeof(node));
    (*r)->k = root->k;
    (*r)->fc = (*r)->fs =NULL;
    (*r)->nc = 0;
    (*r)->p = p;
    if (p != NULL) {
        p->nc++;
    }
    processTree(root->fc, &((*r)->fc), *r);
    processTree(root->fs, &((*r)->fs), p);
}

void printNode(node *r) {
    if (r == NULL) {
        return;
    }
    printf("node%d: children=%d, parent=%d\n", r->k, r->nc, r->p == NULL? 0: r->p->k);
    printNode(r->fc);
    printNode(r->fs);
}

int main() {
    node *r = NULL;
    element elements[4];
    for (int i = 0; i < 4; i++) {
        elements[i].k = i + 1;
        elements[i].fc = elements[i].fs = NULL;
    }
    elements[0].fc = &elements[1];
    elements[1].fc = &elements[2];
    elements[1].fs = &elements[3];

    processTree(&elements[0], &r, NULL);
    printNode(r);
}