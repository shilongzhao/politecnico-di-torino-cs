//
// Created by Shilong Zhao on 08/01/2018.
//

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct node node;
struct node {
    char *str;
    node *left;
    node *right;
};

int isomorph(struct node *t1, struct node *t2) {
    if (t1 == NULL && t2 == NULL) {
        return 1;
    }
    if (t1 != NULL && t2 == NULL) {
        return 0;
    }

    if (t1 == NULL && t2 != NULL) {
        return 0;
    }

    return (strcmp(t1->str, t2->str) == 0
            && isomorph(t1->left, t2->left)
            && isomorph(t1->right, t2->right));
}

int main() {
    node nodes[3];
    for (int i = 0; i < 3; i++) {
        nodes[i].str = malloc(sizeof(char) * 10);
        nodes[i].left = nodes[i].right = NULL;
        sprintf(nodes[i].str, "str %d", i);
        printf("%s\n", nodes[i].str);

    }
    nodes[0].left = &nodes[1];
    nodes[0].right = &nodes[2];
    printf("tree isomorphic %d\n", isomorph(&nodes[0], &nodes[1]));

}