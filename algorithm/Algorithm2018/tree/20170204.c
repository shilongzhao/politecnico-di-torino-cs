//
// Created by Shilong Zhao on 09/01/2018.
//

#include <stddef.h>
#include <printf.h>

typedef struct node node;
struct node {
    int k;
    struct node *left;
    struct node *right;
};

struct node *treeMirror(struct node *root){
    if (root == NULL) {
        return NULL;
    }


    treeMirror(root->left);
    treeMirror(root->right);
    node *t = root->left;
    root->left = root->right;
    root->right = t;

}

void preorder(node *root) {
    if (root == NULL) {
        return;
    }
    printf("%d ", root->k);
    preorder(root->left);
    preorder(root->right);
}
int main() {
    node t1, t2, t3;
    t2.left = &t1;
    t2.right = &t3;
    t1.k = 1;
    t2.k = 2;
    t3.k = 3;
    t1.left = t1.right = t3.left = t3.right = NULL;
    preorder(&t2);
    printf("\n");
    treeMirror(&t2);
    preorder(&t2);
}