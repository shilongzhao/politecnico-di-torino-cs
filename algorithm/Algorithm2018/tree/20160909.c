//
// Created by Shilong Zhao on 08/01/2018.
//

#include <stddef.h>
#include <printf.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *left;
    node_t *right;
};

node_t *lowest_ancestor(node_t *root, int k1, int k2) {
    if (root == NULL) {
        return NULL;
    }
    if (root->k < k1) {
        return lowest_ancestor(root->right, k1, k2);
    }
    else if (root->k > k2) {
        return lowest_ancestor(root->left, k1, k2);
    }
    else {
        return root;
    }
}

int dist(node_t *root, int k) {
    if (root->k == k) {
        return 0;
    }
    if (root->k > k) {
        return (1 + dist(root->left, k));
    }
    else {
        return (1 + dist(root->right, k));
    }
}

int dist2(node_t *node, int k) {
    int dist = 0;
    while (node->k != k) {
        if (node->k > k) {
            node = node->left;
        }
        else {
            node = node->right;
        }
        dist++;
    }
    return dist;
}

int distance (node_t *root, int key1, int key2) {
    node_t *lowest = lowest_ancestor(root, key1, key2);
    if (lowest == NULL) {
        return -1;
    }

    return dist2(lowest, key1) + dist2(lowest, key2);
}

int main() {
    node_t t1, t2, t3;
    t2.left = &t1;
    t2.right = &t3;
    t1.k = 1;
    t2.k = 2;
    t3.k = 3;
    t1.left = t1.right = t3.left = t3.right = NULL;
    printf("%d", distance(&t2, 1, 3));
}
