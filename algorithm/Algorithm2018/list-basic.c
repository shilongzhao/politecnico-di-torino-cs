
#include <stddef.h>
#include <stdlib.h>
#include <printf.h>

//
// Created by Shilong Zhao on 04/01/2018.
//
typedef struct list_t list_t;

struct list_t {
    int k;
    list_t *next;
};

void insert(list_t **head, int i);
list_t *insert2(list_t *head, int i);

void print_list(list_t *head);
void print_list2(list_t *head);
void print_list_reverse(list_t *head);

int main() {
    list_t *head = NULL;
    for (int i = 0; i < 10; i++) {
        insert(&head, i);
    }

    for (int i = 10; i < 20; i++) {
        head = insert2(head, i);
    }
    print_list2(head);
}

void insert(list_t **head, int i) {
    list_t *t = malloc(sizeof(list_t));
    t->k = i;
    t->next = *head;
    *head = t;
}

list_t *insert2(list_t *head, int i) {
    list_t *t = malloc(sizeof(list_t));
    t->k = i;
    t->next = head;
    return t;
}

void print_list(list_t *head) {
    if (head == NULL) {
        return;
    }
    printf("%d ", head->k);
    print_list(head->next);
}

void print_list_reverse(list_t *head) {
    if (head == NULL) {
        return;
    }
    print_list_reverse(head->next);
    printf("%d ", head->k);
}

void print_list2(list_t *head) {
    for (list_t *t = head; t != NULL; t = t->next) {
        printf("%d ", t->k);
    }
}
