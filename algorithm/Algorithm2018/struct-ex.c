//
// Created by ZhaoShilong on 29/12/2017.
//

#include <printf.h>
#include <stdlib.h>

typedef struct student_t student_t;

struct student_t {
    char *name; // 4 bytes
    double grade; // 8 bytes
};


void f(student_t s1, student_t s2) {

}

void g(student_t *p1, student_t *p2) {

}

int main() {
    student_t *p1, *p2;

    student_t s1, s2;

    p1 = malloc(sizeof(student_t));
    (*p1).name = "zhang";
    p1->name = "zhang";

    (*p1).grade = 100;
    p1->grade = 100;
}