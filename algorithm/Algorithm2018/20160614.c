#include <stdio.h>
#include <stdlib.h>

void matMax (int **m, int r, int c) {
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            int f = 1;

            for (int p = i - 1; p <= i + 1; p++) {
                for (int q = j - 1; q <= j + 1; q++) {
                    if ((p == i && q == j) || p < 0 || p >= r || q < 0 || q >= c) {
                        continue;
                    }
                    if (m[i][j] <= m[p][q]) {
                        f = 0;
                        break;
                    }
                }
            }
            if (f) {
                printf("%d, %d\n", i, j);
            }
        }
    }
}
int main() {
    int m[3][4] = {
            {5,2,3,1},
            {3,1,6,4},
            {3,0,5,2}
    };
    int **mm;
    mm = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        mm[i] = malloc(sizeof(int) * 4);
    }
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            mm[i][j] = m[i][j];
        }
    }
    matMax(mm, 3, 4);
    return 0;
}