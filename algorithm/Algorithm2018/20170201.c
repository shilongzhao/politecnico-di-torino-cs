//
// Created by Shilong Zhao on 03/01/2018.
//

#include <stdlib.h>
#include <printf.h>
#include <string.h>

void frame_sum(int **mat, int n, int **vet){
    *vet = malloc(sizeof(int) * (n+1) / 2);
    for (int x = 0; x <= n / 2; x++) {
        int sum = 0;
        for (int i = n/2 - x; i <= n/2 + x; i++) {
            for (int j = n/2 - x; j <= n/2 + x; j++) {
                sum += mat[i][j];
            }
        }
        (*vet)[(n+1)/2 - 1 - x] = sum;
    }
    for (int i = 0; i < (n + 1)/2 - 1; i++) {
        (*vet)[i] -= (*vet)[i+1];
    }
}

int min(int a, int b) {
    return a < b? a: b;
}
void frame_sum2(int **mat, int n, int **vet){
    *vet = malloc(sizeof(int) * (n+1) / 2);
    memset(*vet, 0, sizeof(int) * (n + 1) / 2);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            int m;
            if (i + j < n) {
                m = min(i, j);
            }
            else {
                m = min(n-i-1 , n-j-1) ;
            }
            (*vet)[m] += mat[i][j];
        }
    }
}
int main() {
    int *vet = NULL;
    int m[5][5] = {
            {5,2,3,1,5},
            {3,1,6,4,1},
            {3,0,5,2,1},
            {1,1,1,1,1},
            {1,1,1,1,1}
    };
    int **mm;
    mm = malloc(sizeof(int *) * 5);
    for (int i = 0; i < 5; i++) {
        mm[i] = malloc(sizeof(int) * 5);
    }
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            mm[i][j] = m[i][j];
        }
    }
    frame_sum2(mm, 5, &vet);
    for (int i = 0; i <= 2; i++) {
        printf("%d\n", vet[i]);
    }
}