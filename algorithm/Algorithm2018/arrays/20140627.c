//
// Created by ZhaoShilong on 30/12/2017.
//

#include <printf.h>

int search (int *vet1, int *vet2, int d1, int d2) {
    for (int i = 0; i < d1 - d2; i++) {
        int match = 1;
        for (int j = 0; j < d2; j++) {
            if (vet2[j] != vet1[i+j]) {
                match = 0;
                break;
            }
        }
        if (match == 1) {
            return i;
        }
    }
    return -1;
}

int main() {
    int a[10] = {1,2,3,4,5,6,7,8,9,0};
    int b[3] = {4,4,5};
    printf("%d\n", search(a, b, 10, 3));
}