#include <printf.h>

//
// Created by ZhaoShilong on 31/12/2017.
//
void invertSequence (int *v1, int n, int *v2) {
    int start = 0, end = 0;

    while (end < n) {
        while (end < n - 1 && v1[end] < v1[end + 1]) {
            end++;
        }
        for (int i = end, j = start; i >= start; i--, j++) {
            v2[j] = v1[i];
        }
        start = end + 1;
        end = end + 1;
    }
}

int main() {
    int a[12] = {1, 2, 3, 4, 5, 4, 0, 12, 13, 14, 15, 3};
    int b[12];
    invertSequence(a, 12, b);
    for (int i = 0; i < 12; i++) {
        printf("%d ", b[i]);
    }
}

