//
// Created by ZhaoShilong on 01/01/2018.
//

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

void mult(int *v1, int *v2, int n, int **pv) {
    int *t = malloc(sizeof(int) * 2 * n);
    *pv = t;
    for (int i = 0; i < 2*n; i++) {
        t[i] = 0;
    }

    for (int i = 0; i < n; i++) {
        int c = 0;
        for (int j = 0; j < n; j++) {
            t[i+j] = v1[i] * v2[j] + c + t[i+j];
            c = t[i+j] / 10;
            t[i+j] = t[i+j] % 10;
        }
        t[i+n] = c + t[i+n];
    }
}

int main() {
    int *pv = NULL;
    int v1[3] = {2,3,0};
    int v2[3] = {3,4,2};
    mult(v1, v2, 3, &pv);
    for (int i = 0; i < 6; i++) {
        printf("%d", pv[i]);
    }
}