//
// Created by ZhaoShilong on 14/02/2018.
//
#include <stdio.h>

int min(int a, int b) {
    if (a < b) {
        return a;
    }
    return b;
}

int submat_write(int **m, int r, int c) {
    for (int size = 2; size <= min(r, c); size++) {

        for (int i = 0; i <= r - size; i++) {
            for (int j = 0; j <= c - size; j++) {
                // print sub matrix
                for (int p = 0; p < size; p++) {
                    for (int q = 0; q < size; q++) {
                        printf("%d ", mat[i+p][j+q]);
                    }
                    printf("\n");
                }

            }
        }
    }

}