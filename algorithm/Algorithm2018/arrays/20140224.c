//
// Created by ZhaoShilong on 30/12/2017.
//

#include <stdlib.h>
#include <printf.h>

void rotate(int *a, int n, int p) {
    int *t = malloc(sizeof(int) * abs(p));
    if (p > 0) {
        for (int i = 0, j = n - p; i < p; i++, j++) {
            t[i] = a[j];
        }
        for (int i = n - p - 1, j = n - 1; i >= 0; i--, j--) {
            a[j] = a[i];
        }
        for (int i = 0; i < p; i++) {
            a[i] = t[i];
        }
    }
    else {
        p = -p;
        for (int i = 0; i < p; i++) {
            t[i] = a[i];
        }
        for (int i = p; i < n; i++) {
            a[i - p] = a[i];
        }
        for (int i = 0, j = n - p ; i < p; i++, j++) {
            a[j] = t[i];
        }
    }
}

int main() {
    int a[5] = {1,2,3,4,5};
    rotate(a, 5, 2);
    for (int i = 0; i < 5; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");

    rotate(a, 5, -2);
    for (int i = 0; i < 5; i++) {
        printf("%d ", a[i]);
    }
}
