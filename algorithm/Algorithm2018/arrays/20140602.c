#include <printf.h>
#include <stdlib.h>

//
// Created by ZhaoShilong on 30/12/2017.
//
// a: 3, 6, 9
// b: 1, 4
// c: 7, 8
// result: 1, 3, 4, 6, 7, 8, 9
int *merge3 (int *a, int *b, int *c, int na, int nb, int nc) {
    int *result = malloc(sizeof(int) * (na + nb + nc));
    int i = 0, j = 0, k = 0;
    int p  = 0;
    for (p = 0; p < na + nb + nc; p++) {
        if ((i < na)
            && (j >= nb || a[i] <= b[j])
            && (k >= nc || a[i] <= c[k])) {
            result[p] = a[i];
            i++;
        } else if ((j < nb)
                   && (i >= na || b[j] <= a[i])
                   && (k >= nc || b[j] <= c[k])) {
            result[p] = b[j];
            j++;
        } else {
            result[p] = c[k];
            k++;
        }
    }
    return result;
}

int main(int argc, char *argv[]) {
    int a[3] = {3,6,9};
    int b[2] = {1,4};
    int c[2] = {7,8};
    int *r = merge3(a, b, c, 3, 2, 2);
    for (int i = 0; i < 7; i++) {
        printf("%d ", r[i]);
    }
}