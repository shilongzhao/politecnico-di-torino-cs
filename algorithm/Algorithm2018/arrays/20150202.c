//
// Created by ZhaoShilong on 31/12/2017.
//


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void eraseDuplicate(char str[]) {
    char *t = malloc(sizeof(char) * (strlen(str) + 1));
    int l = 0;
    for (int i = 0; i < strlen(str); i++) {
        int f = 0;
        for (int j = 0; j < l; j++) {
            if (*(str+i) == t[j]) {
                f = 1;
                break;
            }
        }
        if (f == 0) {
            t[l++] = str[i];
        }
    }
    t[l] = '\0';
    strcpy(str, t);
}

void eraseDup(char *str) {
    int dict[256] = {};
    int i = 0, j = 0;
    // a;b\0;bbbab;
    // c -> dict[c] = ?
    for (int j = 0; j < strlen(str); j++) {
        if (dict[str[j]] == 0) {
            str[i] = str[j];
            i++;
            dict[str[j]] = 1;
        }
    }
    str[i] = '\0';
}

int main() {
    char str[20] = "aa;;;bbbab;";
    eraseDup(str);
    printf("%s\n", str);
    for (int i = 0; i < 11; i++) {
        printf("%c", str[i]);
    }
}