//
// Created by ZhaoShilong on 01/01/2018.
//

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
char *charErase (char *str, int *pos) {
    int *deleted = malloc(sizeof(int) * (strlen(str)));
    memset(deleted, 0, (strlen(str)) * sizeof(int));
    for (int i = 0; i < strlen(str); i++) {
        printf("%d ", deleted[i]);
    }
    for (int i = 0; pos[i] != -1; i++) {
        if (pos[i] < strlen(str)) {
            deleted[pos[i]] = 1;
        }
    }

    char *t = malloc(sizeof(char) * (strlen(str) + 1));
    int j = 0;
    for (int i = 0; i < strlen(str); i++) {
        if (deleted[i] != 1) {
            t[j++] = str[i];
        }
    }
    t[j] = '\0';
    return t;
}

int main() {
    char hello[10] = "Hello!";
    int pos[5] = {3,1,-1};
    printf("\n%s\n", charErase(hello, pos));
}
