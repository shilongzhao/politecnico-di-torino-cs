//
// Created by Shilong Zhao on 30/01/2018.
//
#include <stdio.h>

void subvet_write(int *v, int n) {
    int max_len = 0;
    int len = 0;
    for (int i = 0; i < n; i++) {
        if (v[i] == 0) {
            if (len > max_len) {
                max_len = len;
            }
            len = 0;
        }
        else {
            len++;
        }
    }
    printf("maxlen = %d\n", max_len);
    for (int p = 0; p <= n - max_len; p++) {
        int flag = 1;
        for (int i = 0; i < max_len; i++) {
            if (v[p+i] == 0) {
                flag = 0;
            }
        }
        if (flag) {
            for (int i = p; i < p + max_len; i++) {
                printf("%d ", v[i]);
            }
            printf("\n");
        }

    }
}

int main() {
    int v[20] = {1,0,2,3,-1,
                 0,4,5,0,-2,
                 3,4,8,0,0,
                 0,3,-1,10,6};

    subvet_write(v, 20);

}
