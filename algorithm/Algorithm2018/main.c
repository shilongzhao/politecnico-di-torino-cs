#include <stdio.h>

int sum(int a, int b) {
    return a + b;
}

int sum_array(int a[], int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum += a[i];
    }
    return sum;
}

int main() {
    int x = 15;
    int y = 10;
    int *py;
    int *p;
    p = &x;
    int **pp;
    pp = &p;
    
    *p = 10; // x = 10, dereference

    printf("x == %d, &x == %p\n", x, &x);
    printf ("p == %p, &p == %p\n", p, &p);
    printf ("pp == %p, &pp == %p\n", pp, &pp);

    return 0;
}

