//
// Created by Shilong Zhao on 03/01/2018.
//

#include <stdlib.h>
#include <printf.h>

int sumSubMat(int **mat, int i, int j, int n) {
    int sum = 0;
    for (int p = i; p < i + n; p++) {
        for (int q = j; q < j + n; q++) {
            sum += mat[p][q];
        }
    }
    return sum;
}

int subMatMax(int **mat, int r, int c, int n) {
    int max = 0;
    for (int i = 0; i <= r - n; i++) {
        for (int j = 0; j <= c - n; j++) {
            int sum = sumSubMat(mat, i, j, n);
            if (sum > max) {
                max = sum;
            }
        }
    }
    return max;
}

int main() {
    int m[3][4] = {
            {5,2,3,1},
            {3,1,6,4},
            {3,0,5,2}
    };
    int **mm;
    mm = malloc(sizeof(int *) * 3);
    for (int i = 0; i < 3; i++) {
        mm[i] = malloc(sizeof(int) * 4);
    }
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            mm[i][j] = m[i][j];
        }
    }
    int r = subMatMax(mm, 3, 4, 2);
    printf("%d\n", r);
}