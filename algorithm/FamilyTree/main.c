/*
 *  StQ A&P Examination 08 February 2013
 *
 *  The program can be improved along the following directions:
 *  - name and surname can be dynamic fields
 *  - the children pointers can be avoided resorting only to father and
 *    motehr pointers
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 50+1

typedef enum {M, F} sex;

typedef struct node{
    char name[MAX];
    char surname[MAX];
    sex sex;
    int age;
    struct node *next;
    struct node *mother, *father, *children;
    struct node *nextBySex, *nextByAge;
} node_t;

node_t* readFile(char* filename, int* n_people);
node_t* searchPerson(node_t* rp, char* name, char* surname, int n_people);
node_t* assignParents(node_t* child, node_t* mom, node_t* dad);
node_t* assignChild(node_t* parent, node_t* child);
int findPathByAge(node_t* curr, int age);
int findPathBySex(node_t* curr, int cnt, sex sex);
void printbySex(node_t* rp);
void printbyAge(node_t* rp);
node_t setNULL(node_t pt);

int main (
        int argc,
        char * argv[]
)
{
    node_t *pt, *pathbySex, *pathbyAge;
    int n_people, i, j, max;

    if (argc!=2){
        fprintf(stderr, "Error in passing the arguments. Type <filename>");
        exit(1);
    }

    pt = readFile (argv[1], &n_people);

    /* Path for Males */
    for (max=0, i=0; i<n_people; i++){
        if (pt[i].sex != M) continue;  // Also checked during recursion
        j = findPathBySex (&pt[i], 0, M);
        if (j>max) {
            max = j;
            pathbySex = &pt[i];
        }
    }
    printf ("Path by sex: %d males\n", max);
    printbySex (pathbySex);

    /* Reset Links */
    for (i=0; i<n_people; i++){
        pt[i].nextBySex = NULL;
    }

    /* Path for Females */
    for (max=0, i=0; i<n_people; i++){
        if (pt[i].sex != F) continue;  // Also checked during recursion
        j = findPathBySex (&pt[i], 0, F);
        if (j>max) {
            max = j;
            pathbySex = &pt[i];
        }
    }
    printf ("Path by sex: %d females\n", max);
    printbySex (pathbySex);

    /* Path for Longevity */
    for (i=0, max=0; i<n_people; i++) {
        j = findPathByAge (&pt[i], 0);
        if (j>max) {
            max=j;
            pathbyAge = &pt[i];
        }
    }
    printf ("Path by age: %d years\n", max);
    printbyAge (pathbyAge);

    free(pt);

    return 0;
}

node_t* readFile (char* filename, int *n_people){
    FILE *fp;
    char sex, childN[MAX], childS[MAX];
    char momN[MAX], momS[MAX], dadN[MAX], dadS[MAX];
    int i=0;
    node_t *pt, *current, *curr_mom, *curr_dad;

    fp = fopen (filename, "r");
    if (fp==NULL) {
        fprintf (stderr, "Error in opening file.");
        exit (1);
    }

    if (fscanf (fp, "%d", n_people) == EOF) {
        fprintf (stderr, "Error in reading file.");
        exit (1);
    }

    pt = (node_t *) malloc ((*n_people) * sizeof (node_t));
    if (pt==NULL){
        fprintf (stderr, "Error in allocating the memory.");
        exit (1);
    }

    while (i<*n_people){
        fscanf(fp, "%s %s %c %d", pt[i].name, pt[i].surname, &sex, &pt[i].age);

        pt[i]=setNULL(pt[i]);

        if (sex=='M') {
            pt[i].sex = M;
        } else {
            pt[i].sex = F;
        }

        i++;
    }

    while (fscanf (fp, "%s %s %s %s %s %s",
                   childN, childS, dadN, dadS, momN, momS) != EOF) {
        current = searchPerson (pt, childN, childS, *n_people);
        curr_mom = searchPerson (pt, momN, momS, *n_people);
        curr_dad = searchPerson (pt, dadN, dadS, *n_people);
        current = assignParents (current, curr_mom, curr_dad);
        curr_dad = assignChild (curr_dad, current);
        curr_mom = assignChild (curr_mom, current);
    }

    fclose(fp);

    return (pt);
}

node_t *searchPerson (
        node_t* rp,
        char* name,
        char* surname,
        int n_people
) {
    int i;

    for (i=0; i<n_people; i++){
        if (strcmp(rp[i].name, name)==0 &&
            strcmp(rp[i].surname, surname)==0) {
            return (&rp[i]);
        }
    }

    return NULL;
}

node_t setNULL (
        node_t pt
) {
    pt.mother = pt.father = pt.children = pt.next = NULL;
    pt.nextByAge = pt.nextBySex = NULL;

    return pt;
}

node_t *assignParents (node_t* child, node_t* mom, node_t* dad) {
    child->mother = mom;
    child->father = dad;

    return (child);
}

node_t *assignChild (node_t* parent, node_t* child) {
    child->next = parent->children;
    parent->children = child;

    return (parent);
}

void printbySex (node_t* rp) {
    while (rp!=NULL) {
        printf ("%s %s\n", rp->name, rp->surname);
        rp=rp->nextBySex;
    }

    return;
}

void printbyAge(node_t* rp) {
    while (rp!=NULL) {
        printf("%s %s\n", rp->name, rp->surname);
        rp=rp->nextByAge;
    }

    return;
}

int findPathBySex (
        node_t* curr,
        int cnt,
        sex sex
) {
    int i;
    node_t *pt=curr, *tmp = curr->children;

    if (pt==NULL || pt->sex!=sex)
        return (cnt);

    cnt++;

    while (pt->children != NULL){
        i = findPathBySex (pt->children, cnt, sex);

        if (i > cnt){
            cnt = i;
            (curr)->nextBySex = pt->children;
        }

        pt->children = pt->children->next;
    }

    pt->children = tmp;

    return (cnt);
}

int findPathByAge (
        node_t* curr,
        int age
) {
    int i, max=0;
    node_t *pt=curr, *tmp=curr->children;

    if (pt==NULL) return age;

    if (pt->children==NULL) return age+curr->age;

    while (pt->children != NULL) {
        i=findPathByAge(pt->children, age+pt->age);
        if (i>max){
            max=i;
            (*curr).nextByAge = pt->children;
        }

        pt->children=pt->children->next;
    }

    pt->children=tmp;

    return max;
}




