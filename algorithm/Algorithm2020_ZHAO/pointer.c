//
// Created by zhaos on 3/19/2020.
//

#include <stdio.h>
#include <stdlib.h>

int main() {
    int a = 10;
    printf("value of a = %d, address of a = %p\n", a, &a);

    int *p = &a;
    printf("value of p = %p, address of p = %p\n", p, &p);

    int **p2 = &p;
    printf("value of p2 = %p, address of p2 = %p\n", p2, &p2);

    // pointers with arrays
    int arr[10] = {3,2,1,4,6,5,7,9,8,0};
    printf("starting address of array = %p, address of arr[0] is %p\n", arr, &arr[0]);

    int *pa = arr;
    for (int i = 0; i < 10; i++) {
        printf("value = %d\n", *(pa + i));
    }

    // dynamic allocation memory
    int n;
    scanf("%d", &n);
    int *dp = malloc(n * sizeof(int));
    for (int i = 0; i < n; i++) {
        scanf("%d", &dp[i]);
    }

    char *sep = "";
    for (int i = 0; i < n; i++) {
       printf("%s%d", sep, dp[i]);
       sep = ",";
    }
    printf("\n");

    free(dp);
}