//
// Created by zhaos on 4/19/2020.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct node node;
struct node {
    int v;
    node *next;
};

int main() {
    int a[10] = {1,2,3};
    float f[10] = {1.2f, 3.0f, 4.5f};
    char s[10] = {'a', 'b', 'c'};
    int *p[10] = {NULL, NULL};
    int x = 1;
    int y = 2;
    p[0] = &x;
    p[1] = &y;


    printf("a = %p, address of a[0] = %p\n", a, &a[0]);

    node *na[10];
    na[0] = malloc(sizeof(node));

}