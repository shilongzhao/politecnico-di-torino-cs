//
// Created by zhaos on 4/12/2020.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct node NODE;
struct node{
    int v;
    struct node *next;
};


void print_list(NODE*head){
    char*sep="";
    while(head!=NULL){
        printf("%s%d",sep,head->v);
        sep="-->";
        head = head->next;
    }

}
void int2list(int n,NODE**head){
    int d=1;
    while(n/d>0){
        int x=n/d%10;
        printf("inserting %d \n",x);
        d=10*d;
        NODE*p=malloc(sizeof(NODE));
        p->v=x;
        p->next=*head;
        *head=p;
    }


}

void bar(int *a, int *b, int *c) {
    *a = 1;
    *b = 2;
    *c = 3;
}

struct nums {
    int a;
    int b;
    int c;
};

struct nums *foo() {
    struct nums *result = malloc(sizeof(struct nums));
    result->a = 1;
    result->b = 2;
    result->c = 3;
    return result;
}

int main()
{

    NODE*h1=NULL;
    int2list(23456,&h1);
    print_list(h1);
}