//
// Created by zhaos on 3/19/2020.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
    char s[100]={};
    scanf("%s",s);
    char *ss = malloc(sizeof(char) * (strlen(s) + 1));
    int a[128] = {};

    int k = 0;
    for (int i = 0; i < strlen(s); i++) {
        char c = s[i];
        if (a[c] == 0) {
            ss[k] = c;
            k++;
            a[c] = 1;
        }
    }

    printf("string without duplicates: %s\n", ss);
    free(ss);
}