//
// Created by zhaos on 3/17/2020.
//

#include <stdio.h>

int main() {
    int a[10];
    for (int i = 0; i < 10; i++) {
        scanf("%d", &a[i]);
    }

    printf("Your input is: ");
    char *sep = "";
    for (int i = 0; i < 10; i++) {
        printf("%s%d", sep, a[i]);
        sep = ",";
    }
    printf("\n");

    //TODO: max-length increasing subarray, start, end

    int len = 0;
    int p = 0, q = 0;
    while (q < 10) {
        while (q + 1 < 10 && a[q + 1] > a[q])
            q++;
        int l = q - p + 1;
        if (l > len)
            len = l;
        p = q + 1;
        q = q + 1;
    }

    printf("max length of increasing subarray is: %d\n", len);
}