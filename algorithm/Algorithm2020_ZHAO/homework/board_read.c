
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int R, C;

char ***board_read(char *name) {
    FILE *fp = fopen(name, "r");
    if (fp == NULL) {
        printf("cannot open file %s", name);
        return NULL;
    }
    if (fscanf(fp, "%d %d", &R, &C) != 2) {
        printf("error reading matrix size");
        return NULL;
    }
    char ***m = calloc(R, sizeof(char **));
    if (m == NULL) {
        printf("failed to allocate memory");
        return NULL;
    }
    for (int i = 0; i < R; i++) {
        m[i] = calloc(C, sizeof(char *));
        if (m[i] == NULL)
            return NULL;
    }
    char str[100];
    int r, c;
    while (fscanf(fp, "%d %d %s", &r, &c, str) == 3) {
        m[r][c] = strdup(str);
    }
    return m;
}


int main() {
    // !!you need a file named `board.txt` under the same dir!!
    char ***m = board_read("board.txt");
    if (m == NULL) {
        printf("ERROR allocate memory");
        return -1;
    }
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < C; j++) {
            if (m[i][j] != NULL) {
                printf("%d %d: %s\n", i, j, m[i][j]);
                free(m[i][j]);
            }
        }
        free(m[i]);
    }
    free(m);
    return 0;
}