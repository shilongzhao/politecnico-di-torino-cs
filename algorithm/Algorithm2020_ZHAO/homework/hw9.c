//
// Created by zhaos on 5/1/2020.
//
#include <stdio.h>

#define N 3

struct node {
    int key;
    struct node *child[N];
};

void printlevel(struct node*root,int n){
    if(root==NULL) return;
    if(n==0){
        printf("---%d---",root->key);
    }
    for(int i=0;i<N;i++){
        printlevel(root->child[i],n-1);
    }
}


void visitLevelByLevel (struct node *root, int l1, int l2){
    for(int i = l1;i < l2;i++){
        printlevel(root,i);
    }

}
int main() {

    struct node n1 = {1, NULL, NULL, NULL};
    struct node n2 = {2, NULL, NULL, NULL};
    struct node n3 = {3, NULL, NULL, NULL};
    struct node n4 = {4, NULL, NULL, NULL};
    n1.child[0] = &n2;
    n1.child[1] = &n3;
    n1.child[2] = &n4;

    visitLevelByLevel(&n1, 0, 2);
}