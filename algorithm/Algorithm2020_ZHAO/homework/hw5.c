//
// Created by zhaos on 3/30/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * if k exists in arr, return 1
 * otherwise, return 0
 *
 * @param k
 * @param arr
 * @param n
 * @return
 */
int exist(int k, int *arr, int n) {
    int flag=0;
    for(int i =0; i<n; i++) {
        if (arr[i] == k) {
            flag = 1;
        }
    }
    if(flag==1){
        return 1;
    }else{
        return 0;
    }
}

char *erase(char *str, int *arr) {
    int n;
    for (n = 0; arr[n] != -1; n++) {

    }
    int len = strlen(str) - n + 1;
    char *output = malloc(len * sizeof(char));

    int m = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (!exist(i, arr, n)) {
            output[m] = str[i];
            m++;
        }
    }
    output[m] = '\0';
    return output;
}

char *erase2(char *s, int *arr) {
    int l = strlen(s);
    char *str = strdup(s);
//    char *str = malloc(sizeof(char) * (strlen(s) + 1));
//    strcpy(str, s);
    int n = 0;
    for (int i = 0; arr[i] != -1; i++) {
        int index = arr[i];
        str[index] = '\0';
        n++;
    }
    char *output = malloc(sizeof(char) * (l - n + 1));
    int m = 0;
    for (int i = 0; i <= l; i++) {
        if (str[i] != '\0') {
            output[m++] = str[i];
        }
    }
    output[m] = '\0';
    return output;
}

void allocate_100() {

}

int main()
{
    char *input_string = "abcdefghi";
//    char input[1000] = "abcedfghi";
    int a[10] = {2,5,7,-1};
    char *s = erase2(input_string,a);
    printf("input_string = %s\n",input_string);
    printf("s = %s\n", s);

    int *p = NULL;
    allocate_100(p);
    allocate_100(&p);
    for (int i = 0; i < 100; i++) {
        p[i] = i;
    }
}
