//
// Created by zhaos on 4/8/2020.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct list1_t list1_t;
struct list1_t {
    int v;
    list1_t *next;
};

typedef struct list2_t list2_t;
struct list2_t {
    int v;
    int f;
    list2_t *next;
};

int freq(list1_t *head, int k) {
    int record = 0;
    for (list1_t *p = head; p != NULL; p = p->next) {
        if (p->v == k) {
            record++;
        }
    }
    return record;
}

int exist(list2_t *head, int k) {
    for (list2_t *p = head; p != NULL; p = p->next) {
        if (p->v == k) {
            return 1;
        }
    }
    return 0;
}

void insert(list2_t **head, int v, int f) {
    list2_t *tmp = malloc(sizeof(list2_t));
    tmp->v = v;
    tmp->f = f;
    tmp->next = *head;
    *head = tmp;
}

// 1->2->1->2
// 1,2 -> 2,2
list2_t *list_compress(list1_t *head) {
    list2_t *h2 = NULL;
    for (list1_t *p = head; p != NULL; p = p->next) {
        if (!exist(h2, p->v)) {
            int fpv = freq(p, p->v);
            insert(&h2, p->v, fpv);
        }
    }
    return h2;
}

////////////////////////////////////////////

void insert_list1(list1_t **head, int v) {
    list1_t *t = malloc(sizeof(list1_t));
    t->v = v;
    t->next = *head;
    *head = t;
}

void print_list2(list2_t *h) {
    char *sep = "";
    for (list2_t *p = h; p != NULL; p = p->next) {
        printf("%s(%d, %d)", sep, p->v, p->f);
        sep = "->";
    }
    printf("\n");
}
int main() {
    list1_t *h1 = NULL;
    insert_list1(&h1, 1);
    insert_list1(&h1, 1);
    insert_list1(&h1, 2);
    insert_list1(&h1, 1);
    insert_list1(&h1, 2);
    insert_list1(&h1, 1);
    insert_list1(&h1, 3);
    insert_list1(&h1, 3);
    insert_list1(&h1, 3);
    insert_list1(&h1, 3);
    insert_list1(&h1, 4);
    insert_list1(&h1, 6);

    list2_t *h2 = list_compress(h1);
    print_list2(h2);
}