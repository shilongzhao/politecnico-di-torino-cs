//
// Created by zhaos on 4/15/2020.
//
#include <stdio.h>
#include <stdlib.h>

typedef struct col_t col_t;
struct col_t {
    int col;
    float val;
    col_t *next_col;
};

typedef struct row_t row_t;
struct row_t {
    int row;
    row_t *next_row;
    col_t *next_col;
};

typedef struct mat_t mat_t;
struct mat_t {
    int row;
    int col;
    row_t *head;
};

row_t *find_row(mat_t *M, int r) {
    for(row_t *p= M->head; p != NULL; p = p->next_row){
        if ((p->row) == r) {
            return p;
        }
    }
    return NULL;
}

col_t *find_col(row_t *row, int c) {
    for (col_t *p = row->next_col; p != NULL; p = p->next_col) {
        if (p->col == c) {
            return p;
        }
    }
    return NULL;
}

void matWrite(mat_t *M, float value, int r, int c) {
    row_t *row = find_row(M, r);
    if (row == NULL) {
        row_t *tmp = malloc(sizeof(row_t));
        tmp->row = r;
        tmp->next_row = M->head;
        tmp->next_col = NULL;
        M->head = tmp;

        col_t *tmp_col = malloc(sizeof(col_t));
        tmp_col->next_col = NULL;
        tmp_col->val = value;
        tmp_col->col = c;

        tmp->next_col = tmp_col;

        return;
    }

    col_t *col = find_col(row, c);
    if (col == NULL) {
        col_t *tmp_col = malloc(sizeof(col_t));
        tmp_col->val = value;
        tmp_col->col = c;
        tmp_col->next_col = row->next_col;
        row->next_col = tmp_col;
        return;
    }

    col->val = value;
}

void matPrint(mat_t *M) {
    for (row_t *r = M->head; r != NULL; r = r->next_row) {
        for (col_t *c = r->next_col; c != NULL; c = c->next_col) {
           printf("(%d,%d):%.2f\n", r->row, c->col, c->val);
        }
    }
}
int main() {
//    mat_t M = {.row = 4,.col = 5, .head = NULL};
//    matWrite(&M, 1.2, 0, 2);
//    matWrite(&M, 2.1, 1, 3);

    mat_t *p = malloc(sizeof(mat_t));
    p->col = 5;
    p->row = 4;
    p->head = NULL;
    matWrite(p, 3.3, 0, 1);
    matWrite(p, 1.2, 0, 2);
    matWrite(p, 2.3, 1, 2);
    matWrite(p, 4.4, 1, 2);
    matPrint(p);
}