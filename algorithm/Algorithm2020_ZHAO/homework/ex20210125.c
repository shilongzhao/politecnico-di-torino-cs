//
// Created by zhaos on 1/26/2021.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int is_same(char *s, char *t, int n) {
    int i = 0;
    while (s[i] != t[0]) {
        i++;
    }
    for (int p = i, q = 0; q < n; p = (p + 1) % n, q = q + 1) {
        if (s[p] != t[q]) return 0;
    }
    return 1;
}

int contains(char **s, char *t, int rows, int n) {
    for (int i = 0; i < rows; i++) {
        if (is_same(s[i], t, n)) return 1;
    }
    return 0;
}

void recur(char *str, char **perms, int *count, int rows, int n, char *s, int d, int *used) {
    if (d == n && !contains(perms, s, rows, n)) {
        perms[*count] = strdup(s);
        *count += 1;
        return;
    }
    for (int i = 0; i < n; i++) {
        if (!used[i]) {
            s[d] = str[i];
            used[i] = 1;
            recur(str, perms, count, rows, n, s, d + 1, used);
            s[d] = '\0';
            used[i] = 0;
        }
    }
}
void circularPermutation(char *str) {
    int n = strlen(str);
    int rows = 1;
    for (int i = 1; i < n; i++) {
        rows = rows * i;
    }

    char **perms = malloc(sizeof(char *) * rows);
    int count = 0;
    char *res = calloc(n + 1, sizeof(char));
    int *used = calloc(n, sizeof(int));

    recur(str, perms, &count, rows, n, res, 0, used);

    for (int i = 0; i < rows; i++) {
        printf("%s\n", perms[i]);
    }

    free(used);
    free(res);
    for (int i = 0; i < rows; i++)
        free(perms[i]);
    free(perms);
}




char ***board_read(char *name) {
    FILE *fp = fopen(name, "r");
    if (fp == NULL) {
        printf("cannot open file %s", name);
        return NULL;
    }
    int R, C;
    if (fscanf(fp, "%d %d", &R, &C) != 2) {
        printf("error reading matrix size");
        return NULL;
    }
    char ***m = calloc(R, sizeof(char **));
    if (m == NULL) {
        printf("failed to allocate memory");
        return NULL;
    }
    for (int i = 0; i < R; i++) {
        m[i] = calloc(C, sizeof(char *));
        if (m[i] == NULL)
            return NULL;
    }
    char str[100];
    int r, c;
    while (fscanf(fp, "%d %d %s", &r, &c, str) == 3) {
        m[r][c] = strdup(str);
    }
    return m;
}

int rec3(int n) {
    int i,res;
    if (n == 0) return 1;
    res = 0;
    for (i = 0; i < n; i++){
        res += rec3(i) * rec3(n - 1 - i);
    }
    return res;
}

void myf(char *in, char *out, int *n) {
    char *tmp1, *tmp2;
    int l;
    out[0] = '\0';
    tmp1 = in;
    while (*tmp1 != '\0') {
        while (*tmp1 == ' ') {
            tmp1++;
        }
        tmp2 = tmp1;
        while (*tmp2 != ' ' && *tmp2 != '\0') {
            tmp2++;
        }
        l = tmp2 - tmp1;
        if (l > strlen(out)) {
            *n = l;
            strncpy(out, tmp1, l);
            out[l] = '\0';
        }
        tmp1 = tmp2;
    }
    return;
}
int main() {
    printf("%d\n", rec3(4));
    char out[100];
    int n;
    myf("this is a very loooong string", out, &n);
    printf("%s %d", out, n);
}