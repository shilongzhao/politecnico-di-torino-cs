//
// Created by zhaos on 4/8/2020.
//
#include <stdio.h>
#include <stdlib.h>
typedef struct node node;
struct node {
    int v;
    node *next;
};

// intput ==> 25891
// 1
// 9
// 8
// 5
// 2
//void printDigit(int n) {
//    int d = 1;
//    while (n / d > 0) {
//        int x = n / d % 10;
//        printf("%d\n", x);
//        d = 10 * d;
//    }
//}
// 23589 ===> 2->3->5->8->9->null

void getDigits(int *arr, int n) {
    int d = 1;
    int i = 0;
    while (n / d > 0) {
        int x = n / d % 10;
        arr[i++] = x;
        d = 10 * d;
    }
}

void int2list(int n, node **head) {
    int arr[10] = {};
    for (int i = 0; i < 10; i++) {
        arr[i] = -1;
    }
    getDigits(arr, n);

    for (int i = 0; arr[i] != -1; i++) {
        node *tmp = malloc(sizeof(node));
        tmp->next = *head;
        tmp->v = arr[i];
        *head = tmp;
    }
}

void print_list(node *head) {
    char *sep = "";
    while(head != NULL) {
        printf("%s%d", sep, head->v);
        sep = "-->";
        head = head->next;
    }
}

void pl(node *head) {
    if (head == NULL) printf("\n");
    else {
        printf("%d-->", head->v);
        pl(head->next);
    }
}

int main() {
//    printDigit(23456);

    node *h1 = NULL;
    int2list(25678, &h1);
    pl(h1);

    printf("\n======\n");

    node *h2 = NULL;
    int2list(78901, &h2);
    pl(h2);
}