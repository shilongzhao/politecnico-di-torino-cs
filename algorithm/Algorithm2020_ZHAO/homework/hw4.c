//
// Created by zhaos on 3/21/2020.
//

#include <stdio.h>

void copy(int *v1, int n, int *v2) {
    for (int i = 0; i < n; i++) {
        v2[i] = v1[i];
    }
}

void reverse_cp(int *v1, int n, int *v2) {
    for (int i = 0; i < n; i++) {
        v2[n - i] = v1[i];
    }
}


/**
 * reverse numbers in v1, from index a to index b, and put in v2, from index start
 * @param v1
 * @param a
 * @param b
 * @param v2
 * @param start
 */
void reverse_range(int *v1, int a, int b, int *v2, int start) {
    for (int i = b, j = start; i >= a; i--, j++) {
        v2[j] = v1[i];
    }
}

void reverse_third(int *v1, int n, int *v2) {
//    for (int i = 0, j = n/3 - 1; j >= 0; i++, j--) {
//       v2[j] = v1[i];
//    }
    reverse_range(v1, 0, n/3 - 1, v2, 0);

    for (int i = n/3, j = 2*n/3 - 1; j >= n/3; i++, j--) {
        v2[j] = v1[i];
    }

    for (int i = 2*n/3, j = n - 1; j >= 2*n/3; i++, j--) {
        v2[j] = v1[i];
    }
}

void reverse_half(int *v1, int n, int *v2) {
    reverse_range(v1, 0, n/2 - 1, v2, 0);

    for (int i = n/2; i < n ; i++) {
        int i2 = n + n/2 - i - 1;
        v2[i2] = v1[i];
    }
}

// 1,2,3,0,8
int is_incr(int *v, int n) {
    for(int i = 0; i < n - 1; i++){
        if(v[i] > v[i+1]) {
            return 0;
        }
    }
    return 1;
}

// 2,3,4,0,2,3,-1,-2,5,6
void find_incr(int *v, int n) {
    int p = -1, q = -1;

    while (p < n - 1 && q < n - 1) {
        p = q + 1, q = q + 1;
        while (q < n - 1 && v[q + 1] > v[q]) {
            q++;
        }
        ////////p , q
        printf("found incr seq: [%d, %d]\n", p, q);
    }

}

void invert_seq(int *v1, int n, int *v2) {
    int p = -1, q = -1;
    while (p < n - 1 && q < n - 1) {
        p = q + 1, q = q + 1;
        while (q < n - 1 && v1[q + 1] > v1[q]) {
            q++;
        }
        reverse_range(v1, p, q, v2, p);
        printf("found incr seq: [%d, %d]\n", p, q);
    }
}


int main() {
    int a[10] = {1,2,3,4,5,0,7,8,9,2}; // [0,4], [5,8], [9,9]
    int b[10];

    invert_seq(a, 10, b);

    char *sep = "";
    for (int i = 0; i < 10; i++) {
        printf("%s%d", sep, b[i]);
        sep = ",";
    }

}