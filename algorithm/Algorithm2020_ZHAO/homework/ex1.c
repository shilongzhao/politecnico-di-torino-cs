//
// Created by zhaos on 4/5/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct student stud_t;
struct student {
    int id;
    char *name;
    char *surname;
    char *major;

    struct student *next;
};

void insert(stud_t **h, int id, char *n, char *s, char *m) {
    stud_t *tmp = malloc(sizeof(stud_t));
    tmp->id = id;
    tmp->name = strdup(n);
    tmp->surname = strdup(s);
    tmp->major = strdup(m);

    tmp->next = *h;
    *h = tmp;
}

/// student(id, name, surname, major)
void print(stud_t *h) {
    for (stud_t *p = h; p != NULL; p = p->next) {
        printf("student(%d, %s, %s, %s)\n", p->id, p->name, p->surname, p->major);
    }
}

int main() {
    stud_t *head = NULL;
    insert(&head, 1, "Alex", "Hello", "CS");
    insert(&head, 2, "Bob", "Ciao", "ME");
    insert(&head, 3, "Luca", "Salute", "EE");
    print(head);
}