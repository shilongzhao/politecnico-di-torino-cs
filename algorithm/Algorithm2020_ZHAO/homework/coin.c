//
// Created by zhaos on 5/22/2020.
//
#include <stdio.h>
#include <limits.h>

int min = INT_MAX;
float best_sol[100] = {};
void recur(float *denom, int *coins, int n, float rest, int d, float *c, int lastPick) {
    if (rest < 0.001 && rest >= -0.001) {
//       for (int i = 0; i < d; i++) {
//           printf("%.2f ", c[i]);
//       }
//       printf("\n");
        if (d < min) {
            min = d;
            for (int i = 0; i < d; i++) {
                best_sol[i] = c[i];
            }
        }
       return;
    }

    for (int i = lastPick; i < n; i++) {
        if (coins[i] > 0) {
            coins[i] -= 1;
            c[d] = denom[i];
            recur(denom, coins, n, rest - denom[i], d + 1, c, i);
            coins[i] += 1;
        }
    }
}

void change_making(float *bold, int *coins, int n, float change) {
    float c[100] = {};
    recur(bold, coins, n, change, 0, c, 0);
}

int main(){

    int b[7]={10,12,1,6,9,4,7};//可用数量
    float a[7]={0.01f, 0.02f,0.05f,0.1f,0.2f,0.5f,1.0f};//可用硬币

    change_making(a, b, 7, 1.22f);
    if (min == INT_MAX) {
        printf("\nNo solution found!\n");
    } else {
        printf("\n BEST SOL \n");
        for (int i = 0; i < min; i++) {
            printf("%.2f ", best_sol[i]);
        }
    }
    printf("\n===============================\n");
    int d[2] = {10,2};
    float denom[2] = {0.1f, 0.5f};
    change_making(denom, d, 2, 1.0f);
    printf("\n BEST SOL \n");

    for (int i = 0; i < min; i++) {
        printf("%.2f ", best_sol[i]);
    }
}