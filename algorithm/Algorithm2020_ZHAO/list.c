//
// Created by zhaos on 4/2/2020.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node node_t;
struct node {
    int v;
    node_t *next;
};

void insert(node_t **head, int k) {
    node_t *t = malloc(sizeof(node_t));
    t->v = k;
    t->next = *head;
    *head = t;
}

node_t *insert2(node_t *head, int k) {
    node_t *t = malloc(sizeof(struct node));
    t->v = k;
    t->next = head;
    return t;
}

void print_list_rec(node_t *head) {
    if (head == NULL) return;
    printf("%d ", head->v);
    print_list_rec(head->next);
}

void print_list_rec_rev(node_t *head) {
    if (head == NULL) return;
    print_list_rec_rev(head->next);
    printf("%d ", head->v);
}

void print_list_v1(node_t *head) {
    printf("====== list nodes ======\n");
    for (node_t *p = head; p != NULL; p = p->next) {
        printf("node(%d)\n", p->v);
    }
    printf("====== end list nodes ======");

}

void print_list(node_t *head) {
    char *sep = "";
    while(head != NULL) {
        printf("%s%d", sep, head->v);
        sep = "-->";
        head = head->next;
    }
    printf("\n");
}

/***
 * return deleted node
 * @param head
 * @param k
 * @return
 */
node_t *delete(node_t **head, int k) {
    node_t *h = *head;
    if (h->v == k) {
        *head = h->next;
        return h;
    }
    for (node_t *pp = *head; pp->next != NULL; pp = pp->next) {
        if (pp->next->v == k) {
            node_t *p = pp->next;
            pp->next = pp->next->next;
            return p;
        }
    }
    return NULL;
}

int main() {
    node_t *head = NULL;

    insert(&head, 2);
    insert(&head, 1);
    insert(&head, 3);
    insert(&head, 4);

    head = insert2(head, 5);
    head = insert2(head, 6);
    printf("\n ======== recursive print ========= \n");
    print_list_rec_rev(head);
    printf("\n");

    node_t *p = delete(&head, 6);
    print_list(head);
    if (p != NULL) {
        printf("deleted node %d\n", p->v);
        free(p);
    }

    delete(&head, 4);
    print_list(head);

    delete(&head, 6);
    printf("\n ======== recursive print ========= \n");
    print_list_rec_rev(head);
}