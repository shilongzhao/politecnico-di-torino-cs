//
// Created by zhaos on 4/12/2020.
//

#include <stdlib.h>
#include <stdio.h>

int f(int n) {
    if (n == 0) return 0;
    else return f(n - 1) + n;
}

int fib(int n) {
    if (n == 1 || n == 0) {
        return 1;
    }
    else {
        return fib(n - 1) + fib(n - 2);
    }
}

int sum(int *arr, int n) {
    if (n == 0) return 0;
    else return arr[0] + sum(arr + 1, n - 1);
}

int main() {
    for (int i = 0; i <= 10; i++) {
        printf("fib(%d) == %d\n", i, fib(i));
    }

    int a[10] = {1,2,3,4,5,6,7,8,9,10};
    printf("recursive array sum: %d\n", sum(a, 10));

    printf("%d\n", f(10));
}