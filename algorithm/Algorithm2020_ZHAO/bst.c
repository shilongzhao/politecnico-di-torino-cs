//
// Created by zhaos on 4/27/2020.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *left;
    node_t *right;
};

node_t *lca(node_t *root, int v1, int v2) {
    if (root->v >= v1 && root->v <= v2) return root;
    if (root->v < v1) return lca(root->right, v1, v2);
    return lca(root->left, v1, v2);
}

int dist(node_t *root, int v) {
    if (root->v == v) return 0;
    int d;
    if (root->v < v) d = 1 + dist(root->right, v);
    else d = 1 + dist(root->left, v);
    return d;
}

int distance(node_t *root, int v1, int v2) {
    node_t *l = lca(root, v1, v2);
    int d1 = dist(l, v1);
    int d2 = dist(l, v2);
    return d1 + d2;
}

void inorder(node_t *root) {
    if (root == NULL) return;
    inorder(root->left);
    printf("%d ", root->v);
    inorder(root->right);
}

int main() {
    node_t *nodes[10] = {};
    for (int i = 1; i <= 9; i++) {
        node_t *tmp = malloc(sizeof(node_t));
        tmp->v = i;
        tmp->left = tmp->right = NULL;
        nodes[i] = tmp;
    }

    nodes[5]->left = nodes[3];
    nodes[5]->right = nodes[7];
    nodes[3]->left = nodes[2];
    nodes[3]->right = nodes[4];
    nodes[2]->left = nodes[1];

    nodes[7]->left = nodes[6];
    nodes[7]->right = nodes[8];
    nodes[8]->right = nodes[9];

    inorder(nodes[5]);


    node_t *l = lca(nodes[5], 6, 9);
    printf("\nLCA is %d\n", l->v);

    printf("\ndist 5 -> 6 is %d\n", dist(nodes[5], 6));

}