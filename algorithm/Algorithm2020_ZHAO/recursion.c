//
// Created by zhaos on 5/1/2020.
//
#include <stdio.h>
#include <string.h>

/**
 *
 * @param str candidate chars
 * @param r   result string (state)
 * @param m   target length
 * @param d   depth (current r length)
 */
void gen(char *str, char *r, int m, int d) {
    if (d == m) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = 0; i < strlen(str); i++) {
        r[d] = str[i];
        gen(str, r, m, d + 1);
    }
}

void genNoRepeat(char *str, char *r, int *used, int M, int d) {
    if (d == M) {
        r[d] = '\0';
        printf("%s\n",r);
        return;
    }

    for (int i = 0; i < strlen(str); i++) {
        if (used[i] <= 0) { // max appearance 2 times used[i] <= 1
            r[d] = str[i];
            used[i] = used[i] + 1;
            genNoRepeat(str, r, used, M, d + 1);
            r[d] = '\0';
            used[i] = used[i] - 1;
        }
        // B used[i] = used[i] - 1;
    }
    // C used[i] = used[i] - 1;
}

// strlen(s)

int string_length(char *s) {
    int count = 0;
    for (int i = 0; s[i] != '\0'; i++) {
        count++;
    }
    return count;
}

void build() {
    if (1==1) {
        printf("");
        return;
    };

    for () {
        build();
    }
}
void buildMenu(char **data[], int n) {

}


int main() {
    char s[5] = {'a','b','c','d','\0'};
    printf("%s\n", s);

    char r[10] = {};
    gen("ABCDE", r, 3, 0);

    printf("string length %d\n", string_length(s));

    printf("\n===== result without repeat ====\n");
    int u[10] = {};
    genNoRepeat("ABCDEF", r, u, 4, 0);
}
