//
// Created by zhaos on 5/9/2020.
//

#include <stdio.h>
#include <string.h>

int main() {
    char *s = "abc";// OK
//    s[0] = 'M'; // NOT OK!

    char s2[10] = "abc"; // OK
    s2[0] = 'M'; // OK

    printf("s = %s\n", s); // OK
    printf("s2 = %s\n", s2); // OK

    char *t = strdup("tmp"); // malloc + strcpy
    printf("tmp = %s\n", t); // OK
    t[0] = 'D'; // OK

    int a[5] = {1,2,3}; // 1,2,3,0,0

    char p[10] = {'a', 'b', 'c', 'd', 'e'}; // 96, 97, 98, 0 0 0 0 0 0 0 ==> '\0' == 0
    printf("p = %s\n", p); // OK

    char q[3] = {'a', 'b', 'c'};
    printf("q = %s\n", q); // KO
}