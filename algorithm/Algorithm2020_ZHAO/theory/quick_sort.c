//
// Created by zhaos on 6/4/2020.
//
void exch(int *a, int i, int j) {
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
}

int partition(int *a, int lo, int hi) {
    int v = a[lo];
    int i = lo, j = hi + 1;
    while (1) {
        while (a[++i] < v) {
            if (i == hi) break;
        }
        while (a[--j] > v) {
            if (j == lo) break;
        }
        if (i >= j) break;
        exch(a, i, j);
    }

    exch(a, lo, j);

    return j;
}

void quick_sort(int *a, int lo, int hi) {
    if (lo >= hi) return;
    int p = partition(a, lo, hi);
    quick_sort(a, lo, p - 1);
    quick_sort(a, p + 1, hi);
}