//
// Created by zhaos on 6/6/2020.
//
#include <stdio.h>

#define N 7
int m[N][N] = {};
int visited[N] = {};
int dtime[N] = {};
int endtime[N] = {};

int clock = 0;
void dfs(int G[][N], int s) {
    visited[s] = 1;
    clock++;
    dtime[s] = clock;
    printf("visited node %d\n", s);

    for (int v = 0; v < N; v++) {
        if (G[s][v] == 1 && visited[v] == 0) {
            dfs(G, v);
        }
    }
    clock++;
    endtime[s] = clock;
}

int main() {
    for (int i = 0; i < N; i++) {
        visited[i] = 0;
    }

    m[0][1] = m[0][4] = m[0][5] = 1;
    m[1][0] = m[1][2] = m[1][5] = 1;
    m[2][1] = m[2][3] = m[2][5] = 1;
    m[3][2] = m[3][6] = 1;
    m[4][0] = m[4][6] = 1;
    m[5][0] = m[5][1] = m[5][2] = m[5][6] = 1;
    m[6][3] = m[6][4] = m[6][5] = 1;

    dfs(m, 0);

    for (int i = 0 ; i < N; i++) {
        printf("node %d: %d/%d\n", i, dtime[i], endtime[i]);
    }
}
