//
// Created by zhaos on 5/24/2020.
//

#include <stdio.h>

#define N 8
int sz[N];
int ids[N];

int find(int p) {
    while (p != ids[p]) {
        p = ids[p];
    }
    return p;
}

void union_op(int p, int q) {
    int pid = find(p);
    int qid = find(q);
    if (pid == qid)
        return;

    if (sz[pid] > sz[qid]) {
        ids[qid] = pid;
        sz[pid] += sz[qid];
    } else {
        ids[pid] = qid;
        sz[qid] += sz[pid];
    }
    for (int i = 0; i < N; i++) {
        printf("%d ", ids[i]);
    }
    printf("\n");
}

int main() {
    for (int i = 1; i <= 7; i++) {
        sz[i] = 1;
        ids[i] = i;
    }

    union_op(6, 1);
    union_op(2, 5);
    union_op(4, 7);
    union_op(5, 3);
    union_op(7, 3);
    union_op(4, 5);
    union_op(2, 6);

    for (int i = 1; i <= 7; i++) {
        printf(" %d:id=%d -", i, ids[i]);
    }
}