//
// Created by zhaos on 5/31/2020.
//
#include <stdio.h>
void exch(int *a, int i, int j) {
    int t = a[i]; a[i] = a[j]; a[j] = t;
}

void insertion_sort(int *a, int n) {
    for (int i = 1; i < n; i++) {
        // move a[i] to the right position among a[i - 1], a[i - 2] ...
        for (int j = i; j > 0 && a[j] < a[j - 1]; j--) {
            exch(a, j - 1, j);
        }

        for (int k = 0; k < n; ++k) {
            printf("%d ", a[k]);
        }
        printf("\n");
    }
}

int main() {
    int a[10] = {4,2,8,6,3,0,9,1,5,7};
    insertion_sort(a, 10);
}