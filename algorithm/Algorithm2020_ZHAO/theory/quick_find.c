//
// Created by zhaos on 5/24/2020.
//
#include <stdio.h>

int find(int *ids, int N, int p) {
    return ids[p];
}

void union_op(int *ids, int N, int p, int q) {
    int pid = ids[p];
    int qid = ids[q];
    for (int i = 0; i < N; i++) {
        if (ids[i] == pid) {
            ids[i] = qid;
        }
    }
}

int connected(int *ids, int N, int p, int q) {
    if (find(ids, N, p) == find(ids, N, q))
        return 1;
    else return 0;
}

int main() {
    int ids[11] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int p, q;
    while(1) {
        scanf("%d %d", &p, &q);
        if (p == -1 && q == -1) {
            break;
        }
        union_op(ids, 11, p, q);
        for (int i = 1; i <= 10; i++) {
            printf(" %d:id=%d --", i, ids[i]);
        }
        printf("\n");
    }

    int m, n;
    printf("which two points you want to check connected? ");
    scanf("%d %d", &m, &n);
    printf("%d %d connected? %d", m, n, connected(ids, 11, m, n));

}