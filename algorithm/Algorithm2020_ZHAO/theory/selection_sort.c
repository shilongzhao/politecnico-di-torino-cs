//
// Created by zhaos on 5/31/2020.
//
#include <stdio.h>
void exch(int *a, int i, int j) {
    int t = a[i]; a[i] = a[j]; a[j] = t;
}

void sort(int *a, int n) {
    for (int i = 0; i < n; i++) {
        // find the index of the minimum number in the unsorted part
        int min = i;
        for (int j = i + 1; j < n; j++) {
            if (a[j] < a[min]) min = j;
        }
        exch(a, i, min);

        for (int p = 0; p < n; p++) {
            printf("%d ", a[p]);
        }
        printf("\n");
    }
}

int main() {
    int a[10] = {9,6,4,2,0,1,5,8,7,3};
    sort(a, 10);
}
