//
// Created by zhaos on 6/1/2020.
//
#include <stdlib.h>
#include <stdio.h>
void merge(int *a, int *b, int lo, int mid, int hi) {
    for (int k = lo; k <= hi; k++) {
        b[k] = a[k];
    }

    int i = lo, j = mid + 1;
    for (int k = lo; k <= hi; k++) {
        if (i > mid) a[k] = b[j++]; // left finished
        else if (j > hi) a[k] = b[i++]; // right finished
        else if (b[i] < b[j]) a[k] = b[i++]; // select number from left
        else a[k] = b[j++];
    }
}

int min(int a, int b) {
    if (a < b) return a;
    else return b;
}

// bottom up merge sort
void merge_sort(int *a, int n) {
    int *b = malloc(sizeof(int) * n);
    for (int sz = 1; sz < n; sz = sz + sz) {
        for (int lo = 0; lo < n - sz; lo = lo + 2*sz) {
            merge(a, b, lo, lo + sz - 1, min(n - 1, lo + 2*sz - 1));
            for (int i = 0; i < 10; i++) {
                printf("%d ", a[i]);
            }
            printf("\n");
        }
    }
}
// top-bottom
void sort(int *a, int *b, int lo, int hi) {
    if (lo >= hi) {
        return;
    }

    int mid = lo + (hi - lo)/2;
    sort(a, b, lo, mid);
    sort(a, b, mid + 1, hi);
    merge(a, b, lo, mid, hi);

    for (int i = 0; i < 10; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void merge_sort_top_down(int *a, int n) {
    int *b = malloc(sizeof(int) * n);
    sort(a, b, 0, n - 1);
}

int main() {
   int a[10] = {6,2,3,1,9,0,5,4,7,8};

//   merge_sort(a, 10);
    merge_sort_top_down(a, 10);
}