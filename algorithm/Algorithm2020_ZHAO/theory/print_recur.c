//
// Created by zhaos on 6/14/2020.
//

#include <stdio.h>
void f(int n) {
    if (n >= 20 || n < 0) return;
    printf("%d ", n);
    f(n + 2);
    f(n + 4);
}
int main() {
    f(10);
}