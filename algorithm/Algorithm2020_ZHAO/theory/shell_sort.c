//
// Created by zhaos on 5/31/2020.
//
#include <stdio.h>
void exch(int *a, int i, int j) {
    int t = a[i]; a[i] = a[j]; a[j] = t;
}


void shell_sort(int *a, int n) {
    int h = 1;
    while (h < n/3) h = 3 * h + 1; // 1, 4, 13
    printf("init h = %d\n", h);
    while (h >= 1) {
        for (int i = h; i < n; i++) {
            // insert a[i] into a[i-h], a[i-2h], a[i-3h],...
            for (int j = i; j >= h && a[j] < a[j - h]; j -= h) {
                exch(a, j, j - h);
            }
        }
        h = h/3;

        for (int i = 0; i < n; i++) {
            printf("%d ", a[i]);
        }
        printf("\n");
    }
}

int main() {
    int a[20] = {10,6,2,9,0,11,15,19,7,5,
                 1,3,8,4,13,12,14,18,16,17};
    shell_sort(a, 20);
}

