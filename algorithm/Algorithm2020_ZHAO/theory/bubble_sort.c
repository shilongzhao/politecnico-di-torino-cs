//
// Created by zhaos on 5/31/2020.
//
#include <stdio.h>
void exch(int *a, int i, int j) {
    int t = a[i]; a[i] = a[j]; a[j] = t;
}

void bubble_sort(int a[], int n) {
    // the i-th bubbling
    for (int i = 0; i < n - 1; i++) {
        // move the i-th largest number to the position of n - i.
        for (int j = 0; j < n - i - 1; j++) {
            if (a[j] > a[j + 1]) {
                exch(a, j, j + 1);
            }
        }

        for (int k = 0; k < n; ++k) {
            printf("%d ", a[k]);
        }
        printf("\n");
    }
}

int main() {
    int a[10] = {5,0,3,2,1,9,8,6,4,7};
    bubble_sort(a, 10);
}