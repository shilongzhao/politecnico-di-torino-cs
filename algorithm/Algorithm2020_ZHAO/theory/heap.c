//
// Created by zhaos on 6/5/2020.
//
#include <stdio.h>
void exch(int *a, int i, int j) {
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
}
void swim(int *a, int k) {
    while (k > 1 && a[k] > a[k/2]) {
        exch(a, k/2, k);
        k = k/2;
    }
}
// heap now have a[1], a[2]... a[N], and insert a new value v
void insert(int *a, int v, int N) {
    a[N + 1] = v;
    swim(a, N + 1);
}
// heap now has a[1], a[2], ... , a[N], sink value at position k
void sink(int *a, int k, int N) {
    while (2 * k <= N) { // while k has at least one child
        int j = 2 * k; // assume first child is the larger child
        if (j + 1 <= N && a[j] < a[j + 1]) j = j + 1; // if the second child exists and is larger than the first child, set it as the larger child
        if (a[k] >= a[j]) break; // a[k] is in the right position, break loop
        exch(a, k, j); // exchange k with its larger child
        k = j;
    }
}

// delete max value from a[1], a[2], ..., a[N], the remaining must be also a heap
// and return the max value
int deleteMax(int *a, int N) {
    int max = a[1];
    exch(a, 1, N);
    sink(a, 1, N - 1);
    return max;
}

//
void heap_sort(int *a, int N) {
   // heapify
   for (int k = N/2; k >= 1; k--)
       sink(a, k, N);
   while (N > 1) {
       exch(a, 1, N);
       N = N - 1;
       sink(a, 1, N);
   }
}
int main() {
    int a[11] = {};
    insert(a, 1, 0);
    insert(a, 4, 1);
    insert(a, 3, 2);
    insert(a, 2, 3);
    insert(a, 9, 4);
    insert(a, 8, 5);
    insert(a, 6, 6);
    insert(a, 5, 7);
    insert(a, 7, 8);
    insert(a, 10, 9);

    printf("===== heap after insert ======\n");
    for (int i = 1; i <= 10; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");


    printf("======== delete max =========\n");
    int m = deleteMax(a, 10);
    printf("deleted max = %d\n", m);
    for (int i = 1; i <= 9; i++) {
       printf("%d ", a[i]);
    }
    printf("\n");

    int b[11] = {-1, 9, 5, 6, 2, 1, 0, 4, 3, 7, 8};
    heap_sort(b, 10);
    printf("===== heap sort ====== \n");
    for (int i = 1; i <= 10; i++) {
        printf("%d ", b[i]);
    }
}