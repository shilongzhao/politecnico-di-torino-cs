//
// Created by zhaos on 5/24/2020.
//
#include <stdio.h>

int find(int *ids, int N, int p) {
    while (p != ids[p]) {
        p = ids[p];
    }
    return p;
}

void union_op(int *ids, int N, int p, int q) {
    int pid = find(ids, N, p);
    int qid = find(ids, N, q);
    if (pid == qid)
        return;

    ids[pid] = qid;

    for (int i = 0; i < N; i++) {
        printf("%d ", ids[i]);
    }
    printf("\n");
}

int main() {
    int ids[8] = {0, 1,2,3,4,5,6,7};
    union_op(ids, 8, 6, 1);

    union_op(ids, 8, 2, 5);

    union_op(ids, 8, 4, 7);
    union_op(ids, 8, 5, 3);
    union_op(ids, 8, 7, 3);
    union_op(ids, 8, 4, 5);
    union_op(ids, 8, 2, 6);

    for (int i = 1; i <= 7; i++) {
        printf(" %d:id=%d -", i, ids[i]);
    }
}