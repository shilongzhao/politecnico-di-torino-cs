//
// Created by zhaos on 5/16/2020.
//
#include <stdio.h>
/**
 * make a combination from chars in s, the combo length should be m
 * @param s
 * @param r
 * @param m
 * @param d
 * @param p last picked branch
 */
void combo(char *s, char *r, int m, int d, int p) {
    if (d == m) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = p + 1; s[i] != '\0'; i++) {
        r[d] = s[i];
        combo(s, r, m, d + 1, i);
    }
}

int main() {
    char *s = "ABCDE";
    char r[10] = {};
    combo(s,r,3,0,-1);
}