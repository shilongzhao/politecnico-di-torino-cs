//
// Created by zhaos on 4/19/2020.
//

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *left;
    node_t *right;
};

void preorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%d ", root->v);
    preorder(root->left);
    preorder(root->right);
}

void inorder(node_t*root){
    if (root == NULL) return;

    inorder(root->left);
    printf("%d ", root->v);
    inorder(root->right);
}

void postorder(node_t*root){
    if (root == NULL) return;

    postorder(root->left);
    postorder(root->right) ;
    printf("%d ",root->v);
}

// sum of tree
int sum(node_t *root) {
    if (root == NULL) return 0;
    int r = root->v + sum(root->left) + sum(root->right);
    return r;
}

int max3(int a, int b, int c) {
    int m = a;
    if (b > m) {
        m = b;
    }
    if (c > m) {
        m = c;
    }
    return m;
}
// max of tree
int max(node_t *root) {
    if (root == NULL) return INT_MIN;
    int r = max3(root->v, max(root->left), max(root->right));
    return r;
}

int equals(node_t *r1, node_t *r2) {
    if (r1 == NULL && r2 == NULL) return 1;
    if (r1 == NULL && r2 != NULL) return 0;
    if (r1 != NULL && r2 == NULL) return 0;

    int r = (r1->v == r2->v) &&
            equals(r1->left, r2->left) &&
            equals(r1->right, r2->right);
    return r;
}

int isMirror(node_t *r1, node_t *r2) {
    return 0;
}

void printLevel(node_t *root, int n) {
    if (root == NULL) return;

    if (n == 0) {
        printf("%d ", root->v);
    }

    printLevel(root->left, n - 1);
    printLevel(root->right, n - 1);
}

void printLevels(node_t *root, int start, int end) {
    for (int i = start; i <= end; i++)
        printLevel(root, i);
}

int depth(node_t *root) {
    return 0;
}


int pickApples(node_t *tree, int ladderHeight) {
    if (tree == NULL) return 0;
    if (ladderHeight == 0) return tree->v;
    int sum = 0;
    sum = tree->v + pickApples(tree->left, ladderHeight - 1)
            + pickApples(tree->right, ladderHeight - 1);
    return sum;
}

/// www.leetcode.com, >= 1000

int main() {
    node_t n1 = {1, NULL, NULL};
    node_t n2 = {2, NULL, NULL};
    node_t n3 = {3, NULL, NULL};
    node_t n4 = {4, NULL, NULL};
    node_t n5 = {5, NULL, NULL};

    n1.left = &n2;
    n1.right = &n3;

    n2.left = &n4;
    n2.right = &n5;

    preorder(&n1);

    printf("\n ===== inorder ====== \n"); // 4 2 5 1 3
    inorder(&n1);

    printf("\n ====== postorder ====== \n"); // 4 5 2 3 1
    postorder(&n1);

    printf("\n ====== sum =========\n");
    printf("sum = %d\n", sum(&n1));

    printf("\n ==== max ====== \n");
    printf("max = %d\n", max(&n1));

    printf("\n ==== print level 2 ====\n");
    printLevel(&n1, 2);

    int apples = pickApples(&n1, 2);
    printf("\n === num apples %d ===\n", apples);
}