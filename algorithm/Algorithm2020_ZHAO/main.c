#include <stdio.h>
#include <limits.h>

int main() {

    printf("6 and 8 bit wise operation = %d\n", 6 & 8);

    int sum = 0;
    for (int i = 1; i <= 1000; i += 2) {
        sum += i;
    }

    printf("sum = %d\n", sum);

//    int s = 0;
//    int i = 1;
//    while (i <= 1000) {
//        s += i;
//        i += 2;
//    }
//    printf("s = %d\n", s);


    int a[10] = {10, 22, 13, 8, 0, 7, 12, -3, 24, 11};
    int max = INT_MIN;

    for (int i = 0; i < 10; i++) {
       if (a[i] > max) {
           max = a[i];
       }
    }
    printf("max = %d\n", max);
    // average, min

    char s[10];
    scanf("%s", s);
    // todo: turn s into upper case

    printf("upper case s = %s\n", s);


    return 0;
}