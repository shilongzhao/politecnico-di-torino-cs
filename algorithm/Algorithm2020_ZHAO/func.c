
/*
return_type func_name(param_type param, param_type2 param2...) {
    statements; 
}
*/

#include <stdio.h>
#include <ctype.h>

// calculate a to the power of b, definition
double power(float a, int b) {
    double r = 1;
    for (int i = 0; i < b; i++) {
        r *= a;
    }
    return r;
}

// swap

void swap(int *a, int *b) {
    printf("in swap before: a = %d, b = %d\n", *a, *b);
    int c = *a;
    *a = *b;
    *b = c;
    printf("in swap after: a = %d, b = %d\n", *a, *b);
}

int sum(int *s, int n) {
    int r = 0;
    for (int i = 0; i < n; i++) {
        r += s[i];
    }
    return r;
}

void plus_one(int *s, int n) {
    for (int i = 0; i < n; i++) {
        s[i] += 1;
    }
}

void to_upper(char *s) {
    for (int i = 0; s[i] != '\0'; i++) {
        s[i] = s[i] + 'A' - 'a';
    }
}

void to_lower(char *s) {
    for (int i = 0; s[i] != '\0'; i++) {
        s[i] = s[i] + 'a' - 'A';
    }
}


int main() {
   // call 
    float a = 2.1;
    double d = power(a, 2);
    printf("%lf\n", d);

    // swap
    int x = 12, y = 23;
    printf("in main before: x = %d, y = %d\n", x, y);
    swap(&x, &y);
    printf("in main after: x = %d, y = %d\n", x, y);

    int arr[10] = {1,3,2,6,5,4,8,9,0,7};
    printf("sum of arr = %d\n", sum(arr, 10));
    plus_one(arr, 10);
    printf("sum of arr after plus_one = %d\n", sum(arr, 10));

    char s[100] = "abcde";
    to_upper(s);
    printf("s in upper case: %s", s);

    char s2[5] = {'a', 'b', '1', '2', '3'};
}

