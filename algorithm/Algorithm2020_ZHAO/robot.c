
#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 100

void recursive(int *l, int ls, int *r, int rs, int rem_len) {
    if (rem_len < 0) return;
    if (rem_len == 0) {
        for (int i = 0; i < rs; i++) {
            printf("%d ", r[i]);
        }
        printf("\n");
    }

    for (int i = 0; i < ls; i++) {
        r[rs] = l[i];
        recursive(l, ls, r, rs + 1, rem_len - l[i]);
    }


}

void cover(int *l, int n, int d) {
    int *r = malloc(sizeof(int) * MAX_LEN);
    recursive(l, n, r, 0, d);
    free(r);
}

int main() {
    int l[2] = {2, 6};
    cover(l, 2, 10);
}
