//
// Created by zhaos on 3/21/2020.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct student student;
struct student {
    int id;
    char *name;
    char *surname;
    int age;
};

typedef struct employee employee;
struct employee {
    int id;
    char name[100];
    char surname[100];
    int age;
};

typedef struct pos pos;
struct pos {
    double longitude;
    double latitude;
    double altitude;
};

void increase_age(student s) {
    s.age += 1;
}

// (*v).member    v->member
void increase_age2(struct student *s) {
    s->age += 1;
}

void move_east(pos p) {
    printf("address of p is %p\n", &p);
    p.longitude = p.longitude + 100.0;
}

void move_west(pos *p) {
    p->longitude -= 100.0;
}
// TODO: malloc struct
int main() {
    struct student s1 = {.id = 1, .name = "Alex", .surname = "Hello", .age = 22};

    student s2 = {.id = 2, .name = "Bob", .surname = "Ciao", .age = 23};

    // pointer to struct
    printf("student %s age is %d\n", s1.name, s1.age);
    increase_age(s1);
    printf("student %s age after incr_age is %d\n", s1.name, s1.age);

    increase_age2(&s1);
    printf("student %s age after incr_age2 is %d\n", s1.name, s1.age);

    // pointer to struct
    pos m = {.longitude = 20.12, .latitude= 45.23, .altitude = 99};

    move_east(m);

    printf("address of m is %p\n", &m);
    printf("longitude of m is now: %.2lf\n", m.longitude);

    move_west(&m);
    printf("longitude of m is now: %.2lf\n", m.longitude);

    // dynamic memory allocation
    student *ps = malloc(sizeof(student));
    ps->age = 23;
    ps->surname = "Rossi";
}