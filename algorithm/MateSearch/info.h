//
// Created by Shilong on 21/05/15.
//

#ifndef MATESEARCH_INFO_H
#define MATESEARCH_INFO_H
#include "list.h"
#include "utilities.h"
#include "person.h"

typedef struct entry_t_ entry_t;
struct entry_t_ {
    char key[STRSIZ];
    char value[STRSIZ];
};

typedef struct info_t_ info_t;
struct info_t_ {
    person_t *person;
    List entries;
};

void read_info(person_t *person, info_t *info);
void print_info(info_t *info);

info_t *search_info(List* infolist, person_t *person);

int  compare_info(info_t *this, info_t *other);
int entry_match(entry_t *this, entry_t *other);

#endif //MATESEARCH_INFO_H
