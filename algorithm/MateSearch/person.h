//
// Created by Shilong on 21/05/15.
//

#ifndef MATESEARCH_PERSON_H
#define MATESEARCH_PERSON_H

#include <string.h>
#include "list.h"
#include "utilities.h"

typedef struct person_t_ person_t;
struct person_t_ {
    char surname[STRSIZ];
    char name[STRSIZ];
    char gender;
};


void print_person(person_t *p);

void read_members(char *filename, List *members);

person_t *find_member(List *members, char *surname, char *name);

void print_members(List *members);

int is_same_person(person_t *this, person_t *that);

#endif //MATESEARCH_PERSON_H
