#include "utilities.h"
#include <stdlib.h>
#include <stdio.h>

void error_exit(char *msg) {
    fprintf(stderr, "%s\n", msg);
    exit(-1);
}