//
// Created by Shilong on 21/05/15.
//
#include "list.h"
#include "person.h"
#include <stdio.h>
#include <string.h>

void print_person(person_t *p) {
    fprintf(stdout, "%s %s %c", p->surname, p->name, p->gender);
    return;
}

void read_members(char *filename, List *members) {

    FILE *fp = fopen(filename, "r");
    if (fp == NULL) exit(-1);

    char surname[STRSIZ], name[STRSIZ];
    char gender;
    while (fscanf(fp, "%s %s %c", surname, name, &gender) == 3) {
        person_t *p = malloc(sizeof(person_t));
        if (p == NULL) exit(-1);

        strcpy(p->surname, surname);
        strcpy(p->name, name);
        p->gender = gender;

        list_ins_next(members, list_tail(members), p);
    }
    return;
}

person_t *find_member(List *members, char *surname, char *name) {
    ListElmt *element;
    person_t *p;
    for (element = list_head(members); element != NULL; element = list_next(element)) {
        p = (person_t *) element->data;
        if (strcmp(p->surname, surname) == 0 && strcmp(p->name, name) == 0) {
            return p;
        }
    }

    return NULL;
}

void print_members(List *members) {
    ListElmt *element;
    for (element = list_head(members); element != NULL; element = list_next(element)) {
        print_person( (person_t *)(element->data) );
        printf("\n");
    }
}

int is_same_person(person_t *this, person_t *that) {
    return (strcmp(this->surname, that->surname) == 0 &&
             strcmp(this->name, that->name) == 0 &&
             this->gender == that->gender);
}