//
// Created by Shilong on 21/05/15.
//

#include "info.h"
#include <string.h>
#include "utilities.h"
#include <stdio.h>

void read_info(person_t *person, info_t *info) {
    char filename[3 * STRSIZ];

    info->person = person;
    strcpy(filename, person->surname);
    strcat(filename, "_");
    strcat(filename, person->name);
    strcat(filename, ".txt");

    FILE *fp = fopen(filename, "r");
    if (fp == NULL)
        error_exit("file open failed");

    char key[STRSIZ], value[STRSIZ];
    while(fscanf(fp, "%s %s", key, value) == 2) {
        entry_t *entry = malloc(sizeof(entry_t));
        strcpy(entry->key, key);
        strcpy(entry->value, value);

        list_ins_next(&info->entries, list_tail(&info->entries), entry);
    }
    return;
}

void print_info(info_t *info) {
    print_person(info->person);
    printf(": \n");
    List *entries = &info->entries;

    ListElmt *elmt;
    for (elmt = list_head(entries); elmt != NULL; elmt = list_next(elmt)) {
        entry_t *entry = (entry_t *) elmt->data;
        printf("%s %s\n", entry->key, entry->value);
    }
    return;
}

int entry_match(entry_t *this, entry_t *other) {
    return (strcmp(this->key, other->key) == 0 &&
            strcmp(this->value,other->value)==0);
}

info_t *search_info(List* infolist, person_t *person) {
    ListElmt *element;
    for (element = list_head(infolist); element != NULL; element = list_next(element)) {
        info_t *info = (info_t *) element -> data;
        if (is_same_person(info->person, person))
            return info;
    }
    return NULL;
}

int compare_info(info_t *this, info_t *that) {

    int match_count = 0;

    entry_t *entry_this, *entry_that;
    ListElmt *elmt_this, *elmt_that;
    for(elmt_this = list_head(&this->entries); elmt_this != NULL; elmt_this = list_next(elmt_this)) {
        entry_this = (entry_t *) list_data(elmt_this);
        for(elmt_that = list_head(&that->entries); elmt_that !=NULL; elmt_that = list_next(elmt_that)) {
            entry_that = (entry_t *)list_data(elmt_that);
            if (entry_match(entry_this, entry_that)) {
                #ifdef DEBUG
                printf("matched entry %s=%s %s %s\n", entry_this->key, entry_that->key,
                       entry_this->value, entry_that->value);
                #endif
                match_count++;
            }
        }
    }

    return match_count;
}