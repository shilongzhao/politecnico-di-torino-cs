#include <stdio.h>
#include <string.h>
#include "list.h"
#include "person.h"
#include "info.h"
#define DEBUG


int main(int argc ,char **argv) {
    if (argc != 2) exit(-1);
    List members;
    list_init(&members, free);

    read_members(argv[1], &members); // read all the club members' sur/name and gender
    #ifdef DEBUG
    print_members(&members);
    #endif

    ListElmt    *node;
    List        infolist;

    list_init(&infolist, free);

    for(node = list_head(&members); node != NULL; node = list_next(node)) {

        info_t *info = malloc(sizeof(info_t));

        person_t *current = (person_t *) node->data;

        read_info(current, info); // read information about the current person from file surname_name.txt

        #ifdef DEBUG
        print_info(info);
        #endif

        list_ins_next(&infolist, list_tail(&infolist), info); // insert the information into information list
    }

    char surname[STRSIZ], name[STRSIZ];

    printf("input surname name: ");

    scanf(" %s %s", surname, name);

    if (strcmp(surname, "stop") == 0 && strcmp(name, "program") == 0)
        return 0;

    person_t *p = find_member(&members, surname, name);

    if (p == NULL)
        error_exit("no such person\n");

    info_t *request_info = search_info(&infolist, p);
    if (request_info == NULL) exit(-1);

    info_t *max_match_info = NULL;
    int max_match = 0;
    for(ListElmt *element = list_head(&infolist); element != NULL; element = list_next(element)) {
        info_t *this_info = (info_t *)element->data;
        if (!is_same_person(this_info->person, request_info->person)) {
            int matches = compare_info(request_info, this_info);
            if (matches > max_match) {
                max_match = matches;
                max_match_info = this_info;
            }
        }
    }
    if (max_match_info != NULL) {
        printf("soul mate is ");
        print_person(max_match_info->person);
        printf("\n%d match entries\n", max_match);
    }

    return 0;
}