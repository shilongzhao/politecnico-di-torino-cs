//
//  main.c
//  algorithm201709
//
//  Created by Shilong Zhao on 19/09/2017.
//  Copyright © 2017 Shilong Zhao. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
int mat_search(char **mat, int r, int c, char *s) {
    int i = 0, j = 0;
    int l = (int) strlen(s);
    int cnt = 0;
    for (i = 0; i < r; i++) {
        for (j = 0; j < c; j++) {
            //竖着找
            int k, b = 1;
            for (k = i; k < i + l; k++) {
                if (k >= r || mat[k][j] != s[k-i]) {
                    b = 0;
                }
            }
            if (b == 1) {
                cnt++;
            }
            //横着找
            b = 1;
            int f;
            for (f = j; f < j + l; f++) {
                if (f >= c || mat[i][f] != s[f-j]) {
                    b = 0;
                }
            }
            if (b == 1) {
                cnt++;
            }
        }
    }
    return cnt;
}


typedef struct list1_t list1_t;
typedef struct list2_t list2_t;
//farmer
struct list1_t {
    char *fname;
    list1_t *nf;
    list2_t *np;
};
//product
struct list2_t {
    char *pname;
    list1_t *nf;
    list2_t *np;
};
list1_t *insert_farmer(list1_t **f, char *n) {
    list1_t *l = *f;
    while (l != NULL) {
        if (strcmp(l->fname, n) == 0) {
            return l;
        }
        l = l->nf;
    }
    list1_t *t = malloc(sizeof(list1_t));
    t->fname = strdup(n);
    t->nf = *f;
    t->np = NULL;
    *f = t;
    return t;
}
//对于名字为n的一个product，如果已经存在名字为n的product，
//直接返回该节点，否则创建新的节点并插入头部，然后返回
list2_t *insert_product(list2_t **p, char *n) {
    list2_t *l = *p;
    while (l != NULL) {
        if (strcmp(l->pname, n) == 0) {
            return l;
        }
        l = l->np;
    }
    list2_t *t = malloc(sizeof(list2_t));
    t->pname = strdup(n);
    t->np = *p;
    t->nf = NULL;
    *p = t;
    return t;
}
// f: farmer name, string
// p: product name, string
void insert(list2_t **h2, char *f, char *p) {
    list2_t *prod = insert_product(h2, p);
    insert_farmer(&prod->nf, f);
}

void list_of_list_invert(list1_t *head1, list2_t **head2) {
    while (head1 != NULL) {
        list2_t *prod = head1->np;
        while (prod != NULL) {
            insert(head2, head1->fname, prod->pname);
            prod = prod->np;
        }
        head1 = head1->nf;
    }
}
int min(int a, int b) {
    return a < b ? a : b;
}
// if there is some i with step[i] = 0, then this method will fail
// this recursive solution is very slow, use dynamic programming to improve 
int hop(int steps[], int n, int s, int e) {
    
    if (s == e) {
        return 0;
    }
    if (s + steps[s] >= e) {
        return 1;
    }
    int m = INT32_MAX;
    for (int k = s + 1; k < e; k++) {
        m = min(m, hop(steps, n, k, e) + hop(steps, n, s, k));
    }
    return m;
}

int min_hop(int steps[], int n) {
    return hop(steps, n, 0, n - 1);
}


int main(int argc, const char * argv[]) {
    
    // ------------------------------
    char *s = "foo";
    char **mat = malloc(sizeof(char *) * 4);
    for (int i = 0; i < 4; i++) {
        mat[i] = malloc(sizeof(char) * 6);
    }
    strcpy(mat[0], "xfoox");
    strcpy(mat[1], "foozf");
    strcpy(mat[2], "xo2fo");
    strcpy(mat[3], "g4xao");
    
    int r = mat_search((char **)mat, 4, 5, s);
    printf("result = %d\n", r);
    // -------------------------------
    list1_t *f1 = malloc(sizeof(list1_t));
    f1->fname = strdup("farmer1");
    list2_t *p1 = malloc(sizeof(list2_t));
    p1->pname = strdup("product1");
    list2_t *p2 = malloc(sizeof(list2_t));
    p2->pname = strdup("product2");
    list2_t *p3 = malloc(sizeof(list2_t));
    p3->pname = strdup("product3");

    f1->nf = NULL;
    f1->np = p1;
    p1->np = p2;
    p2->np = p3;
    p3->np = NULL;
    
    list1_t *head1 = f1;
    list2_t *head2 = NULL;
    list_of_list_invert(head1, &head2);
    list1_t *t1;
    list2_t *t2 = head2;
    while (t2 != NULL) {
        printf("%s\n", t2->pname);
        t1 = t2->nf;
        while (t1 != NULL) {
            printf("%s\n", t1->fname);
            t1 = t1->nf;
        }
        t2 = t2->np;
    }
    // -------------------------------
    int steps[7] = {3,5,1,2,1,1,2};
    printf("%d\n", min_hop(steps, 7));
}

