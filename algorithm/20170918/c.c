
int min(int a, int b) {
  return a < b? a: b;
}

int min_hop(int *p, int n) {
  int i;
  int m = MAX_INT;
  for (i = 1, i < n - 1; i++) {
    m = min(m, hop(p, 0, i) + hop(p, i+1, n));
  }
  return m;
}

int hop(int *p, int start, int end) {
  if (start == end) {
    return 0;
  }
  if (p[start] + start >= end) {
    return 1;
  }

  int m = MAX_INT;
  for (int i = start; i < end; i++) {
      int x = hop(p, start, i) + hop(p, i, end);
      m = min(m, x);
  }
  return m;
}
