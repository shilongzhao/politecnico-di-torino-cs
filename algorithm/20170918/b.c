#include <stdlib.h>
#include <string.h>
typedef struct list1_t list1_t;
typedef struct list2_t list2_t;
//farmer
struct list1_t {
  char *fname;
  list1_t *nf;
  list2_t *np;
};
//product
struct list2_t {
  char *pname;
  list1_t *nf;
  list2_t *np;
}
list1_t *insert_farmer(list1_t **f, char *n) {
  list1_t *l = *f;
  while (l != NULL) {
    if (strcmp(l->fname, n) == 0) {
      return l;
    }
    l = l->nf;
  }
  list1_t *t = malloc(sizeof(list1_t));
  t->fname = strdup(n);
  t->nf = *f;
  t->np = NULL;
  *f = t;
  return t;
}
list2_t *insert_product(list2_t **p, char *n) {
  list2_t *l = *p;
  while (l != NULL) {
    if (strcmp(l->pname, n) == 0) {
      return l;
    }
    l = l->np;
  }
  list2_t *t = malloc(sizeof(list2_t));
  t->pname = strdup(n);
  t->np = *p;
  t->nf = NULL;
  *p = t;
  return t;
}
void insert(list2_t **h2, char *f, char *p) {
  list2_t *prod = insert_product(*h2, p);
  insert_farmer(&prod->nf, f);
}
void list_of_list_invert(list1_t *head1, list2_t **head2) {
  while (head1 != NULL) {
    list2_t *prod = head->np;
    while (prod != NULL) {
      insert(head2, head1->fname, prod->pname);
      prod = prod->np;
    }
    head1 = head1->nf;
  }
}
