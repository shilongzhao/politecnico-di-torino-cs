
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int num_words(char v[]) {
    // write your code here
}

int splitPhase(char v[], int ***m) {
    // alloc memory for m, write your code here
    int n = num_words(v);
    
    char *p = strtok(v, " ");
    int i = 0;
    while (p != NULL) {
        // copy new string to m, write your code here
        
        p = strtok(NULL, " ");
        i++;
    }
    return n;
}

int main(int argc, char *argv[]) {
    char v[] = "This is a phase to split into sub-strings";
    char **m;
    int n, i;
    n = splitPhase(v, (int ***)&m);
    for (i = 0; i < n; i++)
        printf("string number %d = %s\n", i, m[i]);
}
