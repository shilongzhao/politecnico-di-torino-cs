#include <stdio.h>
#include <stdlib.h>

int all_included(int m[][9], int *result, int k) {
    int used[9] = {}; // only 1..8 are used, used[0] not considered
    for (int i = 0; i < k; i++) {
        int r = result[i];
        for (int j = 0; j < 9 && m[r][j] != 0; j++)
            used[m[r][j]]++;
    }
    for (int i = 1; i < 9; i++)
        if (used[i] == 0) return 0;
    
    return 1;
}
void play(int m[][9], int next, int *result, int depth, int n, int k) {
    
    if (depth == k) { //end of recursion
        if(all_included(m, result,k)) {
            for(int i = 0; i < k; i++)
                printf("S%d ", result[i]);
            printf("\n");
        }
        return;
    }
    for (int i = next; i < n; i++) {
        result[depth] = i;
        play(m, i + 1, result, depth+1, n, k);
    }
}
void cover(int m[][9], int n, int k) {
    int *result = malloc(sizeof(int) * k); // remembers which row is selected
    play(m, 0, result, 0, n, k);
}

int main(int argc, char *argv[]) {
    int m[5][9] = {
        {1,2,3},
        {2,3,7},
        {7,8},
        {1,3,7,8,2},
        {4,5,6}};
    
    cover(m, 5, 3);
}

