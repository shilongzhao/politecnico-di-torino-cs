
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int num_words(char v[]) {
    int c = 0;
    char *p;
    for (p = v; *p != '\0'; p++) {
        if (*p == ' ')
            c++;
    }
    if (*p == '\0')
        c++;
    return c;
}

int splitPhase(char v[], char ***m) {
    int n = num_words(v);
    *m = malloc(sizeof(char *) * n);

    char *p = strtok(v, " ");
    int i = 0;
    while (p != NULL) {
        (*m)[i] = strdup(p);
        p = strtok(NULL, " ");
        i++;
    }
    return n;
}

int main(int argc, char *argv[]) {
    char v[] = "This is a phase to split into sub-strings";
    char **m;
    int n, i;
    n = splitPhase(v, (char ***)&m);
    for (i = 0; i < n; i++)
        printf("string number %d = %s\n", i, m[i]);
}
