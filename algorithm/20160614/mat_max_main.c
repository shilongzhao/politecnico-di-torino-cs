#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int m0[3][4] = {{5,2,3,1},{3,1,6,4},{3,0,5,2}};
    int **m = malloc(sizeof(int *) * 3);
    int i, j;
    for (i = 0; i < 3; i++) {
        m[i] = malloc(sizeof(int) * 4);
        for (j = 0; j < 4; j++) {
            m[i][j] = m0[i][j];
        }

    }
    matMax(m, 3, 4);
}
