#include <stdio.h>
#include <stdlib.h>

int in(int i, int j , int r, int c) {
    return i < r && i >= 0 && j >=0 && j < c;
}

int high(int **m, int x, int y, int r, int c) {
    int i, j;
    for (i = x - 1; i <= x + 1; i++) {
        for (j = y - 1; j <= y + 1; j++) {
            if (!in(i,j,r,c)) continue;
            if (i == x && j == y) continue;
            if (m[x][y] <= m[i][j])
                return 0;
        }
    }
    return 1;
}

void matMax(int **m, int r, int c) {
    int i, j;
    for (i = 0; i < r; i++) {
        for (j = 0; j < c; j++) {
            if (high(m, i, j, r, c))
                printf("(%d, %d)\n", i, j);
        }
    }
}

int main(int argc, char *argv[]) {
    int m0[3][4] = {{5,2,3,1},{3,1,6,4},{3,0,5,2}};
    int **m = malloc(sizeof(int *) * 3);
    int i, j;
    for (i = 0; i < 3; i++) {
        m[i] = malloc(sizeof(int) * 4);
        for (j = 0; j < 4; j++) {
            m[i][j] = m0[i][j];
        }

    }
    matMax(m, 3, 4);
}
