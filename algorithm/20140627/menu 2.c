#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 3
static char *primi[5] = {"duck_salad", "scotch_egg", "soupe & bread", "baby_squid", NULL};

static char *secondi[4] = {"rabbit_and_bacon", "fish_and_chips", "roast_lamb", NULL};

static char *contorni[4] = {"gateaux_opera", "ice_cream", "cheese_cake", NULL};

int count = 0;

void recursive_build(char **data[], char **result, int depth, int n) {
	if (depth == n) {
		printf("menu_%d: ", ++count);

		for(int i = 0; i < n; i++) {
			printf("%s ", result[i]);
		}
		printf("\n");
		return;
	}

	for (char **p = data[depth]; *p != NULL; p++) {
		result[depth] = strdup(*p);
		recursive_build(data, result, depth+1, n);
		result[depth] = NULL;
	}

}

void buildMenu(char **data[], int n) {
	char **result = malloc(n * sizeof(char *));
	recursive_build(data, result, 0, n);
}

int main(int argc, char *argv[]) {
	char **data[N] = {primi, secondi, contorni};
	buildMenu(data, N);
}
