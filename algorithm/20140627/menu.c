/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


/**
 * 27 Jun 2014 Exam
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct memu_t_ {
    int id;
    char **dishes;
}menu_t;

void generate_menu(char ***data, menu_t *m, int depth);

int main(int argc, char *argv[]) {
    // initialize data
    char *primo[5] = { "duck_salad",
                       "scotch_egg",
                       "soupe_and_bread",
                       "baby_squid",
                       NULL
                     };

    char *secondo[4] = { "rabbit_and_bacon",
                         "fish_and_chips",
                         "roat_lamp",
                         NULL
                        };

    char *contorno[4] = { "gateaux_opera",
                          "ice_cream",
                          "cheese_cake",
                          NULL
                        };

    char ***data = (char ***) malloc(3 * sizeof (char **));
    data[0] = primo;
    data[1] = secondo;
    data[2] = contorno;

    // initialize menu to be filled
    menu_t *menu = malloc(sizeof(menu_t));
    menu->id = 0;
    menu->dishes = (char **) malloc(3 * sizeof (char *));

    generate_menu(data, menu, 0);

}

void generate_menu(char ***data, menu_t *m, int depth) {

    if (depth == 3) {
        m->id++;
        printf("menu_%d: %s %s %s\n", m->id, m->dishes[0], m->dishes[1], m->dishes[2]);
        return;
    }
    char **choices = data[depth];
//  char **dish = choices;
//  for (int i = 0; dish[i] != NULL; i++)

    for (char **dish = choices; *dish != NULL; dish++) {
        m->dishes[depth] = strdup(*dish);
        generate_menu(data, m, depth+1);
    }
}
