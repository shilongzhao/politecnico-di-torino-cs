/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


#include <stdio.h>

int compare(int *v1, int *v2, int d) {
	for(int i = 0; i < d; i++) {
		if (v1[i] != v2[i])
			return 0;
	}
	return 1;
}

int search(int *v1, int *v2, int d1, int d2) {
	
	for (int i = 0; i < d1-d2; i++) {
		if (compare(&v1[i], v2, d2))
			return i;
	}
	return -1;
}

int main() {
	int a[10] = {1,2,3,4,5,6,7};
	int b[3] = {3,4,5};

	printf("%d\n", search(a, b, 10,3));
}
