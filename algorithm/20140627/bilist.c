/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


#include <stdio.h>
#include <stdlib.h>

typedef struct node_t_ node_t;
struct node_t_ {
    int key;
    node_t *left;
    node_t *right;
};


void insertRight(node_t **node, node_t *tmp) {

    (*node)->right = tmp;
    tmp->left = (*node);
    (*node) = tmp;
    return;
}

void insertLeft(node_t **node, node_t *tmp) {
    (*node)->left = tmp;
    tmp->right = (*node);
    (*node) = tmp;
}

void listInsert (node_t **left, node_t **right, int key, int leftRight) {

    node_t *tmp = malloc(sizeof(node_t));
    tmp->key = key;
    tmp->left = NULL;
    tmp->right = NULL;

    if (*left == NULL && *right == NULL) {
        *left = tmp;
        *right = tmp;
        return;
    }

    if (leftRight)
        insertRight(right, tmp);
    else
        insertLeft(left, tmp);

}

// another version of insert
void insert(node_t **left, node_t **right, int key, int left_right) {
    node_t *tmp = malloc(sizeof(node_t));
    tmp->key = key;
    tmp->left = NULL;
    tmp->right = NULL;

    // first inserted node, list was empty
    if (*left == NULL && *right == NULL) {
        *left = tmp;
        *right = tmp;
        return;
    }

    if (left_right) { // insert to right
        (*right)->right = tmp;
        tmp->left = *right;
        (*right) = tmp;

    }
    else { // insert to left
        (*left)->left = tmp;
        tmp->right = *left;
        (*left) = tmp;
    }
}


void displayRightLeft(node_t *node) {
    for(node_t *tmp = node; tmp != NULL; tmp=tmp->left) {
        printf("%d ", tmp->key);
    }
    printf("\n");
    return;
}

void displayLeftRight(node_t *node) {
    for (node_t *tmp = node; tmp != NULL; tmp=tmp->right) {
        printf("%d ", tmp->key);
    }
    printf("\n");
    return;
}

void listDisplay (node_t *left, node_t *right, int leftRight) {
    if(leftRight)
        displayRightLeft(right);
    else
        displayLeftRight(left);
}

int main() {

    node_t *left = NULL;
    node_t *right = NULL;

    for (int i = 0; i < 10; i++) {
        listInsert(&left, &right, i, i%2);
    }

    listDisplay(left, right, 0);
    listDisplay(left, right, 1);

}