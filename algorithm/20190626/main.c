#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// 1
void longestSubstring(char *in, char **out) {
    if (in == NULL) {
        *out = NULL;
        return;
    }

    // currently checking sequence start position and length
    int s = 0, len = 0;
    // remembers longest known sequence start position and length
    int max_s = 0, max_len = 0;
    for (int i = 0; i <= strlen(in); i++) {
        if (in[i] >= 'A' && in[i] <= 'Z') {
            len++;
        } else {
            if (len > max_len) {
                max_len = len;
                max_s = s;
            }
            len = 0;
            s = i;
        }
    }

    *out = calloc((size_t) max_len + 1, sizeof(char));
    strncpy(*out, in + max_s + 1, max_len);
}

// 2
typedef struct node node;

struct node {
    int k;

    node *left;
    node *right;
};

typedef struct path path;

struct path {
    int k;
    path *next;
};

void find(node *root, path *p, int len, int **nums, int *max_len) {

    if (root == NULL) {
        return;
    }

    path *h = malloc(sizeof(path));
    h->k = root->k;
    h->next = p;
    len += 1;

    if (root->left == NULL && root->right == NULL) {
        if (len > *max_len) {
            *max_len = len;
            // copy path into nums[]
            if (*nums != NULL) free(*nums);
            *nums = malloc(len * sizeof(int));
            for (int i = len - 1; i >= 0; i--) {
                (*nums)[i] = h->k;
                h = h->next;
            }
        }
        free(h);
        return;
    }

    if (root->left != NULL && root->left->k > root->k) {
        find(root->left, h, len, nums, max_len);
    }

    if (root->right != NULL && root->right->k > root->k) {
        find(root->right, h, len, nums, max_len);
    }

    free(h);
}

void longestPath(node *root) {
    int *nums = NULL;
    int max_len = 0;
    path *p = NULL;
    int len = 0;

    find(root, p, len, &nums, &max_len);

    if (nums != NULL) {
        printf("max len = %d\n", max_len);
        for (int i = 0; i < max_len - 1; i++) {
            printf("%d, ", nums[i]);
        }
        printf("%d",nums[max_len - 1]);
        free(nums);
    }
    printf("\n");

}

void test1() {
    printf("============ test 1 =============\n");
    printf("expected {1}, result:");
    node n1 = {1, NULL, NULL};
    longestPath(&n1);
}

void test2() {
    printf("============ test 2 =============\n");
    printf("expected {1, 2, 3}, result:");
    node n1 = {1, NULL, NULL};
    node n2 = {2, NULL, NULL};
    node n3 = {3, NULL, NULL};
    n1.right = &n2;
    n2.right = &n3;
    longestPath(&n1);
}

void test3() {
    printf("============ test 3 =============\n");
    printf("expected {1, 2, 3}, result:");
    node n1 = {1, NULL, NULL};
    node n2 = {2, NULL, NULL};
    node n3 = {3, NULL, NULL};
    n1.left = &n2;
    n2.right = &n3;
    longestPath(&n1);
}

void test4() {
    printf("============ test 4 =============\n");
    printf("expected {1, 2, 3, 4}, result:");
    node n1 = {1, NULL, NULL};
    node n2 = {2, NULL, NULL};
    node n3 = {3, NULL, NULL};
    node n4 = {4, NULL, NULL};

    n1.left = &n2;
    n2.right = &n3;
    n3.left = &n4;

    longestPath(&n1);
}

void test5() {
    printf("============ test 5 =============\n");
    printf("expected {1, 3, 4}, result:");
    node n1 = {1, NULL, NULL};
    node n2 = {2, NULL, NULL};
    node n3 = {3, NULL, NULL};
    node n4 = {4, NULL, NULL};

    n1.left = &n2;
    n1.right = &n3;
    n3.left = &n4;

    longestPath(&n1);
}

// 3
/**
 * recursive function
 * @param n remaining value
 * @param nums combinations
 * @param d depth
 * @param p last picked value
 */
void helper(int n, int *nums, int d, int p) {
    if (n == 0) {
        for (int i = 0; i < d - 1; i++) {
            printf("%d, ", nums[i]);
        }
        printf("%d \n", nums[d - 1]);
    }

    for (int i = 1; i <= p && i <= n; i++) {
        nums[d] = i;
        helper(n - i, nums, d + 1, i);
    }
}

void nonIncreasing(int n) {
    int *nums = malloc(sizeof(int) * n);
    for (int i = 1; i <= n; i++) {
        nums[0] = i;
        helper(n - i, nums, 1, i);
    }
}



int main() {

    char *out = NULL;
    longestSubstring("Hi, this IS some MAGIC test, ABRACADABRA", &out);
    printf("out = %s, len = %ld\n", out, strlen(out));
    free(out);

    longestSubstring("Hi, this IS some PROFESSIONALISM test, ABRACADABRA", &out);
    printf("out = %s, len = %ld\n", out, strlen(out));
    free(out);


    test1();
    test2();
    test3();
    test4();
    test5();

    printf("non increasing sequence 1: \n");
    nonIncreasing(1);

    printf("non increasing sequence 3: \n");
    nonIncreasing(3);

    printf("non increasing sequence 5: \n");
    nonIncreasing(5);

    return 0;
}