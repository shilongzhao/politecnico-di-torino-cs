#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @author admin@szhao.org
 */

void invert_sequence(int *v1, int n, int *v2) {
    int i = 0, j = 0;
    while (i < n) {

        j += 1;
        // increase j until non-increasing, or end of array (j == n)
        while (v1[j] > v1[j - 1] && j < n) j++;

        for (int p = i, q = j - 1; p < j; p++, q--) {
            v2[p] = v1[q];
        }

        i = j;
    }
}

typedef struct node node;
struct node {
    int k;
    node *next;
};

void int2list(int n, struct node **head) {
    while (n != 0) {
        int k = n % 10; // get the least important digit

        node *t = malloc(sizeof(node));
        t->k = k;
        t->next = *head;
        *head = t;

        n = n / 10; // shift right (remove the least important digit)
    }
}

/**
 * @author admin@szhao.org
 */

int is_palindrome(const char *s, int start, int end);
void part(char *s, int start, char **result, int n);
char *substring(const char *s, int start, int end);

void partition(char *s) {
    int N = strlen(s);
    char **m = calloc((size_t) N, sizeof(char *));
    part(s, 0, m, 0);
    for (int i = 0; i < N; i++) {
        free(m[i]);
    }
    free(m);
}

void part(char *s, int start, char **result, int n) {
    if (start >= strlen(s)) {
        for (int i = 0; i < n - 1; i++) {
            printf("%s-", result[i]);
        }
        printf("%s\n", result[n - 1]);
    }

    for (int end = start; end < strlen(s); end++) {
        if (is_palindrome(s, start, end)) {
            result[n] = substring(s, start, end);
            part(s, end + 1, result, n + 1);
        }
    }
}

/**
 * substring
 * @param s
 * @param start inclusive
 * @param end inclusive
 * @return
 */
char *substring(const char *s, int start, int end) {
    char *t = malloc(sizeof(char) * (end - start + 2));
    for (int i = 0; i < end - start + 1; i++) {
        t[i] = s[start + i];
    }
    t[end - start + 1] = '\0';
    return t;
}

/**
 *
 * @param s
 * @param start start index inclusive
 * @param end end index inclusvie
 * @return 1 if is palindrome from start to end, 0 otherwise
 */
int is_palindrome(const char *s, int start, int end) {
    while (start < end) {
        if (s[start++] != s[end--]) return 0;
    }
    return 1;
}

int main() {

    int a1[10] = {1,2,3,4,5,0,12,13,14,2};
    int a2[10] = {};

    printf("\n============= testing 1 =============\n");
    invert_sequence(a1, 10, a2);
    for (int i = 0; i < 10; i++) {
        printf("%d ", a2[i]);
    }

    printf("\n============= testing 2 =============\n");
    node *head = NULL;
    int2list(1357, &head);
    for (node *p = head; p != NULL; p = p->next) {
        printf("%d ", p->k);
    }

    printf("\n============= testing 3 =============\n");
    partition("annabelle");

    return 0;
}
