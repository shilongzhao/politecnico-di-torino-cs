/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct solution_t_ {
    int freq[5];
    int N;
    char *str;
}solution_t;

int all_used(int *v, int n) {
    for (int i = 0; i < n; i++) {
        if (v[i] == 0)
            return 0;
    }
    return 1;
}

void generate(solution_t *sol, char *vocals, int len, int depth) {
    if (depth == len) {
        if (all_used(sol->freq, 5))
            printf("%s\n", sol->str);
        return;
    }

    for(int i = 0; i < strlen(vocals); i++) {
        sol->str[depth] = vocals[i];
        sol->freq[i] += 1;
        generate(sol, vocals, len, depth+1);
        sol->freq[i] -= 1;
    }

}

int main(int argc, char *argv[]) {
    int N = atoi(argv[1]);
    solution_t *sol = malloc(sizeof(solution_t));
    sol->N = N;
    sol->str = malloc((N+1) * sizeof(char));
    sol->str[N] = '\0';

    for (int i = 0; i < 5; i++) {
        sol->freq[i] = 0;
    }

    char vocals[6] = "AEIOU";

    generate(sol, vocals, N, 0);
}