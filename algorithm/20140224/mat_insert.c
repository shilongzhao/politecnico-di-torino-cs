/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct node_t_ node_t;
struct node_t_ {
    int c;
    float k;
    node_t *next;
};

typedef struct matr_t_ {
    int nr;
    int nc;
    node_t **array;
} matr_t;

void matr_insert(matr_t *mat, int r, int c, float val) {

    node_t *new = malloc(sizeof(node_t));
    new->k = val;
    new->c = c;
    new->next = mat->array[r];
    mat->array[r] = new;

    return;
}


int main(int argc, char *argv[]) {
    matr_t *mat = malloc(sizeof(matr_t));
    mat->nc = 100;
    mat->nr = 10;
    mat->array = (node_t **)malloc(mat->nr * sizeof(node_t *));

    for ( int i = 0; i < mat->nr; i ++) {
        mat->array[i] = NULL;
    }
    for (int i = 0; i < 10; i ++) {
        for (int j = 0; j < 100; j +=10) {
            matr_insert(mat, i, j, i+j);
        }
    }

    for (int i = 0; i < mat->nr; i++) {
        printf("row %d: ", i);
        for(node_t *tmp = mat->array[i]; tmp != NULL; tmp = tmp->next) {
            printf("%d:%.1f ", tmp->c, tmp->k);
        }
        printf("\n");
    }

}