#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static char *vocals = "AEIOU";

int all_used(char *str) {
    for (int i = 0; i < strlen(vocals); i++) {
        if (strchr(str, vocals[i]) == NULL) {
            return 0;
        }
    }
    return 1;
}
void generate(char *result, int depth, int n) {
    if (depth == n) {
        if (all_used(result)) {
            printf("%s\n", result);
        }
        return;
    }

    for (int i = 0; i < strlen(vocals); i++) {
        result[depth] = vocals[i];
        generate(result, depth+1, n);
    }
}

int main(int argc, char *argv[]) {
    int N = atoi(argv[1]);
    char *result = malloc(sizeof(char) * (N + 1));
    generate(result, 0, N);
}