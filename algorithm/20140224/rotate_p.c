/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>


void rotate_right(int *v, int n, int p) {
	int tmp[p];
	for ( int i = n-p; i < n; i++) {
		tmp[i-(n-p)] = v[i];
	}

	for (int i = n-p-1; i >=0 ; i--) {
		v[i+p] = v[i];
	}

	for (int i = 0; i < p; i++) {
		v[i] = tmp[i];
	}
	return;
}

void rotate_left(int *v, int n, int p) {
	int tmp[p];

	for (int i = 0; i < p; i++) {
		tmp[i] = v[i];
	}

	for (int i = p; i < n; i++) {
		v[i-p] = v[i];
	}

	for (int i = n -p; i < n; i++) {
		v[i] = tmp[i-(n-p)];
	}

	return;
}

void rotate(int *v, int n, int p) {
	if (p > 0)
		rotate_right(v, n, p);

	if (p < 0)
		rotate_left(v, n, -p);
}

int main(int argc, char *argv[]) {
	int a[10] = {1,2,3,4,5,6,7,8,9,10};
	rotate(a, 10, 4);
	for (int i = 0; i < 10; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");
	rotate(a, 10, -4);
	for (int i = 0; i < 10; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");

	return 0;
}
