//
//  main.c
//  class15-bulb
//
//  Created by ZhaoShilong on 19/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int R, C;
int *bulbs; // bulbs[i] = ith bulb is on/1 or off/0

int all_on(int *a) {
    for (int i = 0; i < C; i++) {
        if (a[i] == 0) {
            return 0;
        }
    }
    return 1;
}
void press(int *b, int *s) {
    for (int i = 0; i < C; i++) {
        b[i] = b[i] ^ s[i];
    }
}
/**
 * l: result 中有几个 element
 * k: 下一个可供选择的 switch
 */
void switches(int **m, int *result, int k, int l){
    if (all_on(bulbs)) {
        for (int i = 0; i < l; i++) {
            printf("%d, ", result[i]);
        }
        printf("\n");
    }
    for (int i = k; i < R; i++) {
        result[l] = i;
        press(bulbs, m[i]);
        switches(m, result, i + 1, l + 1);
        press(bulbs, m[i]); // backtracing
    }
}

int main(int argc, const char * argv[]) {
    R = 4;
    C = 5;
    int **mat = malloc(sizeof(int *) * R);
    for (int i = 0; i < R; i++) {
        mat[i] = malloc(sizeof(int) * C);
    }
    
    int a[4][5] =  {
        {1, 1, 0, 0, 1},
        {1, 0, 1, 0, 0},
        {0, 1, 1, 0, 1},
        {1, 0, 0, 1, 0}
    };
    
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < C; j++) {
            mat[i][j] = a[i][j];
        }
    }
    int *result = malloc(sizeof(int) * R);
    bulbs = malloc(sizeof(int) * C);
    
    switches(mat, result, 0, 0);
    return 0;
}
