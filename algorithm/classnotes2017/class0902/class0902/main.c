//
//  main.c
//  class0902
//
//  Created by ZhaoShilong on 01/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
typedef struct btree_t btree_t;
struct btree_t {
    int k;
    btree_t *left;
    btree_t *right;
};

void insert(btree_t **root, int v) {
    btree_t *t = malloc(sizeof(btree_t));
    t->k = v;
    t->left = NULL;
    t->right = NULL;
    if (*root == NULL) {
        *root = t;
        return;
    }
    if ((*root)->k < v) {
        insert(&((*root)->right), v);
    }
    if ((*root)->k > v) {
        insert(&((*root)->left), v);
    }
}

void pre_order(btree_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%d\n", root->k);
    pre_order(root->left);
    pre_order(root->right);
}

void in_order(btree_t *root) {
    if (root == NULL) {
        return;
    }
    in_order(root->left);
    printf("%d\n", root->k);
    in_order(root->right);
}
void post_order(btree_t *root) {
    if (root == NULL) {
        return;
    }
    post_order(root->left);
    post_order(root->right);
    printf("%d\n", root->k);
}

void printPath(btree_t *root, int h, btree_t *path[], int n) {
    if (root == NULL) {
        return;
    }
    if (root->left == NULL && root->right == NULL) {
        for (int i = 0; i < n; i++) {
            printf("%d->", path[i]->k);
        }
        printf("%d\n", root->k);
        return;
    }
    path[n] = root;
    printPath(root->left, h, path, n+1);
    printPath(root->right, h, path, n+1);
}

int main(int argc, const char * argv[]) {
    btree_t *root = NULL;
    insert(&root, 6);
    insert(&root, 2);
    insert(&root, 10);
    insert(&root, 1);
    insert(&root, 4);
    insert(&root, 8);
    insert(&root, 12);

    pre_order(root);
    printf("----INORDER----\n");
    in_order(root);
    printf("----POSTORDER----\n");
    post_order(root);
    
    btree_t *path[5] = {};
    printPath(root, 3, path, 0);
    return 0;
}
