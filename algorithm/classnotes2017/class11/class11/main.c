#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct node node;
struct node{
    int k;
    node *left;
    node *right;
};
/***
 * k1 <= k2
 **/
node *lowest_pa(node *root, int k1, int k2) {
    if (root == NULL) {
        return NULL;
    }
    if (root->k >= k1 && root->k <= k2) {
        return root;
    }
    else if (root->k < k1) {
        return lowest_pa(root->right, k1, k2);
    }
    else {
        return lowest_pa(root->left, k1, k2);
    }
}
/**
 k1 <= k2
 */
node *lowest_pa2(node *root, int k1, int k2) {
    while (root->k < k1 || root->k > k2) {
        if (root->k < k1) {
            root = root->right;
        }
        else {
            root = root->left;
        }
    }
    return root;
}

int dist(node *root, int k) {
    if (root->k == k) {
        return 0;
    }
    else if (root->k < k) {
        return 1 + dist(root->right, k);
    }
    else {
        return 1 + dist(root->left, k);
    }
}

int dist2(node *root, int k) {
    int n = 0;
    while (root->k != k) {
        if (root->k < k) {
            n++;
            root = root->right;
        }
        else {
            n++;
            root = root->left;
        }
    }
    return n;
}

int main(int argc,char *argv[])
{
   
}

