//
//  main.c
//  classTwoPointer
//
//  Created by ZhaoShilong on 18/10/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>

void swap(int *x, int *y) {
    int c;
    c = *x;
    *x = *y;
    *y = c;
}
int main(int argc, const char * argv[]) {
    // insert code here...
    int a = 10;
    int *p = &a;
    printf("a value = %d, a address = %p, p value = %p, p address = %p\n",a, &a, p, &p);
    
    int b = *p;
    printf("b value = %d\n", b);
    
    *p = 20;
    printf("a value = %d, a address = %p, p value = %p, p address = %p\n",a, &a, p, &p);
    printf("b value = %d\n", b);
    // a 20, b 10;
    swap(&a, &b);
    // a 10, b 20
    printf("a value = %d, b value = %d\n ", a, b);
    return 0;
}
