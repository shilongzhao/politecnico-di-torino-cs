//
//  main.c
//  class09
//
//  Created by ZhaoShilong on 01/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
typedef struct list_t list_t;
struct list_t {
    int v;
    list_t *next;
};
//遍历
void print_list(list_t *h) {
    if (h == NULL) {
        return;
    }
    printf("%d", h->v);
    print_list(h->next);
}

void print_list_backwards(list_t *h) {
    if (h == NULL) {
        return;
    }
    print_list_backwards(h->next);
    printf("%d\n", h->v);
}

int fibo(int n ) {
    if (n == 1 || n == 2) {
        return 1;
    }
    
    return fibo(n-1) + fibo(n-2);
}

int f(int n) {
    if (n == 0) {
        return 0;
    }
    return n + f(n-1);
}

int main(int argc, const char * argv[]) {
    // insert code here...
    list_t t1 = {.v = 1, .next = NULL};
    list_t t2 = {.v = 2, .next = &t1};
    list_t t3 = {.v = 3, .next = &t2};
    
    print_list(&t3);
    printf("----\n");
    print_list_backwards(&t3);
    
    printf("%d\n", f(10));
    printf("%d\n", fibo(7));
    return 0;
}
