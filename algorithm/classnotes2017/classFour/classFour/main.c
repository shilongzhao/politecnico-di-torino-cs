//
//  main.c
//  classFour
//
//  Created by ZhaoShilong on 24/10/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <ctype.h>

typedef struct student student_t;
typedef struct teacher teacher_t;
struct student {
    int id;
    char *name;
    float grade;
    float grade2;
    float grade3;
    float average;
};

struct teacher {
    int id;
    char *name;
    char *class;
};

// 163.com e1888@126.com
void printDigit(char *s) {
    for (int i = 0; i < strlen(s); i++) {
        if (isdigit(*(s+i))) {
            printf("%c", s[i]);
        }
    }
}
// tuesday -> TUESDAY, october 12 -> OCTOBER 12
void str2upper(char *s) {
    for (int i = 0; s[i]!= '\0'; i++) {
        if (islower(s[i])) {
            s[i] = toupper(s[i]);
        }
    }
}

void average(student_t s) {
    float a = (s.grade + s.grade2 + s.grade3) / 3;
    s.average = a;
}

void average2(student_t *p) {
    float a = ((*p).grade + (*p).grade2 + p->grade3)/3;
//    (*p).average = a;
    p->average = a;
}
//计算所有学生所有分数的平均值
float all_average(student_t s[], int n) {
    return 0;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    char *p = "126pp234@00.com";
    printDigit(p);

    char str[] = "october 12";
    str2upper(str);
    printf("\n%s\n", str);
    
    student_t s1;
    student_t s2;
    teacher_t t1;
    
//    s1 = {
//        .id = 12345,
//        .name = "zhang",
//    };
    s1.id = 12345;
    s1.name = "zhang";
    s1.grade = 25.5;
    s1.grade2 = 30;
    s1.grade3 = 32;
    s1.average = 0;
    
    s2.id = 123456;
    s2.name = "li";
    s2.grade = 23.8;
    s2.grade2 = 31;
    s2.grade3 = 20;
    
    student_t students[2] = {s1, s2};
    
    t1.id = 12349;
    t1.name = "wang";
    t1.class = "math";
    
    average2(&s1);
    
    printf("%d %s %.2f\n", s1.id, s1.name, s1.average);
    
    printf("%f\n", all_average(students, 2));
}



