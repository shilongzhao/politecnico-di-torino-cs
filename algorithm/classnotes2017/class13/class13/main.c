//
//  main.c
//  class13
//
//  Created by ZhaoShilong on 14/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int N;

// 判断 AEIOU 都在 result 中
// 1: 都在
// 0: 至少有一个字符不存在
int allin(char *result) {
    int a = 0, e = 0, i = 0, o = 0, u = 0;
    for (int k = 0; k < strlen(result); k++) {
        switch (result[k]) {
            case 'A':
                a = 1;
                break;
            case 'E':
                e = 1;
                break;
            case 'I':
                i = 1;
                break;
            case 'O':
                o = 1;
                break;
            case 'U':
                u = 1;
                break;
            default:
                break;
        }
    }
    return a && e && i && o && u;
}
// arrange with repeating 2
void generate(char *result, char *str, int k) {
    if (k == N) {
        result[k] = '\0';
        if (allin(result)) {
            printf("%s\n", result);
        }
        return;
    }
    
    for (int i = 0; i < strlen(str); i++) {
        result[k] = str[i];
        generate(result, str, k+1);
    }
}

int main(int argc, const char * argv[]) {
    N = 6;
    char str[6] = "AEIOU";
    char result[10] = "";
    generate(result, str, 0);
    return 0;
}
