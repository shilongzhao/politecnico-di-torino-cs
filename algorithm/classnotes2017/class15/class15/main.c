//
//  main.c
//  class15
//
//  Created by ZhaoShilong on 19/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <ctype.h>

int N;

/**
 * every char appear no more than N times
 */
int ok(char *result) {
    int a[26] = {}; // a[0] = A frequency, a[1] = 'B' frequency, ... a[25] = 'Z' frequency;
    int b[10] = {}; // b[0] = '0' frequency, ... b[9] = '9' frequency.
    for (int i = 0; i < strlen(result); i++) {
        if (isdigit(result[i])) {
            b[ result[i] - '0' ] += 1;
        } else if (isalpha(result[i])) {
            a[ result[i] - 'A' ] += 1;
        }
    }
    for (int i = 0; i < 26; i++) {
        if (a[i] > N) {
            return 0;
        }
    }
    for (int i = 0; i <= 9; i++) {
        if (b[i] > N) {
            return 0;
        }
    }
    return 1;
}
/**
 * 0 - 9
 */
void password2(char *result, int k) {
    if (k == 5) {
        result[k] = '\0';
        if (ok(result)) {
            printf("%s\n", result);
        }
        return;
    }
    for (int i = 0; i <= 9; i++) {
        result[k] = '0' + i;
        password2(result, k + 1);
    }
}

/**
 * A - Z
 */
void password(char *result, int k) {
    if (k == 3) {
        password2(result, k);
        return;
    }
    for (int i = 0; i < 26; i++) {
        result[k] = 'A' + i;
        password(result, k+1);
    }
}


int main(int argc, const char * argv[]) {
    char result[10] = "";
    N = 1;
    password(result, 0);
}
