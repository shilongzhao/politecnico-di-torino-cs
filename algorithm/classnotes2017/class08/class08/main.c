#include <stdio.h>
#include <stdlib.h>
typedef struct list_t list_t;
struct list_t {
    int v;
    int c;
    list_t *next;
};
typedef struct matr_t matr_t;
struct matr_t {
    int nr;
    int nc;
    list_t **array;
};

void print_list(list_t *h) {
    for (; h != NULL; h = h->next) {
        printf("col = %d, val = %d\n", h->c, h->v);
    }
}

void matInsert (matr_t *mat, int r, int c, float val) {
    list_t *t = malloc(sizeof(list_t));
    t->v = val;
    t->c = c;
    t->next = NULL;
    
    t->next = mat->array[r];
    mat->array[r] = t;
}
int main(int argc, const char * argv[]) {
    matr_t *mat = malloc(sizeof(matr_t));
    mat->nr = 4;
    mat->nc = 6;
    mat->array = malloc(sizeof(list_t *) * (mat->nr));
    for (int i = 0; i < mat->nr; i++) {
        mat->array[i] = NULL;
    }
    
    matInsert(mat, 0, 0, 1);
    matInsert(mat, 1, 1, 2);
    matInsert(mat, 2, 2, 3);
    matInsert(mat, 3, 3, 4);
    for (int i = 0; i < mat->nr; i++) {
        printf("row = %d: ", i);
        print_list(mat->array[i]);
        printf("\n");
    }
    printf("Hello, World!\n");
    return 0;
}
