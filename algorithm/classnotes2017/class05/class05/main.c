//
//  main.c
//  class05
//
//  Created by ZhaoShilong on 26/10/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
typedef struct list_t list_t;
struct list_t {
    int v;
    list_t *next;
};

int main(int argc, const char * argv[]) {
    // insert code here...
    list_t *p = NULL;
    // 插入
    list_t t = {.v = 2, .next = NULL};
    p = &t;
    list_t q = {.v = 5, .next = p};
    p = &q;
    list_t r = {.v = 8, .next = p};
    p = &r;
    // 遍历
    for (list_t *h = p; h != NULL;h = h->next){
        printf("%d -> ", h->v);
    }
    // 查找
    int x = 10;
    int flag = 0;
    for (list_t *h = p; h != NULL;h = h->next) {
        if (h->v == x){
            flag = 1;
        }
    }
    if (flag == 0) {
        printf("10 does not exist!\n");
    }
    // 查找并删除5,打印
    while (p->v == 5) {
        p = p->next;
    }

    for (list_t *h = p; h->next != NULL; h = h->next) {
        if (h->next->v == 5) {
            h->next = h->next->next;
        }
    }
    
    // 遍历
    for (list_t *h = p; h != NULL;h = h->next){
        printf("%d -> ", h->v);
    }
    
    
    return 0;
}
