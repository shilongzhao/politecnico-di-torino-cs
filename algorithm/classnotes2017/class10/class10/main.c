//
//  main.c
//  class10
//
//  Created by ZhaoShilong on 06/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>

typedef struct node node;

struct node {
    int key;
    struct node *left;
    struct node *right;
};

void printPath(struct node *root, int h, struct node *path[], int n) {
    if (root == NULL) {
        return;
    }
    if (root->left == NULL && root->right == NULL) {
        for (int i = 0; i < n; i++) {
            printf("%d->", path[i]->key);
        }
        printf("%d\n", root->key);
        return;
    }
    
    path[n] = root;
    printPath(root->left, h, path, n+1);
    printPath(root->right, h, path, n+1);
}

int main(int argc, const char * argv[]) {
    // insert code here...
    printf("Hello, World!\n");
    return 0;
}
