//
//  main.c
//  class06
//
//  Created by ZhaoShilong on 28/10/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
typedef struct list_t list_t;
struct list_t {
    int v;
    list_t *next;
};
// 创建/插入, 作用域 scope
list_t *insert1(list_t *h, int x) {
    list_t *l = malloc(sizeof(list_t));
    l->v = x;
    l->next = h;
    return l;
}
void insert2(list_t **h, int x) {
    list_t *l = malloc(sizeof(list_t));
    l->v = x;
    l->next = *h;
    *h = l;
}
//遍历
void print_list(list_t *h) {
    printf("printing list: \n");
    for (list_t *t = h; t != NULL; t = t->next) {
        printf("%d ", t->v);
    }
    printf("\n");
}
// 查找, 找到返回1, 没找到返回0
int search(list_t *t, int x) {
    int flag = 0;
    for (list_t *h = t; h != NULL; h = h->next) {
        if (h->v == x) {
            flag = 1;
            break;
        }
    }
    return flag;
}
// 删除所有 x
list_t *delete1(list_t *h, int x) {
    return NULL;
}
void delete2(list_t **h, int x) {
    
}
int main(int argc, const char * argv[]) {
    list_t *p = NULL;

    p = insert1(p, 1);
    print_list(p);
    p = insert1(p, 2);
    p = insert1(p, 3);
    print_list(p);
    insert2(&p, 4);
    insert2(&p, 5);
    print_list(p);
    
    int r = search(p, 3);
    printf("result r = %d\n", r);
    r = search(p, 0);
    printf("result r = %d\n", r);


}
