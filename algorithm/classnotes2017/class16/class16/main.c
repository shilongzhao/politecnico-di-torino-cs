//
//  main.c
//  class16
//
//  Created by ZhaoShilong on 21/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int N;
int *a; // a length = N, a[k] = max possible hops when at position k.
int min = INT_MAX;
/***
 * when at position k
 * already have h hops
 */
void jump(int k, int h) {
    if (k >= N-1) {
        if (h < min) {
            min = h;
        }
        return;
    }
    for (int i = k + 1; i <= k + a[k] && i < N; i++) {
        jump(i, h+1);
    }
}
int main(int argc, const char * argv[]) {
    N = 7;
    a = malloc(sizeof(int) * N);
    int b[7] = {2,0,2,1,2,1,1};
    for (int i = 0; i < 7; i++) {
        a[i] = b[i];
    }
    jump(0, 0);
    printf("min hop = %d\n", min);
    return 0;
}
