//
//  main.c
//  class12
//
//  Created by ZhaoShilong on 09/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int LEN;
// arrange with repeat
void generate(char *result, char *str, int n) {
    if (n == 0) {
        result[0] = '\0';
        printf("%s\n", result - LEN);
        return;
    }
    for (int i = 0; i < strlen(str); i++) {
        result[0] = str[i];
        generate(result+1, str, n-1);
    }
}

// arrange with repeating 2
void generate2(char *result, char *str, int k) {
    if (k == LEN) {
        result[k] = '\0';
        printf("%s\n", result);
        return;
    }
    
    for (int i = 0; i < strlen(str); i++) {
        result[k] = str[i];
        generate2(result, str, k+1);
    }
}

// arrange without repeating
int arr[5] = {0,0,0,0,0};
void arrange(char *result, char *str, int k) {
    if (k == LEN) {
        result[k] = '\0';
        printf("%s\n", result);
        return;
    }
    
    for (int i = 0; i < strlen(str); i++) { // expand
        if (arr[i] == 0) {
            result[k] = str[i];
            arr[i] = 1;
            arrange(result, str, k+1);
            // 图示讲解
            arr[i] = 0; // backtrace
        }
    }
}
// combination
// k: 下一个字符从哪个位置取
// l: 记录生成的字符串的长度
void combo(char *result, char *str, int k, int l) {
    if (l == LEN) {
        result[l] = '\0';
        printf("%s\n", result);
        return;
    }
    
    for (int  i = k; i < strlen(str); i++) {
        result[l] = str[i];
        combo(result, str, i + 1, l + 1);
    }
}

int main(int argc, const char * argv[]) {
    // generate string from ABCDE, each char can appear 0 - * times, with for loop
    char result[10] = "";
    char str[10] = "ABCDE";
    
    LEN = 3;
    
    
//    generate2(result, str, 0);
//    arrange(result, str, 0);
    combo(result, str, 0, 0);
    
    return 0;
}
