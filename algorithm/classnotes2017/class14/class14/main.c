//
//  main.c
//  class14
//
//  Created by ZhaoShilong on 15/11/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

FILE *out;
int N;
void genererate(char **mat, int n, char *result) {
    if (n == N) {
        result[n] = '\0';
        fprintf(out, "%s", result);
        return;
    }
    for (int i = 0; i < strlen(mat[n]); i++) {
        result[n] = mat[n][i];
        genererate(mat, n+1, result);
    }
}
//2014 min balance diff
// n: length of a
// k: length of r
void print_array(float *r, int k) {
    for (int i = 0; i < k; i++) {
        printf("%.2f ", r[i]);
    }
    printf("\n");
}
int *used;
float min_diff = INT_MAX;
float balance_diff(float *r, int n) {
    float max = INT_MIN, min = INT_MAX;
    float remain = 0;
    for (int i = 0; i < n; i++) {
        remain += r[i];
        if (remain > max){
            max = remain;
        }
        if (remain < min) {
            min = remain;
        }
    }
    return max - min;
}

void arrange(float *a, int n, float *r, int k) {
    if (k == n) {
        float diff = balance_diff(r, n);
        if (diff < min_diff) {
            min_diff = diff;
        }
        return;
    }
    for (int i = 0; i < n; i++) {
        if (used[i] == 0) {
            used[i] = 1;
            r[k] = a[i];
            arrange(a, n, r, k+1);
            used[i] = 0;
        }
    }
}
// child-sibling tree
typedef struct node_t node_t;
struct node_t {
    char *name;
    char *surname;
    int grade;
    node_t *child;
    node_t *sibling;
};

void print_tree(node_t *root) {
    if (root == NULL) {
        return;
    }
    
    print_tree(root->child);
    printf("%s %s %d\n", root->name, root->surname, root->grade);
    print_tree(root->sibling);
}


void password(char *result, int k) {
    if (k == 5) {
        result[k] = '\0';
        printf("%s\n",result);
        return;
    }
    if (k <= 2) {
        for (int i = 0; i < 26; i++) {
            result[k] = 'A' + i;
            password(result, k+1);
        }
    }
    else {
        for (int i = 0; i < 10; i++) {
            result[k] = '0' + i;
            password(result, k + 1);
        }
    }
}
int main(int argc, const char * argv[]) {
    // insert code here...
//    out = fopen("output.txt", "w");
//    N = 3;
//    char **m = malloc(sizeof(char *) * N);
//    for (int i = 0; i < N; i++) {
//        m[i] = malloc(sizeof(char) * 11);
//    }
//    strcpy(m[0], "ABC");
//    strcpy(m[1], "23");
//    strcpy(m[2], "xyz");
//    char *result = malloc(sizeof(char) * (N+1));
//    genererate(m, 0, result);
//
    //--------------- 2014 min balance diff -------------
//    int M = 4;
//    used = malloc(sizeof(float) * M);
//    for (int i = 0; i < M; i++) {
//        used[i] = 0;
//    }
//    float a[4] = {-5, 10, -8, 7};
//    float r[4] = {};
//    arrange(a, 4, r, 0);
//
//    printf("%.2f\n", min_diff);
    // ---------------- GENERATE PASSWORD -------------------
    char result[10] = "";
    password(result, 0);
    
    return 0;
}
