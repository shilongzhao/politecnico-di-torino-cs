//
//  main.c
//  classOne
//
//  Created by ZhaoShilong on 17/10/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>

int fsum(int x) {
    int sum = 0;
    for (int i = 1; i <= x; i++) {
        sum += i;
    }
    return sum;
}

int ff(int x, int y) {
    int sum = 0;
    for (int i = x; i <= y; i++) {
        sum += i;
    }
    return sum;
}

int array_sum(int m[], int n) {
    int s = 0;
    for (int i = 0; i < n; i++) {
        s += m[i];
    }
    return s;
}

int max_array(int x[], int y) {
    int max = x[0];
    for (int i = 0; i < y; i++) {
        if (max < x[i]) {
            max = x[i];
        }
    }
    return max;
}

int max_array2(int *x, int y) {
    int max = *x;
    for (int i = 0; i < y; i++) {
        if (max < *(x+i)) {
            max = x[i];
        }
    }
    return max;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    printf("sum in main = %d\n", fsum(100));
    printf("sum 1000 = %d\n", fsum(1000));
    printf("%d\n", fsum(10000));
    printf("%d\n", ff(1,2));
    
    //===================================
    int a[4] = {2,4,6,8};
    int b[3] = {1,2,3};
    
    printf("sum array a = %d\n", array_sum(a, 4));
    printf("sum array b = %d\n", array_sum(b, 3));

    //====================================
    printf("max array a = %d\n", max_array(a, 4));

    return 0;
}
