//
//  main.c
//  class07
//
//  Created by ZhaoShilong on 29/10/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct list_t list_t;
struct list_t {
    char *surname;
    char *name;
    list_t *next;
};

int compare(list_t *t1, list_t *t2){
    int r = strcmp(t1->surname, t2->surname);
    if (r != 0) {
        return r;
    }
    return strcmp(t1->name, t2->name);
}


int orderInsert (list_t **list, char *surname, char *name) {
    list_t *t = malloc(sizeof(list_t));
    t->surname = malloc(sizeof(char)* (strlen(surname) + 1));
    strcpy(t->surname, surname);
    t->name = malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(t->name, name);
    if (*list == NULL) {
        t->next = NULL;
        *list = t;
        return 1;
    }
    if (compare(t, *list) < 0) {
        t->next = *list;
        *list = t;
        return 1;
    }
    if (compare(t, *list) == 0) {
        return 0;
    }
    list_t *h;
    for (h = *list; h->next != NULL; h = h->next) {
        if (compare(h->next, t) == 0) {
            return 0;
        }
        else if (compare(h->next, t) < 0) {
            continue;
        }
        else {
            t->next = h->next;
            h->next = t;
            return 1;
        }
    }
    h->next = t;
    t->next = NULL;
    return 1;
}

int main(int argc, const char * argv[]) {
    list_t *h = NULL;
    
    orderInsert(&h , "zhang", "san");
    orderInsert(&h, "li", "si");
    orderInsert(&h, "wang", "wu");
    orderInsert(&h, "zhao", "liu");
    orderInsert(&h, "li", "si");
    
    for (list_t *p = h; p != NULL; p = p->next) {
        printf("%s %s\n", p->surname, p->name);
    }
    
    return 0;
}
