//
//  main.c
//  classThree
//
//  Created by ZhaoShilong on 19/10/2017.
//  Copyright © 2017 ZhaoShilong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int sum2array(int *a, int *b, int na, int nb) {
    int sum = 0;
    for (int i = 0; i < na; i++) {
        sum += a[i];
    }
    for (int i = 0; i < nb; i++) {
        sum += *(b + i);
    }
    return sum;
}

int *copy(int *m, int n){
    int *p = malloc(sizeof(int)*n);
    for (int i = 0; i < n; i++) {
        p[i] = m[i];
    }
    return p;
}

int *merge(int *a, int na, int *b, int nb) {
    // fill your code here
    return NULL;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    int arr[5] = {3,4,5,1,2};
    int b[3] = {6,7,8};
    for (int i = 0; i < 5; i++) {
        printf("%d \n", *(arr + i));
    }
    printf("%d\n", sum2array(arr, &b[0], 5, 3));
    
    int *mm = merge(arr, 5, b, 3);
    for (int i = 0; i < 8; i++) {
        printf("mm = %d\n", mm[i]);
    }
    // ===========================
    int *p;
    p = malloc(10 * sizeof(int));
    for (int i = 0; i < 10; i++) {
        p[i] = i;
    }
    int sum = 0;
    for (int i = 0; i < 10; i++) {
        sum += *(p+i);
    }
    printf("%d\n", sum);
    // ============================
    int m[5] = {1,2,3,4,5};
    int *q = NULL;
    
    q = copy(m, 5);
    for (int i = 0; i < 5; i++) {
        printf("p[i] = %d\n", *(q+i));
    }

    int * r = m;
    for (int i = 0; i < 5; i++) {
        printf("r[i] = %d\n", *(r+i));
    }
    r[0] = 4;
    printf("m[0] = %d\n", m[0]);
    
    return 0;
}
