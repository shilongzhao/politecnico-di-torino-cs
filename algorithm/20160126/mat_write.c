
row_t *insert_row(row_t *head, int r) {
    row_t *tmp = malloc(sizeof(row_t));
    tmp->row = r;
    tmp->next_row = head->next_row;
    head->next_row = tmp;
    return head;
}

node_t *insert_node(node_t *head, int c, float val) {
    node_t *tmp = malloc(sizeof(node_t));
    tmp->val = val;
    tmp->col = c;
    tmp->next = head->next;
    head->next = tmp;
    return head;
}

void matWrite(mat_t *M, float value, int r, int c) {

    if (M == NULL || r >= M->nr || c >= M->nc) return;

    row_t *curr_r = NULL;
    node_t *curr_n = NULL;
    for (curr_r = M->first_row; curr_r != NULL; curr_r = curr_r->next_row) {
        if (curr_r->row == r) {
            for (curr_n = curr_r->row_node; curr_n != NULL; curr_n = curr_n->next) {
                if (curr_n->col == c) {
                    break;
                }
            }
            break;
        }
    }

    // delete node
    if (value == 0 && curr_n != NULL) {
        node_t *p = curr_r->row_node;
        if (p == curr_n) {
            curr_r->row_node = curr_r->row_node->next;
            free(p);
        }

        while(p->next != curr_n) p = p->next;
        p->next = curr_n->next;
        free(curr_n);
    }

    // node exist
    if (value != 0 && curr_n != NULL) {
        curr_n->val = value;
    }

    // no row and no node
    if (curr_r == NULL) {
        // insert a row
        M->first_row = insert_row(M->first_row, r);
        // insert a node to that row
        M->first_row->row_node = insert_node(M->first_row->row_node, c, value);
    }

    // there is row, but no node
    if (curr_r != NULL && curr_n == NULL) {
        // insert a node
        curr_r->row_node = insert_node(curr_r->row_node, c, value);
    }
}
