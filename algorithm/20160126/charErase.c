char *charErase(char *str, int *ptr) {

    int len = strlen(str);
    char *result = malloc((len + 1) * sizeof(char));
    // deleted[] marks if the char's already been deleted
    int *deleted = malloc((len + 1) * sizeof(int));
    
    memset(deleted, 0, (len + 1) * sizeof(int));
    for (int i = 0; ptr[i] != -1; i++) {
        if(ptr[i] < len)
            deleted[ptr[i]] = -1;
    }

    int index = 0;
    for (int i = 0; i < len; i++) {
        if (deleted[i] != -1) {
            result[index] = str[i];
            index += 1;
        }
    }
    result[index] = '\0';

    printf("result = %s\n", result);

    return result;
}

char *charEraseV2(char *str, int *ptr) {
    int len = strlen(str);
    if (len > 64) exit(-1);
    long deleted = 0;

    for(int i = 0; ptr[i] != -1; i++) {
        deleted = deleted | (1 << ptr[i]);
    }

    int index = 0;
    char *result = malloc(sizeof(len + 1) *sizeof(char));
    for (int i = 0; i < len; i++) {
        if (!(deleted &(1 << i))) {
            result[index++] = str[i];
        }
    }
    result[index] = '\0';

    printf("result = %s v2", result);

    return result;
}
