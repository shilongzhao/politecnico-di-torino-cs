# Algorithm

## Analysis of Complexity 
- Big-O upperbond / Omega lower bound / Theta annotation 

## Search 
- Sequential Search 
- Binary Search

## Union Find 
- Quick find, slow union
- Quick union, 
- Weighted union