#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


void matrix_check(int **mat, int r, int c, int **M) {
    for (int i = 0; i <= r - 3; i++) {
        for (int j = 0; j <= c - 3; j++) {
            int flag = 1;

            for (int p = 0; p < 3; p++) {
                for (int q = 0; q < 3; q++) {
                    if ( (mat[i + p][j + q] != 0) && (M[p][q] == 0) ) {
                        flag = 0;
                        break;
                    }
                }
                if (flag == 0) {
                    break;
                }
            }

            if (flag == 1) {
                printf("%d, %d\n", i, j);
            }
        }
    }

}

// 2
typedef struct list1_t list1_t;
typedef struct list2_t list2_t;

struct list1_t {
    int val;
    int freq;
    list1_t *next;
};

struct list2_t {
    int val;
    list2_t *next;
};

list2_t *list_expand(list1_t *p1) {
    // TODO
    return NULL;
}


// 3
void print_result(float *vals, int *result, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < result[i]; j++) {
            printf("%.2f ", vals[i]);
        }
    }
    printf("\n");
}

int num_coins(int *result, int n) {
    int num = 0;
    for (int i = 0; i < n; i++) {
        num += result[i];
    }
    return num;
}
void gen(float *vals, int *coins, int *sol, int n, float change, int lastPick, int *min, int *best_sol) {
    if (change < 0) {
        return;
    }
    if (change < 0.0001 && num_coins(sol, n) < *min) {
        *min = num_coins(sol, n);
        for (int i = 0; i < n; i++) {
            best_sol[i] = sol[i];
        }
        return;
    }

    for (int i = lastPick; i < n; i++) {
        if (coins[i] > 0) {
            sol[i] += 1;
            coins[i] -= 1;
            gen(vals, coins, sol, n, change - vals[i], i, min, best_sol);
            coins[i] += 1;
            sol[i] -= 1;
        }
    }

}
// TODO: greedy, first choose the max denom, if not OK, then go back, choose the second.?
// Better solution? BFS, N-ary tree print all nodes in the level which has the most nodes
void change_making(float *vals, int *coins, int n, float change) {
    int *sol = calloc(n, sizeof(int));
    int *best_sol = calloc(n, sizeof(int));
    if (sol == NULL || best_sol == NULL) {
        return;
    }
    int min = INT_MAX;

    gen(vals, coins, sol, n, change, 0, &min, best_sol);
    if (min != INT_MAX) {
        print_result(vals, best_sol, n);
    }
    free(sol);
}
int main() {
    // 1
    int mat1[5] = {8,6,8,0,0};
    int mat2[5] = {0,5,0,2,3};
    int mat3[5] = {1,4,0,1,0};
    int mat4[5] = {0,3,0,5,8};
    int mat5[5] = {7,1,0,0,0};
    int *mat[5] = {mat1, mat2, mat3, mat4, mat5};
    int m1[3] = {0,1,0};
    int m2[3] = {1,1,1};
    int m3[3] = {0,1,0};
    int *M[3] = {m1,m2,m3};
    matrix_check(mat, 5, 5, M);

    // 2
    // TODO ...

    // 3
    float vals[7] = {0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1};
    int coins[7] = {20,12,1,0,9,4,7};

    change_making(vals, coins, 7, 1.15);

    return 0;
}