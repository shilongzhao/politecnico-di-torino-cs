#include <stdio.h>
#include <stdlib.h>
#define N     3

struct node {
  int key;
  struct node *child[N];
};

void visit (struct node *root,int level,int l){
  int i;

  if (root==NULL)
    return;

  if (l == level) {
    fprintf (stdout, "%d ", root->key);
    return;
  }

  for (i=0; i<N; i++) {
    visit (root->child[i], level, l+1);
  }
  return;
}

void visitLevelByLevel (struct node *root,int l1,int l2){
  int i;

  for (i=l1; i<=l2; i++) {
    fprintf (stdout, "Level %d: ", i);
    visit (root, i, 0);
    fprintf (stdout, "\n");
  }
  return;
}

int main()
{
    // Create Fake Tree
    struct node *root;

    root = (struct node *) malloc (1 * sizeof (struct node));
    root->key = 0;
    root->child[0] = (struct node *) malloc (1 * sizeof (struct node));
    root->child[1] = (struct node *) malloc (1 * sizeof (struct node));
    root->child[2] = (struct node *) malloc (1 * sizeof (struct node));
    root->child[0]->key = 10;
    root->child[1]->key = 12;
    root->child[2]->key = 14;
    root->child[0]->child[0] = NULL;
    root->child[0]->child[1] = NULL;
    root->child[0]->child[2] = NULL;
    root->child[1]->child[0] = NULL;
    root->child[1]->child[1] = NULL;
    root->child[2]->child[2] = NULL;
    root->child[2]->child[0] = NULL;
    root->child[2]->child[1] = NULL;
    root->child[2]->child[2] = (struct node *) malloc (1 * sizeof (struct node));
    root->child[2]->child[2]->key = 16;
    root->child[2]->child[2]->child[0] = NULL;
    root->child[2]->child[2]->child[1] = NULL;
    root->child[2]->child[2]->child[2] = NULL;

    visitLevelByLevel(root, 1, 2);
    return 0;
}
