#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 100

void readFile (FILE *fp, char ***mat, int *n){
     int i, j;
  char **tmp, s[MAX];

  if (fscanf (fp, "%d", n) == EOF) {
    fprintf (stderr, "File error.\n");
    return;
  }

  tmp = (char **) malloc (*n * sizeof (char *));
  if (tmp == NULL) {
    fprintf (stderr, "Allocation error.\n");
    return;
  }

  i = 0;
  while (fscanf (fp, "%s", s) != EOF && i<(*n)) {
    tmp[i] = (char *) malloc ((strlen(s)+2) * sizeof (char *));
    if (tmp[i] == NULL) {
      fprintf (stderr, "Allocation error.\n");
      return;
    }
    tmp[i][0] = strlen (s);
    for (j=0; j<strlen (s); j++) {
      tmp[i][j+1] = s[j];
    }
    i++;
  }

  *mat = tmp;

  return;
}

void play (FILE *fp, char **mat, int n, char *vet, int level){
    int i;

  if (level == n) {
    for (i=0; i<n; i++) {
      fprintf (fp, "%c", vet[i]);
    }
    fprintf (fp, "\n");
    return;
  }

  for (i=1; i<=((int) mat[level][0]); i++) {
    vet[level] = mat[level][i];
    play (fp, mat, n, vet, level+1);
  }

  return;
}

int main()
{
    FILE *fpR, *fpW;
    int n;
    char name1[MAX], name2[MAX], **mat, *vet;

    fprintf (stdout, "NameI NameO: ");
    scanf ("%s %s", name1, name2);
    fpR = fopen (name1, "r");
    fpW = fopen (name2, "w");
    if (fpR==NULL || fpW==NULL) {
      fprintf (stderr, "Error opening file.\n");
      exit (-1);
    }

    readFile (fpR, &mat, &n);

    vet = (char *) malloc (n * sizeof (char));
    if (vet == NULL) {
      fprintf (stderr, "Allocation error.\n");
      return (-1);
    }
    play (fpW, mat, n, vet, 0);

    fclose (fpR);
    fclose (fpW);
    return 0;
}
