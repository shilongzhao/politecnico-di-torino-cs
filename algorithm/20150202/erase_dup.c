/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int contains(char *str, char c) {
    for (int i = 0; i < strlen(str); i++) {
        if (str[i] == c) return 1;
    }
    return 0;
}
void eraseDuplicate(char *str) {
    int len = strlen(str);
    char *tmp = malloc((len+1) * sizeof(char));
    tmp[0] = '\0';
    int size = 0;
    for(int i = 0; i < len; i++) {
        if(!contains(tmp, str[i])) {
            tmp[size] = str[i];
            size += 1;
            tmp[size] = '\0';
        }
    }
    strcpy(str, tmp);
    return;

}

void erase(char *str) {
    int len = strlen(str);

    int *array = malloc(sizeof(int) * strlen(str));
    for (int i = 0;i < strlen(str); i++) 
        array[i] = 0;

    for (int i = 0; i < len; i++) {
        for (int j = i + 1; j < len; j++) {
            if (str[j] == str[i]) {
                array[j] = 1;
            }
        }
    }

    int index = 0;
    for (int i = 0; i < len; i++) {
        if (array[i] == 0) {
            str[index] = str[i];
            index++;
        }
    }
}

void eraseDuplicate3(char *str) {
    int len = strlen(str);
    char *tmp = malloc((len+1) * sizeof(char));
    tmp[0] = '\0';
    int size = 0;
    for(int i = 0; i < len; i++) {
        int flag = 0;
        for (int j = 0; j < size; j++) {
            if (tmp[j] == str[i]) {
                flag = 1;
                break;
            }
        }
        if (flag == 0) {
            tmp[size] = str[i];
            size++;
        }
    }
    tmp[size] = '\0';
    strcpy(str, tmp);
    return;
}
/**
 * erase duplicate in place, no assist memory
 * hash table
 */
void eraseDuplicate2(char *str) {
    /**
    * 256 bits taken = 0x0000...0000 
    * each bit represnets a character
    */
    unsigned char taken[32] = "";
    for (char *p = str; *p != '\0'; p++) {
        taken[(*p)/8] |= (0x1 << (7 - (*p) % 8));
    }

    int j = 0;
    for (int i = 0; i < 256; i++) {
        if (taken[i/8] & (0x1 << (7 - i%8))) {
            str[j++] = i;
        }
    }
    str[j] = '\0';
}


int main(int argc, char *argv[]) {
    char str[100] = "zz123aa;;;bbbabxxxyyy###)))xxx";
    eraseDuplicate2(str);
    printf("%s\n", str);
}
