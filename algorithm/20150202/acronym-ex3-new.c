#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 10


void generate(char **set, char *result, int depth, int size, FILE *fout){
    if(depth == size){
        fprintf(fout, "%s\n",result);
        return;
    }
    for(int i = 0; i < strlen(set[depth]); i++){
        result[depth] = set[depth][i];
        generate(set, result, depth+1, size, fout);
    }
}

int main(int argc, char *argv[]){
    FILE *fp;
    if((fp=fopen(argv[1],"r"))==NULL){
        printf("%s open failed.\n", argv[1]);
        return -1;
    }

    // read size of set
    int size;
    if (fscanf(fp, "%d", &size) != 1) {
      printf("read set size failed\n");
      exit(-1);
    }

    // allocate memory for set
    char **set;
    set = (char **)malloc(size * sizeof(char *));
    if (set == NULL) {
      printf("memory allocation failed\n");
      exit(-1);
    }

    // read file string to set[i], i = 0, 1, 2, ..., size-1;
    for(int i=0; i < size; i++){
      char line[N+1];
      if (fscanf(fp, "%s", line) != 1) {
        printf("read line failed\n");
        exit(-1);
      }
      // set[i] = strdup(line);
      set[i] = (char *)malloc(sizeof(char) * (strlen(line) + 1));
      strcpy(set[i], line);
    }

    // all memories are ready
    char *result = malloc(sizeof(char) * (size + 1));
    int depth = 0;

    // open output file
    FILE *fout = fopen(argv[2], "w");
    if (fout == NULL) {
        printf("output file open failed\n");
        exit(-1);
    }

    // start generation
    generate(set, result, depth, size, fout);

    // close files
    fclose(fp);
    fclose(fout);
    return 0;
}
