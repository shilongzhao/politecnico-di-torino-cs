#include <stdio.h>

static char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
static int  counter[26] = {};

void generate(char *result, int depth, int len) {
	if (depth == len) {
		printf("%s\n", result);
		return;
	}

	for (int i = 0; i < 26; i++) {
		if (counter[i] < 1) {
			result[depth] = alphabet[i];
			counter[i] += 1;
			generate(result, depth+1, len);
			counter[i] -= 1;
		}
	}

}

int main()  {
	char result[100] = "";
	generate(result, 0, 3);
	return 0;
}
