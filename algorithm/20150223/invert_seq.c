/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>


void invertSequence(int *v1, int n, int *v2) {
    int seq_start = 0;
    for (int i = 1; i < n; i++) {
        /**
         * two different situations when a sequence ends:
         * 1. ends when a smaller number appears
         * 2. ends when reaches array end
         */
        if (i == n-1) {
            for (int j =seq_start; j< n; j++) {
                v2[j] = v1[n-1-(j-seq_start)];
            }
        }
        
        if (v1[i] < v1[i-1]) {
            for (int j = seq_start; j < i; j++) {
                v2[j] = v1[i-1-(j-seq_start)];
            }
            seq_start = i;
        }
        else if (v1[i] >= v1[i-1])
            continue;
    }
}

int main(int argc, char *argv[]) {
    int v1[12] = {1, 2, 3, 4, 5, 0, 12, 13, 14, 2, 3 ,4};
    int v2[12];

    invertSequence(v1, 12, v2);
    for (int i = 0; i < 12; i++) {
        printf("%d ", v2[i]);
    }
}