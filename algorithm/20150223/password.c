/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#define LEN 5

void generate(int *frequency, char *anagram, int depth) {
	
	if (depth == LEN) {
		printf("%s\n", anagram);
		return;	
	}

	if (depth < 3) { // generate alphabets part
		for (int i = 0; i < 26; i++) {
			if(frequency[i] > 0) {
				char c = i + 'A'; // transform into char
				anagram[depth] = c;
				frequency[i]--;
				generate(frequency, anagram, depth+1);
				frequency[i]++;
			}	
		}
	}
	else { // generate digits part
		for (int i = 26; i < 36; i++) {
			if(frequency[i]>0) {
				char c = i - 26 + '0';
				anagram[depth] = c;
				frequency[i]--;
				generate(frequency, anagram, depth+1);
				frequency[i]++;
			}
		}
	}
}
int main(void) {
	int n;
	int frequency[36];
	printf("input max frequency:");
	scanf("%d", &n);
	for (int i = 0 ; i < 36; i++)
		frequency[i] = n;
	
	char anagram[LEN+1];
	anagram[LEN] = '\0';
	
	generate(frequency, anagram, 0);
	
}
