#include <stdio.h>

static char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

void generate(char *result, int depth, int len) {
	if (depth == len) {
		printf("%s\n", result);
		return;
	}

	for (int i = 0; i < 26; i++) {
		result[depth] = alphabet[i];
		generate(result, depth+1, len);
	}

}

int main()  {
	char result[100] = "";
	generate(result, 0, 3);
	return 0;
}
