#include <stdio.h>

static char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
static int  counter[26];

static char *digit = "0123456789";

void generate(char *result, int depth, int len) {
	if (depth == len) {
		printf("%s\n", result);
		return;
	}

	if (depth < 3) {
		for (int i = 0; i < 26; i++) {
			if (counter[i] < 2) {
				result[depth] = alphabet[i];
				counter[i] += 1;
				generate(result, depth+1, len);
				counter[i] -= 1;
			}
		}
	}
	else {
		for (int i = 0; i < 10; i++) {
			result[depth] = digit[i];
			generate(result, depth+1, len);	
		}

	}

}

int main()  {
	char result[100] = "";
	generate(result, 0, 5);
	return 0;
}
