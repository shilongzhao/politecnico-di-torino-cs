/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct node node_t;

struct node {
    int key;
    node_t *left;
    node_t *right;
};

void printPath(struct node *root, int h, node_t **path, int depth) {

    path[depth] = root;
    if (root->left == NULL && root->right == NULL) {
        for (int i = 0; i <= depth; i++) {
            printf("%d  ", path[i]->key);
        }
        printf("\n");
        return;
    }

    if (root->left != NULL)
        printPath(root->left, h, path, depth+1);
    if (root->right != NULL)
        printPath(root->right, h, path, depth+1);
}

int main(int argc, char *argv[]) {
    int h = 5;
    int num = 11;
    node_t **path = (node_t **) malloc(h * sizeof(node_t*));

    node_t **array_nodes = (node_t **)malloc(num * sizeof(node_t *));

    for (int i = 0; i < num; i++) {
        node_t *new_node = malloc(sizeof(node_t));
        new_node->key = i;
        array_nodes[i] = new_node;
    }
    // generate the binary tree 
    for (int i = 0; i <= num/2 -1; i++) {
        (array_nodes[i])->left = array_nodes[2*i+1];
        (array_nodes[i])->right = array_nodes[2*i+2];
    }

    printPath(array_nodes[0], h, path, 0);
}