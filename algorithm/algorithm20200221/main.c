#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void histogram(char *str) {
    int freq[26] = {};
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] >= 'a' && str[i] <= 'z') freq[str[i] - 'a'] += 1;
    }

    char *sep = "";
    for (int i = 0; i < 26; i++) {
        if (freq[i] != 0) {
            printf("%s\"%c\": %d", sep, 'a' + i, freq[i]);
            sep = ",";
        }
    }
}
typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *left;
    node_t *right;
};

// extremely low efficiency
int equals(node_t *tree1, node_t *tree2) {
    if (tree1 == NULL && tree2 == NULL) return 1;
    if (tree1 != NULL && tree2 != NULL) {
       if (tree1->k == tree2->k)
           return equals(tree1->left, tree2->left) && equals(tree1->right, tree2->right);
    }
    return 0;
}

int subtree(node_t *t1, node_t *t2) {
    if (t1 == NULL && t2 == NULL) return 1;
    if (t1 != NULL && t2 != NULL)
        return equals(t1, t2) || subtree(t1->left, t2) || subtree(t1->right, t2);
    if (t1 == NULL) return 0;
    return 1;
}


void print_result(int *arr, int size) {
    printf("{");
    char *sep = "";
    for (int i = 0; i < size; i++) {
        printf("%s%d", sep, arr[i]);
        sep = ",";
    }
    printf("}\n");
}


/**
 *
 * @param str   input string
 * @param used  if a digit has been used or not
 * @param arr   to store the selected numbers
 * @param size  size of array, current number of digits in arr
 * @param cap   the capacity of arr
 * @param rem   remaining sum
 */
void recur(const char *str, int *used, int *arr, int size, int cap, int rem ) {
    if (rem == 0 && size <= cap) {
        print_result(arr, size);
        return;
    }

    if (size > cap || rem < 0) {
        return;
    }

    for (int i = 0; i < strlen(str); i++) {
        if (!used[i]) {
            used[i] = 1;
            arr[size] = str[i] - '0';
            recur(str, used, arr, size + 1, cap, rem - arr[size]);
            used[i] = 0;
        }
    }
}

void generate(char *string, int n, int m) {
    int *used = calloc(strlen(string), sizeof(int));
    int *arr = calloc(n, sizeof(int));
    recur(string, used, arr, 0, n, m);
}

int main() {
    histogram("helloworld");

    node_t n1 = {1, NULL, NULL};
    node_t n2 = {2, NULL, NULL};
    node_t n3 = {3, NULL, NULL};
    node_t n4 = {4, NULL, NULL};
    n1.left = &n2;
    n1.right = &n3;
    n2.left = &n4;

    node_t m1 = {2, NULL, NULL};
    node_t m2 = {4, NULL, NULL};
    m1.left = &m2;

    int r = subtree(&n1, &m1);
    printf("\n r = %d \n", r);

    generate("111223", 2, 5);

    printf("\n ======================\n");
    generate("0123456789", 2, 9);

}