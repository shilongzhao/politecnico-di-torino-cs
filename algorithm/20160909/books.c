
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @param vet	vet[i] is genre of book i
 * @param n  	vet length, total number of books
 * @param k		number of books to select
 * @param selected 	array of length m, selected[i] represents if genre i is selected or not
 * @param next_start the start position of next selection
 * @param depth 	recursion depth
 * @param result	array of length k, books already selected
 */
void f(int *vet, int n, int k, int *selected, int next_start, int depth, int *result) {
	if (depth == k) {
		for (int i = 0; i < k; i++)
			printf("%d ", result[i]);
		printf("\n");
	}
	for (int i = next_start; i < n; i++) {
		if (selected[vet[i]]) continue;
		selected[vet[i]] = 1;
		result[depth] = i;
		f(vet, n, k, selected, i+1, depth + 1, result);
		selected[vet[i]] = 0;
	}
}
int birthday(int *vet, int n, int m, int k) {
	int *selected = malloc(sizeof(int) * (m + 1));
	memset(selected, 0, sizeof(int) * (m + 1));

	int *result = malloc(sizeof(int) * k);
	memset(result, 0, sizeof(int) * k);

	f(vet, n, k, selected, 0, 0, result);
	return 0;
}

int main(int argc, char *argv[]) {
	int vet[5] = {2,1,1,4,3};
	birthday(vet, 5, 4, 3);
}
