#include <stddef.h>
#include <stdio.h>
/**
 * 20160909 distance of two keys in binary search tree
 * find their lowest common parent and calculate distances seprately
 */

typedef struct node_t {
    int k;
    struct node_t *left, *right;
} node_t;

int dist_recur(node_t *root, int k) {
    if (root == NULL) {
        fprintf(stderr, "key %d does not exist!\n", k);
        return -1;
    }
    if (root->k == k) return 0;
    return (k < root->k)? dist_recur(root->left,k): dist_recur(root->right, k);
}

int dist(node_t *root, int k) {
    int dist = 0;
    while (root != NULL && root->k != k) {
        root = k > root->k ? root->right: root->left;
        dist++;
    }
    if (root == NULL) {
        fprintf(stderr, "key %d does not exist!\n", k);
        return -1;
    }
    return dist;
}
int distance(node_t *root, int k1, int k2) {
    if (root == NULL) {
        fprintf(stderr, "error: empty tree!\n");
        return -1;
    }
    int k = root->k;
    if (k1 < k && k2 < k)
        return distance(root->left, k1, k2);
    else if (k1 > k && k2 > k)
        return distance(root->right, k1, k2);
    else {
        int d1 = dist(root, k1);
        int d2 = dist(root, k2);
        if (d1 < 0 || d2 < 0) return -1;
        return d1 + d2;
    }
}
int main(int argc, char **argv) {
    node_t nodes[6];
    node_t *root = &nodes[4];
    for (int i = 1; i <= 5; i++) {
        nodes[i].k = i;
        nodes[i].left = nodes[i].right = NULL;
    }
    nodes[4].left = &nodes[2];
    nodes[4].right = &nodes[5];

    nodes[2].left = &nodes[1];
    nodes[2].right = &nodes[3];
    for (int i = 0; i <= 6; i++) {
        for (int j = 0; j <= 6; j++) {
            int dist = distance(root , i, j);
            printf(" ------------- dist(%d, %d) = %d ---------\n", i, j, dist);
        }
    }

}

