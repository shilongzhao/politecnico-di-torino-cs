#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node_t node_t;
typedef struct row_t row_t;
typedef struct mat_t mat_t;
struct node_t{
    int col;
    float val;
    node_t  *next;
};

struct row_t{
    int row;
    node_t *row_node;
    row_t *next_row;
} ;

struct mat_t {
    int nr;
    int nc;
    row_t *first_row;
};



char *charErase(char *str, int *ptr) {

    int len = strlen(str);
    char *result = malloc((len + 1) * sizeof(char));
    int *deleted = malloc((len + 1) * sizeof(int));

    for (int i = 0; ptr[i] != -1; i++) {
        if(ptr[i] < len)
            deleted[ptr[i]] = -1;
    }

    int index = 0;
    for (int i = 0; i < len; i++) {
        if (deleted[i] != -1) {
            result[index] = str[i];
            index += 1;
        }
    }
    result[index] = '\0';

    printf("result = %s\n", result);

    return result;
}

char *charEraseV2(char *str, int *ptr) {
    int len = strlen(str);
    if (len > 64) exit(-1);
    long deleted = 0;

    for(int i = 0; ptr[i] != -1; i++) {
        deleted = deleted | (1 << ptr[i]);
    }

    int index = 0;
    char *result = malloc(sizeof(len + 1) *sizeof(char));
    for (int i = 0; i < len; i++) {
        if (!(deleted &(1 << i))) {
            result[index++] = str[i];
        }
    }
    result[index] = '\0';

    printf("result = %s v2\n", result);

    return result;
}

void insertRow(row_t **head, int r) {
    row_t *tmp = malloc(sizeof(row_t));
    tmp->row = r;
    tmp->next_row = (*head)->next_row;

    *head = tmp;
}

void insertNode(node_t **head, int c, float val) {
    node_t *tmp = malloc(sizeof(node_t));
    tmp->val = val;
    tmp->col = c;
    tmp->next = (*head)->next;

    *head = tmp;
}

// seek row
row_t *seekR(mat_t *M, int r) {
    for (row_t *p = M->first_row; p != NULL; p = p->next_row) {
        if (p->row == r) return p;
    }
    return NULL;
}

// seek nodes
node_t *seekN(row_t *row, int c) {
    for (node_t *q = row->row_node; q != NULL; q = q->next) {
        if (q->col == c) return q;
    }
    return NULL;
}

void matDelete(mat_t *M, int r, int c) {
    row_t *row = seekR(M, r);
    if (row == NULL) return;

    node_t *node = seekN(row, c);
    if (node == NULL) return;

    node_t *p = row->row_node;
    if ( p == node) {
        p->next = node->next;
        free(node);
    }
    else {
        while(p->next != node) p = p->next;
        p->next = node->next;
        free(node);
    }

}

void matUpdate(mat_t *M, float value, int r, int c) {
    row_t *row = seekR(M, r);
    if (row == NULL) {
        // insert the node, in a new row
        insertRow(&(M->first_row), r);
        insertNode(&(M->first_row->row_node), c, value);
    }
    else {
        // insert the node, in a existed row
        node_t *node = seekN(row, c);
        if (node == NULL) {
            insertNode(&(row->row_node), c, value);
        }
        else {
            // update the node value, if the node exists already
            node->val = value;
        }
    }

}


void matWriteV2(mat_t *M, float value, int r, int c) {
    if (M == NULL || r >= M->nr || c >= M->nc) return;
    if (value == 0) {
        // try to  delete the node
        matDelete(M, r, c);
    }
    else {
        // try to update or insert the node
        matUpdate(M, value, r, c);
    }
}


void printSplittedString(char *str, int *frag_lens, int n) {
    printf("{");
    int start = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < frag_lens[i]; j++) {
            printf("%c",str[start+j]);
        }
        if (i != n-1) printf(", ");
        start += frag_lens[i];
    }
    printf("}\n");

}
/**
 * @length      array that stores the possible length of fragments
 * @n           array length of paramenter @length
 * @frag_lens    array that stores the length of each fragment
 * @count       denotes the length of already split string length
 */
int stringSplitHelper(char *str, int n, int *length, int *frag_lens, int count, int level) {
    if (count > strlen(str)) return 0;
    if (count == strlen(str)) {
        printSplittedString(str, frag_lens, level);
        return 1;
    }

    int rc = 0;
    for (int i = 0; i < n; i++) {
        frag_lens[level] = length[i];
        count += length[i];
        rc |= stringSplitHelper(str, n, length, frag_lens, count, level+1);
        count -= length[i];
    }
    return rc;
}
int stringSplit(char *str, int n, int *length) {
    int *frag_lens = malloc(strlen(str) * sizeof(int));
    int count = 0, level = 0;
    int rc = stringSplitHelper(str, n, length, frag_lens, count, level);
    if (rc == 0) printf("no possible fragmentations\n");
    return rc;
}


int main(int argc, char *argv[]) {

    /* test for ex1 */
    char *str = "ThisIsAString";
    int ptr[6] = {7,4,2,0,11,-1};
    charErase(str, ptr);
    charEraseV2(str, ptr);

    /* TODO: test for ex2 */

    /* test for ex3 */
    char *test_str = "HelloWorld";

    #define N 3
    int length[N] = {4,3,2};
    stringSplit(test_str, N, length);
}