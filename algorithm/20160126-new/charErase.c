#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *charErase(char *str, int *pos) {
	int len = (int) strlen(str);
	for (int i = 0; pos[i] != -1; i++) {
		str[pos[i]] = '\0';
	}
    
	char *result = malloc(sizeof(char) * (len + 1) );
	int j = 0;
	for (int i = 0; i < len; i++) {
		if(str[i] != '\0') {
			result[j++] = str[i];
			printf("%c\n", str[i]);
		}
	}
	result[j] = '\0';

	return result;
}

int main(int argc, char *argv[]) {
	int pos[6] = {7,4,2,0,11,-1};
	char str[] = "ThisIsAString";


	printf("%s\n", 	 charErase(str, pos));
	return 0;
}
