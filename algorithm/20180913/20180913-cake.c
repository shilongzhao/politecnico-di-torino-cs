//
// Created by zhaos on 2/21/2019.
//
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

typedef struct array_t array_t;
struct array_t {
    int code;
    int quantity;
    int weight;
};

void generate(array_t *v, int n, int rem, int *sol, int *min, int *best_sol, int lastPick) {
    if (rem < 0) {
        return;
    }

    if (rem < *min) {
        *min = rem;
        for (int i = 0; i < n; i++) {
            best_sol[i] = sol[i];
        }
    }

    for (int i = lastPick; i < n; i++) {
        if (v[i].quantity > 0) {
            sol[i] += 1;
            v[i].quantity -= 1;
            generate(v, n, rem - v[i].weight, sol, min, best_sol, i);
            v[i].quantity += 1;
            sol[i] -= 1;
        }
    }
}
void basket_generate (array_t *v, int n, int w) {

    int *sol = malloc(sizeof(int) * n);
    memset(sol, 0, sizeof(int) * n);
    int *best_sol = malloc(sizeof(int) * n);
    memset(best_sol, 0, sizeof(int) * n);
    int min = INT_MAX;
    generate(v, n, w, sol, &min, best_sol, -1);

    printf("best solution, remaining weight to fill: %d\n", min);
    for (int i = 0; i < n; i++) {
        printf("cake %d - %d \n", v[i].code, best_sol[i]);
    }

    free(sol);
    free(best_sol);
}

int main() {
    array_t c1 = {1, 2, 50};
    array_t c2 = {2, 4, 90};
    array_t c3 = {3, 3, 15};
    array_t c4 = {4, 1, 100};
    array_t c5 = {5, 2, 70};



    array_t v[4] = {c1, c2, c3, c4, c5};

    basket_generate(v, 5, 670);
}