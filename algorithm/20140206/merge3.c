/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <stdlib.h>
int *merge(int *a, int *b, int *c, int na, int nb, int nc) {
    int total_size = na + nb + nc;
    int *result = malloc(total_size*sizeof(int));
    int *_a = malloc(total_size*sizeof(int));
    int *_b = malloc(total_size*sizeof(int));
    int *_c = malloc(total_size*sizeof(int));
    for (int i = 0; i < na; i++) {
        _a[i] = a[i];
    }
    for (int i = na; i < total_size; i++) {
        _a[i] = INT32_MAX;
    }
    for (int i = 0; i < nb; i++) {
        _b[i] = b[i];
    }
    for (int i= nb; i < total_size; i++) {
        _b[i] = INT32_MAX;
    }
    for (int i = 0; i < nc; i++) {
        _c[i] = c[i];
    }
    for (int i = nc; i < total_size; i++) {
        _c[i] = INT32_MAX;
    }

    int ai = 0, bi = 0, ci = 0;
    for (int i = 0; i < na+nb+nc; i++) {
        if (_a[ai] <= _b[bi] && _a[ai] <= _c[ci]) {
            result[i] = _a[ai];
            ai++;
        }
        else if (_b[bi] <= _a[ai] && _b[bi] <= _c[ci]) {
            result[i] = _b[bi];
            bi++;
        }
        else {
            result[i] = _c[ci];
            ci++;
        }
    }
    free(_a);
    free(_b);
    free(_c);
    return result;
}

// another version of merge3
int *merge3v2(int *a, int *b, int *c, int na, int nb, int nc) {
    int n = na + nb + nc;
    int *result = malloc(n * sizeof(int));

    int *_a = (int *)malloc( (na + 1)*sizeof(int));
    for (int i = 0; i < na; i++) _a[i] = a[i];
    _a[na] = INT32_MAX;

    int *_b = (int *)malloc((nb+1)*sizeof(int));
    for (int i = 0; i < na; i++) _b[i] = b[i];
    _b[nb] = INT32_MAX;

    int *_c = (int *)malloc((nc+1)+sizeof(int));
    for (int i = 0; i < na; i++) _c[i] = c[i];
    _c[nc] = INT32_MAX;

    int p = 0, q = 0, r = 0;
    for(int i = 0; i < n; i++) {
        if (_a[p] < _b[q] && _a[p] < _c[r]) {
            result[i] = _a[p++];
        }
        else if (_b[q] < _a[p] && _b[q] < _c[r]) {
            result[i] = _b[q++];
        }
        else
            result[i] = _c[r++];
    }

    return result;
}

int *merge3v3(int *a, int *b, int *c, int na, int nb, int nc) {
    int *result = malloc(sizeof(int) *(na + nb + nc));
    int p = 0, q = 0, r = 0;
    for (int i = 0; i < na + nb + nc; i++) {
        int x = (p >= na) ? INT32_MAX : a[p];
        int y = (q >= nb) ? INT32_MAX : b[q];
        int z = (r >= nc) ? INT32_MAX : c[r];
    
        if (x <= y && x <= z) {
            result[i] = x;
            p++;
        }
        else if (y < x && y <= z) {
            result[i] = y;
            q++;
        }
        else {
            result[i] = z;
            r++;
        }
    }
    return result;
}

int *merge3v4(int *a, int *b, int *c, int na, int nb, int nc) {
    int *result = malloc(sizeof(int) * (na + na + nc));
    int i = 0, j = 0, k = 0;
    int l = 0;
    while (i < na && j < nb && k < nc) {
        if (a[i] <= b[j] && a[i] <= c[k]){
            result[l++] = a[i++];
        }
        else if (b[j] <= a[i] && b[j] <= c[k]) {
            result[l++] = b[j++];
        }
        else {
            result[l++] = c[k++];
        }
    }
    
    while (i < na && j < nb) {
        if (a[i] < b[j]) result[l++] = a[i++];
        else result[l++] = b[j++];
    }
    
    while (i < na && k < nc) {
        if (a[i] < c[k]) result[l++] = a[i++];
        else result[l++] = c[k++];
    }
    
    while (j < nb && k < nc) {
        if (b[j] < c[k]) result[l++] = b[j++];
        else result[l++] = c[k++];
    }
    
    while (i < na) result[l++] = a[i++];
    
    while (j < nb) result[l++] = b[j++];
    
    while (k < nc) result[l++] = c[k++];
    
    return result;
}
int main() {
    int a[5] = {4, 6, 9, 13, 32};
    int b[6] = {1, 2, 3, 4, 5, 6};
    int c[5] = {0, 33, 34, 35, 36};

    int *result = merge3v4(a, b, c, 5, 6, 5);
    for (int i = 0; i < 16; i++) {
        printf("%d\n", result[i]);
    }
}


