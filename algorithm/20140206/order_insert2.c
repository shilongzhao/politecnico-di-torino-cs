#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct list_t list_t;
struct list_t {
    char *surname;
    char *name;
    list_t *next;
};

int compare(list_t *node, char *surname, char *name) {
    return stcmp(node->surname, surname) == 0 ? 
        strcmp(node->name, name) : strcmp(node->surname, surname);
}
/**
 * there is an error with the original problem, so the 
 * first parameter type is changed to list_t ** instead of list *
 */
int orderInsert(list_t **list, char *surname, char *name) {

    list_t *t = malloc(sizeof(list_t));
    t->name = strdup(name);
    t->surname = strdup(surname);
    t->next = NULL;

    if (*list == NULL) {
        *list = t;
        return 1;
    }
    
    if (compare(*list, surname, name) > 0) {
        t->next = *list;
        *list = t;
        return 1;    
    }

    if (compare(*list, surname, name) == 0) {
        printf("surname %s name %s exists already!\n", surname, name);
        return 0;
    }

    list_t *p = *list;
    while(p->next != NULL && compare(p->next, surname, name) < 0) {
        p = p->next;
    }
    // insert after p or surname, name exists
    // if surname, name exists
    if (p->next != NULL && compare(p->next, surname, name) == 0) {
        printf("surname %s name %s exists already!\n", surname, name);
        return 0;
    }
    // insert after p; end or not end list does not matter
    t->next = p->next;
    p->next = t;
    return 1;
}

// TODO: main()
