/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

 /**
 * WARNING: 
 * This program does not implement the function 
 * checking the validity of a solution given by user. 
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>



int index_char(char *str, char c) {
    for (int i = 0; i < strlen(str); i++) {
        if (str[i] == c) return i;
    }
    return -1;
}

void remove_dup(char *str) {
    char *t = malloc((strlen(str) + 1)*sizeof(char));

    int t_i = 0;
    t[0] = '\0';
    for (int i = 0; str[i] != '\0'; i++) {
        if(index_char(t, str[i]) == -1) {
            t[t_i] = str[i];
            t_i++;
            t[t_i] = '\0';
        }

    }
    strcpy(str, t);
    free(t);
    return;
}

// translate the str to int, according to the assign
int str_int(char *str, char *table, int *assign) {
    int val = 0;
    for(int i = 0; i < strlen(str); i++) {
        int index = index_char(table, str[i]);
        if (index == -1) {
            fprintf(stderr, "error, char %c not found in %s\n", str[i], table);
            exit(-1);
        }
        val = 10*val + assign[index];
    }

    return val;
}


int check(char **ops, char *table, int *assign) {
    int num[3];
    for (int i = 0; i < 3; i++) {
        num[i] = str_int(ops[i], table, assign);
    }
    if (num[0] + num[1] != num[2])
        return 0;

    for (int i = 0; i < 3; i++) {
        int index_first_char = index_char(table, ops[i][0]);
        if (index_first_char == -1) {
            fprintf(stderr, "char %c is not contained in %s\n", ops[i][0], table);
            exit(-1);
        }
        if (assign[index_first_char] == 0)
            return 0;
    }

    return 1;
}


void expand(char **ops, char *table, int *assign, int *used, int depth) {

    if (depth == strlen(table)) {

        if(check(ops, table, assign)) {
            for(int i = 0; i < 3; i++) {
                printf("%s = %d\n ", ops[i], str_int(ops[i], table, assign));
            }
            for(int i = 0; i < strlen(table); i++) {
                printf("%c = %d\n", table[i], assign[i]);
            }
        }
        return;
    }

    for (int i = 0; i < 10; i++) {
        if (!used[i]) {
            used[i] = 1;
            assign[depth] = i;
            expand(ops, table, assign, used, depth+1);
            used[i] = 0;
        }
    }

}


int main(int argc, char *argv[]) {

    if (argc != 4) {
        fprintf(stderr, "Wrong arguments!\n Usage: send_more_money.exe SEND MORE MONEY\n");
        exit(-1);
    }

    char **operands = (char **)malloc(3 * sizeof(char *));

    int max_len = 0;
    for (int i = 0; i < 3; i++) {
        operands[i] = strdup(argv[i+1]);
        if (strlen(operands[i]) > 8) {
            fprintf(stderr, "string %s is too long\n", operands[i]);
            exit(-1);
        }
        max_len += strlen(operands[i]);
    }

    //table is all the alphabets without repetition
    char *table = malloc((max_len + 1) * sizeof (char));
    for (int i = 0; i < 3; i++) {
        table = strcat(table, operands[i]);
    }
    // remove duplicate alphabets in table
    remove_dup(table);

    printf("%s\n", table);

    // used[i] denotes if digit i is already used or not;
    int used[10];
    for (int i = 0; i < 10; i++) {
        used[i] = 0;
    }

    //assign[i] is the digit assigned for alphabet table[i]
    int *assign = malloc(strlen(table) * sizeof(char));

    for (int i = 0; i <strlen(table); i++) {
        assign[i] = -1;
    }

    expand(operands, table, assign, used, 0);
}