/**
 * webmaster@dreamhack.it
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DEBUG
typedef struct list_t {
    char *sname;
    char *name;
    struct list_t *next;
} list_t;

#ifdef DEBUG
void printList(list_t *head) {
    for (list_t *p = head; p != NULL; p = p->next) {
        printf("%s %s\n", p->sname, p->name);
    }
    printf("-------\n");
}
#else 
void printList(list_t *haed) {}
#endif

int insertOrderList(list_t **head, char *surname, char *name) {

    list_t *tmp = malloc(sizeof(list_t));
    tmp->name = strdup(name);
    tmp->sname = strdup(surname);
    tmp->next = NULL;

    if (*head == NULL || strcmp(surname, (*head)->sname) < 0) {
        tmp->next = *head;
        *head = tmp;
        printList(*head);
        return 1;
    }

    list_t *p = *head;
    while(p->next != NULL && strcmp(surname, p->next->sname) > 0) {
        p = p->next;
    }

    if (p->next == NULL || strcmp(surname, p->next->sname) < 0) {
        tmp->next = p->next;
        p->next = tmp;
        printList(*head);
        return 1;
    }

    printList(*head);
    return 0;
}

int main(int argc, char *argv[]) {
    list_t *head = NULL;
    int n = 5;
    char *surnames[] = {"Bob","Alice","Cici","David", "Cici"};
    char *names[] = {"Bob","Alice","Cici","David", "Cici"};

    for (int i = 0 ; i < n; i++) {
        insertOrderList(&head, surnames[i], names[i]);
    }




}
