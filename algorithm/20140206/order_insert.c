/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct list_t_ list_t;

struct list_t_ {
    char *surname;
    char *name;
    list_t *next;
};

int compare(list_t *this, list_t *that) {
    if (strcmp(this->surname, that->surname) == 0)
        return strcmp(this->name, that->name);
    else
        return strcmp(this->surname, that->surname);
}
/**
 * list must NOT be null, since it's impossible to change value of list;
 * head node list has to be smaller than ALL possible nodes
 */
int orderInsert(list_t *list, char *surname, char *name) {

    if (list == NULL) {
        fprintf(stderr,"list cannot be NULL\n");
        exit(-1);
    }

    list_t *new = malloc(sizeof(list_t));
    new->surname = strdup(surname);
    new->name = strdup(name);
    new->next = NULL;

    list_t *cur = list;
    list_t *pre = NULL;

    while (cur != NULL) {
        int rc = compare(cur, new);

        if (rc == 0 ) return 0;

        if (rc > 0) {
            pre->next = new;
            new->next = cur;
            return 1;
        }

        if (rc < 0) {
            pre = cur;
            cur = cur->next;
            continue;
        }
    }

    pre->next = new;
    return 1;
}


int main() {
    list_t *head = malloc(sizeof(list_t));
    head->name = strdup("");
    head->surname = strdup("");
    head->next = NULL;

    char *surnames[4] = {"Bob","Alice","Cici","David"};
    char *names[4] = {"Bob","Alice","Cici","David"};

    for (int i = 0 ; i < 4; i++) {
        orderInsert(head, surnames[i], names[i]);
    }

    for(list_t *tmp = head; tmp != NULL; tmp=tmp->next) {
        printf("%s %s\n", tmp->surname, tmp->name);
    }
}

