/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <stdlib.h>
// #define DEBUG

typedef struct solution_t_ {
    int *v;
    int min_diff;
} solution_t;

int max_balance(int *v, int n) {
    int max_balance = INT32_MIN;
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum += v[i];
        if (sum > max_balance) {
            max_balance = sum;
        }
    }

    return max_balance;
}

int min_balance(int *v, int n) {
    int min_balance = INT32_MAX;
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum += v[i];
        if (sum < min_balance) {
            min_balance = sum;
        }
    }
    return min_balance;
}

// combine functions max_balance and min_balance into one function
int diff_max_min(int *v, int n) {
    int min = INT32_MAX, max = INT32_MIN, cur = 0;
    for (int i = 0; i < n; i ++) {
        cur += v[i];
        if (cur > max) max = cur;
        if (cur < min) min = cur;
    }
    return max - min;
}

void generate(solution_t *best, int *current, int *input, int *used, int N, int depth) {

    if (depth == N) {
        #ifdef DEBUG
        for (int i = 0; i < N; i++) {
            printf("%d ", current[i]);
        }
        printf("\n max balance: %d\n", max_balance(current, N));
        printf("min balance: %d\n", min_balance(current, N));
        #endif
        // int current_balance = diff_max_min(current, N);
        int current_balance = max_balance(current, N) - min_balance(current, N);
        if (current_balance < best->min_diff) {
            // printf("max balance %d\n", max_balance(current, N));
            // printf("min balance %d\n", min_balance(current, N));
            best->min_diff = current_balance;
            for (int i = 0; i < N; i++) {
                best->v[i] = current[i];
            }
        }
        return;
    }

    for(int i = 0; i < N; i++) {
        if (!used[i]) {
            used[i] = 1;
            current[depth] = input[i];
            generate(best, current, input, used, N, depth + 1);
            used[i] = 0;
        }
    }

}

int main(int argc, char *argv[]) {
    int input[4] = {10, -5, 7, -8};
    int N = 4;
    solution_t *best = malloc(sizeof(solution_t));
    best->v = malloc(N * sizeof(int));
    best->min_diff = INT32_MAX;

    int current[N]; // current solution
    int used[N];
    for (int i = 0; i < N; i++) {
        used[i] = 0;
    }

    generate(best, current, input, used, N, 0);

    printf("best solution: %d\n", best->min_diff);
    for (int i = 0; i < N; i++) {
        printf("%d ", best->v[i]);
    }

    printf("\n");

    return 0;
}