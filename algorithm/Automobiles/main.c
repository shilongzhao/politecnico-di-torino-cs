/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */
// exam 2014 06

/**
* WARNING: this program is not complete
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define DEBUG
#define MAXLEN 50

typedef struct access_t_ access_t;
struct access_t_ {
    char name[MAXLEN];
    float price;
    access_t *next;
};

typedef struct model_t_ model_t;
struct model_t_ {
    char name[MAXLEN];
    char filename[MAXLEN];
    model_t *left;
    model_t *right;
    access_t *accessories;
};

typedef struct producer_t_ producer_t;
struct producer_t_ {
    char name[MAXLEN];
    char filename[MAXLEN];
    producer_t *left;
    producer_t *right;
    model_t *models;
};


int producer_comp(producer_t *this, producer_t *that) {
    return strcmp(this->name, that->name);
}

producer_t *producer_search(producer_t *node, char *name) {
    if (node == NULL || strcmp(node->name, name) == 0)
        return node;
    else if (strcmp(node->name, name) > 0)
        return producer_search(node->left,name);
    else
        return producer_search(node->right, name);
}

producer_t *insert(producer_t *node, producer_t *new)  {

    if (node == NULL) {
        node = new;
        return node;
    }

    if (producer_comp(node, new) == 0) return node;
    else if (producer_comp(node, new) > 0)
        node->left = insert(node->left, new);
    else
        node->right = insert(node->right, new);

    return node;

}

void post_traversal(producer_t *root) {
    if (root == NULL) return;
    post_traversal(root->left);
    post_traversal(root->right);
    printf("%s\n", root->name);
}

void preorder_traversal(producer_t *root) {
    if (root == NULL) return;
    printf("%s\n", root->name);
    preorder_traversal(root->left);
    preorder_traversal(root->right);
}

producer_t *read_producers(char *filename, producer_t *producer_bst) {

    FILE *f_producers = fopen(filename, "r");
    if (f_producers == NULL) {
        fprintf(stderr, "file %s failed\n", filename);
        exit(-1);
    }
    char name[MAXLEN], fname[MAXLEN];
    while(fscanf(f_producers, "%s %s", name, fname) == 2) {
        producer_t *node = malloc(sizeof(producer_t));
        strcpy(node->name,name);
        strcpy(node->filename, fname);
        node->left = node->right = NULL;
        producer_bst = insert(producer_bst, node);
    }
#ifdef DEBUG
//    post_traversal(producer_bst);
    preorder_traversal(producer_bst);
#endif
    return producer_bst;
}


model_t *model_search(model_t *node, char *name) {
    if (node == NULL || strcmp(node->name, name) == 0)
        return node;
    else if (strcmp(node->name, name) > 0)
        return  model_search(node->left,name);
    else
        return model_search(node->right, name);

}

model_t *model_insert(model_t *node, model_t *new) {

    if (node == NULL) {
        node = new;
        return node;
    }

    if (strcmp(node->name, new->name) == 0) return node;
    else if (strcmp(node->name, new->name) > 0)
        node->left = model_insert(node->left, new);
    else
        node->right = model_insert(node->right, new);

    return node;
}

void post_order_models(model_t *root) {
    if (root == NULL) return;
    post_order_models(root->left);
    post_order_models(root->right);
    printf("%s\n", root->name);
}

void read_models(producer_t *node) {
    FILE *f_models = fopen(node->filename, "r");
    if (f_models == NULL) exit(-1);

    char m_name[MAXLEN], acc_filename[MAXLEN];
    while(fscanf(f_models, "%s %s", m_name, acc_filename) == 2) {
        model_t *tmp = malloc(sizeof(model_t));
        strcpy(tmp->name, m_name);
        strcpy(tmp->filename, acc_filename);
        tmp->left = NULL;
        tmp->right = NULL;

        node->models = model_insert(node->models, tmp);
    }
#ifdef DEBUG
    post_order_models(node->models);
#endif
    return;
}

void read_accessories(model_t *node) {
    FILE *f_acc = fopen(node->filename, "r");
    if (f_acc == NULL) {
        fprintf(stderr, "file open %s failed\n", node->filename);
        exit(-1);
    }

    char acc_name[MAXLEN];
    float price;

    while(fscanf(f_acc, "%s %f", acc_name, &price) == 2) {
        access_t *tmp = malloc(sizeof(access_t));
        strcpy(tmp->name, acc_name);
        tmp->price = price;

        tmp->next = node->accessories;
        node->accessories = tmp;
    }
    #ifdef DEBUG
    for(access_t *e = node->accessories; e!=NULL; e = e->next) {
        printf("%s \n", e->name);
    }
    #endif
    return;
}


int main(int argc, char **argv) {


    producer_t *producer_bst = read_producers(argv[1], NULL);

    printf("input producer name: ");

    char producer_name[MAXLEN];
    scanf("%s", producer_name);
    producer_t *result = producer_search(producer_bst, producer_name);

    if (result != NULL) {
        read_models(result);
    }
    else {
        fprintf(stderr, "producer %s not found\n", producer_name);
        exit(-1);
    }

    char model_name[MAXLEN];
    printf("input model name: ");
    scanf("%s", model_name);

    model_t *model_result = model_search(result->models, model_name);
    if (model_result == NULL) {
        fprintf(stderr, "model %s not found\n", model_name);
        exit(-1);
    }

    read_accessories(model_result);
}


void find_predecessor(producer_t *node, char *name) {}
void remove_producer(producer_t *node, char *name) {

    producer_t *t = producer_search(node, name);

}