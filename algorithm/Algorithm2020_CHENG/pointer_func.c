//
// Created by zhaos on 5/11/2020.
//

#include <stdio.h>

void triple(int x) {
    x = x * 3;
}

void triple_p(int *p) {
    *p = (*p) * 3;
}
int main() {
    int a = 10;
    printf("a = %d\n", a);
    triple(a);
    printf("after triple(a), a = %d\n", a);
    triple_p(&a);
    printf("after triple_p(a), a = %d\n", a);
}