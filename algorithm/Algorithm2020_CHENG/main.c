#include <stdio.h> // standard input out


int main() {

    // variable
    int x = 11;

    // data types
    float f = 1.23f;
    double d = 2.22;
    char c = '1'; // 1 bytes, ASCII character => 0 - 125

    printf("char c = %c, or it is integer %d\n", c, c);
    printf("char c = %c, or it is integer %d\n", 'a', 'a');

    int y = 1; // 4 bytes
    // flow control
    // operators:
    // logical operators: &&, ||, !
    // assignment operator 赋值运算符: =
    // compare operators: ==, !=, >, <, >=, <=
    // 算数运算符：+,-,*,/,%

    // if-else
    int age;
    printf("input your age: ");
    scanf("%d", &age); // & 取地址运算符
    if (age < 18) {
        printf("not adult\n");
    }
    else if (age < 30) {
        printf("adult");
    }
    else {
        printf("old");
    }
    // for-loop, while-loop
    int sum = 0;

    for(int i = 0; i <= 100; i++) {
        sum = sum + i;
    }
    printf("sum = %d\n", sum);

    sum = 0;
    for (int i = 0; i <= 100; i++) {
        if (i % 2 != 0) {
            sum += i;
        }
    }
    printf("sum = %d\n", sum);

    sum = 0;
    for (int i = 1; i <= 100; i = i + 2) {
        sum += i;
    }
    printf("sum = %d\n", sum);

    sum = 0;
    for (int i = 1; i <= 1000; i = 2 * i) {
        sum += i;
    }
    printf("sum (2^n) = %d\n", sum);
    // structure
    // array 数组
    // 2-dimensional array -> array, continuous memory space
    int arr1[10];
    int arr2[10] = {1,2,3};
//    arr1[1] = 2;
//    printf("%d", arr1[1]);
    for (int i = 0; i < 10; i++) {
        printf("arr1[%d] = %d, arr2[%d] = %d\n", i,arr1[i],i,arr2[i]);
    }

    int a[2][3] = {{1,2,3},{4,5,6}};

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 3; j++) {
            printf("a[%d][%d] = %d\n", i, j, a[i][j]);
        }
    }
    // struct


    // string
    char s1[10] = "abc";
    char *s2 = "xyz";
    printf("s1 = %s, s2 = %s\n", s1, s2);
    s1[0] = 'A';
    printf("after changing s1[0], s1 = %s\n", s1);
//    s2[0] = 'X';
//    printf("s2 = %s\n", s2);
    return 0;
}
