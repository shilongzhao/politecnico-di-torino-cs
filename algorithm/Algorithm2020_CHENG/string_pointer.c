//
// Created by zhaos on 5/12/2020.
//

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

void str2upper(char *s) {
    for (int i = 0; i < strlen(s); i++) {
        if (islower(s[i])) {
            char u = toupper(s[i]);
            s[i] = u;
        }
    }
}
// HOMEWORK: 20200512
// calculcate length of s
int length(char *s) {
    int len = 0;
    for (int i = 0; s[i] != '\0'; i++) {
        len++;
    }
    return len;
}
// make a copy of s, malloc, NO strcpy
// return the address of the newly copied string
char *copy(char *s) {
    char *c=malloc((strlen(s)+1)*sizeof(char));
    for(int i=0;i<strlen(s);i++){
        c[i]=s[i];
    }
    printf("Address is:%p", c);
    return c;
}

// calculate frequencies of a-z (26 chars)
// abcdefaaabded
// return start address of an integer array, the integer array has size 26
// malloc
int *freq(char *s) {
    int *f = malloc(26 * sizeof(char));
    for (int i = 0; i < 26; i++) {
        f[i] = 0;
    }

    for (int i = 0; s[i] != '\0'; i++) {
        int x = s[i] - 'a';
        f[x] += 1;
    }

    return f;
}

// remove duplicates,
// input string: aaabbbbbbaacccbefghhhh
char *remove_dup(char *s) {
    return NULL;
}

int main() {
    char *s = "abcdefgh809NBVGijklmnopq"; // cannot be modified, constant

    char s2[10] = "abc";
    char s3[10] = {97, 98, 99};

    printf("s2 = %s\n", s2);
    s2[0] = 'N';
    printf("s2 = %s\n", s2);

    printf("length of string s = %d, length of string s2 = %d, length of s3 = %d\n", strlen(s), strlen(s2), strlen(s3));

    printf("value of s2 = %p, address of s2[0] = %p\n", s2, &s2[0]);

    printf("s = %s\n", s);
//    s[0] = 'A';// KO
//    printf("s = %s\n", s);
    printf("lower? %d\n", islower('c'));
    printf("lower? %d\n", islower('A'));
    printf("upper of d = %c\n", toupper('d'));
    printf("upper of a = %c\n", toupper(97));

    printf("s2 =  %s\n", s2);
    str2upper(s2);
    printf("s2 = %s\n", s2);

    // str2upper(s); // KO, cannot be modified

    // memory allocation, malloc() --> Memory ALLOCation
    char *us = malloc((strlen(s) + 1)* sizeof(char));
    // for each lowercase letter in s, turn it into uppercase and save it in us
    for (int i = 0; i < strlen(s); i++) {
        if (islower(s[i])) {
            char u = toupper(s[i]);
            us[i] = u;
        } else {
            us[i] = s[i];
        }
    }
    us[strlen(s)] = '\0';
    printf("upper s = %s\n", us);
    free(us);

    int *fr = freq("aaabbccaaddefef");
    for (int i = 0; i < 26; i++) {
        printf("freq %c = %d\n", i + 'a', fr[i]);
    }
}
