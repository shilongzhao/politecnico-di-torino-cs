//
// Created by zhaos on 5/22/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *next;
};


void printList(node_t *head) {
    for (node_t *i = head; i != NULL; i = i->next) {
        printf("%d ", i->v);
    }
    printf("\n");
}

void insert(node_t **head, int k) {
    node_t *t = malloc(sizeof(node_t));
    t->v = k;
    t->next = *head;
    *head = t;
}
void delete(node_t **head, int k) {

    if (*head == NULL) {
        return;
    }

    if (k == (*head)->v) {
        *head = (*head)->next;
        return;
    }

    node_t *p = *head;
    node_t *q =(*head)->next;
    // homework
    // while loop
    while(q!=NULL && k!=q->v){
        p=p->next;
        q=q->next;
    }
    if(q != NULL){
        p->next=q->next;
        free(q);
    }
}
int main() {
    node_t *h = NULL;
    insert(&h, 0);
    insert(&h, 1);
    insert(&h, 2);
    insert(&h, 3);
    insert(&h, 4);
    printList(h);// 4, 3, 2, 1, 0

    delete(&h, 5); // 4, 3, 2, 1, 0
    printList(h);

    delete(&h, 4);
    printList(h);

    delete(&h, 1);
    printList(h);
}

