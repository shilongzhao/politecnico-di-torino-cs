//
// Created by zhaos on 5/26/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define N 5
typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *child[N];
};

typedef struct bnode_t bnode_t;
struct bnode_t {
    int v;
//    bnode_t *c0;
//    bnode_t *c1;
    bnode_t *c[2]; // c[0] = c0, c[1] = c1;
};

void printLevelBT(bnode_t *root, int level) {
    if (root == NULL) return;
    if (level == 0) printf("%d ", root->v);
    printLevelBT(root->c[0], level - 1);
    printLevelBT(root->c[1], level - 1);
}

// homework: 打印第level层的所有节点
void printLevel(node_t *root, int level) {
    if(root==NULL) return ;
    if(level==0) printf("%d\n",root->v);
    for (int i = 0; i < N; i++) {
        printLevel(root->child[i], level - 1);
    }
}

// homework: 求N叉树有多少层
int levels(node_t *root) {
    return 0;
}


void iterate(node_t *root, int *npl, int level) {
    if (root == NULL) return;
    npl[level] += 1;
    for (int i = 0; i < N; i++) {
        iterate(root->child[i], npl, level + 1);
    }
}

// backtracking....
int maxLevel(node_t *root) {
    int totalLevels = levels(root);
    int *nodesPerLevel = malloc(sizeof(int) * totalLevels);
    iterate(root, nodesPerLevel, 0);

    int max = nodesPerLevel[0];
    int resultMaxLevel = 0;
    for (int i = 1; i < totalLevels; i++) {
        if (nodesPerLevel[i] > max) {
            max = nodesPerLevel[i];
            resultMaxLevel = i;
        }
    }
    return resultMaxLevel;
}

void tree_max_level(node_t *root) {
    int level = maxLevel(root);
    printLevel(root, level);
}

// TODO: solution with BFS?


int main() {
    node_t n1,n2,n3,n4,n5,n6;

    n1.v = 1;
    n2.v = 2;
    n3.v = 3;
    n4.v = 4;
    n5.v = 5;
    n6.v = 6;

    n1.child[0] = n1.child[2] = n1.child[4] = NULL;
    n1.child[1] = &n2;
    n1.child[3] = &n3;

    n2.child[2] = n2.child[4] = NULL;
    n2.child[0] = &n4;
    n2.child[1] = &n5;
    n2.child[3] = &n6;

    for (int i = 0; i < 5; i++) {
        n3.child[i] = n4.child[i] =  n5.child[i] = n6.child[i] = NULL;
    }
}