//
// Created by zhaos on 5/20/2020.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct node node;

struct node {
    int v;
    node *next;
};
void insert(node **head, int k) {
    node *t = malloc(sizeof(node));
    t->v = k;
    t->next = *head;
    *head = t;
}

void int2list(int n, struct node **head) {
    while (n != 0) {
        int d = n % 10;
        insert(head, d);
        n = n / 10;
    }
}

int main() {
    node *head = NULL;
    int2list(27329134, &head);
    for (node *p = head; p != NULL; p = p->next) {
        printf("%d->", p->v);
    }
}