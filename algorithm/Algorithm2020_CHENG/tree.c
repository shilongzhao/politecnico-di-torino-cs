//
// Created by zhaos on 5/24/2020.
//

#include <stdio.h>
#include <limits.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *left;
    node_t *right;
};

void preorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%d ", root->v);
    preorder(root->left);
    preorder(root->right);
}

void inorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    inorder(root->left);
    printf("%d ", root->v);
    inorder(root->right);
}

void postorder(node_t *root) {
    if (root == NULL) return;
    postorder(root->left);
    postorder(root->right);
    printf("%d ", root->v);
}

int identical(node_t *t1, node_t *t2) {
    if (t1 == NULL && t2 == NULL) return 1;
    if (t1 == NULL && t2 != NULL) return 0;
    if (t1 != NULL && t2 == NULL) return 0;

    return (t1->v == t2->v)
        && identical(t1->left, t2->left)
        && identical(t1->right, t2->right);
}

int isMirror(node_t *t1, node_t *t2) {
    if (t1 == NULL && t2 == NULL) return 1;
    if (t1 == NULL && t2 != NULL) return 0;
    if (t1 != NULL && t2 == NULL) return 0;
    return (t1->v == t2->v) && isMirror(t1->left, t2->right) && isMirror(t1->right, t2->left);
}
int levels(node_t *root) {
    if (root == NULL) return 0;
    int l = levels(root->left);
    int r = levels(root->right);
    if (l > r) {
        return l + 1;
    }
    else {
        return r + 1;
    }
}

// 201806
int tree_diameter(node_t *r) {
    if (r == NULL) return 0;
    int a = levels(r->left) + levels(r->right);
    int b = tree_diameter(r->left);
    int c = tree_diameter(r->right);
    if (a > b && a > c) {
        return a;
    } else if (b > a && b > c) {
        return b;
    } else {
        return c;
    }
}
int main() {
    node_t n1 = {1, NULL, NULL};
    node_t n2 = {2, NULL, NULL};
    node_t n3 = {3, NULL, NULL};
    node_t n4 = {4, NULL, NULL};
    node_t n5 = {5, NULL, NULL};
    node_t n6 = {6, NULL, NULL};

    n1.left = &n2;
    n1.right = &n3;
    n2.left = &n4;
    n2.right = &n5;
    n3.right = &n6;

    preorder(&n1);
    printf("\n");
    inorder(&n1);
}