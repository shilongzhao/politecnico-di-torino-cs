//
// Created by zhaos on 5/10/2020.
//

#include <stdio.h>
int main() {
    int a = 10;
    printf(" value of a = %d, address of a = %p\n", a, &a);
    int *p = &a;
    printf(" value of p = %p, "
           " address of p = %p, "
           " variable value that p points to is %d\n", p, &p, *p);

    *p = 100;
    printf(" value of a = %d, address of a = %p\n", a, &a);
    //DONE: 20200511 int **pp, why use pointer, swap, change variable value in another function
    int **q = &p;
    int ***r = &q;


}
