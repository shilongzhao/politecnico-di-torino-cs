//
// Created by zhaos on 5/22/2020.
//

#include <stdio.h>

int f(int n) {
    if (n == 0) return 0;
    int r = n + f(n - 1);
    return r;
}
int g(int n){
    if(n==1){
        return 1;
    }
    return n*g(n-1);
}
int main() {
    printf("%d", f(5));

}