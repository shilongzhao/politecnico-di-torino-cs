//
// Created by zhaos on 5/15/2020.
//

#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <limits.h>

typedef int in;
typedef double dbl;
typedef struct university_t univ;

typedef struct student_t student_t;

struct student_t {
    int age;
    char *name;
    char *surname;
};

struct university_t {
    char *name;
    char *city;
    int foundedYear;
};

typedef struct teacher_t {

} t_t;


void str2upper(char *s) {
    for (int i = 0; i < strlen(s); i++) {
        if (islower(s[i])) {
            char u = toupper(s[i]);
            s[i] = u;
        }
    }
}

void toUpperNameSurname(student_t s) {
    str2upper(s.name);
    str2upper(s.surname);
}

void toUpperNameSurnameNew(student_t *s) {
   str2upper(s->name);
   str2upper(s->surname);
}

// calculate average age of students
/**
 *
 * @param students array of students
 * @param n  number of students
 * @return  average age.
 */
double averageAge(student_t *students, int n) {
    int sum=0;
//    for(int i=0;i<n;i++){
//        student_t *p=&students[i];
//        sum= sum + (p->age);
//    }
    for(int i = 0; i < n; i++) {
        student_t s = students[i];
        sum = sum + s.age;
    }
    double average=(double)sum/n;
    return average;
}

/**
 * @param students, array of student pointers
 * @param n
 * @return
 */
double averageAgeNew(student_t **students, int n) {
    int sum = 0;
//    for (int i = 0; i < n; i++) {
//        student_t s = *students[i];
//        sum = sum + s.age;
//    }
    for (int i = 0; i < n; i++) {
        student_t *s = students[i];
        sum += s->age;
    }
    double average = (double) sum /n ;
    return average;
}
/**
 * HOMEWORK: find the memory address of the oldest student
 * @param students
 * @param n
 * @return
 *
 * this is wrong, because you are returning an address of a local variable
 */
student_t *oldestStudent(student_t **students, int n) {
    int max=INT_MIN;
    student_t *p = NULL;
    for(int i=0;i<n;i++){
        student_t *sp = students[i];
        student_t s = *sp;
        if(s.age>max){
            max=s.age;
            p=&s;
            printf("---> updated p = %d %s %s\n", p->age, p->name, p->surname);
        }
    }
    return p;
}

student_t *oldestStudentNew(student_t **students, int n) {
    int max = INT_MIN;
    student_t *p = NULL;
    for (int i = 0; i < n; i++) {
        struct student_t *s = students[i];
        if (s->age > max) {
            max = s->age;
            p = s;
        }
    }
    return p;
}

/**
 * find student by name, if found return memory addres of the student, otherwise return NULL:
 *
 * @param students
 * @param name
 * @return
 *
 * xm, xh, xx, ALEX, BOB    ALEX
 */
student_t *findStudentByName(student_t **students, int n, char *name) {
    for(int i=0;i<n;i++){
        student_t *s = students[i];
        if(strcmp(s->name, name) == 0){
            return s;
        }
    }
    return NULL;
}

/**
 * Merge two sorted arrays into one new array, the newly generated array must also be sorted,
 * return the memory address of the new array.
 * @param a
 * @param na
 * @param b
 * @param nb
 * @return
 */
int *merge(int *a, int na, int *b, int nb) {
    int *r = malloc(sizeof(int) * (na + nb));
    int i = 0;
    int j = 0;
    int k = 0;
    while(i<na && j<nb){
        if(a[i]<b[j]){
            r[k]=a[i];
            i++;
            k++;
        }
        else{
            r[k]=b[j];
            j++;
            k++;
        }
    }

    while(i<na){
        r[k]=a[i];
        i++;
        k++;
    }
    while(j<nb){
        r[k]=b[j];
        j++;
        k++;
    }

    return r;
}

void increaseAge(student_t s) {
    s.age = s.age + 1;
}

void increaseAgeNew(student_t *s) {
    s->age = s->age + 1;
}

int main() {
    dbl d = 1.23;
    in a = 2;


    student_t s1 = {.age = 22, .surname = "Hello", .name = "Alex"};
    student_t s2;
    s2.age = 22;
    s2.name = "Carla";
    s2.surname = "Ciao";


    char city[10] = "TORINO";
    univ u1 = {.name = "Polito", .city = city, .foundedYear = 1885};
    univ u2 = {.name = "Unito", .city = city, .foundedYear = 1800};
    printf("u1 city = %s\n", u1.city);
    printf("u2 city = %s\n", u2.city);

    strcpy(city, "MILANO");

    printf("u1 city = %s\n", u1.city);
    printf("u2 city = %s\n", u2.city);

    // ==================================== //

    char bj[10] = "Beijing";
    univ u3 = {.name = "PKU", .city = strdup(bj), .foundedYear = 1900};
    univ u4 = {.name = "THU", .city = strdup(bj), .foundedYear = 1899};
    printf("u3 city = %s\n", u3.city);
    printf("u4 city = %s\n", u4.city);

    strcpy(bj, "Shanghai");
    printf("u3 city = %s\n", u3.city);
    printf("u4 city = %s\n", u4.city);

    t_t teacher1;

    int arr[10];
    univ us[10];

    univ *p = &u3;

    printf("%s\n", (*p).name);
    printf("%s\n", p->name);


    student_t xm = {.name = "xm", .surname = "wang", .age = 22};
    student_t xh = {23, "xh", "li"};
    student_t xx = {23, "xx", "zhao"};

    student_t sts[3] = {xm, xh, xx};
    double aa = averageAge(sts, 3);
    printf("%.2f average age\n", aa);

    increaseAge(xm);
//    xm.age = xm.age + 1;
    printf("after increase age, xm.age = %d\n", xm.age);

    increaseAgeNew(&xm);
    printf("after increase age new, xm.age = %d\n", xm.age);

    student_t alex = {.name = strdup("alex"), .surname = strdup("hello"), .age = 25};
    student_t bob = {.name = strdup("bob"), .surname = strdup("Ciao"), .age = 21};

    toUpperNameSurname(alex);

    printf("after toUpperNameSurname, name = %s, surname = %s, age = %d\n", alex.name, alex.surname, alex.age);

    char *cs = "abc";
//     cs[0] = 'A'; // KO

    char ms[10] = "abc";
    ms[0] = 'A'; // OK

    char *ds = malloc(sizeof(char) * 4);
    strcpy(ds, cs);
    ds[0] = 'A'; // OK

    char *es = strdup(cs);
    es[0] = 'A'; // OK

    toUpperNameSurnameNew(&bob);
    printf("after toUpperNameSurname, name = %s, surname = %s\n", bob.name, bob.surname);



    student_t *all[5] = {&xm, &xh, &xx, &alex, &bob};

    printf("average age = %.2f\n", averageAgeNew(all, 5));


    student_t *oldest = oldestStudentNew(all, 5);
    if (oldest != NULL) {
        printf("oldest student is %s %s, age is %d\n", oldest->name, oldest->surname, oldest->age);
    }

    student_t *sr = findStudentByName(all, 5, "ALEX");
    if (sr == NULL) {
        printf("there is no student with name\n");
    } else {
        printf("found student, his age is %d, his surname is %s\n", sr->age, sr->surname);
    }

    int aaa[5] = {1,2,7,8,9};
    int bbb[5] = {0,3,4,5,6};

    int *mab = merge(aaa, 5, bbb, 5);
    for (int i = 0; i < 10; i++) {
        printf("%d ", mab[i]);
    }
    printf("\n");
}