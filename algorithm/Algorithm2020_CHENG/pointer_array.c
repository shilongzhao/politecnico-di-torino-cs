//
// Created by zhaos on 5/11/2020.
//

#include <stdio.h>

int sum_array(int a[], int n) {
    int s = 0;
    for (int i = 0; i < n; i++) {
        s += *(a + i);
    }
    return s;
}

int sum_array2(int *a, int n) {
    int s = 0;
    for (int i = 0; i < n; i++) {
        s += a[i];// a[i] <=====> *(a + i)
    }
    return s;
}

int main() {
    int arr[10] = {2,3,4,1,5,6,9,8,7,0};
    int *p = &arr[0];

    printf("sum1 = %d, sum2 = %d", sum_array(arr, 10), sum_array2(arr, 10));

    printf("value of p = %p, value of arr = %p\n", p, arr);
}