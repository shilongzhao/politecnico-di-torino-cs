//
// Created by zhaos on 5/19/2020.
//

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *next;
};

void insert(node_t **head, int k) {
    node_t *t = malloc(sizeof(node_t));
    t->v = k;
    t->next = *head;
    *head = t;
}

node_t *insert2(node_t *head, int k) {
    node_t *t = malloc(sizeof(node_t));
    t->v = k;
    t->next = head;
    return t;
}

void triple(int *a) {
    *a = (*a) * 3;
}

int sum(node_t *head) {
    if (head == NULL) return 0;
    return head->v + sum(head->next);
}

int max(node_t *head) {
    if (head == NULL) return INT_MIN;
    if (head->v > max(head->next))
        return head->v;
    else
        return max(head->next);
}

int fib(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return fib(n - 1) + fib(n - 2);
}

int sum_array(int *a, int n) {
    if (n == 0) return 0;
    return a[0] + sum_array(&a[1], n - 1);
}

void print_array(int *a, int n) {

}

void print_array_reverse(int *a, int n) {
    if(n==0)
        return;
    printf("%d ", a[n-1]);
    print_array_reverse(a,n-1);
}

void print_array_reverse2(int *a, int n) {
    if (n == 0)
        return;
    print_array_reverse2(&a[1], n - 1);
    printf("%d ", a[0]);
}

void printList(node_t *list) {
    if (list == NULL) return;
    printf("%d ", list->v);
    printList(list->next);
}

void reversePrintList(node_t *list) {
    if (list == NULL) return;
    reversePrintList(list->next);
    printf("%d ", list->v);
}

int main() {
    node_t *head = NULL;
    insert(&head, 1);
    insert(&head, 2);
    insert(&head, 3);
    insert(&head, 6);
    head = insert2(head, 4);
    head = insert2(head, 5);
    printList(head);
    printf("\n");
    reversePrintList(head);

    printf("\n sum = %d\n", sum(head));
    printf("max = %d\n", max(head));
    printf("fib 10 = %d\n", fib(10));

    int a = 1;
    triple(&a);

    int arr[5] = {2,1,4,7,9};
    printf("sum arr = %d\n", sum_array(arr, 5));

    printf("\n --------------- \n");
    print_array_reverse2(arr, 5);
}