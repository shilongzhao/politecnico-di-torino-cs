//
// Created by zhaos on 5/27/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void recur(char *s, char *r, int n, int d) {
    if (d == n) {
        r[d] = '\0';
        printf("%s ", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        r[d] = s[i];
        recur(s, r, n, d + 1);
    }
}

void recur_no_repeat(char *s, char *r, int *used, int n, int d) {
    if (d == n) {
        r[d] = '\0';
        printf("%s ", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        if (used[i] == 0) {
            r[d] = s[i];
            used[i] += 1;
            recur_no_repeat(s, r, used, n, d + 1);
            used[i] -= 1;
        }
    }
}

// HOMEWORK: repeat with limit 2 (max appearance 2)
void recur_repeat2(char *s, char *r, int *used, int n, int d) {
    if (d == n) {
        r[d] = '\0';
        printf("%s ", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        if (used[i] < 2) {
            r[d] = s[i];
            used[i] += 1;
            recur_repeat2(s, r, used, n, d + 1);
            used[i] -= 1;
        }
    }
}

int main() {
    char *s = "ABCD";
    int n = 3;
    char *r = malloc(sizeof(char)* (n + 1));
    int *used = calloc(strlen(s), sizeof(int));
//    recur(s, r, n, 0);
    recur_repeat2(s, r, used, n, 0);
}
