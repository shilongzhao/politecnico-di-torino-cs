//
// Created by zhaos on 5/18/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *next;
};

typedef struct student_t student_t;
struct student_t {
    char *name;
    int age;
    student_t *next;
};

void printList(node_t *head) {
    for (node_t *i = head; i != NULL; i = i->next) {
        printf("%d ", i->v);
    }
    printf("\n");
}

student_t *findStudentByName(student_t *head, char *name) {
    for(student_t *i = head; i!= NULL; i= i->next){
        if(strcmp(i->name,name)==0){
            return i;
        }
    }
    return NULL;
}
/**
 *
 * @param head   list head
 * @param n  value to find
 * @return  address of node whose value is n
 */
node_t *findNode(node_t *head, int n) {
    for (node_t *i = head; i != NULL; i = i->next) {
        if (n == i->v) {
            return i;
        }
    }
    return NULL;
}

/**
 *
 * @param head head of list
 * @return address of node whose value is the largest
 */
node_t *findMax(node_t *head) {
    int max = INT_MIN;
    return NULL;
}

void orderInsert(node_t **h, int k) {
    node_t *t = malloc(sizeof(node_t));
    t->v = k;
    t->next = NULL;

    if (*h == NULL) {
       t->next = *h;
       *h = t;
       return;
    }

    if (k < (*h)->v) {
        t->next = *h;
        *h = t;
        return;
    }

    node_t *q = *h;
    node_t *p = (*h)->next;
    while (p != NULL && p->v < k) {
        p = p->next;
        q = q->next;
    }
    q->next = t;
    t->next = p;
}

/**
 * HOMEWORK
 * delete first node with value k
 * @param head
 * @param k
 */
void delete(node_t **head, int k) {
    return;
}

void insert(node_t **head, int k) {
    node_t *t = malloc(sizeof(node_t));
    t->v = k;
    t->next = *head;
    *head = t;
}


int main() {
    node_t n1 = {1, NULL};
    node_t n2 = {2, NULL};
    node_t n3 = {3, NULL};
    node_t n4 = {4, NULL};

    n1.next = &n2;
    n2.next = &n3;
    n3.next = &n4;

    printList(&n1);

    node_t *r = findNode(&n1, 2);
    printf("node 2 address = %p, value = %d\n", r, r->v);

    int x = 1;
    int y = 3;
    {
        printf("x = %d\n", x);
        double x = 2.0;
        printf("x in curly brace = %.2f\n", x);
        printf("y = %d\n", y);
    }
    printf("x = %d\n", x);

    student_t s0 = {.name = "carla", .age = 21, .next = NULL};
    student_t s1 = {.name = "alex", .age = 22, .next = &s0};
    student_t s2 = {.name = "bob", .age = 23, .next = &s1};

    student_t *f = findStudentByName(&s2, "alex");
    printf("find student f: age = %d\n", f->age);

    node_t *orderedList = NULL;
    orderInsert(&orderedList, 3);
    orderInsert(&orderedList, 2);
    orderInsert(&orderedList, 1);
    orderInsert(&orderedList, 4);
    orderInsert(&orderedList, 5);
    for (node_t *p = orderedList; p != NULL; p = p->next) {
        printf("%d ", p->v);
    }
    printf("\n");

    int arr[10] = {3,6,5,2,0,2,0,5,6,7};
    node_t *intHead = NULL;
    for (int i = 9; i >= 0; i--) {
        insert(&intHead, arr[i]);
    }
    for (node_t *p = intHead; p != NULL; p = p->next) {
        printf("%d ", p->v);
    }
    printf("\n");

    node_t *headD = NULL;
    int ab = 2345612;
    while (ab != 0) {
        int d = ab % 10;
        insert(&headD, d);
        printf("%d ", d);
        ab = ab / 10;
    }
}