//
// Created by zhaos on 5/28/2020.
//
#include <stdio.h>
void combo(char *s, char *r, int n, int d, int lastPick) {
    if (d == n) {
        r[d] = '\0';
        printf("%s\n", r);
        return;
    }

    for (int i = lastPick + 1; s[i] != '\0'; i++) {
        r[d] = s[i];
        combo(s, r, n, d + 1, i);
    }
}

int main() {
    char  *s = "ABCDE";
    char r[4] = {};
    combo(s, r, 3, 0, -1);
}