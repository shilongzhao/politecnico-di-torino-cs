//
// Created by zhaos on 5/11/2020.
//

#include <stdio.h>
#include <math.h>

int power(int x, int y);

int main() {
    double d = sqrt(2.0);  // square root
    printf("%.4f\n", d);
    double f = pow(1.2, 2.5);
    printf("%.4f\n", f);

    int p = power(2, 10);
    printf("2 to power of 10 is %d\n", p);

    int q = power(3, 5);
    printf("3 to power of 5 is %d\n", q);
}


int power(int x, int y) {
    int r = 0;
    for (int i = 0; i <= y; i++) {
        r = r * x;
    }
    return r;
}