//
// Created by zhaos on 5/24/2020.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct node_t node_t;
struct node_t {
    int v;
    node_t *left;
    node_t *right;
};

void insertBST(node_t **root, int v) {
    if (*root == NULL) {
        node_t *t = malloc(sizeof(node_t));
        t->left = t->right = NULL;
        t->v = v;
        *root = t;
        return;
    }
    if (v == (*root)->v) {
        return;
    } else if (v < (*root)->v) {
        insertBST(&((*root)->left), v);
    } else {
        insertBST(&((*root)->right), v);
    }
}

// lowest common ancestor
node_t *LCA(node_t *root, int p, int q) {
    if (root == NULL) {
        return NULL;
    }
    if (p < root->v && q < root->v) {
        return LCA(root->left, p, q);
    } else if (p > root->v && q > root->v) {
        return LCA(root->right, p, q);
    } else {
        return root;
    }
}

node_t *findBST(node_t *root, int v) {
    if (root == NULL) {
        return NULL;
    }
    if (root->v == v) {
        return root;
    } else if (root->v < v) {
        return findBST(root->right, v);
    } else {
        return findBST(root->left, v);
    }
}

void inorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    inorder(root->left);
    printf("%d ", root->v);
    inorder(root->right);
}

void printLevel(node_t *root, int level) {
    if (root == NULL) return;
    if (level == 0) printf("%d ", root->v);
    printLevel(root->left, level - 1);
    printLevel(root->right, level - 1);
}

int pickApples(node_t *root, int ladder) {
    if (root == NULL) return 0;
    if (ladder == 0) return root->v;
    return root->v
            + pickApples(root->left, ladder - 1)
            + pickApples(root->right, ladder - 1);
}

int levels(node_t *root) {
    if (root == NULL) {
        return 0;
    }
    int a = levels(root->left);
    int b = levels(root->right);
    if (a > b) return a + 1;
    else return b + 1;
}

void printLevels(node_t *root, int from, int to) {
   for (int i = from; i <= to; i++) {
       printLevel(root, i);
   }
}

int pathLengthBST(node_t *root, int p) {
    if (root->v == p) {
        return 0;
    }
    else if (p < root->v) {
        return pathLengthBST(root->left, p) + 1;
    }
    else {
        return pathLengthBST(root->right, p) + 1;
    }
}

int distanceBST(int *root, int p, int q) {
    return -1;
}

node_t *clone(node_t *root) {
    return NULL;
}

int main() {
    node_t *root = NULL;
    insertBST(&root, 5);
    insertBST(&root, 8);
    insertBST(&root, 3);
    insertBST(&root, 6);
    insertBST(&root, 7);
    insertBST(&root, 2);
    insertBST(&root, 1);
    insertBST(&root, 10);
    insertBST(&root, 9);

    inorder(root);
    printf("\n");

    node_t *p = findBST(root, 4);
    if (p == NULL) {
        printf("4 is not int the BST\n");
    }

    node_t *q = findBST(root, 3);
    if (q != NULL) {
        printf("3 is in the BST\n");
    }

    node_t *l = LCA(root, 7, 9);
    printf("LCA = %d\n", l->v);

    printf("print level: \n");
    printLevel(root, 2);

    printf("\n");
    int len = pathLengthBST(root, 9);
    printf("path length = %d\n", len);

}