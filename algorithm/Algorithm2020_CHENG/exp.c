//
// Created by zhaos on 5/18/2020.
//
#include <limits.h>
#include <stdio.h>

typedef struct trivial_t trivial_t;
struct trivial_t {
    int v;
};

trivial_t *max(trivial_t **array, int n) {
    int m = INT_MIN;
    trivial_t *p = NULL;
    for (int i = 0; i < n; i++) {
        trivial_t t = *(array[i]);
        printf("> address of t = %p, t.v=%d\n", &t, t.v);
        if (t.v > m) {
            m = t.v;
            printf("---> updating p to %p(v: %d) or %p(v: %d)\n", &t, t.v, array[i], array[i]->v);
            p = &t;
        }
    }
    printf("value of p = %p (v: %d)\n", p, p->v);
    return p;
}

trivial_t *max2(trivial_t **array, int n) {
    int m = INT_MIN;
    trivial_t *p = NULL;
    for (int i = 0; i < n; i++) {
        if (array[i]->v > m) {
            p = array[i];
            m = array[i]->v;
        }
    }
    return p;
}
int main() {
    trivial_t a = {1};
    trivial_t b = {2};
    trivial_t c = {4};
    trivial_t d = {3};

    trivial_t *arr[4] = {&a, &b, &c, &d};
    trivial_t *r = max(arr, 4);
    printf("max = %d\n", r->v);
}