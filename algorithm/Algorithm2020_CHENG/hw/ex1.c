//
// Created by zhaos on 5/11/2020.
//

#include <stdio.h>
int main() {
    char c = 'A';
    printf("address of c is %p, value of c is %c\n", &c , c);
    char* p = &c;
    printf("value of p is %p, address of p is %p", p, &p);

    // change value of char c by p
    *p = 'a'; // dereference
}