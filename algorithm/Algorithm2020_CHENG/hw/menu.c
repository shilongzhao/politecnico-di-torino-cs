//
// Created by zhaos on 5/29/2020.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void recur(char **data[], char **build, int n, int d) {
    if (d == n) {
        for (int i = 0; i < n; i++) {
            printf("%s ", build[i]);
        }
        printf("\n");
        return;
    }

    for (char **i = data[d]; *i != NULL; i++) {
        build[d] = strdup(*i);
        recur(data, build, n, d + 1);
    }
}
void buildMenu(char **data[], int n) {
   char **build = malloc(sizeof(char *) * n);
   recur(data, build, n, 0);
}

int main() {
    char **dishes[4] = {};
    char *first[5] = {};
//    char *s1 = "abc";
//    char s2[10] = "abc";
//    s1[0] = 'm'; // NO possible!
//    s2[0] = 'm';

    first[0] = "duck_salad";
    first[1] = "scotch_egg";
    first[2] = "soupe_and_bread";
    first[3] = "baby_squid";
    first[4] = NULL;
    dishes[0] = &first[0];

//    int *a = 5;
//    printf("--&a %p\n", &a);
//    printf("--a %p\n", a);
//    printf("--*a %d\n", *a);

//    printf("%s\n", first[0]);
//    printf("address of first[0] = %p\n", &first[0]);

    char *second[4] = {};
    second[0] = "rabbit";
    second[1] = "fish";
    second[2] = "lamb";
    second[3] = NULL;
    dishes[1] = &second[0];

    char *desserts[4] = {};
    desserts[0] = "gateaux";
    desserts[1] = "ice_cream";
    desserts[2] = "cheese_cake";
    desserts[3] = NULL;
    dishes[2] = &desserts[0];

    char *drinks[4] = {};
    drinks[0] = "coke";
    drinks[1] = "tea";
    drinks[2] = "wine";
    drinks[3] = NULL;
    dishes[3] = &drinks[0];

    buildMenu(dishes, 4);
}