//
// Created by zhaos on 5/20/2020.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct list1_t list1_t;
typedef struct list2_t list2_t;

struct list1_t {
    int val;
    int freq;
    list1_t *next;
};

struct list2_t {
    int val;
    list2_t *next;
};
void orderInsert(list2_t **head, int v) {
    list2_t *t = malloc(sizeof(list2_t));
    t->val = v;
    t->next = NULL;

    if (*head == NULL) {
        t->next = *head;
        *head = t;
        return ;
    }

    if (v < (*head)->val) {
        t->next = *head;
        *head = t;
        return ;
    }

    list2_t *q = *head;
    list2_t *p = (*head)->next;
    while (p != NULL && p->val < v) {
        p = p->next;
        q = q->next;
    }
    q->next = t;
    t->next = p;
}

list2_t *list_expand(list1_t *head) {
    list2_t *h=NULL;
    int d;
    for(list1_t *i=head;i!=NULL;i=i->next){
        for(int j=0;j<i->freq;j++){
            d=i->val;
            orderInsert(&h,d);
        }
    }
    return h;
}

int main(){
    list1_t e0 = {.val = 6, .freq = 2, .next = NULL};
    list1_t e1 = {.val = 2, .freq = 1, .next = &e0 };
    list1_t e2 = {.val = 4, .freq = 3, .next = &e1};
    list1_t e3 = {.val = 3, .freq = 5, .next = &e2};

    for( list2_t *g=list_expand(&e3); g!=NULL;g=g->next){
        printf("%d",g->val);
    }

}