//
// Created by zhaos on 5/13/2020.
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// exercise 0513, return 0 if not palindrome, 1 otherwise
int palindrome(char *s) {
    int l, r;
    l = 0;
    r = strlen(s) - 1;
    while (l < r) {
        if (s[l] == s[r]) {
            l++;
            r--;
        } else {
            return 0;
        }
    }
    return 1;
}

// revert a string in place
void revert(char *s) {
    int r, l;
    l = 0;
    r = strlen(s) - 1;
    while (l < r) {
        char tmp = s[r];
        s[r] = s[l];
        s[l] = tmp;
        l++;
        r--;
    }
}

// revert a string out of place
char *revert2(char *s) { // abc
    char *t = malloc(sizeof(char) * (strlen(s) + 1));
    for (int i = strlen(s) - 1, j = 0; i >= 0; i--, j++) { //
        t[j] = s[i];
    }
    t[strlen(s)] = '\0';
    return t;
}

//HOMEWORK: erase duplicates, and return the new string
// aaabbbcccabcedde -> abced // freq
char *eraseDup(char *s) {
    int *f = malloc(sizeof(int) * 26);
    for (int i = 0; i < 26; i++) {
        f[i] = 0;
    }
    char *s2 = malloc(sizeof(char) * (strlen(s) + 1));
    int j = 0;
    for (int i = 0; i < strlen(s); i++) {
        char c = s[i];
        if (f[c - 'a'] == 0) {
            s2[j] = c;
            j++;
            f[c - 'a'] = 1;
        }
    }
    s2[j] = '\0';
    return s2;
}

// follow up: input contains other characters (digits, alphabets, special chars)
// aa;;bb3!!^^^221312 -> a;b321
char *eraseDup2(char *s) {
    int *f = malloc(sizeof(int) * 128);
    for (int i = 0; i < 128; i++) {
        f[i] = 0;
    }
    char *s2 = malloc(sizeof(char) * (strlen(s) + 1));
    int j = 0;
    for (int i = 0; i < strlen(s); i++) {
        char c = s[i];
        if (f[c] == 0) {
            s2[j] = c;
            j++;
            f[c] = 1;
        }
    }
    s2[j] = '\0';
    return s2;
}


int main() {
    int i = palindrome("abcba");
    printf("i = %d\n", i);

    char s[10] = "1234567";
    revert(s);
    printf("%s\n", s);

    char s2[10] = "abcdefgh";
    char *p = revert2(s2);
    printf("p = %s, s2 = %s\n", p, s2);

    char *r = eraseDup2("aaab@#$@#$RTERTERTERbbccabc");
    printf("after erase dup = %s \n", r);
}