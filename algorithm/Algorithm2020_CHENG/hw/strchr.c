//
// Created by zhaos on 5/22/2020.
//
#include <string.h>
#include <stdio.h>

#include <stdlib.h>

typedef struct node_t node_t;

struct node_t {
    char *s;
    node_t *next;
};

node_t *insert(node_t *head, char *s) {
    node_t *t = malloc(sizeof(node_t));
    t->s = strdup(s);
    t->next = head;
    return t;
}

void insert2(node_t **head, char *s) {
    node_t *t = malloc(sizeof(node_t));
    t->s = strdup(s);
    t->next = *head;
    *head = t;
}


node_t *splitString(char *str) {
    node_t *h = NULL;
    int i = 0;
    int j = 0;
    int l = strlen(str);
    while (i <= l) {
        if (str[i] == '.' || str[i] == '\0') {
            str[i] = '\0';
            h = insert(h, &str[j]);
            i = i + 1;
            j = i;
        } else {
            i++;
        }
    }
    return h;
}

node_t *splitString2(char *s) {
    node_t *h = NULL;
    char r[10] = {};// TODO: dynamically allocate r or copy s and use \0???
    int j = 0;
    for (int i = 0; i <= strlen(s); i++) {
        if (s[i] == '.' || s[i] == '\0') {
            r[j] = '\0';
            // strdup
            insert2(&h, r);
//            printf("%s\n", r);
            j = 0;
        } else {
            r[j] = s[i];
            j++;
        }
    }
    return h;
}


int main() {

    char *s = "1223.33.4444.789";
    node_t *hh = splitString2(s);
    for (node_t *p = hh ; p != NULL; p = p->next) {
        printf("%s\n", p->s);
    }

    printf("\n================\n");

    char s2[20] = "aaa.bb.cccc.d.eee";
    node_t *h = splitString(s2);
    for (node_t *p = h; p != NULL; p = p->next) {
        printf("%s\n", p->s);
    }

}