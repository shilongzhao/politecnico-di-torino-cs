//
// Created by zhaos on 5/13/2020.
//
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int sum(int *a, int n) {
    int s = 0;
    for (int i = 0; i < n; i++) {
        s += a[i];
    }
    return s;
}

int sum2d(int *a[], int n, int m) {
    int s = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            s += a[i][j];
        }
    }
    return s;
}

int sum2dx(int **a, int n, int m) {
    int s = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            s += a[i][j];
        }
    }
    return s;
}

int max(int **a, int n, int m) {
    int mm = INT_MIN;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (a[i][j] > mm) {
                mm = a[i][j];
            }
        }
    }
    return mm;
}

/**
 * homework: given a matrix a, size nxm, print all sum of sub matrix of kxk
 * @param a
 * @param n
 * @param m
 * @param k
 * @return
 */
void printSubMatrix(int **a, int n, int m, int k) {
    for (int i = 0; i + k <= n; i++) {
        for (int j = 0; j + k <=  m; j++) {
            int sum=0;
            printf("corner (%d,%d)\n", i, j);
            for (int p = i; p < i + k; p++) {
                for (int q = j; q < j + k; q++) {
                    sum+=a[p][q];
                    printf(" --> adding %d to sum ", a[p][q]);
                }
            }
            printf("sum is:%d\n",sum);
        }
    }
}
int main() {
    int **arr = malloc(2 * sizeof(int *));
    arr[0] = malloc(3 * sizeof(int));
    arr[0][0] = 0;
    arr[0][1] = 1;
    arr[0][2] = 3;
    arr[1] = malloc(3 * sizeof(int));
    arr[1][0] = 4;
    arr[1][1] = 6;
    arr[1][2] = 4;

    int r = max(arr, 2, 3);
    printf("%d\n",r);

    printSubMatrix(arr, 2, 3, 2);
}