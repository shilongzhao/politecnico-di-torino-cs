//
// Created by zhaos on 5/11/2020.
//
#include <stdio.h>
void swap(int x, int y) {
    printf("x = %d, y = %d\n", x, y);
    int z = x;
    x = y;
    y = z;
    printf("after swap, x = %d, y = %d\n", x, y);
}

void swap_p(int *pa, int *pb) {
    int z = *pa;
    *pa = *pb;
    *pb = z;
}
int main() {
    int a, b;

    char s[10] = "";

    // 20200512: malloc, string - pointer, pointer - array, struct, pointer - struct, array - function, string - function
    // scanf("%s", s);
    scanf("%d", &a); // 1
    scanf("%d", &b); // 2

    printf("a = %d, b = %d\n", a, b);
    // swap values of a, b
    swap_p(&a, &b);
    printf("after swap_p, a = %d, b = %d\n", a, b);
}