//
// Created by zhaos on 5/19/2020.
//

#include <stdio.h>
#include <stdlib.h>

int search(int *vet1, int *vet2, int d1, int d2) {
    int F, flag;
    F = 0;//no
    for (int i = 0; i < d1; i++) {
        if (vet1[i] == vet2[0]) {
            if (i + d2 <= d1) {
                flag = 0;//find
                for (int j = i; j < i + d2; j++) {
                    if (vet1[j] != vet2[j - i]) {
                        flag = 1;//no
                        break;
                    }
                }
                if (flag == 0) {
                    F = 1;
                    return i;
                }
            }
        }
    }
    if (F == 0) {
        return -1;
    }

}

int main() {
    int a[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int b[4] = {7,8,9,10};
    printf("%p, %p, %p\n", a, &a[0], &a);
    int r = search(a, b, 10, 4);
    printf("start index %d\n", r);
}
