//
// Created by zhaos on 5/28/2020.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node_t node_t;
struct node_t {
    int k;
    node_t *left;
    node_t *right;
};

int levels(node_t *root) {
    if (root == NULL) return 0;
    int l = levels(root->left);
    int r = levels(root->right);
    if (l > r) {
        return l + 1;
    }
    else {
        return r + 1;
    }
}

// 201806
int tree_diameter(node_t *r) {
    if (r == NULL) return 0;
    int a = levels(r->left) + levels(r->right);
    int b = tree_diameter(r->left);
    int c = tree_diameter(r->right);
    if (a > b && a > c) {
        return a;
    } else if (b > a && b > c) {
        return b;
    } else {
        return c;
    }
}

void preorder(node_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%d ", root->k);
    preorder(root->left);
    preorder(root->right);

}

int main() {
    node_t t30,t19,t21,t45,t38,t36,t31,t67,t63,t69;
    t30.k=30;
    t19.k=19;
    t21.k=21;
    t45.k=45;
    t38.k=38;
    t36.k=36;
    t31.k=31;
    t67.k=67;
    t63.k=63;
    t69.k=69;
    t30.left=&t19;
    t30.right=&t45;
    t19.left=NULL;
    t19.right=NULL;
    t21.left=NULL;
    t21.right=NULL;
    t45.left=&t38;
    t45.right=&t67;
    t38.left=&t36;
    t38.right=NULL;
    t36.left=&t31 ;
    t36.right=NULL;
    t31.left=NULL;
    t31.right=NULL;
    t67.left=&t63;
    t67.right=&t69;
    t63.left=NULL;
    t63.right=NULL;
    t69.left=&t21;
    t69.right=NULL;

    preorder(&t30);

    int r = tree_diameter(&t30);
    printf("final result = %d\n", r);
}