//
// Created by zhaos on 6/1/2020.
//

#include <stdio.h>
#include <stdlib.h>

void insert_sequence(int *v1, int n, int *v2){
    int i=0;
    int j=0;
    while(i<n){
        for(j=i;j<n;j++){
            if(v1[j]<v1[i]){
                int p=i;
                for(int k=j-1;k>=i;k--){
                    v2[p]=v1[k];
                    p++;
                }
            }
        }
        i=j;
    }
}

int main(){
    int v1[10]={1,2,3,4,5,0,12,13,14,2};
    int v2[10]={};
    insert_sequence(v1,10,v2);
    for (int i = 0; i < 10; i++)
        printf("%d\n",v2[i]);
}
