//
// Created by zhaos on 5/10/2020.
//
#include <stdio.h>
#include <limits.h>

int main() {
    int a[10] = {};
    for (int i = 0; i < 10; i++) {
       printf("input the %dth integer: ", i + 1);
       scanf("%d", &a[i]);
    }

    int max = INT_MIN;
    // : write your coder here, find the max value

    // END your code
    printf("max of the numbers is %d\n", max);

    int min = INT_MAX;
    // : find the min value

    // END
    printf("min of the numbers is %d\n", min);

    double average = 0;
    // : calculate average
    int sum = 0;
    for (int i = 0; i < 10; i++) {
        sum += a[i];
    }
    average = sum / 10.0;
    // END
    printf("average of the numbers is %.2f\n", average);

}