//
// Created by zhaos on 5/23/2020.
//

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

typedef struct list_t list_t;

struct list_t {
    char *surname;
    char *name;
    list_t *next;
};

int compareNode(list_t *n1, list_t *n2) {
    if (strcmp(n1->surname, n2->surname) == 0) {
        return strcmp(n1->name, n2->name);
    }
    return strcmp(n1->surname, n2->surname);
}

int orderInsert(list_t **list, char *surname, char *name) {
    list_t *t = malloc(sizeof(list_t));
    t->surname = strdup(surname);
    t->name = strdup(name);
    t->next = NULL;

    if (*list == NULL) {
        *list = t;
        return 1;
    }

    if (compareNode(t, *list) == 0) {
        return 0;
    }

    if (compareNode(t, *list) < 0) {
        t->next = *list;
        *list = t;
        return 1;
    }

    list_t *p = *list;
    list_t *q = (*list)->next;
    while (q != NULL && compareNode(t, q) > 0) {
        p = p->next;
        q = q->next;
    }

    if (q != NULL && compareNode(t, q) == 0) {
        return 0;
    }

    t->next = q;
    p->next = t;
    return 1;
}

int main() {
    list_t *h = NULL;
    orderInsert(&h, "li", "an");
    orderInsert(&h, "wang", "er");
    orderInsert(&h, "an", "qi");
    orderInsert(&h, "li", "bo");
    orderInsert(&h, "an", "qi");
    orderInsert(&h, "li", "an");

    for (list_t *p = h;  p != NULL; p = p->next) {
        printf("%s %s\n", p->surname, p->name);
    }
}