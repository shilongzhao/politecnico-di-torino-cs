//
// Created by zhaos on 5/19/2020.
//

#include <stdlib.h>
#include <stdio.h>
typedef struct binode binode;
struct binode {
    int v;
    binode *prev;
    binode *next;
};

void insertHead(binode **h, int k) {
    binode *t = malloc(sizeof(binode));
    t->v = k;
    t->next = *h;
    t->prev = NULL;

    if (*h != NULL) {
        (*h)->prev = t;
    }

    *h = t;
}

// HOMEWORK:
void insertTail(binode **t, int k) {
    binode *h = malloc(sizeof(binode));
    h->v = k;
    h->next = NULL;
    h->prev = *t ;

    if (*t != NULL) {
        (*t)-> next= h;
    }

    *t= h;
}

int main() {
    binode *head = NULL;
    insertHead(&head, 1);
    insertHead(&head, 2);
    insertHead(&head, 4);

    for (binode *p = head; p != NULL; p = p->next) {
        printf("%d ", p->v);
    }

    printf("\n ================= \n");
    binode *tail = NULL;
    insertTail(&tail, 1);
    insertTail(&tail, 2);
    insertTail(&tail, 4);
    for (binode *p = tail; p != NULL; p = p->prev) {
        printf("%d ", p->v);
    }
}