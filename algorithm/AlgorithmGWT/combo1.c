//
// Created by zhaos on 1/3/2019.
//
#include <stdio.h>
#include <string.h>
/**
 *
 * @param s  candidate string
 * @param r  result string
 * @param n  target length
 * @param rs current result length
 * @param k  last choice
 */
void combo(char *s, char *r, int n, int rs, int k) {
    if (n == rs) {
        r[rs] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = k + 1; i < strlen(s); i++) {
        r[rs] = s[i];
        combo(s, r, n, rs + 1, i);
    }
}

int main() {
    char s[6] = "ABCDE";
    char r[6] = "";
    combo(s, r, 4, 0, -1);
}