//
// Created by zhaos on 12/26/2018.
//
#include <stdio.h>
#include <string.h>
void arrange(char *s, char *r, int n, int m, int *used) {
    if (n == 0) {
        r[m] = '\0';
        printf("%s\n", r);
        return;
    }
    
    for (int i = 0; i < strlen(s); i++) {
        if (used[i] == 0) {
            r[m] = s[i];
            used[i] = 1;
            arrange(s, r, n - 1, m + 1, used);
            used[i] = 0;
        }
    }
}

int main() {
    char s[6] = "ABCDE";
    char r[5] = "";
    int used[5] = {};
    arrange(s, r, 4, 0, used);
}