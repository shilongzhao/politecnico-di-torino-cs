//
// Created by zhaos on 12/26/2018.
//
#include <stdio.h>
#include <string.h>
/**
 * 
 * @param s candidate characters
 * @param r result
 * @param n length of string to be generated
 * @param m current length of result
 */
void arrange(char *s, char *r, int n, int m) {
    if (n == 0) {
        r[m] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        r[m] = s[i];
        arrange(s, r, n - 1, m + 1);
    }
}

int main() {
    char s[5] = "ABCD";
    char r[4] = "";
    
    arrange(s, r, 3, 0);
}