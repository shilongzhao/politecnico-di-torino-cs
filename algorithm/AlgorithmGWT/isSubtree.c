//
// Created by zhaos on 1/11/2019.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct tree_t tree_t;

struct tree_t {
    int v;
    tree_t *left;
    tree_t *right;
};

int isIsomorph(tree_t *t1, tree_t *t2) {
    if (t1 == NULL && t2 == NULL)
        return 1;
    if (t1 != NULL && t2 == NULL)
        return 0;
    if (t1 == NULL && t2 != NULL)
        return 0;

    if ((t1->v == t2->v) &&
        isIsomorph(t1->left, t2->left) &&
        isIsomorph(t1->right, t2->right)) {
        return 1;
    }
    else {
        return 0;
    }
}

int subtree_check(tree_t *t1, tree_t *t2) {
    if (t1 == NULL && t2 == NULL) return 1;
    if (t1 != NULL && t2 == NULL) return 0;
    if (t1 == NULL && t2 != NULL) return 0;

    if (isIsomorph(t1, t2)) {
        return 1;
    }
    else {
        return subtree_check(t1, t2->left) || subtree_check(t1, t2->right);
    }
}

int main() {

    tree_t t8 = {.v = 8, .left = NULL, .right = NULL};
    tree_t t9 = {.v = 9, .left = NULL, .right = NULL};

    tree_t t4 = {.v = 4, .left = &t8, .right = &t9};
    tree_t t5 = {.v = 5, .left = NULL, .right = NULL};
    tree_t t6 = {.v = 6, .left = NULL, .right = NULL};
    tree_t t7 = {.v = 7, .left = NULL, .right = NULL};

    tree_t t2 = {.v = 2, .left = &t4, .right = &t5};
    tree_t t3 = {.v = 3, .left = &t6, .right = &t7};

    tree_t t1 = {.v = 1, .left = &t2, .right = &t3};


    tree_t n2 = {.v = 8, .left = NULL, .right = NULL};
    tree_t n3 = {.v = 9, .left = NULL, .right = NULL};

    tree_t n1 = {.v = 4, .left = &n2, .right = &n3};

    int r = subtree_check(&n1, &t1);
    printf("is n1 subtree of t1? %d\n", r);

}
