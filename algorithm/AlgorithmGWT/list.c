//
// Created by zhaos on 12/6/2018.
//

#include <stdio.h>
#include <stdlib.h>
typedef struct list_t list_t;
struct list_t {
    int v;
    list_t *next;
};

void print_list(list_t *h) {
    for (list_t *t = h; t != NULL; t = t->next) {
        printf("%d\n", t->v);
    }
}

// insert with return value
/**
 * Insert value v into list h
 * @param h list header
 * @param v value
 * @return new list header
 */
list_t *insert(list_t *h, int v) {
    list_t *t = malloc(sizeof(list_t));
    t->v = v;
    t->next = h;
    return t;
}
// insert without return value
void insert2(list_t **h, int v) {
    list_t *t = malloc(sizeof(list_t));
    t->v = v;
    t->next = *h;
    *h = t;
}

int main() {
    list_t *head = NULL;
    insert2(&head, 1);
    insert2(&head, 2);
    insert2(&head, 3);
//    head = insert(head, 1);
//    head = insert(head, 2);
//    head = insert(head, 3);

    print_list(head);
}