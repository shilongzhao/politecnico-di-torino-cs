//
// Created by zhaos on 12/26/2018.
//

#include <stdlib.h>
#include <stdio.h>

typedef struct element element;
struct element {
    char *name;
    char *surname;
    int grade;
    element *child;
    element *sibling;
};

void print_tree(element *root) {
    if (root == NULL) {
        return;
    }
    printf("%s %s %d\n", root->name, root->surname, root->grade);
    print_tree(root->child);
    print_tree(root->sibling);
}

int main(){
    element n1 = {
            .name = "Alex",
            .surname = "Hello",
            .grade = 18,
            .child = NULL,
            .sibling = NULL
    };

    element n2 = {
            .name = "Bob",
            .surname = "Green",
            .grade = 19,
            .child = NULL,
            .sibling = NULL
    };

    element n3 = {
            .name = "Carla",
            .surname = "Red",
            .grade = 22,
            .child = NULL,
            .sibling = NULL
    };

    element n4 = {
            .name = "David",
            .surname = "Yellow",
            .grade = 33,
            .child = NULL,
            .sibling = NULL
    };

    n1.child = &n2;
    n2.child = &n4;
    n2.sibling = &n3;

    print_tree(&n1);
}