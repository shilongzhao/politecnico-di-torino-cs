//
// Created by zhaos on 1/3/2019.
//

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct boat_t boat_t;

struct boat_t {
    int len;
    int port_num;
};

int min = INT_MAX;
int num_ports_used(int N, int L, int *space) {
    int r = 0;
    for (int i = 0; i < N; i++) {
        if (space[i] != L) {
            r++;
        }
    }
    return r;
}

void place(int b, int *space, int N, int L, boat_t **boats, int m) {
    if (b == m) {
        if (num_ports_used(N, L, space) < min) {
            min = num_ports_used(N, L, space);
        }
        return;
    }
    for (int i = 0; i < N; i++) {
        if (space[i] >= boats[b]->len) {
            boats[b]->port_num = i;
            space[i] -= boats[b]->len;
            place(b + 1, space, N, L, boats, m);
            space[i] += boats[b]->len;
        }
    }
}