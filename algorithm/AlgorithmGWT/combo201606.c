//
// Created by zhaos on 1/11/2019.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int coversAll(int m[][9], int n, int *result, int k) {
    int *covered = malloc(sizeof(int) * 9);
    memset(covered, 0, sizeof(int) * 9);
    for (int i = 0; i < k; i++) {
        int row = result[i];
        for (int j = 0; j < 9; j++) {
            int v = m[row][j];
            covered[v] = 1;
        }
    }
    for (int i = 1; i < 9; i++) {
        if (covered[i] != 1) {
            return 0;
        }
    }
    return 1;
}

void comb_recursive(int m[][9], int n, int k, int *result, int numSelected, int lastPick) {
    if (numSelected == k ) {
        if (coversAll(m, n, result, k)) {
            for (int  i = 0; i < k; i++) {
                printf("%d ", result[i]);
            }
            printf("\n");
        }
    }

    for (int i = lastPick + 1; i < n; i++) {
        result[numSelected] = i;
        comb_recursive(m, n, k, result, numSelected + 1, i);
    }
}


void cover(int m[][9], int n, int k) {
    int *result = malloc(sizeof(int) * k);
    memset(result, 0, sizeof(int) * k);

    comb_recursive(m, n, k, result, 0, -1);
}

int main() {
    int m[5][9] = {
            {1,2,3},
            {2,3,7},
            {4,8},
            {4,5,6,8},
            {5,8}
    };

    cover(m, 5, 3);
}


