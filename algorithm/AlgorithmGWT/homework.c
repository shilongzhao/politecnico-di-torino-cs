//
// Created by zhaos on 12/6/2018.
//
#include <stdio.h>
void revert(int *v, int n) {
    for (int i = 0, j = n - 1; i < j; i++, j--) {
        int t = v[i];
        v[i] = v[j];
        v[j] = t;
    }
}

void increase_seq(int *v, int n) {
    int max = 0;
    int i = 0;
    int j = i + 1;
    while(i < n && j < n){
        while(v[j] > v[j-1] && i < n && j < n){
            j++;
        }
        int l = j - i;
        if(l > max){
            max = l;
        }
        i = j;
        j = i+1;
    }
    printf("max increase seq length = %d\n", max);
}

int main() {
    int v[7] = {1,2,3,0,5,6,7};
    //revert(v, 7);
    for (int i = 0; i < 7; i++) {
        printf("%d\n", v[i]);
    }
    increase_seq(v, 7);
}