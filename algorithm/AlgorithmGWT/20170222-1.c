//
// Created by zhaos on 1/23/2019.
//
#include <stdlib.h>
int min(int x, int y) {
    if (x < y)
        return x;
    else
        return y;
}
void copySubMat(int **m1, int r1, int c1, int **m2, int r2, int c2, int x, int y) {
    int p = min(x + r2, r1);
    int q = min(y + c2, c1);

    for (int i = x; i < p; i++) {
        for (int j = y; j < q; j++) {
            m1[i][j] = m2[i - x][j - y];
        }
    }
}
typedef struct element element;

struct element {
    int id;
    element *child;
    element *sibling;
    element *parent;
    int numChild;
};

void processTree(element *cur, element *p) {
    if (cur == NULL) return;

    if (p != NULL) p->numChild++;
    cur->parent = p;

    processTree(cur->sibling, p);
    processTree(cur->child, cur);
}

int main() {
    element *root;
    // .....
    processTree(root, NULL);
}

