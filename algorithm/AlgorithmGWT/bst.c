//
// Created by zhaos on 12/16/2018.
//
#include <stdio.h>
#include <stdlib.h>

typedef struct element element;
struct element {
    int x;
    element *left;
    element *right;
};

void insert(element **root, int v) {
    element *n = malloc(sizeof(element));
    n->x = v;
    n->left = NULL;
    n->right = NULL;

    if (*root == NULL) {
        *root = n;
        return;
    }

    if (v < (*root)->x){
//        insert(&( (*root)->left ), v);
        element *p = (*root)->left;
        insert(&p, v);
        (*root)->left = p;
    }
    else if (v > (*root)->x) {
//        insert(&( (*root)->right ), v);
        element *q = (*root)->right;
        insert(&q, v);
        (*root)->right = q;
    }
    else {
        return;
    }
}

void preorder(element *root) {
    if (root == NULL) {
        return;
    }
    printf("%d ", root->x);
    preorder(root->left);
    preorder(root->right);
}


void inorder(element *root) {
    if (root == NULL) {
        return;
    }
    inorder(root->left);
    printf("%d ", root->x);
    inorder(root->right);
}

void postorder(element *root) {
    if (root == NULL) {
        return;
    }
    postorder(root->left);
    postorder(root->right);
    printf("%d ", root->x);
}
int main() {
    element *root = NULL;
    insert(&root, 5);
    insert(&root, 2);
    insert(&root, 1);
    insert(&root, 4);
    insert(&root, 3);
    insert(&root, 8);
    insert(&root, 6);
    insert(&root, 7);
    insert(&root, 10);
    preorder(root);
    printf("\n");
    inorder(root);
    printf("\n");
    postorder(root);
}