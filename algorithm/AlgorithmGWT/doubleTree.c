//
// Created by zhaos on 1/13/2019.
//
#include <stdio.h>
#include <stdlib.h>
typedef struct node element;
struct node {
    int v;
    element *left;
    element *right;
};
void doubleTree(struct node *root) {
    if (root == NULL) {
        return;
    }
    doubleTree(root->left);
    doubleTree(root->right);
    element *t = malloc(sizeof(element));
    t->v = root->v;
    t->left = root->left;
    t->right = NULL;
    root->left = t;
}

void preorder(element *root) {
    if (root == NULL)
        return;
    printf("%d ", root->v);
    preorder(root->left);
    preorder(root->right);
}

int main() {
    element t1 = {.v = 1, .left = NULL, .right = NULL};
    element t3 = {.v = 3, .left = NULL, .right = NULL};

    element t2 = {.v = 2, .left = &t1, .right = &t3};

    doubleTree(&t2);

    preorder(&t2);
}