//
// Created by zhaos on 12/11/2018.
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define N 21
typedef struct list_t list_t;
struct list_t {
    char name[N];
    char surname[N];
    list_t *next;
};

/**
 * compare two nodes, compare surname if surnames are the same, then compare names
 * @param first
 * @param second
 * @return negative integer if first is smaller than second, zero if equal, positive if greater than
 */
int compare(list_t *first, list_t *second) {
    if (second == NULL) {
        return -1;
    }

    int r = strcmp(first->surname, second->surname);
    if (r != 0) {
        return r;
    }
    return strcmp(first->name, second->name);
}

int orderInsert(list_t **list, char *surname, char *name) {
    list_t *q = malloc(sizeof(list_t));
    strcpy(q->surname, surname); // string copy from surname to q->surname
    strcpy(q->name, name);

    if (compare(q, *list) < 0) {
        q->next = *list;
        *list = q;
        return 1;
    }
    for (list_t *t = *list; t != NULL; t = t->next) {
        int x = compare(q, t->next);
        if (x == 0) {
            return 0;
        }
        if (x < 0) {
            q->next = t->next;
            t->next = q;
            return 1;
        }
    }
}

int main() {
    list_t *l = NULL;
    orderInsert(&l, "Han", "XX");
    orderInsert(&l, "Zhang", "MM");
    orderInsert(&l, "An", "YY");
    for (list_t *t = l; t != NULL; t = t->next) {
        printf("%s %s\n", t->surname, t->name);
    }
}