//
// Created by zhaos on 1/13/2019.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct mat_t mat_t;
typedef struct row_t row_t;
typedef struct cell_t cell_t;

struct mat_t {
    int row;
    int column;
    row_t *prow;
};

struct row_t {
    int r;
    row_t *nextRow;
    cell_t *pcell;
};
struct cell_t {
    int c;
    double val;
    cell_t *next;
};
row_t *containsRow(row_t *h, int r) {
    for (row_t *p = h; p != NULL; p = p->nextRow) {
        if (p->r == r) {
            return p;
        }
    }
    return NULL;
}
cell_t *containsCol(cell_t *h, int c) {
    for (cell_t *p = h; p != NULL; p = p->next) {
        if (p->c == c) {
            return p;
        }
    }
    return NULL;
}
void matWrite (mat_t *M, float value, int r, int c) {
    row_t *pr = containsRow(M->prow, r);
    if (pr != NULL) {
        // row r exists
        cell_t *pc = containsCol(pr->pcell, c);
        if (pc != NULL) {
            // column c exists
            pc->val = value;
        }
        else {
            cell_t *tmp = malloc(sizeof(cell_t));
            tmp->val = value;
            tmp->c = c;
            tmp->next = pr->pcell;
            pr->pcell = tmp;
        }
    }
    else {
        cell_t *tmp = malloc(sizeof(cell_t));
        tmp->next = NULL;
        tmp->val = value;
        tmp->c = c;

        row_t *tmp2 = malloc(sizeof(row_t));
        tmp2->pcell = tmp;
        tmp2->r = r;
        tmp2->nextRow = M->prow;
        M->prow = tmp2;
    }
}

