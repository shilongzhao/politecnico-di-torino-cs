//
// Created by zhaos on 1/6/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int numCombo = 0;

void selectBooks(int numBooks,
        int *selectedBooks,
        int numSelectedBooks,
        int targetNum,
        int lastPick,
        int *bookGenres,
        int *genresSelected) {
    if (targetNum == numSelectedBooks) {
        numCombo++;
        for (int  i = 0; i < targetNum; i++) {
            printf("%d ", selectedBooks[i]);
        }
        printf("\n");
        return;
    }

    for (int i = lastPick + 1; i < numBooks; i++) {
        int g = bookGenres[i];
        if (genresSelected[g] == 0) {
            genresSelected[g] = 1;
            selectedBooks[numSelectedBooks] = i;
            selectBooks(numBooks, selectedBooks, numSelectedBooks + 1,
                    targetNum, i, bookGenres, genresSelected);
            genresSelected[g] = 0;
        }
    }
}

int birthday(int *vet, int n, int m, int k) {
    int numBooks = n;
    int numGenres = m;
    int targetNum = k;

    int *selectedBooks = malloc(sizeof(int) * targetNum);
    memset(selectedBooks, 0, sizeof(int) * targetNum);

    int *genresSelected = malloc(sizeof(int) * (numGenres + 1));
    memset(genresSelected, 0, sizeof(int) * (numGenres + 1));

    selectBooks(numBooks, selectedBooks, 0, targetNum, -1, vet, genresSelected);

    return numCombo;
}

int main() {
    int vet[5] = {2,1,1,4,3};
    int n = 5;
    int m = 4;
    int k = 3;
    int r = birthday(vet, n, m , k);
    printf("There are %d different combinations!\n", r);
}

