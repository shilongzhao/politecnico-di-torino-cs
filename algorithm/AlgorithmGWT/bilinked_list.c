//
// Created by zhaos on 12/14/2018.
//
#include <stdio.h>
#include <stdlib.h>

typedef struct element element;
struct element {
    int x;
    element *next;
    element *prev;
};
void listInsert(element **left, element **right, int key, int leftRight) {
    element *p = malloc(sizeof(element));
    p->x = key;
    p->prev = NULL;
    p->next = NULL;
    // list is empty
    if (*left == NULL && *right == NULL) {
        *left = p;
        *right = p;
        return;
    }
    if (leftRight == 0) {
        p->next = *left;
        (*left)->prev = p;
        *left = p;
    }
    else {
        p->prev = *right;
        (*right)->next = p;
        *right = p;
    }
}
// TODO: homework 20181216
void listDisplay(element *left, element *right, int leftRight) {

}

int main() {
    element *left = NULL;
    element *right = NULL;
    listInsert(&left, &right, 1, 0);
    listInsert(&left, &right, 2, 0);
    listInsert(&left, &right, 3, 0);
    listInsert(&left, &right, 4, 1);
    listInsert(&left, &right, 5, 1);
    listInsert(&left, &right, 6, 1);

    for (element *t = left; t != NULL; t = t->next) {
        printf(" %d ", t->x);
    }
}