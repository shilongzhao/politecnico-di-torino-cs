//
// Created by zhaos on 12/15/2018.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * repeatable candidate arrange
 * @param s  candidate string
 * @param n  target string length
 * @param r  result
 * @param rs result size (number of chars)
 */
void repeatable_arrange(char *s, int n, char *r, int rs) {
    if (n == 0) {
        r[rs] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        r[rs] = s[i];
        repeatable_arrange(s, n - 1, r, rs + 1);
    }
}
int main() {
    char str[5] = "ABCD";
    char result[5]; // ABB\0, ABC

    repeatable_arrange(str, 4, result, 0);
}