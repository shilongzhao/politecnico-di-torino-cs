//
// Created by zhaos on 12/17/2018.
//

#include <stdio.h>
#include <string.h>
void combo(char *str, int n, char *r, int rs, int m) {
    if (n == 0) {
        r[rs] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = m + 1; i < strlen(str); i++) {
        r[rs] = str[i];
        combo(str, n - 1, r, rs + 1, i);
    }
}

int main() {
    char str[6] = "ABCDE";
    char r[4];
 
    combo(str, 3, r, 0, -1);
}