//
// Created by zhaos on 12/15/2018.
//
#include <stdio.h>
#include <string.h>
void arrange_np(char *s, int n, char *r, int rs, int *used) {
    if (n == 0) {
        r[rs] = '\0';
        printf("%s\n", r);
        return;
    }
    for (int i = 0; i < strlen(s); i++) {
        if (used[i] == 0) {
            r[rs] = s[i];
            used[i] = 1;
            arrange_np(s, n - 1, r, rs + 1, used);
            used[i] = 0;
        }
    }
}

int main() {
    char s[5] = "ABCD";
    char r[4];
    int used[4] = {};
    arrange_np(s, 3, r, 0, used);
}