//
// Created by zhaos on 12/17/2018.
//
#include <stdio.h>
#include <stdlib.h>
int all_on(const int *bulbs, int n) {
    for (int i = 0; i < n; i++) {
        if (bulbs[i] == 0) {
            return 0;
        }
    }
    return 1;
}

/**
 *
 * @param m  matrix
 * @param i  switch i
 * @param bulbs  states of all bulbs
 * @param n  number of bulbs
 */
void press(int **m, int i, int *bulbs, int n) {
    for (int k = 0; k < n; k++) {
        bulbs[k] = bulbs[k] ^ m[i][k];
    }
}

/**
 *
 * @param mat  input matrix
 * @param r  row
 * @param c  columns
 * @param bulbs states of bulbs
 * @param switches set of switches taken
 * @param s size of switches set
 * @param m last chosen switch
 */
void switch_bulbs(int **mat, int r, int c, int *bulbs, int *switches, int s, int m) {
    if (all_on(bulbs, c)) {
        for (int i = 0; i < s; i++) {
            printf("%d ", switches[i]);
        }
        printf("\n");
        return;
    }

    for (int i = m + 1; i < r; i++) {
        press(mat, i, bulbs, c);
        switches[s] = i;
        switch_bulbs(mat, r, c, bulbs, switches, s + 1, i);
        press(mat, i, bulbs, c);
    }
}


int main() {

    int input[4][5] = {
            {1,1,0,0,1},
            {1,0,1,0,0},
            {0,1,1,1,0},
            {1,0,0,1,0}
    };


    int *mat[4];
    for (int i = 0; i < 4; i++) {
        mat[i] = malloc(sizeof(int) * 5);
        for (int j = 0; j < 5; j++) {
            mat[i][j] = input[i][j];
        }
    }

    int bulbs[5] = {};
    int switches[4] = {-1,-1,-1,-1};
    switch_bulbs(mat, 4, 5, bulbs, switches, 0, -1);


}