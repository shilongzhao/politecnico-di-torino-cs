//
// Created by zhaos on 1/12/2019.
//

#include <stdio.h>
#include <stdlib.h>

int coversAll(int m[][9], int n, int *result, int k) {
    int covered[9] = {};
    for (int i = 0; i < k; i++) {
        int r = result[i];
        for (int j = 0; j < 9; j++) {
            int v = m[r][j];
            covered[v] = 1;
        }
    }

    for (int i = 1; i < 9; i++) {
        if (covered[i] == 0) {
            return 0;
        }
    }

    return 1;
}

void combo(int m[][9], int n, int k, int *result, int numSelected, int lastPick) {
    if (k == numSelected) {
        if (coversAll(m, n, result, k)) {
            for (int i = 0; i < k; i++) {
                printf("%d ", result[i]);
            }
            printf("\n");
        }
        return;
    }

    for (int i = lastPick + 1; i < n; i++) {
        result[numSelected] = i;
        combo(m, n, k, result, numSelected + 1, i);
    }
}


void cover(int m[][9], int n, int k) {
    int *result = malloc(sizeof(int) * k);
    combo(m, n, k, result, 0, -1);
    free(result);
}

int main() {
    int result[5] = {};
    int m[5][9] = {
            {1,2,3},
            {4,5,6},
            {5,6,7},
            {6,7,8},
            {7,8}
    };

    cover(m, 5, 3);

}