//
// Created by zhaos on 1/12/2019.
//

#include <stdio.h>
#include <stdlib.h>

void combo(int remaining, int *result, int numSelected, int *coins, int k, int lastPick) {
    if (remaining == 0) {
        for (int i = 0; i < numSelected; i++) {
            printf("%d ", result[i]);
        }
        printf("\n");
        return;
    }
    if (remaining < 0) {
        return;
    }
    for (int i = lastPick; i < k; i++) {
        result[numSelected] = coins[i];
        combo(remaining - coins[i], result, numSelected + 1, coins, k, i);
    }
}

void change(int n, int *coins, int k) {
    int *result = malloc(sizeof(int) * n);
    combo(n, result, 0, coins, k, -1);
    free(result);
}

int main() {
    int coins[3] = {2,3,6};
    change(10, coins, 3);
}