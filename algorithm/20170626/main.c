#include <stdio.h>

#include <stdlib.h>

#include <string.h>



float my_atof(char *s) {

    int sign = 1;

    if (s[0] == '-') {

        sign = -1;

    }

    s++;

    float x = 0;

    for (; *s != '.'; s++ ) {

        x = x * 10 + (*s) - '0';

    }

    s++;

    float y = 0, z = 1;

    for (; *s != '\0'; s++) {

        z =  z * 10;

        y = y * 10 + (*s) - '0';

    }

    

    return (x + y/z)*sign;

}



typedef struct bst_t bst_t;

typedef struct list1_t list1_t;

typedef struct list2_t list2_t;

struct bst_t {

    char *model;

    list1_t *list;

    bst_t *left;

    bst_t *right;

};



struct list1_t {

    char *dealer;

    float price;

    list1_t *next;

};



struct list2_t {

    char *model;

    char *dealer;

    float min_price;

    list2_t *next;

};



list1_t *min(list1_t *head) {

    if (head == NULL) {

        return NULL;

    }

    list1_t *m = head;

    for (; head != NULL; head = head->next) {

        if (m->price > head->price) {

            m = head;

        }

    }

    return m;

}

void bst2list(bst_t *bst, list2_t **p) {

    if (bst == NULL) {

        return;

    }

    bst2list(bst->right, p);

    

    list2_t *t = malloc(sizeof(list2_t));

    t->model = strdup(bst->model);

    list1_t *m = min(bst->list);

    t->dealer = strdup(m->dealer);

    t->min_price = m->price;

    t->next = *p;

    *p = t;

    

    bst2list(bst->left, p);

}



void sum_recursive(int *arr,int start_index, int a_size, int *combo, int c_size, int k) {

    int sum = 0;

    for (int i = 0; i < c_size; i++) {

        sum += combo[i];

    }

    

    if (sum == k) {

        printf("print set combo[]... ");

    }

    if (sum > k) {

        return;

    }

    

    for (int i = start_index; i< a_size; i++) {

        combo[c_size] = arr[i];

        c_size++;

        sum_recursive(arr, i, a_size, combo, c_size, k);

    }

}



void balance(int **mat, int r, int c, int k) {

    int *arr = *mat;

    int *combo = malloc(sizeof(int) * r * c);

    sum_recursive(arr, 0, r*c, combo, r*c, k);

}





int main(int argc, char *argv[]) {

    printf("=============== Exercise 1 ===============\n");

    char f1[10] = "-123.45";

    if (my_atof(f1) + 123.45 < 0.0001) {

        printf("[OK] my_atof\n");

    }

    printf("=============== Exercise 2 ===============\n");

    

    list1_t fiat_a = {"dealerA", 1110, NULL};

    list1_t fiat_b = {"dealerB", 1109, &fiat_a};

    list1_t fiat_c = {"dealerC", 1111, &fiat_b};

    list1_t *m = min(&fiat_c);

    if ((strcmp(m->dealer, "dealerB") == 0) && (m->price == 1109)) {

        printf("[OK] min\n");

    }

    list1_t volk_a = {"dealer_V", 11111, NULL};

    bst_t volk = {"VOLKSWAGEN", &volk_a, NULL, NULL};

    

    list1_t bmw_a = {"dealer_B", 22222, NULL};

    bst_t bmw = {"BMW", &bmw_a, NULL, NULL};

    

    bst_t fiat = {"FIAT", &fiat_c, &bmw, &volk};

    list2_t *p = NULL;

    bst2list(&fiat, &p);

    while(p != NULL) {

        printf("%s %s %.2f\n", p->model, p->dealer, p->min_price);

        p = p->next;

    }

    

    printf("=============== Exercise 3 ===============\n");

    int mat[3][4] = {

        {2,3,1,2},

        {6,4,2,5},

        {2,4,3,2}

    };

}