#include <stdio.h>

#include <stdlib.h>
#include <mem.h>
#include <limits.h>

#define DEBUG

void longest(char *in, int n, char ***out) {
    *out = calloc((size_t) n, sizeof(char *));

    int i = 0;
    int count = 0;
    while (i < strlen(in)) {
        if (in[i] <= 'A' || in[i] >= 'Z') {
            i++;
            continue;
        }

        int j = i;
        // overflow? no worry
        while (in[j] >= 'A' && in[j] <= 'Z') {
            j++;
        }

        (*out)[count] = calloc((size_t) (j - i + 1), sizeof(char));
        strncpy((*out)[count], in + i, (size_t) (j - i));
#ifdef DEBUG
        printf("copied: %s\n", (*out)[count]);
#endif
        count += 1;

        i = j;
    }
}

void free_out(char **out, int n) {
    for (int i = 0; i < n; i++) {
        free(out[i]);
    }
    free(out);
}

// ================= 2 ================== //
typedef struct node node;
struct node {
    char *k;
    node *left;
    node *right;
};

node *search_p(node *root, char *k);

void treeMerge(struct node **in, int n) {
    for (int i = 1; i < n; i++) {
        node *p = search_p(in[0], in[i]->k);
        if (p == NULL) {
            fprintf(stderr, "ERROR: cannot find key %s\n", in[i]->k);
            return;
        }

        p->left = in[i]->left;
        p->right = in[i]->right;

        free(in[i]);
    }
}

/**
 * Search the parent of node whose key is k
 * @param root
 * @param k key to search
 * @return
 */
node *search_p(node *root, char *k) {
    if (root == NULL) return NULL;

    if (strcmp(root->k, k) == 0)
        return root;

    node *l = search_p(root->left, k);
    if (l != NULL)
        return l;

    return search_p(root->right, k);
}

// ================ 3 =================== //
int min_diff = INT_MAX;
void recur(int nTask, int *times, int nServer, int n, int **assign, int **best, int *totalTime);

void balance(int nTask, int *times, int nServer) {
    int **assign = calloc((size_t) nServer, sizeof(int *));
    for (int i = 0; i < nServer; i++) {
        assign[i] = calloc((size_t) nTask, sizeof(int));
    }

    int **best = calloc((size_t) nServer, sizeof(int *));
    for (int i = 0; i < nServer; i++) {
        best[i] = calloc((size_t) nTask, sizeof(int));
    }

    int *totalTime = calloc((size_t) nServer, sizeof(int));

    recur(nTask, times, nServer, 0, assign, best, totalTime);

    // free...
}

int max(int *arr, int n) {
    int max = arr[0];
    for (int i = 1; i < n; i++) {
        if (arr[i] > max) max = arr[i];
    }
    return max;
}

int min(int *a, int n) {
    int min = a[0];
    for (int i = 0; i < n; i++) {
        if (a[i] < min) min = a[i];
    }
    return min;
}

void recur(int nTask, int *times, int nServer, int n, int **assign, int **best, int *totalTime) {
    if (n == nTask) {
        int max_val= max(totalTime, nServer);
        int min_val = min(totalTime, nServer);
        int diff = max_val - min_val;
        if (diff < min_diff) {
            min_diff = diff;
            for (int i = 0; i < nServer; i++) {
                for (int j = 0; j < nTask; j++) {
                    best[i][j] = assign[i][j];
                }
            }
        }
        return;
    }
    for (int i = 0; i < nServer; i++) {
        assign[i][n] = 1;
        totalTime[i] += times[n];
        recur(nTask, times, nServer, n + 1, assign, best, totalTime);
        assign[i][n] = 0;
        totalTime[i] -= times[n];
    }
}

// ================ main ================ //
int main() {

    // test 1
    char *s = "THIS is a STring incLUDing small and capITAL LETTERS";
    char **out = NULL;
    longest(s, 5, &out);
    free_out(out, 5);

    // TODO: test 2

    // test 3
    int a[11] = {2,3,1,4,5,9,6,7,8,0,10};
    balance(11, a, 2);
    return 0;
}