排列问题和组合问题虽然都可以用backtracking解决，但是在细节上有不同，
组合问题的每次expand都是从next_start开始，而不是从0开始，而传到下一次递归时，next_start=i+1,这样组合问题不需要使用used数组，并且叶节点上不会有重复的组合

recur(int arr[], int N, int depth, int next_start) {
	for(int i = next_start; i < N; i++) {
		sol[depth] = arr[i];
		recur(arr, N, depth+1, i+1);
	}
}

