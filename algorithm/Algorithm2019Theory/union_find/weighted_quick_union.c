//
// Created by zhaos on 10/6/2019.
//
//
// Created by zhaos on 10/6/2019.
//

#include <stdio.h>

#define N 11

int id[N];
int sz[N];

int find(int p) {
    while(id[p] != p) p = id[p];
    return p;
}

void union_op(int p, int q) {
    int i = find(p);
    int j = find(q);

    if (i == j) return;

    if (sz[i] <= sz[j]) {id[i] = j; sz[j] += sz[i];}
    else                {id[j] = i; sz[i] += sz[j];}
}

void print_array(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    for (int i = 0; i < N; i++) {
        id[i] = i;
    }

    for (int i = 0; i < N; i++) {
        sz[i] = 1;
    }

    int p, q;
    while(scanf("%d %d", &p, &q) == 2) {
        union_op(p, q);
        print_array(id, N);
    }

    for (int i = 0; i < N; i++) {
        printf("%d ", id[i]);
    }
}