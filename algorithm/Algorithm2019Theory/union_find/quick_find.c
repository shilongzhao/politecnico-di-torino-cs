//
// Created by zhaos on 10/6/2019.
//

#include <stdio.h>

#define N 11

int id[N];

int find(int p) {
    return id[p];
}

void union_op(int p, int q) {
    int pId = find(p);
    int qId = find(q);

    if (pId == qId) return;

    for (int i = 0; i < N; i++) {
        if (id[i] == pId) id[i] = qId;
    }
}

int main() {
    for (int i = 0; i < N; i++) {
        id[i] = i;
    }

    int p, q;
    while(scanf("%d %d", &p, &q) == 2) {
        union_op(p, q);

        for (int i = 0; i < N; i++) {
           printf("%d ", id[i]);
        }
        printf("\n");
    }

}