//
// Created by zhaos on 10/6/2019.
//

#include <stdio.h>

#define N 11

int parent[N];

int find(int p) {
    while (p != parent[p]) p = parent[p];
    return p;
}

void union_op(int p, int q) {
    int rootP = find(p);
    int rootQ = find(q);

    if (rootP == rootQ) return;

    parent[rootP] = rootQ;
}

int main() {
    for (int i = 0; i < N; i++) {
        parent[i] = i;
    }

    int p, q;
    while (scanf("%d %d", &p, &q) == 2) {
        union_op(p, q);

        for (int i = 0; i < N; i++) {
            printf("%d ", parent[i]);
        }
        printf("\n");
    }
}
