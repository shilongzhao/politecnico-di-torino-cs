//
// Created by zhaos on 10/6/2019.
//

#include <stdio.h>
#include "utils.h"

void print_array(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}