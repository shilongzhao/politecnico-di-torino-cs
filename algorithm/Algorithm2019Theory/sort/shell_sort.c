//
// Created by zhaos on 10/20/2019.
//

void shell_sort(int a[], int n) {
    int h = 1;
    while (h < n / 3) h = 3 * h + 1;
    while (h >= 1) {
        for (int i = h; i < n; i++) {
            // insert a[i] among a[i-h], a[i-2*h], a[i-3*h] ... 
            for (int j = i; j >= h && a[j] < a[j - h]; j -= h) {
                int tmp = a[j];
                a[j] = a[j - h];
                a[j - h] = tmp;
            }
        }
        h = h / 3;
    }
}

int main() {

}