#include <stdio.h>

void swap(int *a, int i, int j) {
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
}

int partition(int *a, int l, int r) {
    printf("partitioning[%d, %d]\n", l, r);

    int i = l - 1; // will point to the first number greater or equal to pivot
    int j = r; // will point to the first number less or equal to pivot
    int pivot = a[r];

    while (i < j) {
        while (a[++i] < pivot)
            ;
        while (j > l && a[--j] >= pivot)
            ;
        if (i < j)
            swap(a, i, j);
    }
    swap(a, i, r);
    
    return i;
}

void sort(int *a, int l, int r) {
    if (r <= l) return;
    int c = partition(a, l, r);
    sort(a, l, c - 1);
    sort(a, c + 1, r);
}

int main() {
    int a[17] = {1,10,72,41,71,0,8,55,91,14,32,19,13,73,7,3,31};
    sort(a, 0, 16);
}