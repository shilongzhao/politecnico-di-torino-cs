//
// Created by ZhaoShilong on 01/09/2020.
//
#include <stdlib.h>
#include <stdio.h>

void print_array(int *a, int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void counting_sort(int *a, int n) {
    int max = 0;
    for (int i = 0; i < n; i++) {
        if (a[i] > max) max = a[i];
    }

    int k = max + 1;
    int *c = calloc(k, sizeof(int));
    for (int i = 0; i < n; i++) {
        int v = a[i];
        c[v] += 1;
    }

    print_array(c, k);

    for (int j = 1; j < k; ++j) {
        c[j] = c[j] + c[j - 1];
    }
    print_array(c, k);
    
    int *output = malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++) {
        int index = c[a[i]] - 1;
        output[index] = a[i];
        c[a[i]] -= 1;
    }

    printf("c at end of sort: ");
    print_array(c, k);

    printf("output is: ");
    print_array(output, n);
}

int f(int n ) {
    int i, res;
    if (n <= 0) return 1;
    res = 0;
    for (i = 0; i < 2; i++)
        res += f(n - i - 2) + f(n - i - 3);
    return res;
}

int main() {
    
    printf("-- %d\n", f(4));
    int a[15] = {10,3,2,4,10,3,8,0,8,3,2,9,6,10,2};
    counting_sort(a, 15);
    
}

