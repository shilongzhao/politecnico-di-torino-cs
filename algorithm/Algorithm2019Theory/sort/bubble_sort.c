//
// Created by zhaos on 10/13/2019.
//
#include <stdio.h>
#include "../utils/utils.h"

void bubble_sort(int a[], int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (a[j] > a[j+1]) {
                int tmp = a[j];
                a[j] = a[j+1];
                a[j+1] = tmp;
            }
        }
        print_array(a, n);
    }
}

int main() {
    int a[10] = {6,3,5,1,7,21,8,4,12,0};
    bubble_sort(a, 10);
}