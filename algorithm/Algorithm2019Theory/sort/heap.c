//
// Created by zhaos on 12/11/2019.
//

// number of elements currently in the heap
#include <stdio.h>
void exch(int *a, int i, int j) {
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
}

void swim(int *a, int k) {
    while (k > 1 && a[k/2] < a[k]) {
        exch(a, k/2, k);
        k = k / 2;
    }
}

/**
 * a has elements in indices 1 to N, we do not use index 0
 * @param a
 * @param k
 * @param N
 */
void sink(int *a, int k, int N) {
    while (2 * k <= N) {
        int j = 2 * k;
        if (j < N && a[j] < a[j+1]) j = j + 1;
        if (a[k] >= a[j]) break;
        exch(a, k, j);
        k = j;
    }
}

void insert(int *a, int k, int N) {
    N = N + 1;
    a[N] = k;
    swim(a, N);
}


int delMax(int *a, int N) {
    int max = a[1];
    exch(a, 1, N);
    N--;
    sink(a, 1, N);
    return max;
}

// still we ignore the a[0], sort through a[1] to a[M]
// a is size of M + 1
void heap_sort(int *a, int M) {
    for (int k = M/2; k >= 1; k--) {
        sink(a, k, M);
    }
    while (M > 1) {
        exch(a, 1, M--);
        sink(a, 1, M);
    }
}

int main() {
    int a[16] = {-1, 19,5,31,7,8,6,11,3,22,4,12,17,12,13,43};
    heap_sort(a, 16);
    
    for (int i = 1; i < 16; i++) {
        printf("%d, %d\n", i, a[i]);
    }
}