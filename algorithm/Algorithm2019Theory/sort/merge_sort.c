#include <stdlib.h>
#include <stdio.h>

//
// Created by zhaos on 11/20/2019.
//

void merge(int *a, int *b, int lo, int mid, int hi) {

    for (int i = lo; i <= hi; i++) {
        b[i] = a[i];
    }

    int i = lo, j = mid + 1;
    for (int k = lo; k <= hi; k++) {
        if (i > mid) {
            a[k] = b[j++];
        }
        else if (j > hi) {
            a[k] = b[i++];
        }
        else if (b[i] < b[j]) {
            a[k] = b[i++];
        }
        else {
            a[k] = b[j++];
        }
    }

    printf("\n=== merge after lo %d mid %d hi %d ===\n", lo, mid, hi);
    for (int k = 0; k < 16; k++) {
        printf("%d ", a[k]);
    }
}

void sort(int *a, int *b, int lo, int hi) {
    if (hi <= lo) return;

    int mid = lo + (hi - lo)/2;
    sort(a, b, lo, mid);
    sort(a, b, mid + 1, hi);

    merge(a, b, lo, mid, hi);
}

void merge_sort(int *a, int n) {
    int *b = malloc(sizeof(int) * n);

    sort(a, b, 0, n - 1);
}

int main() {
    int a[16] = {15,4,16,2,8,0,1,11,3,12,6,7,13,9,5,14};
    merge_sort(a, 16);
//    for (int i = 0; i < 10; i++) {
//        printf("%d ", a[i]);
//    }
}

