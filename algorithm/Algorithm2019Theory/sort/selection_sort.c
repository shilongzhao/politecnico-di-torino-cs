//
// Created by zhaos on 10/13/2019.
//

#include <stdio.h>
#include "../utils/utils.h"

void selection_sort(int a[], int n) {
    for (int i = 0; i < n; i++) {
        int min = i;
        // exchange a[i] with the smallest entry in a[i+1] to a[N]
        for (int j = i + 1; j < n; j++) {
            if (a[j] < a[min]) {
                min = j;
            }
        }
        int tmp = a[i];
        a[i] = a[min];
        a[min] = tmp;

        print_array(a, n);
    }
}


int main() {
    int a[10] = {8, 2, 5, 1, 0, 4, 7, 3, 6, 9};

    selection_sort(a, 10);

}
