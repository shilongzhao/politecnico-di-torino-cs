//
// Created by zhaos on 10/13/2019.
//
#include "../utils/utils.h"
#include <stdio.h>

void exch(int *a, int p, int q) {
    int t = a[p];
    a[p] = a[q];
    a[q] = t;
}

void insertion_sort(int a[], int n) {
    for (int i = 1; i < n; i++) {
        // move a[i] among a[i-1], a[i-2], ... 
        for (int j = i; j > 0 && a[j] < a[j-1]; j--)
            exch(a, j, j-1);
    }
}

int main() {
    int a[10] = {8, 2, 5, 1, 0, 4, 7, 3, 6, 9};

    insertion_sort(a, 10);
    
    print_array(a, 10);
}