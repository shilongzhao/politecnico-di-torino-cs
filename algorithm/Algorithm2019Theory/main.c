#include <stdio.h>
#include <string.h>

void f(char *s1, char *s2) {
    int i, j, flag;
    i = 0;
    while (i <= strlen(s1) - strlen(s2)) {
        j = 0;
        flag = 1;
        while (j < strlen(s2) && flag == 1) {
            if (*(s1 + i + j) != *(s2 + j)) {
                flag = 0;
            }
            j++;
        }
        if (flag == 1) {
            printf("%d\n", i);
            i += strlen(s2);
        } else {
            i++;
        }
    }
}
//
//typedef struct vertex_t vertex_t;
//typedef struct graph_t graph_t;
//typedef struct edge_t edge_t;
//
//int max_depth = 0;
//int num_tree_edges = 0;
//int num_backwards_edges = 0;
//int num_forward_edges = 0;
//int num_crossing_edges = 0;
//void graph_dfs_r(graph_t *g, vertex_t *n, int currTime, int depth) {
//    if (depth > max_depth) max_depth = depth;
//    edge_t *e;
//    vertex_t *tmp;
//    n->color = GREY;
//    n->disc_time = ++curtTime;
//    
//    e = n->head;
//    while (e != NULL) {
//        tmp = e->dst;
//        if (tmp->color == WHITE) {
//            num_tree_edges += 1;
//            tmp->pred = n;
//            currTime = graph_dfs_r(g, tmp, currTime, depth + 1);
//            
//        }
//        if (tmp->color == GREY) {
//            num_backwards_edges += 1;
//        }
//        if (tmp->color == BLACK) {
//            vertex_t *tp = tmp->pred;
//            int flag = 0;
//            while (tp != null) {
//                tp = tp->pred;
//                if (tp == n) {
//                    flag = 1;
//                    break;
//                }
//            }
//            if (flag == 1) {
//                num_forward_edges += 1;
//            }
//            else {
//                num_crossing_edges += 1;
//            }
//        }
//        
//        e = e->next;
//    }
//    n->color = BLACK;
//    n->endp_time = ++ currTime;
//    
//    return;
//}
//
///**
// * 
// */
//
//typedef struct list_t list_t;
//typedef struct position_t position_t;
//struct position_t {
//    int x; 
//    int y;
//};
//
//position_t next(position_t p, int C) {
//    p.x = p.x + 1;
//    p.y = (p.y + 1) % C;
//    return p;
//}
//
//void part(char **m, int R, int C, int number_subsets, position_t pos, list_t *lists) {
//    if (pos.x == R && pos.y == 0) {
//        for (int i = 1; i < number_subsets; i++) {
//            if (!equals(lists[i], lists[i - 1])) {
//                return;
//            }
//        }
//        for (int i = 0; i < number_subsets; i++) {
//            print_list(lists[i]);
//        }
//    }
//    for (int i = 0; i <number_subsets; i++) {
//        if (size(lists[i]) < R * C / number_subsets) {
//            add(lists[i], pos);
//            part(m, R, C, number_subsets, next(pos, C), lists);
//            remove(lists[i], pos);
//        }
//    }
//}
//void partition(char **m, int R, int C) {
//    int size = R * C; 
//    position_t pos = {0, 0};
//    for (int i = 2; i < size; i++) {
//        if (size % i == 0) {
//            list_t *lists = malloc(i * sizeof(list_t *));
//            part(m, R, C, i, pos, lists);
//        }
//    }
//}
//

int main() { 
    f("ABCDEFABCDEABCDABCABA", "ABC");
    printf("Hello, World!\n");
    return 0;
}