//
// Created by zhaos on 11/5/2019.
//
#include <math.h>
#include <stdio.h>

typedef struct coordinate coordinate;
struct coordinate {
    double x;
    double y;
    double z;
};

double distance(coordinate a, coordinate b) {
    double s = (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z);
    double r = sqrt(s);
    return r;
}

double dist(coordinate *pa, coordinate *pb) {
//    double d1 = (*pa).x - (*pb).x;
    double d1 = pa->x - pb->x;
//    double d2 = (*pa).y - (*pb).y;
    double d2 = pa->y - pb->y;
    double d3 = (*pa).z - (*pb).z;

    return sqrt(d1 * d1 + d2 * d2 + d3 * d3);
}

int main() {
    coordinate p1 = {1.1, 2.2, 3.3};
    coordinate p2 = {3.4, 4.5, 5.6};

    printf("distance between p1 p2 is %.3f\n", distance(p1, p2));

    double dd = dist(&p1, &p2);
    printf("dist between p1 p2 is %.3f\n", dd);
}