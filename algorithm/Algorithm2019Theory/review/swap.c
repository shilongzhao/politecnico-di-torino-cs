//
// Created by zhaos on 10/26/2019.
//
#include <stdio.h>

void swap(int* x, int* y) {
    int t = *x;
    *x = *y;
    *y = t;
}

int main() {
    int a = 1;
    int b = 2;
    swap(&a, &b);
    printf("a = %d, b = %d\n", a, b);
}