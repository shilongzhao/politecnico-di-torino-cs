#include <stdio.h>

//
// Created by zhaos on 10/28/2019.
//
typedef struct student student_t;

struct student {
    int age;
    char gender;
    char *name;
    char *surname;
    float average;
};

struct car {
    char brand[20];
    float price;
    char plate[10];
    int kilometer;
};

struct university {
    char name[50];
    char address[50];
    int number_students;
};

int main() {
    struct car c = {
            .brand = "renault",
            .plate = "TO123BD",
            .price = 12345.67,
            .kilometer = 0
    };

    printf("car brand is %s\n", c.brand);
    printf("car price is %f\n", c.price);

    student_t s = {
            .surname = "Li",
            .name = "XM",
            .age = 22,
            .gender = 'M',
            .average = 28.8
    };

    s.surname = "Wang";

    int a[10] = {56452,3214,2,3};
    student_t all[10];
    all[0].average = 22.22;
    all[0].age = 22;

    printf("a = %p\n", a);
    printf("address of a[0] = %p\n", &a[0]);
    printf("*a = %d\n", *a);

    printf("value of a + 1 = %p\n", a + 1);
    printf("address of a[1] is %p\n", &a[1]);

    printf("dereference a + 1 = %d\n", *(a + 1));
}