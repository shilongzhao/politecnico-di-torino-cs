//
// Created by zhaos on 10/26/2019.
//
#include <stdio.h>
int main() {
    int a = 1;
    printf("value of a is %d\n", a);
    a = 2;
    int b = 3;
    printf("a + b is %d\n", a + b);

    char c = 'M';
    printf("char c is %c\n", c);

    // pointer
    printf("address of a is %p\n", &a);
    printf("address of c is %p\n", &c);

    int x = 6;
    int* y = &a;

    printf("value of y is %p\n", y);
    printf("address of y is %p\n", &y);

    int** p = &y;

    printf("value of p is %p\n", p);
    printf("address of p is %p\n", &p);

    int*** pp = &p;

    // dereference
    printf("value of a is %d\n", a);
    *y = 100;
    printf("value of a is %d\n", a);
}
