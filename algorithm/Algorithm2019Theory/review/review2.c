//
// Created by zhaos on 11/3/2019.
//

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

//float average(int a[], int n) {
// definition array ==> pointer
float average(int *a, int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum = sum + a[i];
    }
    return sum / (float) n;
}

char *toUpper(char *s) {
    for (int i = 0; s[i] != '\0'; i++) {
        s[i] = (char) toupper(s[i]);
    }
    return s;
}

// malloc, allocate memory dynamically
char *toUpperV2(char *s) {
    int n = strlen(s); // s = "abc", n == 3
    char *p = malloc(sizeof(char) * (n + 1)); // abc\0, strlen == 3
    for (int i = 0; i < n; i++) {
        p[i] = (char) toupper(s[i]);
    }
    p[n] = '\0';
    return p;
}

int main() {
    int a = 1;
    char c = 'A';

    int array[10] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 0};

    int *p = &a;
    char *pc = &c;

    *p = 2;
    *pc = 'B';

    int *pa = array;
    int *pa0 = &array[0];

    // pa == pa0
    // pointer == array, situation 1

    for (int i = 0; i < 10; i++) {
        printf("%d\n", array[i]);
    }

    for (int i = 0; i < 10; i++) {
        printf("%d\n", *(pa + i));
    }

    // pointer == array, situation 2
    // in function parameters ==> array => pointer
    float v = average(array, 10);
    printf("average score is %.2f\n", v);

    // pointer != array
    char str[20] = "xyz";
    char *str2 = "abc";

    for (int i = 0; i < 3; i++) {
        printf("char is %c\n", *(str + i)); // str[i] OK
    }

    printf("str = %s\n", str);
    printf("str2 = %s\n", str2);

    str[0] = 'X';
    printf("new str = %s\n", str);

//    str2[0] = 'A';
//    printf("new str2 = %s\n", str2);

    printf("Upper case str = %s\n", toUpper(str));
    char *upper = toUpperV2(str2);
    printf("Upper case str2 = %s\n", upper);
    free(upper);
}