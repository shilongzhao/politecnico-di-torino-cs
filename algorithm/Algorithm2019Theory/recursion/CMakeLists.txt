cmake_minimum_required(VERSION 3.13)
project(Algorithm2019Theory C)

set(CMAKE_C_STANDARD 99)

add_executable(recursion recursion.c)