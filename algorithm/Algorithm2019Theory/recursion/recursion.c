//
// Created by zhaos on 11/17/2019.
//

#include <stdio.h>

// recursion sum of 0 + 1 + 2 + ... + n
int f(int n) {
    if (n == 0) {
        return 0; // f(0) = 0
    }
    int r = f(n - 1) + n; // f(n) = f(n-1) + n
    return r;
}

int fib(int n) {
    if (n == 1) return 1;
    if (n == 2) return 1;
    int r = fib(n - 1) + fib(n - 2);
    return r;
}

int gcd(int a, int b) {
    if (b == 0) return a;
    return gcd(b, a%b);
}

int main() {
    int s = f(5);
    printf("f(5) is %d\n", s);

    printf("fib(10) is %d\n", fib(10));
}

