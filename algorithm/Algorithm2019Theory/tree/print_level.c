//
// Created by ZhaoShilong on 01/09/2020.
//

typedef struct node_s node_t;
void print_level(node_t *root, int level) {
    if (root == null || level == 0) {
        print("%s", root->val);
        return;
    }
    print_level(root->left, level - 1);
    print_level(root->right, level - 1);
}  

int max_depth(node_t *root) {
    if (root == null) return 0;
    int l = max_depth(root->left) + 1;
    int r = max_depth(root->right) + 1;
    if (l > r) return l;
    return r;
}

void heap_print(node_t *root) {
    int N = max_depth(root);
    for (int i = 0; i <= N; i++) {
        print_level(node_t *root, i);
    }
}