//
// Created by zhaos on 11/29/2019.
//
#include <stdlib.h>
#include <stdio.h>

typedef struct node node;

struct node {
    int k;
    node *left;
    node *right;
};

node *insert(node *root, int k) {
    if (root == NULL) {
        node *n = malloc(sizeof(node));
        n->k = k;
        n->left = NULL;
        n->right = NULL;
        return n;
    }
    if (k > root->k) {
        root->right = insert(root->right, k);
    }
    else {
        root->left = insert(root->left, k);
    }
    return root;
}

node *find_max(node *root) {
    if (root->right == NULL)
        return root;
    else
        return find_max(root->right);
}

node *delete_max(node *root) {
    if (root->right == NULL) return root->left;
    root->right = delete_max(root->right);
    return root;
}

node *delete(node *root, int k) {
    if (root == NULL) return NULL;

    if (k < root->k) {
        root->left = delete(root->left, k);
        return root;
    } else if (k > root->k) {
        root->right = delete(root->right, k);
        return root;
    } else {
        if (root->right == NULL) return root->left;
        if (root->left == NULL) return root->right;
        node *t = root;
        root = find_max(root->left);
        root->left = delete_max(t->left);
        root->right = t->right;
        free(t);
        return root;
    }
}

void inorder(node *root) {
    if (root == NULL) return;

    inorder(root->left);
    printf("%d ", root->k);
    inorder(root->right);
}

int main() {
    int nums[11] = {8, 4, 15, 1, 3, 6, 10, 18, 14, 11, 0};

    node *root = NULL;
    for (int i = 0; i < 11; i++) {
        root = insert(root, nums[i]);
    }

    printf("\n=== inorder ====\n");
    inorder(root);

    printf("\n=== after deletion 10 =====\n");
    root = delete(root, 10);
    inorder(root);

    printf("\n=== after deletion 14 =====\n");
    root = delete(root, 14);
    inorder(root);

}

