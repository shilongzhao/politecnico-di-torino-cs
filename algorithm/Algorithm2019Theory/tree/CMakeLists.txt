cmake_minimum_required(VERSION 3.13)
project(Algorithm2019Theory C)

set(CMAKE_C_STANDARD 99)

add_executable(binary_tree binary_tree.c)

add_executable(bst bst.c)