//
// Created by zhaos on 11/27/2019.
//

#include <stdio.h>

typedef struct tree_t tree_t;

struct tree_t {
    int k;
    tree_t *left;
    tree_t *right;
};

int numNodes(tree_t *root) {
    if (root == NULL) return 0;

    int a = numNodes(root->left);
    int b = numNodes(root->right);

    return 1 + a + b;
}

void preorder(tree_t *root) {
    if (root == NULL) return;

    printf("%d ",root->k);
    preorder(root->left);
    preorder(root->right);
}

void inorder(tree_t *root) {
    if (root == NULL) return;

    inorder(root->left);
    printf("%d ", root->k);
    inorder(root->right);
}

void postorder(tree_t *root) {
    if (root == NULL) return;

    postorder(root->left);
    postorder(root->right);
    printf("%d ", root->k);
}

int main() {
    tree_t t1 = {1, NULL, NULL};
    tree_t t2 = {2, NULL, NULL};
    tree_t t3 = {3, NULL, NULL};
    tree_t t4 = {4, NULL, NULL};
    tree_t t5 = {5, NULL, NULL};
    tree_t t6 = {6, NULL, NULL};

    t1.left = &t2;
    t1.right = &t3;
    t2.left = &t4;
    t2.right = &t5;
    t3.left = &t6;

    int r = numNodes(&t1);

    printf("number of nodes = %d\n", r);

    printf("\n ========== preorder =========== \n");
    preorder(&t1);

    printf("\n ========== inorder =========== \n");
    inorder(&t1);

    printf("\n ========== postorder =========== \n");
    postorder(&t1);
}

