//
// Created by zhaos on 11/5/2019.
//

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

typedef struct node node;
struct node {
    int value;
    node *next;
};

void print_list(node *head) {
    for (node *p = head; p != NULL; p = p->next) {
        printf("%d\n", p->value);
    }
}
// recursion print from head to tail
void print_list_recur(node *head) {
    if (head == NULL) {
        return;
    }
    printf("%d\n", head->value);
    print_list_recur(head->next);
}
// recursion print from tail to head
void print_reverse(node *head) {
    // TODO?
    // print_reverse()
}

int sum_list(node *h) {
    int sum  = 0;
    for (node *p = h; p != NULL; p = p->next) {
        sum = sum + p->value;
    }
    return sum;
}

int sum_list_recur(node *h) {
    if (h == NULL) {
        return 0;
    }
    int r = h->value + sum_list_recur(h->next);
    return r;
}

int max_value(node *h) {
    int max = INT_MIN;
    for (node *p = h; p != NULL; p = p->next) {
        if (p->value > max) {
            max = p->value;
        }
    }
    return max;
}

int max2(int a, int b) {
    if (a >= b) return a;
    else return b;
}

int max_recur(node *h) {
    if (h == NULL) {
        return INT_MIN;
    }
    int z = h->value;
    int y = max_recur(h->next);
    return max2(z, y);
}

node *max_node(node *h) {
    node *mn = h;
    for (node *p = h; p != NULL; p = p->next) {
        if (p->value > mn->value) {
            mn = p;
        }
    }
    return mn;
}

int main() {
    node n1 = {1, NULL};
    node n2 = {2, NULL};
    node n25 = {5, NULL};
    node n3 = {3, NULL};
    node n4 = {4, NULL};

    n4.next = &n3;
    n3.next = &n2;
    n2.next = &n25;
    n25.next = &n1;

    node head = n4;

    printf("==========================\n");
    print_list_recur(&n4);
    printf("list max = %d\n", max_recur(&n4));
    printf("==========================\n");

    printf("%d\n", head.value); // n4
    node *p = head.next; // n3
    printf("%d\n", p->value);
    node *q = p->next; // n2
    printf("%d\n", q->value);
    node *r = q->next; // n1
    printf("%d\n", r->value);

    node *s = r->next; // NULL


    node *pp = &n4;
    printf("%d\n", pp->value);
    pp = pp->next;
    printf("%d\n", pp->value);
    pp = pp->next;
    printf("%d\n", pp->value);
    pp = pp->next;
    printf("%d\n", pp->value);


    for (node *qq = &n4; qq != NULL; qq = qq->next) {
        printf("%d\n", qq->value);
    }

}