//
// Created by zhaos on 11/7/2019.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct node node;

struct node {
    int value;
    node *next;
};

void print_list(node *head) {
    for (node *p = head; p != NULL; p = p->next) {
        printf("%d\n", p->value);
    }
}

void insert(node **head, int v) {
    node *p = malloc(sizeof(node));
    p->value = v;
    p->next = *head;
    *head = p;
}

void swap1(int a, int b) {}

void swap2(int *a, int *b) {}

int main() {
    int x = 1, y = 2;
    swap1(x, y);
    swap2(&x, &y);

    node *h = NULL;
    insert(&h, 1);
    insert(&h, 2);
    insert(&h, 3);
    insert(&h, 4);
    // h == ?
    print_list(h);
}
