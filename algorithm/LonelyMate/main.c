/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define DEBUG

#define MAX 101


typedef struct  feature_t_{
    char entry[MAX];
    struct feature_t_ *next;
} feature_t;

typedef struct member_t_{
    char name[MAX];
    char surname[MAX];
    char sex;
    feature_t *features;
    struct member_t_ *next;
}member_t;

member_t *load_members(char *filename);
void print_members(member_t *head);
void load_features(member_t *member);
member_t *search_member(member_t *head, char *surname, char *name);
int match_features(member_t *this, member_t *that);



int main(int argc, char **argv)
{
    member_t *member_list = malloc(sizeof(member_t));
    member_list = load_members(argv[1]);

    member_t *tmp;
    for (tmp = member_list; tmp!= NULL; tmp = tmp->next) {
        load_features(tmp);
    }

#ifdef DEBUG
    print_members(member_list);
#endif

    char surname[MAX], name[MAX];
    printf("input surname name: ");
    scanf("%s %s", surname, name);

    member_t *request = search_member(member_list, surname, name);
    if (request == NULL) {
        printf("No such person\n");
        exit(-1);
    }

    member_t *mate = NULL;
    int     max_match = 0;
    for (tmp = member_list; tmp != NULL; tmp = tmp->next) {
        if (tmp->sex == request->sex)
            continue;
        int match = match_features(request, tmp);
        if (match > max_match) {
            max_match = match;
            mate = tmp;
        }
    }

    printf("mate is %s %s \n", mate->surname, mate->name);
    printf("matches = %d\n", max_match);

    return 0;
}

int match_features(member_t *this, member_t *that) {
    feature_t *this_features = this->features;
    feature_t *that_featrues = that->features;
    int match = 0;
    for (feature_t *a = this_features; a != NULL; a = a->next) {
        for (feature_t *b = that_featrues; b != NULL; b = b->next) {
            if (strcmp(b->entry, a->entry) == 0)
                match++;
        }
    }
    return match;
}

member_t *search_member(member_t *head, char *surname, char *name) {
    member_t *mnode;
    for (mnode = head; mnode != NULL; mnode = mnode->next) {
        if (strcmp(surname, mnode->surname) == 0 &&
                strcmp(name, mnode->name) ==0)
            return mnode;
    }
    return NULL;
}

void load_features(member_t *member) {
    char filename[MAX];
    sprintf(filename, "%s_%s.txt", member->surname, member->name);

    FILE *fp = fopen(filename, "r");
    if (fp == NULL) exit(-1);

    feature_t *tmp;
    member->features = NULL;
    char key[MAX], value[MAX];

    while(fscanf(fp, "%s %s", key, value) == 2) {
        tmp = (feature_t *)malloc(sizeof(feature_t));
        sprintf(tmp->entry, "%s %s", key, value);
        tmp->next = member->features;
        member->features = tmp;
    }
    return;
}

member_t *load_members(char *filename){
    FILE *fp;
    char name[MAX],surname[MAX];
    char sex;
    member_t *tmp;
    member_t *head = NULL;

    fp=fopen(filename,"r");
    if(fp==NULL){
        fprintf(stderr,"error opening");
        exit(1);
    }
    while(fscanf(fp,"%s %s %c",surname,name,&sex) == 3){
        tmp=(member_t *)malloc(sizeof(member_t));
        if(tmp==NULL){
            fprintf(stderr,"error allocating");
            exit(1);
        }
        strcpy(tmp->name,name);
        strcpy(tmp->surname,surname);
        tmp->sex=sex;

        tmp->features=NULL;

        tmp->next=head;
        head=tmp;

    }
    return head;

}

void print_members(member_t *head) {
    member_t *node = head;
    while(node != NULL)  {
        printf("%s %s %c\n", node->surname, node->name, node->sex);
        feature_t *fnode;
        for(fnode = node->features; fnode != NULL; fnode = fnode->next) {
           printf("%s\n", fnode->entry);
        }
        node = node->next;
    }
}
