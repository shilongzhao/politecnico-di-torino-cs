package diet;

/**
 * Represents a complete menu.
 * It consist of packaged products and servings of recipes.
 *
 */
public class Menu implements NutritionalElement {
    private String name;
    private Food food;
    double calories;
    double proteins;
    double carbs;
    double fat;
	/**
	 * Menu constructor.
	 * The reference {@code food} of type {@link Food} must be used to
	 * retrieve the information about recipes and products 
	 * 
	 * @param nome unique name of the menu
	 * @param food object containing the information about products and recipes
	 */
	public Menu(String name, Food food){
	    this.name = name;
	    this.food = food;

	}
	
	/**
	 * Adds a given serving size of a recipe.
	 * The recipe is a name of a recipe defined in the {@code food}
	 * argument of the constructor.
	 * 
	 * @param recipe the name of the recipe to be used as ingredient
	 * @param quantity the amount in grams of the recipe to be used
	 */
	public void addRecipe(String recipe, double quantity) {
	    NutritionalElement r = food.getRecipe(recipe);
	    double q = quantity / 100;
	    calories += r.getCalories() * q;
	    proteins += r.getProteins() * q;
	    carbs += r.getCarbs() * q;
        fat += r.getFat() * q;
	}

	/**
	 * Adds a unit of a packaged product.
	 * The product is a name of a product defined in the {@code food}
	 * argument of the constructor.
	 * 
	 * @param product the name of the product to be used as ingredient
	 */
	public void addProduct(String product) {
	    NutritionalElement p = food.getProduct(product);
	    calories += p.getCalories();
	    proteins += p.getProteins();
	    carbs += p.getCarbs();
	    fat += p.getFat();
	}

	public String getName() {
		return name;
	}

	public double getCalories() {
		return calories;
	}

	public double getProteins() {
		return proteins;
	}

	public double getCarbs() {
		return carbs;
	}

	public double getFat() {
		return fat;
	}

	public boolean per100g() {
		// nutritional values are provided for the whole menu.
		return false;
	}
}
