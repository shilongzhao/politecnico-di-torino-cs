package io;

import java.io.*;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class BufferedReaderExample {
    public static void main(String[] args) throws IOException {
        Reader fileReader = new FileReader("/Users/zhaoshilong/IdeaProjects/LearningJava4th/a.txt");
        BufferedReader bufferedReader  = new BufferedReader(fileReader);

        Writer writer = new FileWriter("/Users/zhaoshilong/IdeaProjects/LearningJava4th/c.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(writer);

        String s;
        while((s = bufferedReader.readLine()) != null) {
            bufferedWriter.write(s);
            bufferedWriter.newLine();
        }
        bufferedReader.close();
        bufferedWriter.close();
    }
}
