package io;

import java.io.*;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class SimpleReadWrite {
    public static void main(String[] args) throws IOException {
        Reader reader = null;
        Writer writer = null;
        try {
            reader = new FileReader("/Users/zhaoshilong/IdeaProjects/LearningJava4th/a.txt");
            writer = new FileWriter("/Users/zhaoshilong/IdeaProjects/LearningJava4th/b.txt");
            char c;
            while ((c = (char )reader.read()) != (char) -1) {
                writer.write(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) reader.close();
            if (writer != null) writer.close();
        }
    }
}
