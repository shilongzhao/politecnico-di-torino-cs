package io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class InputOutputStreamExample {
    public static void main(String[] args) throws IOException{
        FileInputStream in = new FileInputStream("/Users/zhaoshilong/IdeaProjects/LearningJava4th/a.txt");
        byte c;

        FileOutputStream out = new FileOutputStream("/Users/zhaoshilong/IdeaProjects/LearningJava4th/d.txt");
        while((c = (byte) in.read()) != (byte)-1) {
            out.write(c);
        }
        in.close();
        out.close();


//        FileReader reader = new FileReader("/Users/zhaoshilong/IdeaProjects/LearningJava4th/a.txt");
//        while ((c = (char) reader.read()) != (char) -1) {
//            System.out.print(c);
//        }
    }
}
