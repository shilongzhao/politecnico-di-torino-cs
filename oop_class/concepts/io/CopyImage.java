package io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class CopyImage {
    public static void main(String[] args) throws IOException {
        FileInputStream in = new FileInputStream(
                "/Users/zhaoshilong/IdeaProjects/LearningJava4th/java_io_classes.png");

        FileOutputStream out = new FileOutputStream("/Users/zhaoshilong/IdeaProjects/LearningJava4th/2.png");
//        byte c;
//        while ((c = (byte) in.read()) != -1) {
//            out.write(c);
//        }
//        in.close();
//        out.close();
        int c;
        while ((c = in.read()) != -1) {
            out.write(c);
        }
        in.close();
        out.close();
    }
}
