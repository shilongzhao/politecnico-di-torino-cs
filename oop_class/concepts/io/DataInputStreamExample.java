package io;

import java.io.*;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class DataInputStreamExample {
    public static void main(String[] args) throws IOException {
        FileOutputStream out = new FileOutputStream(
                "/Users/zhaoshilong/IdeaProjects/LearningJava4th/num.dat");
        DataOutputStream dataOut = new DataOutputStream(out);
        dataOut.writeInt(77);
        dataOut.close();

        FileInputStream in = new FileInputStream(
                "/Users/zhaoshilong/IdeaProjects/LearningJava4th/num.dat");
        DataInputStream dataIn = new DataInputStream(in);
        while (dataIn.available() > 0) {
            int d = dataIn.readInt();
            System.out.println(d);
        }
    }

}
