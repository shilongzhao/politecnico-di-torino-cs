package lambda;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */
public class MethodReferenceRadix {
    public static void main(String[] args) {
        String hexDigits = "0123456789ABCDEF";
        String twentyDigits = "0123456789ABCDEFGPOL";
        Radix radixHex = hexDigits::charAt;

        Radix radixTwenty = new Radix() {
            @Override
            public char convert(int n) {
                return twentyDigits.charAt(n);
            }
        };
        System.out.println(radixHex.convert(15));
        System.out.println(radixTwenty.convert(17));
    }
}

interface Radix {
    char convert(int n);
}
