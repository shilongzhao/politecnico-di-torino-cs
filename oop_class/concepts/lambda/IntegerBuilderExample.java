package lambda;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */
public class IntegerBuilderExample {
    public static void main(String[] args) {
        IntegerBuilder builder = new IntegerBuilder() {
            @Override
            public Integer build(int value) {
                return new Integer(value);
            }
        };
    }
}

interface IntegerBuilder {
    Integer build(int value);
}
