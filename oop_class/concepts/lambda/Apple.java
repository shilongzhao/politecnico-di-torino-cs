package lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */
/// Predicate Example

public class Apple {
    String color;
    int    weight;
    public String getColor() {
        return color;
    }

    public int getWeight() {
        return weight;
    }
    public String toString() {
        return "Apple: " + color + " " + weight;
    }
    public Apple(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }
    public static List<Apple> filterHeavyApple(List<Apple> list) {
        List<Apple> result = new ArrayList<>();
        for (Apple a: list) {
            if (a.weight > 150)
                result.add(a);
        }
        return result;
    }

    public static List<Apple> filterGreenApple(List<Apple> list) {
        List<Apple> result = new ArrayList<>();
        for (Apple a: list) {
            if (a.color.equals("green"))
                result.add(a);
        }
        return result;
    }

    public static List<Apple> filterApple(List<Apple> list, Predicate<Apple> p) {
        List<Apple> result = new ArrayList<>();
        for (Apple a: list) {
            if (p.test(a))
                result.add(a);
        }
        return result;
    }

    public boolean isGreen() {
        return this.color.equals("green");
    }

    public boolean isHeavy() {
        return this.weight > 150;
    }

    public static void main(String[] args) {
        List<Apple> appleList = new ArrayList<>();
        appleList.add(new Apple("green", 160));
        appleList.add(new Apple("red", 129));

//        Predicate<Apple> heavyApple = a -> a.weight > 150;

        Predicate<Apple> greenApple = new Predicate<Apple>() {
            @Override
            public boolean test(Apple apple) {
                return apple.isGreen();
            }
        };

        List<Apple> r = filterApple(appleList, a -> a.weight > 150);
        for (Apple a: r) {
            System.out.println(a.color + " " + a.weight);
        }
        filterApple(appleList, Apple::isGreen);
        filterApple(appleList, a -> a.isGreen());
        filterApple(appleList, new Predicate<Apple>() {
            @Override
            public boolean test(Apple apple) {
                return apple.isGreen();
            }
        });
    }
}
