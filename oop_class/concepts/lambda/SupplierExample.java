package lambda;

import java.util.*;
import java.util.function.Supplier;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */

public class SupplierExample {
    public static <T, S extends Collection<T>, D extends  Collection<T>>
    D transfer(S source, Supplier<D> factory) {
        D result = factory.get();
        for (T element: source) {
            result.add(element);
        }
        return result;
    }

    public static void main(String[] args) {
        List<Apple> listOfApple = new ArrayList<>();
        listOfApple.add(new Apple("green", 130));

        Set<Apple> setOfApple = transfer(listOfApple, new Supplier<Set<Apple>>() {
            @Override
            public Set<Apple> get() {
                return new HashSet<>();
            }
        });
    }

}
