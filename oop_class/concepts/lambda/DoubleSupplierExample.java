package lambda;

import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */
public class DoubleSupplierExample {
    public static void main(String[] args) {
        Supplier<Double> doubleSupplier = Math::random;
        DoubleSupplier supplier = Math::random;

        for (int i = 0; i < 10; i++)
            System.out.println(doubleSupplier.get());
    }
}
