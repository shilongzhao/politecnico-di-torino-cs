package lambda;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */
public class FactoryExample {
    public static Apple getApple() {
        return new Apple("green", 0);
    }

    public static void main(String[] args) {
        Apple a1 = FactoryExample.getApple();
    }
}
