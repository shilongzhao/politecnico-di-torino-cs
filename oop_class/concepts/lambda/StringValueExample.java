package lambda;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */
public class StringValueExample {
    public static void main(String[] args) {
        StringValue v = new StringValue() {
            @Override
            public int apply(String s) {
                return s.length();
            }
        };
        System.out.println(v.apply("hello"));
    }
}

interface StringValue {
    int apply(String s);
}
