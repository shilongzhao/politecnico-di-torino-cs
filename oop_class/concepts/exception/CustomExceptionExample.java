package exception;

import java.io.IOException;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class CustomExceptionExample {
    public static void foo(int i) {
        try {
            if (i == 0) {
                System.out.println("ready to throw exception");
                throw new MyException("My exception");
            }
            else {
                System.out.println("no exception");
            }
        } catch (MyException e) {
            e.printStackTrace();
            System.out.println("Exception caught");
        }
        System.out.println("exception handled");
    }

    public static void bar(int i) throws MyException {
        if (i == 0) {
            System.out.println("bar: ready to throw exception");
            throw new MyException("bar: My exception");
        }
        else {
            System.out.println("bar: no exception");
        }
    }
    public static void main(String[] args) throws MyException {
        foo(0);
        foo(1);
        bar(0);
        bar(1);
    }
}

class MyException extends Exception {
    public MyException(String name) {
        super(name);
    }
}
