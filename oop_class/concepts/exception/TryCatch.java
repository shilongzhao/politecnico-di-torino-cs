package exception;

import java.io.IOException;

/**
 * author: zhaoshilong
 * date: 10/03/2017
 */
public class TryCatch {
    public void getCustomerInfo() {
        try {
            throw new IOException();
            // do something that may cause an Exception
        } catch (java.io.FileNotFoundException ex) {
            System.out.print("FileNotFoundException!");
        } catch (java.io.IOException ex) {
            System.out.print("IOException!");
        } catch (java.lang.Exception ex) {
            System.out.print("Exception!");
        }
    }
    public static void main(String[] args) {
        TryCatch tc = new TryCatch();
        tc.getCustomerInfo();
    }
}
