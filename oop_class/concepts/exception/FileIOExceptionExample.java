package exception;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class FileIOExceptionExample {
    public static void readFile(String name) {
        try {
            FileInputStream f = new FileInputStream(name);
            System.out.println("open file");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("exception caught");
        }
        System.out.println("open file finished");
    }

    public static void main(String[] args) {
        readFile("/Users/zhaoshilong/test.db");
    }
}
