package exception;

/**
 * author: zhaoshilong
 * date: 11/03/2017
 */
public class FinallyBlock {
    public static String output = "";
    public static void foo(int i) {
        try {
            output+="1";
            if (i == 1) throw new Exception();
            output+="2";
        } catch (Exception e) {
            output += "3";
            return; // if no return?
        } finally {
            output += "4";
        }
        output += "5";
    }

    public static void main(String[] args) {
//        foo(0);
        foo(1);
        System.out.println(output);
    }
}
