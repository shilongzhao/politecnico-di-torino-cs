package exception;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class DevideException {
    public static double divide(double a, double b) {
        if (b == 0) {
            throw new RuntimeException("cannot divide by 0");
        }
        return a/b;
    }
    public static void main(String[] args) {
        System.out.println(divide(1.0, 0));
        System.out.println(divide(2.0, 3.0));
    }
}
