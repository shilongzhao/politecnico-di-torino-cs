package generics;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */

public class Student extends Person{
    public Student(String name, int age) {
        super(name, age);
    }
}
