package generics;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class ObjectList {
    Object[] objects = new Object[100];
    int index;
    public void insert(Object o) {
        objects[index++] = o;
    }

    public static void main(String[] args) {
        ObjectList intList = new ObjectList();
        ObjectList strList = new ObjectList();
        intList.insert(2);
        intList.insert("abd");

        int sum = 0;
        for (Object e: intList.objects) {
            int a = (Integer) e;
            sum += a;
        }

        strList.insert("abc");
    }
}
