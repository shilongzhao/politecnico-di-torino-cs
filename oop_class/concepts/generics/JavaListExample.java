package generics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */

public class JavaListExample {
    private static List<String> listString;
    private static List<Integer> listInt;
    private static List<Person> listPerson;
    public static void main(String[] args) {
        listString = new LinkedList<>();
        listString.add("abc");
        listString.add(0, "xyz");
        int r = listString.indexOf("abc");
        System.out.println(r);
        for (String s: listString) {
            System.out.println(s);
        }
    }
}
