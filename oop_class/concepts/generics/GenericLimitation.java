package generics;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class GenericLimitation<E> {
     // no generic type array, why? type erasure
    // E[] elements = new E[100];
    // not in static methods or static
//    private static E e1;
//    public static void foo(E e) {}
}
