package generics;

import java.util.ArrayList;
import java.util.List;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class GenericStore<T extends Vendible> {
    List<T> goods = new ArrayList<>();
    T[] elements = (T[]) new Object[100];
    public GenericStore(){

    }
    public void add(T good) {
        goods.add(good);
    }

    public void printAllGoods() {
        for (T good: goods) {
            System.out.println(good.toString());
        }
    }
    public static  void main(String[] args) {
        GenericStore<Food> foodStore = new GenericStore<>();
        GenericStore<Toy> toyStore = new GenericStore<>();
        Toy t = new Toy("toyCar", 1.2);
        Food f = new Food("bread", 1.3);
        foodStore.add(f);
    }
}
interface Vendible {
    String getName();
    double getPrice();
}
class Food implements Vendible {
    private String name;
    private double price;
    public Food(String name, double Price) {
        this.name = name;
        this.price = price;
    }
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return 0;
    }

    public String toString() {
        return "Food: " + name + " " + price;
    }
}

class Candy extends Food {
    public Candy(String name, double price) {
        super(name, price);
    }
}
class Toy implements Vendible {
    private String name;
    private double price;

    public Toy(String name, double Price) {
        this.name = name;
        this.price = price;
    }
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return 0;
    }

    public String toString() {
        return "Toy: " + name + " " + price;
    }
}

class Pencil implements Vendible {
    private String name;
    private double price;
    public Pencil(String name, double Price) {
        this.name = name;
        this.price = price;
    }
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return 0;
    }

    public String toString() {
        return "Pencil: " + name + " " + price;
    }
}