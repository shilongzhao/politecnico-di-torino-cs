package generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * author: zhaoshilong
 * date: 05/04/2017
 */
public class SortPerson {
    public static void main(String[] args) {
        Person a = new Person("A", 12);
        Person b = new Person("B", 10);
        Person c = new Person("C", 9);
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(b);
        persons.add(a);
        persons.add(c);
        PersonNameComparator comparatorName = new PersonNameComparator();
        Collections.sort(persons, new Comparator<Person>(){
            public int compare(Person a, Person b) {
                return a.age - b.age;
            }
        });

        for (Person p : persons) {
            System.out.println(p);
        }
        Collections.sort(persons, comparatorName);

        for (Person p : persons) {
            System.out.println(p);
        }
    }
}



class PersonNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return o1.name.compareTo(o2.name);
    }
}