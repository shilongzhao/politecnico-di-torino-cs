package generics;

import java.util.List;

/**
 * author: zhaoshilong
 * date: 05/04/2017
 */
// public class SortElements<T extends Comparable<T>> {
public class SortElements<T extends Comparable<? super T>> {

    public void sort(List<T> elements) {
    }

    public static void main(String[] args) {
        SortElements<AA> sortAA = new SortElements<>();
        SortElements<BB> sortBB = new SortElements<BB>();
    }
}

class AA implements Comparable<AA> {
    @Override
    public int compareTo(AA o) {
        return 0;
    }
}

class BB extends AA {

}

class CC extends BB {

}