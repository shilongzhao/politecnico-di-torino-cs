package generics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * author: zhaoshilong
 * date: 05/04/2017
 * run javap java.util.List
 */
public class DataListExample {
    public static void main(String[] args) {
        List<Date> dateList = new ArrayList<>();
        if (dateList instanceof List) {

        }
        // why?
//        if (dateList instanceof List<Date>) {
//
//        }

    }
}
