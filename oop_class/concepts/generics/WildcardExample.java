package generics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * author: zhaoshilong
 * date: 05/04/2017
 */
public class WildcardExample {
    public static void main(String[] args) {
        List<?> aList = new ArrayList<Date>();
        aList = new ArrayList<String>();
        List<String> stringList = new ArrayList<>();

//        stringList = new ArrayList<Date>();
//        Wrong
//        List<Object> objectList = new ArrayList<String>();
        List<? extends Object> upperBoundList = new ArrayList<String>();
        List<? super B> lowerBoundList = new ArrayList<A>();
        lowerBoundList = new ArrayList<B>();
//        lowerBoundList = new ArrayList<C>();
    }
}

class A {

}

class B extends A{

}

class C extends B {

}

