package generics;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class ComparableFruit implements Comparable<ComparableFruit> {
    private String name;
    public ComparableFruit(String name) {
        this.name = name;
    }
    @Override
    public int compareTo(ComparableFruit o) {
        return this.name.compareTo(o.name);
    }
}
