package generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * author: zhaoshilong
 * date: 02/04/2017
 */
public class Person implements Comparable<Person> {
   public String name;
   public Integer age;

   public Person(String name, int age) {
      this.name = name;
      this.age = age;
   }
   public int compareTo(Person o) {
      return age - o.age;
   }

   public String toString() {
      return name + " " + age;
   }

   public static void main(String[] args) {
      Person a = new Person("A", 12);
      Person b = new Person("B", 10);
      Person c = new Person("C", 9);
      ArrayList<Person> persons = new ArrayList<>();
      persons.add(b);
      persons.add(a);
      persons.add(c);
      Collections.sort(persons);
      for (Person p: persons) {
         System.out.println(p);
      }
      sort(persons);
   }

   public static void sort(List<? extends Comparable> listToSort) {

   }
}
