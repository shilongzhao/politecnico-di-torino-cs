package generics;


import java.util.Arrays;

/**
 * author: zhaoshilong
 * date: 2/04/2017
 */
//T can implement Comparable<? super T>, not just Comparable<T>.
public class ExtendsSuperT {
    public static void main() {
        Student[] students = new Student[100];
        // Student does not implement Comparable<Student>, but why this is right?
        // Arrays.sort signature : public static <T extends Comparable<? super T>> void sort(List<T> list)

        Arrays.sort(students);
    }
}
