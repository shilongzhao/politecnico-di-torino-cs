package stream;

import lambda.Apple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * author: zhaoshilong
 * date: 16/04/2017
 */
public class AppleStream {
    public static void main(String[] args) {
        List<Apple> appleList = new ArrayList<>();

        Apple a1 = new Apple("green", 120);
        Apple a2 = new Apple("red", 155);
        Apple a3 = new Apple("green", 180)
        appleList.add(a1);
        appleList.add(a2);
        appleList.add(a3);

        Apple a4 = new Apple("green", 220);
        List<Apple> appleList2 = new ArrayList<>();
        appleList2.add(a4);

        appleList.stream().filter(Apple::isGreen).forEach(System.out::println);
        int totalWeight = appleList.stream().map(Apple::getWeight).reduce(0, (a, b) -> a + b);
        System.out.println(totalWeight);

        Stream.of(a1,a2,a3).filter(a -> a.getWeight() > 200).forEach(System.out::println);

        Stream.of(appleList, appleList2).flatMap(Collection::stream).filter(a -> a.getWeight() > 200).collect(Collectors.toList());
    }
}
