package it.dreamhack.gof.decorator;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class Soy extends CondimentDecorator {
    Beverage beverage;

    public Soy(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return "Soy " + beverage.getDescription();
    }

    @Override
    public double cost() {
        return 0.10 + beverage.cost();
    }
}
