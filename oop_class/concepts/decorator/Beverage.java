package it.dreamhack.gof.decorator;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public abstract class Beverage {
    protected String description = "beverage without name";
    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
