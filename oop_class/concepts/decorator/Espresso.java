package it.dreamhack.gof.decorator;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class Espresso extends Beverage {
    public Espresso() {
        description = "Espresso";
    }
    @Override
    public double cost() {
        return 1.00;
    }
}
