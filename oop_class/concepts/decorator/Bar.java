package it.dreamhack.gof.decorator;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class Bar {
    public static void main(String[] args) {

        Beverage decafWithSoy = new Soy(new Decaf());
        Beverage espressoWithChocolate = new Chocolate(new Espresso());
        Beverage espressoWithDoubleChocolate = new Chocolate(new Chocolate(new Espresso()));
        Beverage espressoWithSoyAndChocolate = new Chocolate(new Soy(new Espresso()));

        System.out.println(decafWithSoy.getDescription() + " " + decafWithSoy.cost());
        System.out.println(espressoWithChocolate.getDescription() + " " + espressoWithChocolate.cost());
        System.out.println(espressoWithDoubleChocolate.getDescription() + " " + espressoWithDoubleChocolate.cost());
        System.out.println(espressoWithSoyAndChocolate.getDescription() + " " + espressoWithSoyAndChocolate.cost());

    }
}
