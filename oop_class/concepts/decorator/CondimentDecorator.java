package it.dreamhack.gof.decorator;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public abstract class CondimentDecorator extends Beverage{
    public abstract String getDescription();
}
