package it.dreamhack.gof.decorator;

/**
 * author: zhaoshilong
 * date: 12/04/2017
 */
public class Chocolate extends CondimentDecorator {
    private Beverage beverage;

    public Chocolate(Beverage beverage) {
        this.beverage = beverage;
    }
    @Override
    public String getDescription() {
        return "Chocolate " + beverage.getDescription();
    }

    @Override
    public double cost() {
        return 0.20 + beverage.cost();
    }
}
