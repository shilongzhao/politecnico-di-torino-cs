# C and Algorithm Exam Answers

The repository contains example codes for
previous c and algorithm exams of Politecnico di Torino.


## Disclaimer


* The questions are retrieved from public website or
third parties

* This repository is intended to help students to better study
computer science and algorithms at Politecnico

* If any file violates any permissions, regulations, laws,
let me know and it will be deleted

* The codes have ALL been tested but they are  not guaranteed to be the right answers

* It's not allowed to use the codes to make any financial profits
