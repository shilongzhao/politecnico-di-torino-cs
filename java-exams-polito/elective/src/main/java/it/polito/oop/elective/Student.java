package it.polito.oop.elective;

public class Student {
    private String id;
    private double averageGrade;

    public Student(String id, double averageGrade) {
        this.id = id;
        this.averageGrade = averageGrade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(double averageGrade) {
        this.averageGrade = averageGrade;
    }
}
