package it.polito.oop.elective;

public class Course {

    private String name;
    private int availablePositions;

    public Course(String name, int availablePositions) {
        this.name = name;
        this.availablePositions = availablePositions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvailablePositions() {
        return availablePositions;
    }

    public void setAvailablePositions(int availablePositions) {
        this.availablePositions = availablePositions;
    }
}
