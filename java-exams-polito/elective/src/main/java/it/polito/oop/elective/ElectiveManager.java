package it.polito.oop.elective;

import java.util.*;

/**
 * Manages elective courses enrollment.
 * 
 *
 */
public class ElectiveManager {

    private Map<String, Course> courseMap = new HashMap<>();
    private Map<String, Student> studentMap = new HashMap<>();

    private Map<String, List<String>> studentRequestsMap = new HashMap<>();

    private Map<String, List<Long>> courseRequestsMap = new HashMap<>();

    private Map<String, List<String>> assignmentsMap = new HashMap<>();

    private Notifier notifier;
    /**
     * Define a new course offer.
     * A course is characterized by a name and a number of available positions.
     * 
     * @param name : the label for the request type
     * @param availablePositions : the number of available positions
     */
    public void addCourse(String name, int availablePositions) {
        Course course = new Course(name, availablePositions);
        courseMap.put(name, course);

        List<Long> list = new ArrayList<>();
        list.add(0L); list.add(0L); list.add(0L);
        courseRequestsMap.put(name, list);
    }
    
    /**
     * Returns a list of all defined courses
     * @return
     */
    public SortedSet<String> getCourses(){
        TreeSet<String> result = new TreeSet<>();
        Set<String> courseNames = courseMap.keySet();
        result.addAll(courseNames);
        return result;
    }
    
    /**
     * Adds a new student info.
     * 
     * @param id : the id of the student
     * @param gradeAverage : the grade average
     */
    public void loadStudent(String id, double gradeAverage){
        if (studentMap.containsKey(id)) {
            Student s = studentMap.get(id);
            s.setAverageGrade(gradeAverage);
        }
        else {
            Student s = new Student(id, gradeAverage);
            studentMap.put(id, s);
        }
    }

    /**
     * Lists all the students.
     * 
     * @return : list of students ids.
     */
    public Collection<String> getStudents(){
        return studentMap.keySet();
    }
    
    /**
     * Lists all the students with grade average in the interval.
     * 
     * @param inf : lower bound of the interval (inclusive)
     * @param sup : upper bound of the interval (inclusive)
     * @return : list of students ids.
     */
    public Collection<String> getStudents(double inf, double sup){
        List<String> result = new ArrayList<>();
        for (Student s: studentMap.values()) {
            if (s.getAverageGrade() >= inf && s.getAverageGrade() <= sup) {
                result.add(s.getId());
            }
        }
        return result;
    }


    /**
     * Adds a new enrollment request of a student for a set of courses.
     * <p>
     * The request accepts a list of course names listed in order of priority.
     * The first in the list is the preferred one, i.e. the student's first choice.
     * 
     * @param id : the id of the student
     * @param courses : a list of of requested courses, in order of decreasing priority
     * 
     * @return : number of courses the user expressed a preference for
     * 
     * @throws ElectiveException : if the number of selected course is not in [1,3] or the id has not been defined.
     */
    public int requestEnroll(String id, List<String> courses)  throws ElectiveException {
        if (courses.size() == 0 || courses.size() > 3) {
            throw new ElectiveException();
        }
        if (!studentMap.containsKey(id)) {
            throw new ElectiveException();
        }

        for (String c: courses) {
            if (!courseMap.containsKey(c)) {
                throw new ElectiveException();
            }
        }

        studentRequestsMap.put(id, courses);

        for (int i = 0; i < courses.size(); i++) {
            String course = courses.get(i);
            List<Long> requests = courseRequestsMap.get(course);
            Long val = requests.get(i) + 1;
            requests.set(i, val);
        }

        if (notifier != null) {
            notifier.requestReceived(id);
        }
        return courses.size();
    }
    
    /**
     * Returns the number of students that selected each course.
     * <p>
     * Since each course can be selected as 1st, 2nd, or 3rd choice,
     * the method reports three numbers corresponding to the
     * number of students that selected the course as i-th choice. 
     * <p>
     * In case of a course with no requests at all
     * the method reports three zeros.
     * <p>
     * 
     * @return the map of list of number of requests per course
     */
    public Map<String,List<Long>> numberRequests(){
        return courseRequestsMap;
    }
    
    
    /**
     * Make the definitive class assignments based on the grade averages and preferences.
     * <p>
     * Student with higher grade averages are assigned to first option courses while they fit
     * otherwise they are assigned to second and then third option courses.
     * <p>
     *  
     * @return the number of students that could not be assigned to one of the selected courses.
     */
    public long makeClasses() {
        List<Student> students = new ArrayList<>(studentMap.values());
        students.sort((s1, s2) -> {
            if (s1.getAverageGrade() < s2.getAverageGrade()) {
                return 1;
            } else if (s1.getAverageGrade() > s2.getAverageGrade()) {
                return -1;
            } else {
                return 0;
            }
        });

        int numUnassignedStudents = 0;
        for (Student s: students) {
            List<String> requests = studentRequestsMap.get(s.getId());

            if (requests == null) {
                numUnassignedStudents += 1;
                continue;
            }

            boolean studentAssigned = false;

            for (String courseName: requests) {
                Course course = courseMap.get(courseName);
                if (course.getAvailablePositions() > 0) {

                    if (assignmentsMap.containsKey(courseName)) {
                        List<String> assignments = assignmentsMap.get(courseName);
                        assignments.add(s.getId());
                    } else {
                        List<String> assignments = new ArrayList<>();
                        assignments.add(s.getId());
                        assignmentsMap.put(courseName, assignments);
                    }

                    if (notifier != null) {
                        notifier.assignedToCourse(s.getId(), courseName);
                    }
                    int available = course.getAvailablePositions() - 1;
                    course.setAvailablePositions(available);
                    studentAssigned = true;
                    break;
                }
            }

            if (!studentAssigned) {
                numUnassignedStudents += 1;
            }
        }

        return numUnassignedStudents;
    }
    
    
    /**
     * Returns the students assigned to each course.
     * 
     * @return the map course name vs. student id list.
     */
    public Map<String,List<String>> getAssignments(){
        return assignmentsMap;
    }
    
    
    /**
     * Adds a new notification listener for the announcements
     * issues by this course manager.
     * 
     * @param listener : the new notification listener
     */
    public void addNotifier(Notifier listener) {
        this.notifier = listener;
    }
    
    /**
     * Computes the success rate w.r.t. to first 
     * (second, third) choice.
     * 
     * @param choice : the number of choice to consider.
     * @return the success rate (number between 0.0 and 1.0)
     */
    public double successRate(int choice){
        return -1;
    }

    
    /**
     * Returns the students not assigned to any course.
     * 
     * @return the student id list.
     */
    public List<String> getNotAssigned(){
        return null;
    }
    
    
}
