package it.polito.oop.milliways;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Race {
	private String name;
	private Set<String> requirements = new TreeSet<>();

	public Race(String name) {
		this.name = name;
	
	}
	public void addRequirement(String requirement) throws MilliwaysException {
	
		if(requirements.contains(requirement)) throw new MilliwaysException();
		requirements.add(requirement);
	}

	public List<String> getRequirements() {
        return requirements.stream().collect(Collectors.toList());
	}
	
	public String getName() {
        return name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((requirements == null) ? 0 : requirements.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Race other = (Race) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (requirements == null) {
			if (other.requirements != null)
				return false;
		} else if (!requirements.equals(other.requirements))
			return false;
		return true;
	}
	
	
}
