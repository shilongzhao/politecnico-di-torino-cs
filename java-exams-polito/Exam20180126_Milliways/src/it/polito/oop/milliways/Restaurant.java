package it.polito.oop.milliways;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Restaurant {
	
	private HashMap<String,Race> races = new HashMap<>();
	private List<Party> parties = new ArrayList<>();
	private List<Hall> hallslist = new ArrayList<>();
	private HashMap<Integer, Hall> halls = new HashMap<>();
	
	private HashMap<Party,Hall> seats = new HashMap<>();
    public Restaurant() {
    	
	}
	
	public Race defineRace(String name) throws MilliwaysException{
		if(races.containsKey(name)) throw new MilliwaysException();
		Race r = new Race(name);
	    races.put(name,r);
	    return r;
	}
	
	public Party createParty() {
		Party p = new Party();
		parties.add(p);
	    return p;
	}
	
	public Hall defineHall(int id) throws MilliwaysException{
		if(halls.containsKey(id)) throw new MilliwaysException();
		Hall h = new Hall(id);
		halls.put(id, h);
		hallslist.add(h);
	    return h;
	}

	public List<Hall> getHallList() {	
		return hallslist;
	}

	public Hall seat(Party party, Hall hall) throws MilliwaysException {
		if(!hall.isSuitable(party))	throw new MilliwaysException();
        hall.addSeat(party);
        return hall;
	}

	public Hall seat(Party party) throws MilliwaysException {
		for(Hall h : hallslist)
        	if(h.isSuitable(party)){
        		h.addSeat(party);
        		return h;
        	}
        throw new MilliwaysException();
	}

	public Map<Race, Integer> statComposition() {
    	Map<Race, Integer> stats = new HashMap<>();
    	parties.forEach(party -> {
    		party.getCompanions().forEach((race,num) -> {
    			if (stats.containsKey(race)) {
    				num += stats.get(race);
				}
				stats.put(race, num);
			});
		});
    	return stats;
	}

	public List<String> statFacility() {
        return null;
	}
	
	public Map<Integer,List<Integer>> statHalls() {
        return null;
	}

}
