package it.polito.oop.milliways;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hall {
	private int id;
	private List<String> faci = new ArrayList<>();
	private List<Party> seats = new ArrayList<>();

	/**
	 * @param id
	 */
	public Hall(int id) {
		
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void addFacility(String facility) throws MilliwaysException {
		if(faci.contains(facility)) throw new MilliwaysException();
		faci.add(facility);
	}

	public List<String> getFacilities() {
        return faci.stream()
        		.sorted().collect(Collectors.toList());
	}
	
	int getNumFacilities(){
		
        return faci.size();
	}

	public boolean isSuitable(Party party) {
		for(String s : party.getRequirements())
			if(!faci.contains(s)){
				return false;
			}
		return true;
	}
	public void addSeat(Party p){
		seats.add(p);
	}
	@Override
	public String toString() {
		return "Hall [id=" + id + "]";
	}
	

}
