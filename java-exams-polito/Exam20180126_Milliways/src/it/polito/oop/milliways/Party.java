package it.polito.oop.milliways;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Party {
	private Map<Race,Integer> companions = new HashMap<>();
	
	public Party() {
	}

	public void addCompanions(Race race, int num) {
		if (companions.containsKey(race)) {
			num += companions.get(race);
		}
    	companions.put(race, num);
	}

	public int getNum() {
		int num =0;
		for(int a :companions.values()){
			num = num + a;
		}
        return num;
	}

	public int getNum(Race race) {  
	    return companions.get(race);
	}

	public List<String> getRequirements() {
      return companions.keySet().stream()
    		  .map(Race::getRequirements)
    		  .flatMap(l -> l.stream())
    		  .distinct().sorted().collect(Collectors.toList());	
	}

    public Map<String,Integer> getDescription(){
    	Map<String, Integer> description = new HashMap<>();
    	companions.forEach((r, v) -> description.put(r.getName(), v));
    	return description;
    }

    public Map<Race, Integer> getCompanions() {
		return companions;
	}

}
