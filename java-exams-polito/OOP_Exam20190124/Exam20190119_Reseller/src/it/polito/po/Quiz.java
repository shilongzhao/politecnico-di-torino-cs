package it.polito.po;

public class Quiz {
	final static public String[] questions = {
	"Quali di queste affermazioni sono valide? / Which among the following statements are correct?",
	"Quali di queste affermazioni sono valide per una espressione lambda? / Which among the following statements are correct for a lambda expression?",
	"Quali di queste affermazioni sono valide per un comparatore? / Which among the following statements are correct for a comparator?"
	};
	final static public String[][] options = {
	{
		"Mainline sta a versioni di sistema come codeline sta a versioni di componente. / Mainline stands to system versions as codeline stands to component versions",
		"La proprietà collettiva e' un obiettivo della programmazione estrema. / Collective ownership is an extreme programming goal",
		"Il pair programming puo' facilitare il training on the job dei neoassunti. / Pair programming may ease training on the job for new employees",
		"Il CMM ha l'obiettivo di rivoluzionare il processo di sviluppo del software. / CMM has the goal of revolutionizing software development",
		"Nella programmazione estrema il refactoring si riferisce alla pianificazione incrementale dello sviluppo del software. / In extreme programming, refactoring refers to the incremental planning of software development"	},
	{
		"Una lambda implementa solitamente i metodi di default / A lambda usually implements defaults methods",
		"Una lambda implementa una interfaccia funzionale / A lambda implements a functional interface",
		"Una lambda puo' implementare una qualunque interfaccia / A lambda can implement any interface",
		"Una lambda restituisce un oggetto / A lambda returns an object",
		"Una lambda implementa sempre una classe astratta / A lambda implements always an abstract class"	},
	{
		"Un comparatore e' un oggetto che implementa l'interfaccia Comparator<T> / A comparator is an object implementing interface Comparator<T>",
		"Un comparatore deve contenere solo metodi statici / A comparator must contain static methods only",
		"Un comparatore si puo' passare al costruttore di un HashSet / A comparator can be passed as an argument to the constructor of HashSet",
		"Un comparatore contiene dei metodi astratti / A comparator contains abstract methods",
		"Un comparatore implementa il metodo compareTo() / A comparator implements method compareTo()"	}
	};
	
	/**
	 * Return the index of the right option(s) for the given question 
	 * If no option is correct return an empty array
	 */
	public static int[] answer(int question){
		// TODO: answer the question
		
		switch(question){
			case 0: return null; // replace with your answers
			case 1: return null; // replace with your answers
			case 2: return null; // replace with your answers
		}
		return null; // means: "No answer"
	}

	/**
	 * When executed will show the answers you selected
	 */
	public static void main(String[] args){
		for(int q=0; q<questions.length; ++q){
			System.out.println("Question: " + questions[q]);
			int[] a = answer(q);
			if(a==null || a.length==0){
				System.out.println("<undefined>");
				continue;
			}
			System.out.println("Answer" + (a.length>1?"s":"") + ":" );
			for(int i=0; i<a.length; ++i){
				System.out.println(a[i] + " - " + options[q][a[i]]);
			}
		}
	}
}

