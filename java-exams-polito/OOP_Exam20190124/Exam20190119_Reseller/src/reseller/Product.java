package reseller;

import java.util.ArrayList;
import java.util.List;

public class Product {
    private int initialLevel;
    private String name;
    private int stock;
    private List<Supplier> suppliers = new ArrayList<>();

    private List<CustomerOrder> customerOrders = new ArrayList<>();
    private List<SupplierOrder> supplierOrders = new ArrayList<>();

    public Product(String name, int initialLevel) {
        this.name = name;
        this.initialLevel = initialLevel;
        this.stock = initialLevel;
    }

    public String getName() {
        return name;
    }

    public int getStock() {
        return stock;
    }

    public int getInitialLevel() {
        return initialLevel;
    }

    public List<Supplier> getSuppliers() {
        return suppliers;
    }

    public List<CustomerOrder> getCustomerOrders() {
        return customerOrders;
    }

    public List<SupplierOrder> getSupplierOrders() {
        return supplierOrders;
    }

    public void setStock(int i) {
        this.stock = i;
    }


}
