package reseller;

public class SupplierOrder {
    private String supplierName;
    private String productName;
    private int    amount;
    private boolean fulfilled;

    public SupplierOrder(String supplierName, String productName, int amount) {
        this.supplierName = supplierName;
        this.productName = productName;
        this.amount = amount;
        this.fulfilled = false;
    }

    public int getAmount() {
        return amount;
    }

    public boolean isFulfilled() {
        return fulfilled;
    }

    public String getSupplierName() {
        return supplierName;
    }

    @Override
    public String toString() {
        return String.format("%s,%d,%s", supplierName, amount, fulfilled? "f":"p");
    }

    public void setFulfilled(boolean b) {
        this.fulfilled = b;
    }
}
