package reseller;

import java.util.*;

public class Reseller {
    private Map<String, Product> productMap = new HashMap<>();
    private Map<String, Supplier> supplierMap = new HashMap<>();
    //R1
    public void setStock(int initialLevel, String... productNames) {
        for (String prodName: productNames) {
            Product p = new Product(prodName, initialLevel);
            productMap.put(prodName, p);
        }
    }

    public Integer getStock(String productName) {
        if (productMap.containsKey(productName)) {
            return productMap.get(productName).getStock();
        }
        return null;
    }

    public void addSupplier(String supplierName, String... productNames) {
        Supplier s = new Supplier(supplierName);
        for (String productName: productNames) {
            Product p = productMap.get(productName);
            s.getProducts().add(p);
            p.getSuppliers().add(s);
        }
        supplierMap.put(supplierName, s);
    }

    public List<String> getProductsWithoutSuppliers() {
        List<Product> allProducts = new ArrayList<>(productMap.values());
        List<String> productsWithoutSuppliers = new ArrayList<>();
        for (Product p: allProducts) {
            if (p.getSuppliers().isEmpty()) {
                productsWithoutSuppliers.add(p.getName());
            }
        }
        productsWithoutSuppliers.sort((o1, o2) -> o1.compareTo(o2));
        return productsWithoutSuppliers;
    }

    public List<String> getSupplierNames(String productName) {
        Product p = productMap.get(productName);
        List<String> supplierNames = new ArrayList<>();
        for (Supplier s: p.getSuppliers()) {
            supplierNames.add(s.getName());
        }
        // Method Reference
        supplierNames.sort((s1, s2) -> s1.compareTo(s2));

        return supplierNames;
    }

    private String getSupplierNameWithMinimumOrder(String productName) {
        Product p = productMap.get(productName);
        List<Supplier> suppliers = p.getSuppliers();
        Map<String, Integer> supplierNumOrderMap = new HashMap<>();
        for (Supplier s: suppliers) {
            supplierNumOrderMap.put(s.getName(), 0);
        }

        List<SupplierOrder> supplierOrders = p.getSupplierOrders();
        for (SupplierOrder o: supplierOrders) {
            int numOrder = supplierNumOrderMap.get(o.getSupplierName());
            supplierNumOrderMap.put(o.getSupplierName(), numOrder + 1);
        }

        List<Map.Entry<String, Integer>> entries = new ArrayList<>(supplierNumOrderMap.entrySet());
        entries.sort((o1, o2) -> {
            if (!o1.getValue().equals(o2.getValue())) {
                return o1.getValue() - o2.getValue();
            }
            else {
                return o1.getKey().compareTo(o2.getKey());
            }
        });

        return entries.get(0).getKey();
    }

    //R3
    public void enterCOrder(String customerName, String productName, int n) {
        CustomerOrder c = new CustomerOrder(customerName, productName, n);
        Product p = productMap.get(productName);
        p.getCustomerOrders().add(c);

        if (getExpectedSupplies(productName) == 0) {
            if (p.getStock() >= n) {
                c.setFulfilled(true);
                p.setStock(p.getStock() - n);
            }
            else {
                String supplierName = getSupplierNameWithMinimumOrder(productName);
                SupplierOrder s = new SupplierOrder(supplierName, productName, p.getInitialLevel() + n);
                p.getSupplierOrders().add(s);
            }
        }
        else {
            if (getCustomerNeeds(productName) > p.getStock() + getExpectedSupplies(productName)) {
                String supplierName = getSupplierNameWithMinimumOrder(productName);
                SupplierOrder s = new SupplierOrder(supplierName, productName, p.getInitialLevel() + n);
                p.getSupplierOrders().add(s);
            }
        }
    }

    public int getCustomerNeeds(String productName) {
        Product p = productMap.get(productName);
        List<CustomerOrder> customerOrders = p.getCustomerOrders();
        int total = 0;
        for (CustomerOrder o: customerOrders) {
            if (!o.isFulfilled()) {
                total += o.getAmount();
            }
        }
        return total;
    }

    public int getExpectedSupplies(String productName) {
        Product p = productMap.get(productName);
        List<SupplierOrder> supplierOrders = p.getSupplierOrders();
        int total = 0;
        for (SupplierOrder o: supplierOrders) {
            if (!o.isFulfilled()) {
                total += o.getAmount();
            }
        }
        return total;
    }

    public String getCOrders(String productName) {
        Product p = productMap.get(productName);
        List<CustomerOrder> customerOrders = p.getCustomerOrders();
        if (customerOrders.isEmpty()) {
            return "[]";
        }
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        int i;
        for (i = 0; i < customerOrders.size() - 1; i++) {
            builder.append(customerOrders.get(i).toString());
            builder.append(";");
        }
        builder.append(customerOrders.get(i).toString());
        builder.append("]");

        return builder.toString();
    }

    public String getSOrders(String productName) {
        Product p = productMap.get(productName);
        List<SupplierOrder> supplierOrders = p.getSupplierOrders();
        if (supplierOrders.isEmpty())
            return "[]";

        StringBuilder builder = new StringBuilder();
        builder.append("[");
        int i;
        for (i = 0; i < supplierOrders.size() - 1; i++) {
            builder.append(supplierOrders.get(i).toString());
            builder.append(";");
        }
        builder.append(supplierOrders.get(i).toString());
        builder.append("]");
        return builder.toString();
    }

    public void delivery(String supplierName, String productName) {
        Product p = productMap.get(productName);
        List<SupplierOrder> supplierOrders = p.getSupplierOrders();
        for (SupplierOrder supplierOrder: supplierOrders) {
            if (supplierName.equals(supplierOrder.getSupplierName())) {
                supplierOrder.setFulfilled(true);
                p.setStock(p.getStock() + supplierOrder.getAmount());
            }
        }

        List<CustomerOrder> customerOrders = p.getCustomerOrders();
        for (CustomerOrder c: customerOrders) {
            if (p.getStock() >= c.getAmount() && (!c.isFulfilled())) {
                c.setFulfilled(true);
                p.setStock(p.getStock() - c.getAmount());
            }
        }
    }

    //R4
    public SortedMap<String, Long> nOrdersPerCustomer() {
        List<CustomerOrder> customerOrders = new ArrayList<>();
        Collection<Product> allProducts = productMap.values();
        for (Product p: allProducts) {
            customerOrders.addAll(p.getCustomerOrders());
        }

        SortedMap<String, Long> customerNumOrders = new TreeMap<>();
        for (CustomerOrder o: customerOrders) {
            String customerName = o.getCustomerName();
            if (customerNumOrders.containsKey(customerName)) {
                long numOrder = customerNumOrders.get(customerName);
                customerNumOrders.put(customerName,  numOrder + 1);
            }
            else {
                customerNumOrders.put(customerName, 1L);
            }
        }
        return customerNumOrders;
    }


    public SortedMap<String, Integer> ratioSOrdersCOrdersPerProduct() {
        SortedMap<String, Integer> ratioMap = new TreeMap<>();
        Collection<Product> products = productMap.values();
        for (Product p: products) {
            if (!p.getCustomerOrders().isEmpty()) {
                int ratio = p.getSupplierOrders().size() * 100 / p.getCustomerOrders().size();
                ratioMap.put(p.getName(), ratio);
            }
        }
        return ratioMap;
    }

}
