package reseller;

public class CustomerOrder {
    private String customerName;
    private String productName;
    private int amount;
    private boolean fulfilled;

    public CustomerOrder(String customerName, String productName, int amount) {
        this.customerName = customerName;
        this.productName = productName;
        this.amount = amount;
        this.fulfilled = false;
    }

    public boolean isFulfilled() {
        return fulfilled;
    }

    public int getAmount() {
        return amount;
    }

    public void setFulfilled(boolean b) {
        this.fulfilled = b;
    }
    @Override
    public String toString() {
        String status = "";
        if (fulfilled) {
            status = "f";
        }
        else {
            status = "p";
        }
        return String.format("%s,%d,%s", customerName, amount, status);
    }

    public String getCustomerName() {
        return customerName;
    }
}
