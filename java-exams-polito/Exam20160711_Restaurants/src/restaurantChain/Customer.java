package restaurantChain;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	private String name;
	private int persons;
	List<Menu> orders = new ArrayList<>();
	private boolean paid = false;
	
	public Customer(String name, int persons) {
		this.name = name;
		this.persons = persons;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPersons() {
		return persons;
	}
	public void setPersons(int persons) {
		this.persons = persons;
	}
	public List<Menu> getOrders() {
		return orders;
	}
	public void setOrders(List<Menu> orders) {
		this.orders = orders;
	}
	public boolean isPaid() {
		return paid;
	}
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	
}
