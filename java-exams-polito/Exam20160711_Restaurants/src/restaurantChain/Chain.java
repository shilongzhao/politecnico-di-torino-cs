package restaurantChain;

import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Chain {	
		private Map<String, Restaurant> restaurants = new Hashtable<>();

		public void addRestaurant(String name, int tables) throws InvalidName{
			if (restaurants.containsKey(name)) {
				throw new InvalidName();
			}
			restaurants.put(name, new Restaurant(name, tables));
		}
		
		public Restaurant getRestaurant(String name) throws InvalidName{
			if (!restaurants.containsKey(name)) {
				throw new InvalidName();
			}
			return restaurants.get(name);
		}
		
		public List<Restaurant> sortByIncome(){
			return restaurants.values().stream().sorted(new Comparator<Restaurant>() {
				@Override
				public int compare(Restaurant o1, Restaurant o2) {
					if (o2.getIncome() > o1.getIncome()) {
						return 1;
					} else if (o2.getIncome() < o1.getIncome()) {
						return -1;
					}
					else {
						return 0;
					}
				}
			}).collect(Collectors.toList()) ;
		}
		
		
		public List<Restaurant> sortByRefused(){
			return restaurants.values().stream().sorted((r1, r2) -> r1.getRefused() - r2.getRefused()).collect(Collectors.toList());
		}
		
		public List<Restaurant> sortByUnusedTables(){
			return restaurants.values().stream().sorted((r1, r2) -> r1.getUnusedTables() - r2.getUnusedTables())
					.collect(Collectors.toList());
		}
}
