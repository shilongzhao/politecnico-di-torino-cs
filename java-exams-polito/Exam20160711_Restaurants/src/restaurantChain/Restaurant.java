package restaurantChain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Restaurant {
	private String name;
	private int tables;
	public int getTables() {
		return tables;
	}

	public void setTables(int tables) {
		this.tables = tables;
	}

	private int unusedTables;
	private int refused;
	private double totalIncome = 0.0;
	
	private Map<String, Menu> menus = new HashMap<>();
	private Map<String, Customer> customers = new HashMap<>();
//	private Map<String, List<Menu>> orders = new HashMap<>();
//	private Map<String, Boolean> payments = new HashMap<>();
	public Restaurant(String name, int tables) {
		super();
		this.name = name;
		this.tables = tables;
		this.unusedTables = tables;
		this.refused = 0;
	}

	public String getName(){
		return name;
	}
	
	public void addMenu(String name, double price) throws InvalidName{
		if (menus.containsKey(name)) {
			throw new InvalidName();
		}
		menus.put(name, new Menu(name, price));
	}
	
	public int reserve(String name, int persons) throws InvalidName{
		if (customers.containsKey(name)) {
			throw new InvalidName();
		}
		int requiredTables = (int) Math.ceil(persons / 4.0);
		if (unusedTables >= requiredTables) {
			unusedTables -= requiredTables;
			customers.put(name, new Customer(name, persons));
			return requiredTables;
		}
		refused += persons;
		return 0;
		
	}
	
	public int getRefused(){
		return refused;
	}
	
	public int getUnusedTables(){
		return unusedTables;
	}
	
	public boolean order(String name, String... menu) throws InvalidName{
		if (!customers.containsKey(name)) {
			throw new InvalidName();
		}
		for (String m: menu) {
			if (!menus.containsKey(m)) {
				throw new InvalidName();
			}
		}
		Customer c = customers.get(name);
		if (menu.length < c.getPersons()) {
			return false;
		}
		
		List<Menu> menuList = new ArrayList<>();
		for (String m: menu) {
			menuList.add(menus.get(m));
		}
//		c.getOrders().addAll(menuList);
		c.setOrders(menuList);
//		orders.put(name, menuList);
		
		return true;
	}
	
	public List<String> getUnordered(){
//		return customers.values().stream()
//				.filter(c -> c.getOrders().size() == 0)
//				.map(c -> c.getName())
//				.sorted((s1, s2) -> s1.compareTo(s2))
//				.collect(Collectors.toList());
		List<String> result = new ArrayList<>();
		for (Customer c: customers.values()) {
			if (c.getOrders().size() == 0) {
				result.add(c.getName());
			}
		}
		Collections.sort(result, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		return result;
	}
	
	public double pay(String name) throws InvalidName{
		if (! customers.containsKey(name)) {
			throw new InvalidName();
		}
		Customer c = customers.get(name);
		double sum = c.getOrders().stream()
				.map(m -> m.getPrice())
				.reduce(0.0, (a, b) -> a + b);
		
		c.setPaid(true);
		totalIncome += sum;
		return sum;
				
	}
	
	public List<String> getUnpaid(){
		return customers.values().stream().filter(c -> !c.isPaid()).map(c -> c.getName())
				.sorted((s1, s2) -> s1.compareTo(s2)).collect(Collectors.toList());
	}
	
	public double getIncome(){
		return  totalIncome;
	}

}
