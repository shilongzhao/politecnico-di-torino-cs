package iterable;

import java.util.Iterator;

/**
 * author: zhaoshilong
 * date: 28/04/2017
 */
public class MyList implements Iterable<Integer> {
    int[] nums = {1,2,3,4,6,7};
    int currentIndex = 0;
    @Override
    public Iterator<Integer> iterator() {
        return new MyIterator();
    }
    class MyIterator implements Iterator<Integer> {

        @Override
        public boolean hasNext() {
            return currentIndex < 6;
        }

        @Override
        public Integer next() {
            return nums[currentIndex++];
        }
    }

    public static void main(String[] args) {
        MyList myList = new MyList();
//        for (int i : myList) {
//            System.out.println(i);
//        }
        MyIterator it = (MyIterator) myList.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

    }
}


