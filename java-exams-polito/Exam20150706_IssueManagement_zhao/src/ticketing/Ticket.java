package ticketing;

/**
 * Class representing the ticket linked to an issue or malfunction.
 * 
 * The ticket is characterized by a severity and a state.
 */
public class Ticket {
    private int id;
    private String description;
    private String solution;
    private Severity severity;
    private Component component;
    private User author;
    private State state;
    private User assignee;
    /**
     * Enumeration of possible severity levels for the tickets.
     * 
     * Note: the natural order corresponds to the order of declaration
     */
    public enum Severity { Blocking, Critical, Major, Minor, Cosmetic };
    
    /**
     * Enumeration of the possible valid states for a ticket
     */
    public static enum State { Open, Assigned, Closed }

    public Ticket(int id, User author, Component component, String description, Severity severity) {
        this.id = id;
        this.author = author;
        this.component = component;
        this.description = description;
        this.severity = severity;
        this.state = State.Open;
    }
    public int getId(){
        return id;
    }

    public String getDescription(){
        return description;
    }
    
    public Severity getSeverity() {
        return severity;
    }

    public String getAuthor(){
        return author.getUsername();
    }
    
    public String getComponent(){
        return component.getPath();
    }
    
    public State getState(){
        return state;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User u) {
        this.assignee = u;
        state = State.Assigned;
    }

    public void close(String solution) {
        state = State.Closed;
        this.solution = solution;
        assignee.solve();
    }

    public String getSolutionDescription() throws TicketException {
        if (state != State.Closed) throw new TicketException();
        return solution;
    }
}
