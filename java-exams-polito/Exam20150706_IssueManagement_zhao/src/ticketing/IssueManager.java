package ticketing;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IssueManager {
    Map<String, User> users = new HashMap<>(); // username -> user
    Map<String, Component> components = new HashMap<>(); // full path -> component
    int numTickets = 0;
    List<Ticket> tickets = new ArrayList<>();
    /**
     * Eumeration of valid user classes
     */
    public static enum UserClass {
        /** user able to report an issue and create a corresponding ticket **/
        Reporter, 
        /** user that can be assigned to handle a ticket **/
        Maintainer }
    
    /**
     * Creates a new user
     * 
     * @param username name of the user
     * @param classes user classes
     * @throws TicketException if the username has already been created or if no user class has been specified
     */
    public void createUser(String username, UserClass... classes) throws TicketException {
        if (users.containsKey(username)) throw new TicketException();
        if (classes == null || classes.length == 0) throw new TicketException();
        users.put(username, new User(username, new HashSet<UserClass>(Arrays.asList(classes))));
    }

    /**
     * Creates a new user
     * 
     * @param username name of the user
     * @param classes user classes
     * @throws TicketException if the username has already been created or if no user class has been specified
     */
    public void createUser(String username, Set<UserClass> classes) throws TicketException {
        if (users.containsKey(username)) throw new TicketException();
        if (classes == null || classes.size() == 0) throw new TicketException();
        users.put(username, new User(username, classes));
    }
   
    /**
     * Retrieves the user classes for a given user
     * 
     * @param username name of the user
     * @return the set of user classes the user belongs to
     */
    public Set<UserClass> getUserClasses(String username){
        return users.get(username).getUserClasses();
    }
    
    /**
     * Creates a new component
     * 
     * @param name unique name of the new component
     * @throws TicketException if a component with the same name already exists
     */
    public void defineComponent(String name) throws TicketException {
        String path = "/" + name;
        if (components.containsKey(path)) throw new TicketException();
        Component c = new Component(path);
        components.put(path, c);
    }
    
    /**
     * Creates a new sub-component as a child of an existing parent component
     * 
     * @param name unique name of the new component
     * @param parentPath path of the parent component
     * @throws TicketException if the the parent component does not exist or 
     *                          if a sub-component of the same parent exists with the same name
     */
    public void defineSubComponent(String name, String parentPath) throws TicketException {
        String path = parentPath + "/" + name;
        if (!components.containsKey(parentPath)) throw new TicketException();
        if (components.containsKey(path)) throw new TicketException();
        Component c = new Component(path);
        components.put(path, c);
        components.get(parentPath).addSubcomponent(c);
    }
    
    /**
     * Retrieves the sub-components of an existing component
     * 
     * @param path the path of the parent
     * @return set of children sub-components
     */
    public Set<String> getSubComponents(String path){
        Component c = components.get(path);
        return c.getSubcomponents().stream().map(a -> {
            String[] dirs = a.getPath().split("/");
            return dirs[dirs.length - 1];
        }).collect(Collectors.toSet());
    }

    /**
     * Retrieves the parent component
     * 
     * @param path the path of the parent
     * @return name of the parent
     */
    public String getParentComponent(String path){
        String[] dirs = path.split("/");
        if (dirs.length == 2) return null;
        return dirs[dirs.length - 2];
    }

    /**
     * Opens a new ticket to report an issue/malfunction
     * 
     * @param username name of the reporting user
     * @param componentPath path of the component or sub-component
     * @param description description of the malfunction
     * @param severity severity level
     * 
     * @return unique id of the new ticket
     * 
     * @throws TicketException if the user name is not valid, the path does not correspond to a defined component, 
     *                          or the user does not belong to the Reporter {@link IssueManager.UserClass}.
     */
    public int openTicket(String username, String componentPath, String description, Ticket.Severity severity) throws TicketException {
        if (!users.containsKey(username)) throw new TicketException();
        if (!components.containsKey(componentPath)) throw new TicketException();
        User u = users.get(username);
        if (!u.getUserClasses().contains(UserClass.Reporter)) throw new TicketException();
        Component c = components.get(componentPath);
        Ticket t = new Ticket(++numTickets, u, c, description, severity);
        tickets.add(t);
        return numTickets;
    }
    
    /**
     * Returns a ticket object given its id
     * 
     * @param ticketId id of the tickets
     * @return the corresponding ticket object
     */
    public Ticket getTicket(int ticketId){
        return tickets.get(ticketId - 1);
    }
    
    /**
     * Returns all the existing tickets sorted by severity
     * 
     * @return list of ticket objects
     */
    public List<Ticket> getAllTickets(){
        return tickets.stream().sorted(Comparator.comparing(Ticket::getSeverity)).collect(Collectors.toList());
    }
    
    /**
     * Assign a maintainer to an open ticket
     * 
     * @param ticketId  id of the ticket
     * @param username  name of the maintainer
     * @throws TicketException if the ticket is in state <i>Closed</i>, the ticket id or the username
     *                          are not valid, or the user does not belong to the <i>Maintainer</i> user class
     */
    public void assignTicket(int ticketId, String username) throws TicketException {
        if (ticketId > numTickets || ticketId <= 0) throw new TicketException();
        Ticket t = getTicket(ticketId);
        if (t.getState() == Ticket.State.Closed) throw new TicketException();
        if (!users.containsKey(username)) throw new TicketException();
        User u = users.get(username);
        if (!u.getUserClasses().contains(UserClass.Maintainer)) throw new TicketException();
        t.setAssignee(u);
    }

    /**
     * Closes a ticket
     * 
     * @param ticketId id of the ticket
     * @param description description of how the issue was handled and solved
     * @throws TicketException if the ticket is not in state <i>Assigned</i>
     */
    public void closeTicket(int ticketId, String description) throws TicketException {
        Ticket t = getTicket(ticketId);
        if (t.getState() != Ticket.State.Assigned) throw new TicketException();
        t.close(description);
    }

    /**
     * returns a sorted map (keys sorted in natural order) with the number of  
     * tickets per Severity, considering only the tickets with the specific state.
     *  
     * @param state state of the tickets to be counted, all tickets are counted if <i>null</i>
     * @return a map with the severity and the corresponding count 
     */
    public SortedMap<Ticket.Severity,Long> countBySeverityOfState(Ticket.State state){
        return tickets.stream().filter(t -> {
            if (state == null) return true;
            else return t.getState() == state;
        }).collect(Collectors.groupingBy(Ticket::getSeverity, TreeMap::new, Collectors.counting()));
    }

    /**
     * Find the top maintainers in terms of closed tickets.
     * 
     * The elements are strings formatted as <code>"username:###"</code> where <code>username</code> 
     * is the user name and <code>###</code> is the number of closed tickets. 
     * The list is sorter by descending number of closed tickets and then by username.

     * @return A list of strings with the top maintainers.
     */
    public List<String> topMaintainers(){
        return users.values().stream().filter(u -> u.getTicketsSolved() > 0).
                sorted(Comparator.comparing(User::getTicketsSolved)).map(u -> u.getUsername() + ": " + u.getTicketsSolved()).
                collect(Collectors.toList());
    }

}
