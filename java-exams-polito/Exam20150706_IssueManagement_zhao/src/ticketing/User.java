package ticketing;

import java.util.Set;

/**
 * author: zhaoshilong
 * date: 25/04/2017
 */
public class User {
    String username;
    Set<IssueManager.UserClass> userClasses;
    int ticketsSolved = 0;
    public User(String username, Set<IssueManager.UserClass> set) {
        this.userClasses = set;
        this.username = username;
    }
    public String getUsername() {
        return username;
    }
    public Set<IssueManager.UserClass> getUserClasses() {
        return userClasses;
    }
    public void solve() {
        ticketsSolved++;
    }
    public int getTicketsSolved() {
        return ticketsSolved;
    }
}
