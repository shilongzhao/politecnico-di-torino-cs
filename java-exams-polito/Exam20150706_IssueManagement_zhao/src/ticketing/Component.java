package ticketing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * author: zhaoshilong
 * date: 25/04/2017
 */
public class Component {
    private String path;
    private Set<Component> subcomponents = new HashSet<>();
    public Component(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
    public void addSubcomponent(Component c) {
        subcomponents.add(c);
    }
    public Set<Component> getSubcomponents(){
        return subcomponents;
    }
}
