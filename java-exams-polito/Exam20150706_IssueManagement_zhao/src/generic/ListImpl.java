package generic;

import java.util.ArrayList;

/**
 * author: zhaoshilong
 * date: 28/04/2017
 */
public class ListImpl<T> implements AList<T> {
    Object[] objs = new Object[1000];
    @Override
    public void add(T o) {

    }

    @Override
    public T get(int index) {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public void delete(int index) {

    }

    public static void main(String[] args) {
        ListImpl<Teacher> l = new ListImpl<>();
        l.add(new Teacher());
        ListImpl<Student> l2 = new ListImpl<>();
        l2.add(new Student());
        ListImpl<String> l3 = new ListImpl<>();
        l3.add("addd");
    }


}
class Student {
    String getStudentId() {
        return "";
    }
}
class Teacher {
    String getTeacherId() {
        return "...";
    }
}
