package generic;

/**
 * author: zhaoshilong
 * date: 28/04/2017
 */
public interface AList<T> {
    void add(T o);
    T get(int index);
    int  size();
    void delete(int index);
}
