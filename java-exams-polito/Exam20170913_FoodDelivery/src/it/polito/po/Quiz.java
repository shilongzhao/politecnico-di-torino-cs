package it.polito.po;

public class Quiz {
	final static public String[] questions = {
	"Che operazione crea una copia locale di un repository SVN? What operation makes a local copy of an SVN repository?",
	"Per cosa NON sono adatti i metodi tradizionali (non agili)? / What traditional (non-agile) methods are NOT suitable for?",
	"A cosa serve l'operatore freccia '-->' ? / What is the purpose of the arrow operator '->'?"
	};
	final static public String[][] options = {
	{
		"Delete",
		"Update",
		"Lock",
		"Merge",
		"Check-out"	},
	{
		"La facilita' di test",
		"Il costo dello sviluppo",
		"L'affidabilita' del software",
		"La stabilita' del design",
		"La variabilita' dei requisiti"	},
	{
		"A indicare una riferimento ad un metodo / To write a method reference",
		"A confrontare due valori differenti / To compare two distinct values",
		"A indicare un'inferenza di tipo per una classe generica / To define a type inference for a generic class",
		"A scrivere una lambda function / To write a lambda function",
		"A dereferenziare un puntatore / To dereference a pointer"	}
	};
	
	/**
	 * Return the index of the right answer(s) for the given question 
	 */
	public static int[] answer(int question){
		// TODO: answer the question
		
		switch(question){
			case 0: return null; // replace with your answers
			case 1: return null; // replace with your answers
			case 2: return null; // replace with your answers
		}
		return null; // means: "No answer"
	}

	/**
	 * When executed will show the answers you selected
	 */
	public static void main(String[] args){
		for(int q=0; q<questions.length; ++q){
			System.out.println("Question: " + questions[q]);
			int[] a = answer(q);
			if(a==null || a.length==0){
				System.out.println("<undefined>");
				continue;
			}
			System.out.println("Answer" + (a.length>1?"s":"") + ":" );
			for(int i=0; i<a.length; ++i){
				System.out.println(a[i] + " - " + options[q][a[i]]);
			}
		}
	}
}

