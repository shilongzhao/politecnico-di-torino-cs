package delivery;

public class Customer {
	private String address;
	private String name;
	private String email;
	private String phone;
	private int cusID = 0;
	public Customer(String name,String address,String email,String phone){
		this.address= address;
		this.name=name;
		this.email=email;
		this.phone = phone;
	}

	public int getCusID() {
		return cusID;
	}

	public void setCusID(int cusID) {
		this.cusID = cusID;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return name +","+address+","+phone+","+email ;
	}
	

}
