package delivery;

/**
 * @author S219486
 *
 */
public class Item {
	private Double price;
	private String description;
	private String category;
	private int prepTime;
	public Item (String description, double price, String category, int prepTime){
		this.price = price;
		this.description = description;
		this.category = category;
		this.prepTime = prepTime;
		
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getPrepTime() {
		return prepTime;
	}
	public void setPrepTime(int prepTime) {
		this.prepTime = prepTime;
	}
	@Override
	public String toString() {
		return  "["+ category +"]" + description +":" + String.format("%.2f", price);
	}
	
	
	
}
