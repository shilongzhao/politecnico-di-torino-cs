package fit;

import javax.swing.text.html.parser.Entity;
import java.util.*;


public class Fit {
    
    public static int MONDAY    = 1;
    public static int TUESDAY   = 2;
    public static int WEDNESDAY = 3;
    public static int THURSDAY  = 4;
    public static int FRIDAY    = 5;
    public static int SATURDAY  = 6;
    public static int SUNDAY    = 7;

    private int numCustomers = 0;
    private Map<String, Gym> gymMap = new HashMap<>();
	private Map<Integer, Customer> customerMap = new HashMap<>();
	private Map<String, Lesson> timeTable = new HashMap<>();

	public Fit() {
	
	}
	// R1 
	
	public void addGymn (String name) throws FitException {
		if (gymMap.containsKey(name)) {
			throw new FitException();
		}

		Gym g = new Gym(name);
		gymMap.put(name, g);
	}
	
	public int getNumGymns() {
		return gymMap.size();
	}
	
	//R2
	public void addLessons (String gymnName,
	                        String activity, 
	                        int maxAttendees,
	                        String slots, 
	                        String ... allowedInstructors) throws FitException{
	    if (!gymMap.containsKey(gymnName)) {
	    	throw new FitException();
		}

	    String[] dayTimes = slots.split(",");

		for (String dayTime: dayTimes) {
			Slot s = new Slot(dayTime);
			if (s.getDay() < 1 || s.getDay() > 7) throw new FitException();
			if (s.getHour() < 8 || s.getHour() > 20 ) throw new FitException();

			String key = String.format("%s.%s", gymnName, dayTime);
			if (timeTable.containsKey(key)) {
				throw new FitException();
			}

			Lesson lesson = new Lesson(gymnName, dayTime, activity, maxAttendees, Arrays.asList(allowedInstructors));
			timeTable.put(key, lesson);
		}
	}
	
	//R3
	public int addCustomer(String name) {
		numCustomers += 1;
		Customer c = new Customer(numCustomers, name);
		customerMap.put(numCustomers, c);
		return numCustomers;
	}
	
	public String getCustomer (int customerid) throws FitException{
	    if (!customerMap.containsKey(customerid)) {
	    	throw new FitException();
		}
	    return customerMap.get(customerid).getName();
	}
	
	//R4

	/**
	 * The reservation is successful if:
	 *
	 * the customer code is valid
	 * the name of the gym is valid
	 * the day and the slot are valid
	 * there are still available seats for the lesson
	 * the customer has not already signed up for the lesson.
	 */
	public void placeReservation(int customerId, String gymnName, int day, int slot) throws FitException{
		if (!customerMap.containsKey(customerId)) throw new FitException();

		if (!gymMap.containsKey(gymnName)) throw new FitException();

		if (day < 1 || day > 7 || slot < 8 || slot > 20) throw new FitException();

		String key = String.format("%s.%s.%s", gymnName, day, slot);
		if (!timeTable.containsKey(key)) throw new FitException();

		Lesson lesson = timeTable.get(key);
		if (!lesson.isSeatAvailable()) throw new FitException();

		Customer c = customerMap.get(customerId);

		if (lesson.getCustomers().contains(c)) throw new FitException();

		c.getLessons().add(lesson);

		lesson.getCustomers().add(c);
	}
	
	public int getNumLessons(int customerId) {
		Customer c = customerMap.get(customerId);
		if (c == null) return 0;
		return c.getLessons().size();
	}

	//R5

	/**
	 * The method must throw an exception if:
	 *
	 * The gym does not exist
	 * The day and the slot are not valid
	 * The instructor is not among those associated with the lesson.
	 */
	public void addLessonGiven (String gymnName, int day, int slot, String instructor) throws FitException {
		if (day < 1 || day > 7 || slot < 8 || slot > 20) throw new FitException();
		if (!gymMap.containsKey(gymnName)) throw new FitException();
		String key = String.format("%s.%d.%d", gymnName, day, slot);
		if (!timeTable.containsKey(key)) throw new FitException();
		Lesson l = timeTable.get(key);
		if (!l.getInstructors().contains(instructor)) throw new FitException();
		l.setInstructor(instructor);
	}
	
	public int getNumLessonsGiven (String gymnName, String instructor) throws FitException{
		if (!gymMap.containsKey(gymnName)) throw new FitException();
		return (int) timeTable.entrySet().stream()
				.filter(entry -> entry.getKey().startsWith(gymnName))
				.filter(entry -> instructor.equals(entry.getValue().getInstructor()))
				.count();
	}
	//R6
	
	public String mostActiveGymn() {
		Map<String, Integer> gymNumLessons = new HashMap<>();
		Integer maxLessons = 0;
		String maxGym = null;
		for (Map.Entry<String, Lesson> entry: timeTable.entrySet()) {
			String key = entry.getKey();
			String gym = key.split("\\.")[0];

			int numLessons = gymNumLessons.getOrDefault(gym, 0);
			numLessons += 1;

			gymNumLessons.put(gym, numLessons);

			if (numLessons > maxLessons) {
				maxGym = gym;
				maxLessons = numLessons;
			}
		}
		return maxGym;
	}
	
	public Map<String, Integer> totalLessonsPerGymn() {
		Map<String, Integer> gymNumLessons = new HashMap<>();

		gymMap.forEach((k, v) -> gymNumLessons.put(k, 0));

		for (Map.Entry<String, Lesson> entry: timeTable.entrySet()) {
			String k = entry.getKey();
			String gym = k.split("\\.")[0];
			int numLessons = gymNumLessons.getOrDefault(gym, 0);
			gymNumLessons.put(gym, numLessons + 1);

		}


		return gymNumLessons;
	}
	
	public SortedMap<Integer, List<String>> slotsPerNofParticipants(String gymnName) throws FitException{
		Map<String, Integer> timeSlotNumPart = new HashMap<>();
		for (Map.Entry<String, Lesson> entry: timeTable.entrySet()) {
			String k = entry.getKey();
			if (k.startsWith(gymnName)) {
				Lesson s = timeTable.get(k);
				timeSlotNumPart.put(s.getSlot().toString(), s.getCustomers().size());
			}
		}

		SortedMap<Integer, List<String>> map = new TreeMap<>();
		for (Map.Entry<String, Integer> slotPart: timeSlotNumPart.entrySet()) {
			Integer part = slotPart.getValue();
			String slot = slotPart.getKey();
			List<String> slots = map.getOrDefault(part, new ArrayList<>());
			slots.add(slot);
			map.put(part, slots);
		}
		return map;
	}
	

	
	
	
	


}
