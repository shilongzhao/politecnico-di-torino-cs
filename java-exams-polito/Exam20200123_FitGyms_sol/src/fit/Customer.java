package fit;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Customer {
    private int id;
    private String name;

    private Set<Lesson> lessons;

    public Customer(int id, String name) {
        this.id = id;
        this.name = name;
        this.lessons = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id &&
                Objects.equals(name, customer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }
}
