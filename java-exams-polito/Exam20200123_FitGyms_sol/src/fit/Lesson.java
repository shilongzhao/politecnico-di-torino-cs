package fit;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Lesson {
    private String gym;
    private String name;
    private int maxParticipants;
    private Set<Customer> customers;
    private Slot slot;
    private Set<String> instructors;
    private String instructor;
    public Lesson(String gym, String slotStr, String name, int maxParticipants, List<String> instructors) {
        this.gym = gym;
        this.name = name;
        this.maxParticipants = maxParticipants;
        this.slot = new Slot(slotStr);
        this.instructors = new HashSet<>(instructors);
        this.customers = new HashSet<>();
    }

    public void addParticipant(Customer customer) {
        this.customers.add(customer);
    }

    public boolean isSeatAvailable() {
        return this.customers.size() < maxParticipants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return Objects.equals(gym, lesson.gym) &&
                Objects.equals(name, lesson.name) &&
                Objects.equals(slot, lesson.slot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gym, name, slot);
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public Set<String> getInstructors() {
        return instructors;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getInstructor() {
        return instructor;
    }

    public Slot getSlot() {
        return slot;
    }
}
