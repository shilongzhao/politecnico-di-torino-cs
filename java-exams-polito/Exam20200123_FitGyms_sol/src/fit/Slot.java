package fit;

import java.util.Objects;

public class Slot {
    private int day;
    private int hour;

    public Slot(String slot) {
        String[] strings = slot.split("\\."); // escape
        day = Integer.valueOf(strings[0]);
        hour = Integer.valueOf(strings[1]);
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slot slot = (Slot) o;
        return day == slot.day &&
                hour == slot.hour;
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, hour);
    }

    @Override
    public String toString() {
        return String.format("%d.%d", day, hour);
    }
}
