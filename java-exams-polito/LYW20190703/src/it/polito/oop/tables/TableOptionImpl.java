package it.polito.oop.tables;

/**
 * author: zhaoshilong
 * date: 2019-07-04
 */
public class TableOptionImpl implements TableOption {
    private String time;
    private int table;
    private double occupation;

    public TableOptionImpl(String time, int table, double occupation) {
        this.time = time;
        this.table = table;
        this.occupation = occupation;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public int getTable() {
        return table;
    }

    @Override
    public double getOccupation() {
        return occupation;
    }

    @Override
    public String toString() {
        return String.format("%s, %d = %f", time, table, occupation);
    }
}
