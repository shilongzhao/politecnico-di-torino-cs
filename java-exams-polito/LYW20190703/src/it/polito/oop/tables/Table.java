package it.polito.oop.tables;

public class Table {
	private int id;
	private int seat;

	private Client client;
	private String arrivalTime;

	public Table(int id, int seat) {
		this.id = id;
		this.seat = seat;
	}

	public int getId() {
		return id;
	}

	public int getSeat() {
		return seat;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getClient() {
		return client;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}
}
