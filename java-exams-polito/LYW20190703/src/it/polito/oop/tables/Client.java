package it.polito.oop.tables;

public class Client {
	private int id;
	private String name;
	private int count;
	private String mobile;

	public Client(int id, String name, int count, String mobile) {
		this.id = id;
		this.name = name;
		this.count = count;
		this.mobile = mobile;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getCount() {
		return count;
	}

	public String getMobile() {
		return mobile;
	}
}
