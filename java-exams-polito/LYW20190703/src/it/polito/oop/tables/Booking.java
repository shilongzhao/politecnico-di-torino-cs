package it.polito.oop.tables;

/**
 * author: zhaoshilong
 * date: 2019-07-04
 */
public class Booking {
    private int tableId;
    private int clientId;
    private String time;

    public Booking(int tableId, int clientId, String time) {
        this.tableId = tableId;
        this.clientId = clientId;
        this.time = time;
    }

    public int getTableId() {
        return tableId;
    }

    public int getClientId() {
        return clientId;
    }

    public String getTime() {
        return time;
    }
}
