package it.polito.oop.tables;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a restaurant with a set of tables, it allows parties of client
 * to sit and leave tables.
 *
 */
	
public class Restaurant {
	private String openingTime;
	private String closingTime;

	private Map<Integer,Table>	tableMap = new HashMap<>();
	private Map<Integer, Client> clientMap = new HashMap<>();
	private Map<Integer, List<Booking>> tableBookingMap = new HashMap<>();
	private Map<Integer, List<Client>> servedClientMap = new HashMap<>();
	private Map<Integer, List<Occupation>> occupationMap = new HashMap<>();

    public void defineTables(int... seats) {
    	for (int seat: seats) {
    		int id = tableMap.size() + 1;
    		Table t = new Table(id, seat);
    		tableMap.put(id, t);
		}
    }
    
    public int getSeats() {
		return tableMap.values().stream().map(Table::getSeat).reduce(0, Integer::sum);
    }

    public int tableSize(int tableId) throws IllegalArgumentException {
    	if (!tableMap.containsKey(tableId)) {
			throw new IllegalArgumentException("tableId does not exist: " + tableId);
		}
    	return tableMap.get(tableId).getSeat();
    }
    
    public String getOpeningTime() {
        return openingTime;
    }
    
    public void setOpeningTime(String time) {
    	this.openingTime = time;
    }
    
    public String getClosingTime() {
        return closingTime;
    }
    
    public void setClosingTime(String time) {
    	this.closingTime = time;
    }

    public int newParty(String name, int count, String mobile) {
    	int id = clientMap.size() + 1;
		Client client = new Client(id, name, count, mobile);
		clientMap.put(id, client);
		return id;
    }
    
    public String getPartyName(int partyId)	{
    	if(!clientMap.containsKey(partyId)){
    		throw new IllegalArgumentException();
    	}
        return clientMap.get(partyId).getName();
    }
    
    public int getPartySize(int partyId) {
    	if(!clientMap.containsKey(partyId)) {
            throw new IllegalArgumentException();
        }
    	return clientMap.get(partyId).getCount();
    }
    
    public String getPartyPhone(int partyId) {
    	if(!clientMap.containsKey(partyId)){
    		throw new IllegalArgumentException();
    	}
    	return clientMap.get(partyId).getMobile();
    }
    
    public int countParties() {
        return clientMap.size();
    }
    
    public void takeTable(int table, int partyId,String time) {
        Table t = tableMap.get(table);
        if (t == null) {
            throw new IllegalArgumentException();
        }

        Client c = clientMap.get(partyId);
        if (c == null) {
            throw new IllegalArgumentException();
        }

        t.setArrivalTime(time);
        t.setClient(c);
    }
    
    public int leaveTable(int table, String time) {
        Table t = tableMap.get(table);
        if (t == null) throw new IllegalArgumentException();
        Client c = t.getClient();
        t.setClient(null);

        if (servedClientMap.containsKey(table)) {
            servedClientMap.get(table).add(c);
        } else {
            List<Client> clients = new ArrayList<>();
            clients.add(c);
            servedClientMap.put(table, clients);
        }

        Occupation occupation = new Occupation(table, c.getId(), t.getArrivalTime(), time);
        int estimatedMin = 40 + c.getCount() * 5;
        int elapsedMin = minutes(time) - minutes(t.getArrivalTime());
        occupation.setEstimationError(estimatedMin - elapsedMin);
        if (occupationMap.containsKey(table)) {
            occupationMap.get(table).add(occupation);
        } else {
            List<Occupation> occupations = new ArrayList<>();
            occupations.add(occupation);
            occupationMap.put(table, occupations);
        }

        return c.getId();
    }
    private int minutes(String time) {
        String[] times = time.split(":");
        int h = Integer.valueOf(times[0]);
        int m = Integer.valueOf(times[1]);
        return h * 60 + m;
    }

    private String toTime(int minutes) {
        int h = minutes / 60;
        int m = minutes % 60;
        return String.format("%s:%s", h, m);
    }
    
    public String estimateAvailable(int table) {
        Table t = tableMap.get(table);

        if (t == null) throw new IllegalArgumentException();

        Client client;
        String baseTime;
        if (tableBookingMap.containsKey(table)) {
            List<Booking> bookings = tableBookingMap.get(table);
            bookings.sort((b1, b2) -> compareHourMinute(b1.getTime(), b2.getTime()));

            Booking b = bookings.get(bookings.size() - 1);
            client = clientMap.get(b.getClientId());
            baseTime = b.getTime();

        } else {
            client = t.getClient();
            if (client == null) {
                return openingTime;
            }
            baseTime = t.getArrivalTime();
        }

        int minutes = minutes(baseTime);

        return toTime(minutes + 40 + client.getCount() * 5);
    }
    
    public List<TableOption> estimateWaiting(int partySize){
        return tableMap.values().stream().filter(table -> table.getSeat() >= partySize)
                .filter(table -> compareHourMinute(estimateAvailable(table.getId()), closingTime) < 0)
                .map(table -> {
                    return new TableOptionImpl(estimateAvailable(
                            table.getId()), table.getId(), partySize/ (double) table.getSeat() );
                }).sorted((t1, t2) -> {
                    if (t1.getOccupation() != t2.getOccupation()) {
                        return Double.compare(t2.getOccupation(), t1.getOccupation());
                    }
                    return compareHourMinute(t1.getTime(), t2.getTime());
                }).collect(Collectors.toList());
    }
    
    public void bookTable(int table, int partyId, String time) {
        Booking b = new Booking(table, partyId, time);
        if (tableBookingMap.containsKey(table)) {
            List<Booking> bookings = tableBookingMap.get(table);
            bookings.add(b);
        } else {
            List<Booking> bookings = new ArrayList<>();
            bookings.add(b);
            tableBookingMap.put(table, bookings);
        }
    }

    private int compareHourMinute(String t1, String t2) {
        return minutes(t1) - minutes(t2);
    }

    public int countServedCustomers() {
        return servedClientMap.values()
                .stream().flatMap(Collection::stream)
                .map(Client::getCount)
                .reduce(0, Integer::sum);
    }
    
    public int countServedParties() {
        return servedClientMap.values().stream()
                .map(List::size).reduce(0, Integer::sum);
    }

    public Map<Integer,Long> tableTurnover() {
        Map<Integer, Long> turnover = new HashMap<>();
        for (Integer t: tableMap.keySet()) {
            List<Client> servedClients = servedClientMap.get(t);
            turnover.put(t, (long) (servedClients == null? 0: servedClients.size()));
        }
        return turnover;
    }
    
    public int estimationError(int table) {
        if (!occupationMap.containsKey(table)) {
            return 0;
        }

        int total = occupationMap.get(table).stream().map(Occupation::getEstimationError).reduce(0, Integer::sum);
        return total/occupationMap.get(table).size();
    }
}
