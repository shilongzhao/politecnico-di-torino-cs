package it.polito.oop.tables;

/**
 * author: zhaoshilong
 * date: 2019-07-04
 */
public class Occupation {
    private int tableId;
    private int clientId;
    private String arrivalTime;
    private String leavingTime;

    private int estimationError;
    public Occupation(int tableId, int clientId, String arrivalTime, String leavingTime) {
        this.tableId = tableId;
        this.clientId = clientId;
        this.arrivalTime = arrivalTime;
        this.leavingTime = leavingTime;
    }

    public int getTableId() {
        return tableId;
    }

    public int getClientId() {
        return clientId;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public String getLeavingTime() {
        return leavingTime;
    }

    public int getEstimationError() {
        return estimationError;
    }

    public void setEstimationError(int estimationError) {
        this.estimationError = estimationError;
    }
}
