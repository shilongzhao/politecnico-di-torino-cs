package it.polito.po.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polito.oop.tables.Restaurant;

public class TestR3_TableOccupation {
    private static final String OPENING = "19:30";
    private Restaurant resto;
    private int p1;
    private int p2;
    private int p3;
    
    @Before
    public void setUp() {
        resto = new Restaurant();
        resto.defineTables(4,4,2,2,6);
        resto.setOpeningTime(OPENING);

        
        p1 = resto.newParty("Genny", 4, "3334445566");
        p2 = resto.newParty("Remo", 2, "3337778899");
        p3 = resto.newParty("Ugo", 5, "3331112233");
    }

    @Test
    public void testGetLeaveTable() {
        resto.takeTable(1, p1, "19:34");
        
        int p = resto.leaveTable(1, "20:15");
        
        assertEquals(p1,p);
    }
    
    @Test
    public void testEstimate() {
        resto.takeTable(1, p1, "19:34");
        resto.takeTable(4, p2, "20:10");
        resto.takeTable(5, p3, "19:40");
        
        assertEquals("20:34",resto.estimateAvailable(1));    
    }
    
    @Test
    public void testEstimate2() {
        resto.takeTable(4, p2, "20:05");
        
        assertEquals("20:55",resto.estimateAvailable(4));    
    }

    @Test
    public void testEstimate3() {
        
        assertEquals(OPENING,resto.estimateAvailable(1));    
    }
    
    @Test
    public void testEstimate4() {
        resto.takeTable(1, p1, "19:34");
        resto.leaveTable(1, "20:00");
        resto.takeTable(1, p2, "20:05");
       
        
        assertEquals("20:55",resto.estimateAvailable(1));    
    }


}
