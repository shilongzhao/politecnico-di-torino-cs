package it.polito.po.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import it.polito.oop.tables.Restaurant;
import it.polito.oop.tables.TableOption;

public class TestR5_Stats {
    private static final String OPENING = "19:30";
    private Restaurant resto;
    private int p1;
    private int p2;
    private int p3;
    private int p4;
    
    @Before
    public void setUp() {
        resto = new Restaurant();
        resto.defineTables(4,4,2,2,6);
        resto.setOpeningTime(OPENING);
        resto.setClosingTime("23:30");

        
        p1 = resto.newParty("Genny", 4, "3334445566");
        p2 = resto.newParty("Remo", 2, "3337778899");
        p3 = resto.newParty("Ugo", 5, "3331112233");
        
        p4 = resto.newParty("Matteo", 4, "3339876543");

        resto.takeTable(5, p3, "19:40");
        resto.takeTable(1, p1, "20:15");
        resto.takeTable(2, p4, "20:05");
        
        int p = resto.newParty("test", 6, "123456");
        
        resto.bookTable(5, p, "20:45"); 

        resto.leaveTable(5, "20:50");
        resto.takeTable(5, p, "20:55");
        resto.leaveTable(1, "21:30");
        resto.leaveTable(2, "22:00");

    }

    @Test
    public void testServedPeople() {
        assertEquals(13,resto.countServedCustomers());
    }

    @Test
    public void testServedParties() {
        assertEquals(3,resto.countServedParties());
    }

    @Test
    public void testEstimationError() {
        assertEquals(-15,resto.estimationError(1));
    }
    
    @Test
    public void testTurnover() {
        Map<Integer,Long> tom = resto.tableTurnover();
        assertNotNull("Missing turnover",tom);
        assertEquals(5,tom.size());
    }
    
//    @Test
//    public void testTurnover2() {
//        Map<Integer,Long> tom = resto.tableTurnover();
//        assertNotNull("Missing turnover",tom);
//        assertEquals(1L,tom.get(1).longValue());
//        assertEquals(1L,tom.get(2).longValue());
//        assertEquals(0L,tom.get(3).longValue());
//        assertEquals(0L,tom.get(4).longValue());
//        assertEquals(1L,tom.get(5).longValue());
//    }

}
