package it.polito.po.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import it.polito.oop.tables.Restaurant;
import it.polito.oop.tables.TableOption;

public class TestR4_Booking {
    private static final String OPENING = "19:30";
    private Restaurant resto;
    private int p1;
    private int p2;
    private int p3;
    
    @Before
    public void setUp() {
        resto = new Restaurant();
        resto.defineTables(4,4,2,2,8);
        resto.setOpeningTime(OPENING);
        resto.setClosingTime("23:30");

        
        p1 = resto.newParty("Genny", 4, "3334445566");
        p2 = resto.newParty("Remo", 2, "3337778899");
        p3 = resto.newParty("Ugo", 5, "3331112233");
    }

    @Test
    public void testWaitingEstimate() {
        resto.takeTable(5, p3, "19:40");
        
        List<TableOption> options = resto.estimateWaiting(6);
      
        assertNotNull("Estimate not present",options);
        assertEquals("There exactly one table that can host 6 people",1,options.size()); // only one table fits 6 people
        TableOption op = options.get(0);
        assertEquals("Only table 5 can host 6 people",
                        5,op.getTable()); // table 5 will be 
        assertEquals("A party of 5 took table 5 at 19:40; expected occupatio time is 40+5*5=65 ==> 20:45",
                        "20:45",op.getTime()); // available at 20:45
        assertEquals("6 people in a table for 8 means 75% occupation",0.75,op.getOccupation(),0.001); // with 100% occupation
    }

//    @Test
//    public void testWaitingEstimate3() {
//        int p4 = resto.newParty("Matteo", 4, "3339876543");
//
//        resto.takeTable(5, p3, "19:40");
//        resto.takeTable(1, p1, "20:15");
//        resto.takeTable(2, p4, "20:05");
//        
//        List<TableOption> options = resto.estimateWaiting(3);
//      
//        assertNotNull("Estimate not present",options);
//        assertEquals("There are 3 tables that can host 3 people",
//                        3,options.size()); //three tables fit a 4 people party
//        assertEquals("Wrong options order",2,options.get(0).getTable());
//        assertEquals("Wrong options order",1,options.get(1).getTable());
//        assertEquals("Wrong options order",5,options.get(2).getTable());
//    }

    @Test
    public void testWaitingEstimate2() {
        int p4 = resto.newParty("Matteo", 4, "3339876543");

        resto.takeTable(5, p3, "19:40");
        resto.takeTable(1, p1, "20:15");
        resto.takeTable(2, p4, "20:05");
        
        List<TableOption> options = resto.estimateWaiting(3);
      
        assertNotNull("Estimate not present",options);
        assertEquals("There are 3 tables that can host 3 people",
                        3,options.size()); //three tables fit a 4 people party
        Set<Integer> tables = options.stream().map(to -> to.getTable()).collect(Collectors.toSet());
        assertTrue("Missing table 2",tables.contains(2));
        assertTrue("Missing table 1",tables.contains(1));
        assertTrue("Missing table 5",tables.contains(5));
    }
    @Test
    public void testAvailableEstimateBooking() {
        resto.takeTable(5, p3, "19:40");
        
        List<TableOption> options = resto.estimateWaiting(6);
        
        int p = resto.newParty("test", 6, "123456");
        
        resto.bookTable(5, p, "20:45"); 
        
        assertEquals("21:55",resto.estimateAvailable(5));
    }
    
    @Test
    public void testWaitingEstimateBooking() {
        resto.takeTable(5, p3, "19:40");
        
        int p = resto.newParty("test", 6, "123456");
        
        resto.bookTable(5, p, "20:45"); 
        
        List<TableOption> options = resto.estimateWaiting(5);
        
        assertEquals(1,options.size()); // only one table fits 6 people
        TableOption op = options.get(0);
        assertEquals(5,op.getTable()); // table 5 will be 
        assertEquals("21:55",op.getTime()); // available at 20:45
        assertEquals(0.625,op.getOccupation(),0.001); // with 100% occupation

        
    }

    
}
