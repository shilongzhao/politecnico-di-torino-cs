package it.polito.po.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polito.oop.tables.Restaurant;

public class TestR1_Tables {
    Restaurant resto;
    
    @Before
    public void setUp() {
        resto = new Restaurant();
    }

    @Test
    public void testAllSeats() {
        resto.defineTables(4,4,2,2,6);
        
        assertEquals(18,resto.getSeats());
    }

    @Test
    public void testTables() {
        resto.defineTables(4,4,2,2,6);
        
        assertEquals(6,resto.tableSize(5));
        assertEquals(2,resto.tableSize(4));        
    }

    @Test(expected=IllegalArgumentException.class)
    public void testWrongTable() {
        resto.defineTables(4,4,2,2,6);
        
         resto.tableSize(8);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testWrongTable2() {
        resto.defineTables(4,4,2,2,6);
        resto.tableSize(0);        
    }
    
    @Test
    public void testOpening() {
        resto.setOpeningTime("19:30");
        assertEquals("19:30",resto.getOpeningTime());
    }

    @Test
    public void testClosing() {
        resto.setClosingTime("23:30");
        assertEquals("23:30",resto.getClosingTime());
    }

    

}
