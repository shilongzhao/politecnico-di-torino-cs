package it.polito.po.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestR1_Tables.class, TestR2_Customers.class, TestR3_TableOccupation.class, TestR4_Booking.class,
        TestR5_Stats.class })
public class AllTests {

}
