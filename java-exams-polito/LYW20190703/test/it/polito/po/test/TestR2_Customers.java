package it.polito.po.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import it.polito.oop.tables.Restaurant;

public class TestR2_Customers {
    Restaurant resto;
    
    @Before
    public void setUp() {
        resto = new Restaurant();
    }

    @Test
    public void testPartyGetters() {
        int p1 = resto.newParty("Genny", 4, "3334445566");
        
        assertEquals("Genny",resto.getPartyName(p1));
        assertEquals(4,resto.getPartySize(p1));
        assertEquals("3334445566",resto.getPartyPhone(p1));
    }
    
    @Test
    public void testUniqueCode() {
        int p1 = resto.newParty("Genny", 4, "3334445566");
        int p2 = resto.newParty("Remo", 2, "3337778899");

        assertTrue(p1!=p2);
    }
    
    @Test
    public void testPartiesCount() {
        resto.newParty("Genny", 4, "3334445566");
        resto.newParty("Remo", 2, "3337778899");
        resto.newParty("Ugo", 5, "3331112233");
        
        assertEquals(3,resto.countParties());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testPartyGettersExc1() {
        int p1 = resto.newParty("Genny", 4, "3334445566");
        
        resto.getPartyName(p1+99);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testPartyGettersExc2() {
        int p1 = resto.newParty("Genny", 4, "3334445566");
        
        resto.getPartySize(p1-99);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testPartyGettersExc3() {
        int p1 = resto.newParty("Genny", 4, "3334445566");
        
        resto.getPartyPhone(p1-1);
    }

}
