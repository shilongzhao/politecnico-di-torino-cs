package class01;

/**
 * author: zhaoshilong
 * date: 21/04/2017
 */
public class AppInnerClass {
    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {
            public void run() {
                for(int i = 0; i < 5; i++) {
                    System.out.println("Yo " + i);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println("Ciao " + i);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        t2.start();
        t.start();
    }
}
