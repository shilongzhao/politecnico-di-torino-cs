package class01;

/**
 * author: zhaoshilong
 * date: 21/04/2017
 */
class Runner extends Thread {
    @Override
    public void run() {
        for(int i = 0; i < 10; i++) {
            System.out.println("Hello " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
public class App {
    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.start(); // DO NOT call run(), run() will run in the main thread;
        Runner runner1 = new Runner();
        runner1.start();
    }
}
