package class01;

/**
 * author: zhaoshilong
 * date: 21/04/2017
 */
class TaskRunner implements Runnable {

    public void run() {
        for(int i = 0; i < 10; i++) {
            System.out.println("Hello" + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
public class AppRunnable {
    public static void main(String[] args) {
        Thread t = new Thread(new TaskRunner()); // this is how to use when you extends Runnable
        Thread t2 = new Thread(new TaskRunner());
        t.start();
        t2.start();
    }
}
