package class02;

import java.util.Scanner;

/**
 * author: zhaoshilong
 * date: 21/04/2017
 */

class Processor extends  Thread {
    // volatile tells the code using this variable that this variable can be changed during execution,
    // so do a check every time using it! It prevents threads from caching variables
    private volatile boolean running = true;
    public void run() {
        while (running) {
            System.out.println("Hello");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void shutDown() {
        running = false;
    }
}
public class SynchronizationApp {

        public static void main(String[] args) {
            Processor p1 = new Processor();
            p1.start();
            System.out.println("Print return to stop..");
            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
            p1.shutDown();
        }
}
