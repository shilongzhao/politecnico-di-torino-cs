package class07producerconsumer;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * author: zhaoshilong
 * date: 23/04/2017
 */
public class ProducerConsumerApp {
    public static BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(10);

    public static void main(String[] args) {
        Thread prod = new Thread(() -> processor());
        Thread cons = new Thread(() -> consumer());
        prod.start();
        cons.start();
        try {
            prod.join();
            cons.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void processor() {
        Random random = new Random();
        while (true) {

            try {
                Thread.sleep(10);
                queue.put(random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void consumer() {
        Random random = new Random();

        while (true) {
            if (random.nextInt(10) <= 5) {
                Integer v = null;
                try {
                    Thread.sleep(10);
                    v = queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Taken value: " + v + ", queue size: " + queue.size());
            }
        }
    }
}
