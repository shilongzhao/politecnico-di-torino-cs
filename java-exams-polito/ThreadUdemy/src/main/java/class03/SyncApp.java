package class03;

/**
 * author: zhaoshilong
 * date: 21/04/2017
 */
public class SyncApp {
    private  int count = 0; // volatile will not help here
    public static void main(String[] args) {
        SyncApp app = new SyncApp();

        try {
            app.doWork();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public synchronized void increase()  { // solution 1: use synchronized method, // solution 2: synchronize on the same object
        count++;
    }

    public void doWork() throws InterruptedException {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10000; i++)
                increase();
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10000; i++)
                increase();
        });

        t1.start();
        t2.start();
        // the result will be random with or without join(),
        // - without join: since the println function run in main thread.
        //      when it's executed the two threads may not finished yet.
        // - with join(): even if the main thread wait t1 and t2, there is race conditions between t1 and t2.
        t1.join();
        t2.join();
        System.out.println("count = " + count);
    }
}
