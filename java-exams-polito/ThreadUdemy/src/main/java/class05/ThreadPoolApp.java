package class05;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * author: zhaoshilong
 * date: 21/04/2017
 */
class Processor implements Runnable {
    private int id;
    public Processor(int id) {
        this.id = id;
    }
    @Override
    public void run() {
        System.out.println("Starting " + id);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Completed " + id);
    }
}
public class ThreadPoolApp {
    public static void main(String[] args) {
        ExecutorService executors = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 5; i++) {
            executors.submit(new Processor(i));
        }
        executors.shutdown();
        System.out.println("All tasks submitted");

        try {
            executors.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
