package class08waitnotify;

import java.util.Scanner;

/**
 * author: zhaoshilong
 * date: 28/04/2017
 */
public class WaitNotifyProcessor {
    public void produce() throws InterruptedException {
        synchronized (this) {
            System.out.println("Producer thread running ...");
            wait(); // relinquish control of lock, consume() continues
            System.out.println("Producer thread resumed.");
        }
    }

    public void consume() throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        Thread.sleep(2000);
        synchronized (this) {
            System.out.println("Waiting user input.");
            scanner.nextLine(); // pause to wait user input
            System.out.println("Return key pressed.");
            notify(); // will wake up other threads, but it does not relinquish control of the lock.
            System.out.println("Waking up produce() ...");
            Thread.sleep(5000);
        }
    }
}
