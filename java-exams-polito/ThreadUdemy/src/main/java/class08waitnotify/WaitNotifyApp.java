package class08waitnotify;

/**
 * author: zhaoshilong
 * date: 28/04/2017
 */
public class WaitNotifyApp {
    public static void main(String[] args) throws InterruptedException {
        final WaitNotifyProcessor processor  = new WaitNotifyProcessor();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();
    }
}
