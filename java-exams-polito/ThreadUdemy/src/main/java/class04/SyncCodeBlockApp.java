package class04;

/**
 * author: zhaoshilong
 * date: 21/04/2017
 */
public class SyncCodeBlockApp {
    public static void main(String[] args) {
        new Worker().doWork();
    }
}
/*
With synchronized keyword gets right answer, but No running time advantage using multiple thread.
ACTUALLY stageOne and stageTwo CAN run at the same time in different threads, since they are operating
on different data.

Solution: synchronize on different locks;

Starting ...
Time taken: 5343
List1: 2000; List2: 2000

Process finished with exit code 0
 */

/*

Without 'synchronized' key word for stageOne() or stageTwo()
 Starting ...
 Exception in thread "Thread-1" java.lang.ArrayIndexOutOfBoundsException: 366
 at java.util.ArrayList.add(ArrayList.java:459)
 at class04.Worker.stageOne(Worker.java:47)
 at class04.Worker.process(Worker.java:62)
 at java.lang.Thread.run(Thread.java:745)
 Time taken: 2634
 List1: 1179; List2: 1167

Due to concurrent modification of List
 */