package class09producer_consumer_pattern;

import java.util.LinkedList;
import java.util.Random;

/**
 * author: zhaoshilong
 * date: 28/04/2017
 */
public class Processor {
    private LinkedList<Integer> list = new LinkedList<>();
    private final int LIMIT = 10;
    private final Object lock = new Object();

    public void produce() throws InterruptedException {
        int value = 0;
        Random random = new Random();
        while (true) {

            synchronized (lock) {
                while (list.size() == LIMIT) {
                    lock.wait();
                }
                list.add(value++);
                lock.notify();
            }
            Thread.sleep(random.nextInt(1000));
        }
    }

    public void consume() throws InterruptedException {
        Random random = new Random();
        while (true) {
            synchronized (lock) {
                while (list.size() == 0) {
                    lock.wait();
                }
                System.out.print("List size is " + list.size());
                int value = list.removeFirst();
                System.out.println("; value is " + value);
                lock.notify();
            }

            Thread.sleep(random.nextInt(1000));
        }
    }
}
