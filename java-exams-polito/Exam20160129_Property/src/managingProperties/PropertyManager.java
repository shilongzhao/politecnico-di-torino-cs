package managingProperties;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

public class PropertyManager {
	private Map<String, Building> buildings = new HashMap<>();
	private Map<String, List<String>> owners = new HashMap<>(); // key owner id, value list of owned apartments
	private Map<String, String> allApartments = new HashMap<>(); // key: apartment id, value ower id
    private Map<String, List<String>> professions = new TreeMap<>(); // key: profession, value: list of workers
    private Map<String, String> workers = new HashMap<>(); // key: worker id, value: profession
    private Map<Integer, Request> requests = new HashMap<>(); // key: request id, value: request
    private int numRequests = 0;

	public void addBuilding(String building, int n) throws PropertyException {
		if (buildings.containsKey(building)) throw new PropertyException();
		if (n < 1 || n > 100) throw  new PropertyException();
		buildings.put(building, new Building(building, n));
	}

	public void addOwner(String owner, String... apartments) throws PropertyException {
		if (owners.containsKey(owner)) throw new PropertyException();

		for (String a : apartments) {
		    String[] info = a.split(":");
		    if (!buildings.containsKey(info[0])) throw new PropertyException();
		    if (allApartments.containsKey(a)) throw new PropertyException();
		    int n = Integer.valueOf(info[1]);
		    if (n > buildings.get(info[0]).getNumOfApartments() || n < 1) throw new PropertyException();

		    allApartments.put(a, owner); // apartment a is owned by owner

            if (owners.containsKey(owner)) {
                owners.get(owner).add(a);
            }
            else {
                List<String> list = new ArrayList<>();
                list.add(a);
                owners.put(owner, list);
            }
        }
	}

	/**
	 * Returns a map ( number of apartments => list of buildings ) 
	 * 
	 */
	public SortedMap<Integer, List<String>> getBuildings() {
        return buildings.values().stream().
                sorted(comparing(Building::getId)).
                collect(groupingBy(Building::getNumOfApartments,
                        TreeMap::new,
                        mapping(Building::getId, toList())));
	}

	public void addProfessionals(String profession, String... professionals) throws PropertyException {
        if (professions.containsKey(profession)) throw new PropertyException();
        for (String p: professionals) {
            if (workers.containsKey(p)) throw new PropertyException();
            workers.put(p, profession);
        }
        professions.put(profession, new ArrayList<>(Arrays.asList(professionals)));
	}

	/**
	 * Returns a map ( profession => number of workers )
	 *
	 */
	public SortedMap<String, Integer> getProfessions() {
        TreeMap<String, Integer> result = new TreeMap<>();
        for (String s: professions.keySet()) {
            result.put(s, professions.get(s).size());
        }
        return result;
//        return workers.entrySet().stream().map(Map.Entry::getValue).
//                collect(groupingBy(Function.identity(), TreeMap::new, counting()));
	}

	public int addRequest(String owner, String apartment, String profession) throws PropertyException {
		if (!owners.containsKey(owner) ||
                !owners.get(owner).contains(apartment) ||
                !allApartments.containsKey(apartment) ||
                !professions.containsKey(profession)) {
            throw new PropertyException();
        }
        Request r = new Request(++numRequests, owner, apartment, profession);
		requests.put(numRequests, r);
		return numRequests;
	}

	public void assign(int requestN, String professional) throws PropertyException {
	    if (!requests.containsKey(requestN)) throw new PropertyException();
	    Request r = requests.get(requestN);
	    if (!r.isPending()) throw new PropertyException();
	    if (!workers.containsKey(professional)) throw new PropertyException();
	    String profession = workers.get(professional);
	    if (!r.getProfession().equals(profession)) throw new PropertyException();
	    r.setProfessional(professional);
	}

	public List<Integer> getAssignedRequests() {
		return requests.values().stream().filter(Request::isAssigned).map(Request::getId).collect(toList());
	}

	
	public void charge(int requestN, int amount) throws PropertyException {
		if (!requests.containsKey(requestN)) throw new PropertyException();
		Request r = requests.get(requestN);
		if (!r.isAssigned()) throw new PropertyException();
		if (amount < 0 || amount > 1000) throw new PropertyException();

		r.setCharge(amount);
	}

	/**
	 * Returns the list of request ids
	 * 
	 */
	public List<Integer> getCompletedRequests() {
		
		return requests.values().stream().filter(Request::isCompleted).
                map(Request::getId).
                sorted(comparing(Function.identity())).collect(toList()); // ?
	}
	
	/**
	 * Returns a map ( owner => total expenses )
	 * 
	 */
	public SortedMap<String, Integer> getCharges() {
        Map<String, List<Request>> ownerRequest = requests.values().stream().
                filter(Request::isCompleted).collect(groupingBy(Request::getOwnerId));

        List<Request> requestCompleted = requests.values().stream().filter(Request::isCompleted).collect(toList());

        Map<String, Integer> ownerCharge = requests.values().stream().
                filter(Request::isCompleted).collect(Collectors.groupingBy(Request::getOwnerId, summingInt(Request::getCharge)));

        Map<String, Long> ownerCount = requests.values().stream().
                filter(Request::isCompleted).collect(groupingBy(Request::getOwnerId, Collectors.counting()));

        HashMap<String, Integer> orderedOwnerRequest = requests.values().stream().filter(Request::isCompleted).
                collect(groupingBy(Request::getOwnerId, HashMap::new, summingInt(Request::getCharge)));

        String ownersHavingRequests = requests.values().stream().filter(r -> r.getCharge() > 30).map(r -> r.getOwnerId()).collect(joining(","));
        return null;
	}

	/**
	 * Returns the map ( building => ( profession => total expenses) ).
	 * Both buildings and professions are sorted alphabetically
	 * 
	 */
	public SortedMap<String, Map<String, Integer>> getChargesOfBuildings() {
		
		return null;
	}

}
