package managingProperties;

/**
 * author: zhaoshilong
 * date: 23/04/2017
 */
public class Request {
    private int id;
    private String ownerId;
    private String apartmentId;
    private State state;
    private String profession;
    private String professional;
    private int charge;
    public Request(int id, String ownerId, String apartmentId, String profession) {
        this.id = id;
        this.ownerId = ownerId;
        this.apartmentId = apartmentId;
        this.profession = profession;
        this.state = State.PENDING;
    }

    public int getId() {
        return id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getApartmentId() {
        return apartmentId;
    }

    public String getProfession() {
        return profession;
    }

    public boolean isPending() {
        if (state == State.PENDING)
            return true;
        return false;
    }

    public boolean isAssigned() {
        return state == State.ASSIGNED;
    }

    public boolean isCompleted() {
        return state == State.COMPLETED;
    }

    public void toAssigned() {
        state = State.ASSIGNED;
    }

    public void toCompleted() {
        state = State.COMPLETED;
    }

    public void setProfessional(String professional) {
        this.professional = professional;
        toAssigned();
    }

    public String getProfessional() {
        return professional;
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
        toCompleted();
    }

    enum State {
        PENDING, ASSIGNED, COMPLETED
    }
}
