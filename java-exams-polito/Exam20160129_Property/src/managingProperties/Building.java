package managingProperties;

import com.oracle.webservices.internal.api.databinding.Databinding;

/**
 * author: zhaoshilong
 * date: 20/04/2017
 */
public class Building {
    private String id;
    private int numOfApartments;
    public Building(String id, int n) {
        this.id = id;
        this.numOfApartments = n;
    }
    public String getId() {
        return id;
    }

    public int getNumOfApartments() {
        return numOfApartments;
    }
}
