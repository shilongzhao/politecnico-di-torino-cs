package student;

import java.util.*;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.*;
/**
 * author: zhaoshilong
 * date: 25/04/2017
 */
public class Statistics {
    static List<Exam> exams = new ArrayList<>();
    public static void main(String[] args) {
        exams.add(new Exam("Math I", "Li", 24));
        exams.add(new Exam("Math I", "Wang", 28));
        exams.add(new Exam("Physics I", "Li", 18));
        exams.add(new Exam("Physics I", "Wang", 22));
        exams.add(new Exam("CS", "Li", 30));
        exams.add(new Exam("CS", "AAAAAAA", 18));
        //Map  studentName -> exam
        Map<String, List<Exam>> m1 = exams.stream().collect(Collectors.groupingBy(Exam::getStudentName));
        // Map studentName -> examName
        Map<String, List<String>> m2 = exams.stream().collect(
                Collectors.groupingBy(Exam::getStudentName, Collectors.mapping(Exam::getExamName, Collectors.toList())));
        System.out.println(m2);

        String allHighScoreLi = exams.stream().
                filter(e -> e.studentName.equals("Li") && e.result > 20).
                map(Exam::getExamName).collect(joining(","));

        Map<String, Double> m3 = exams.stream().collect(groupingBy(Exam::getStudentName, averagingDouble(Exam::getResult)));
        System.out.println(m3);

        Map<String, Double> m4 = exams.stream().collect(groupingBy(Exam::getStudentName, TreeMap::new, averagingDouble(Exam::getResult)));
        System.out.println(m4);

        Map<String, Double> m5 = exams.stream().collect(groupingBy(Exam::getStudentName,
                () -> new TreeMap<>(new StringLengthComparator()),
                averagingDouble(Exam::getResult)));
        System.out.println(m5);
    }

}

class StringLengthComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        return o1.length() - o2.length();
    }
}