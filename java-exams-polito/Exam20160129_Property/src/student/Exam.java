package student;

/**
 * author: zhaoshilong
 * date: 25/04/2017
 */
public class Exam {
    String examName;
    String studentName;
    int    result;
    public Exam(String examName, String studentName, int result) {
        this.examName = examName;
        this.studentName = studentName;
        this.result = result;
    }
    public String getExamName() {
        return examName;
    }
    public String getStudentName() {
        return studentName;
    }
    public int getResult() {
        return result;
    }
}
