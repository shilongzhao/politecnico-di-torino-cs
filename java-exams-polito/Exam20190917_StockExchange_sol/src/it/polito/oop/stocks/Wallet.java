package it.polito.oop.stocks;

import java.util.HashMap;
import java.util.Map;

public class Wallet {
    private int userId;
    private Map<String, Integer> stockQuantityMap = new HashMap<>();

    public Wallet(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Map<String, Integer> getStockQuantityMap() {
        return stockQuantityMap;
    }

    public void setStockQuantityMap(Map<String, Integer> stockQuantityMap) {
        this.stockQuantityMap = stockQuantityMap;
    }

    public Integer getAllQuantity() {
        return stockQuantityMap.values().stream().reduce(0, Integer::sum);
    }
}
