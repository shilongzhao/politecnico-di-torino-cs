package it.polito.oop.stocks;

import java.util.Collection;
import java.util.Map;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class StockExchange {
    private Map<String, String> stocksMap = new HashMap<>();
    private Map<Integer, User> userMap = new HashMap<>();
    private Map<Integer, Wallet> walletMap = new HashMap<>();
    private Map<String, List<SellingProposal>> proposalMap = new HashMap<>();

    public int defineStocks(String... stocks) {
        int count = 0;
        for (String s : stocks) {
            String[] str = s.split(": ");
            if (str.length != 2) {
                continue;
            }
            String code = str[0].trim();

            if (stocksMap.containsKey(code)) {
                continue;
            }

            stocksMap.put(code, s);
            count += 1;
        }
        return count;
    }

    public String getStock(String stockCode) throws StockException {
        if (!stocksMap.containsKey(stockCode)) {
            throw new StockException();
        }
        return stocksMap.get(stockCode);
    }

    private int userid = 0;

    public int registerUser(String name) {
        userid++;
        User u = new User(userid, 0.00, name);

        userMap.put(userid, u);
        return userid;
    }

    public String getUser(int userId) throws UserException {
        if (!userMap.containsKey(userId)) {
            throw new UserException();
        }
        String name = userMap.get(userId).getName();

        return userId + ": " + name;
    }

    public void addToBalance(int userId, double amount) throws UserException {
        if (!userMap.containsKey(userId)) {
            throw new UserException();
        }
        double b = userMap.get(userId).getBalance() + amount;
        userMap.get(userId).setBalance(b);
    }


    public double getBalance(int userId) throws UserException {
        if (!userMap.containsKey(userId)) {
            throw new UserException();
        }

        User u = userMap.get(userId);
        return u.getBalance();
    }

    public void addToWallet(int userId, String stockCode, int quantity) throws UserException, StockException {
        if (!userMap.containsKey(userId)) {
            throw new UserException();
        }
        if (!stocksMap.containsKey(stockCode)) {
            throw new StockException();
        }

        Wallet wallet = walletMap.containsKey(userId) ? walletMap.get(userId): new Wallet(userId);

        int q = wallet.getStockQuantityMap().getOrDefault(stockCode, 0);

        wallet.getStockQuantityMap().put(stockCode, q + quantity);

        walletMap.put(userId, wallet);
    }

    public Collection<String> getWallet(int userId) throws UserException {
        if (!userMap.containsKey(userId)) {
            throw new UserException();
        }
        Wallet w = walletMap.get(userId);

        if (w == null)
            return new ArrayList<>();

        return w.getStockQuantityMap().entrySet()
                .stream().map(e -> String.format("%s: %d", e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    public void addAsk(int userId, String stockCode, int quantity, double minPrice)
            throws UserException, StockException, WalletException {
        if (!userMap.containsKey(userId)) {
            throw new UserException();
        }
        if (!stocksMap.containsKey(stockCode)) {
            throw new StockException();
        }

        Wallet wallet = walletMap.get(userId);

        if (wallet == null || wallet.getStockQuantityMap().get(stockCode) < quantity) {
            throw new WalletException();
        }

        int q = wallet.getStockQuantityMap().get(stockCode) - quantity;
        if (q == 0) {
            wallet.getStockQuantityMap().remove(stockCode);
        } else {
            wallet.getStockQuantityMap().put(stockCode, q);
        }

        SellingProposal proposal = new SellingProposal(userId, stockCode, quantity, minPrice);

        if (proposalMap.get(stockCode) == null) {
            List<SellingProposal> proposals = new ArrayList<>();
            proposals.add(proposal);
            proposalMap.put(stockCode, proposals);
        }
        else {
            proposalMap.get(stockCode).add(proposal);
        }

    }

    public Collection<String> getBook(String stockCode) throws StockException {
        if (!stocksMap.containsKey(stockCode)) throw new StockException();

        if (proposalMap.get(stockCode) == null) return new ArrayList<>();

        return proposalMap.get(stockCode).stream().sorted((a, b) -> {
            if (a.getMinPrice() == b.getMinPrice()) {
                return b.getQuantity() - a.getQuantity();
            }
            else {
                return a.getMinPrice() > b.getMinPrice()? 1 : -1;
            }
        }).map(SellingProposal::toString).collect(Collectors.toList());
    }

    public boolean executeBid(int userId, String stockCode, int quantity, double maxPrice)
            throws UserException, StockException, BalanceException {
        if (!userMap.containsKey(userId)) throw new UserException();
        if (!stocksMap.containsKey(stockCode)) throw new StockException();
        User buyer = userMap.get(userId);
        if (buyer.getBalance() < quantity * maxPrice) throw new BalanceException();

        if (proposalMap.get(stockCode) == null) return false;

        List<SellingProposal> proposals = proposalMap.get(stockCode).stream()
                .filter(p -> p.getMinPrice() <= maxPrice)
                .sorted((a, b) -> {
                    if (a.getMinPrice() == b.getMinPrice()) {
                        return b.getQuantity() - a.getQuantity();
                    }
                    else {
                        return a.getMinPrice() > b.getMinPrice()? 1 : -1; }
                })
                .collect(Collectors.toList());

        Integer totalQuantity = proposals.stream().map(SellingProposal::getQuantity).reduce(0, Integer::sum);
        if (totalQuantity < quantity) return false;

        int remainingQuantity = quantity;
        Iterator<SellingProposal> iterator = proposals.iterator();
        while (remainingQuantity > 0) {
            SellingProposal p = iterator.next();

            int buyQuantity = Math.min(p.getQuantity(), remainingQuantity);
            remainingQuantity -= buyQuantity;

            p.setQuantity(p.getQuantity() - buyQuantity);

            User seller = userMap.get(p.getUserId());

            addToBalance(seller.getId(), buyQuantity * p.getMinPrice());
            addToWallet(buyer.getId(), stockCode, buyQuantity);

            addToBalance(buyer.getId(), - buyQuantity * p.getMinPrice());
        }
        List<SellingProposal> remaining = proposalMap.get(stockCode).stream().
                filter(pr -> pr.getQuantity() != 0).collect(Collectors.toList());
        proposalMap.put(stockCode, remaining);

        return true;
    }

    public long countActiveWallets() {
        return walletMap.entrySet().stream().filter(e -> e.getValue().getStockQuantityMap().size() >= 1).count();
    }

    public long countAskedStocks() {
        return proposalMap.entrySet().stream().filter(e -> e.getValue().size() >= 1).count();
    }

    public Map<Integer, Long> stocksPerUsers() {
        return walletMap.values().stream().collect(HashMap::new, (map, wallet) -> {
            Long value = (long) wallet.getStockQuantityMap().size();
            Long v = map.getOrDefault(wallet.getUserId(), 0L);
            map.put(wallet.getUserId(), v + value);
        }, (map1, map2) -> map2.forEach((k, v) -> map1.merge(k, v, Long::sum)));
    }

}
