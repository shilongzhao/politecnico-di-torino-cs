package it.polito.oop.stocks;

public class SellingProposal {
    private int userId;
    private String stockCode;
    private int quantity;
    private double minPrice;

    public SellingProposal(int userId, String stockCode, int quantity, double minPrice) {
        this.userId = userId;
        this.stockCode = stockCode;
        this.quantity = quantity;
        this.minPrice = minPrice;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    @Override
    public String toString() {
        return String.format("%d: %d@%.1f", userId, quantity, minPrice);
    }
}
