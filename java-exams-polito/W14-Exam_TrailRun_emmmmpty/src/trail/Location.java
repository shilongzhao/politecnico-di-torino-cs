package trail;

public class Location implements Comparable<Location>{
    private int orderNum;
    private String name;

    public Location(int orderNum, String name) {
        this.orderNum = orderNum;
        this.name = name;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public String getName() {
        return name;
    }


    @Override
    public int compareTo(Location o) {
        return this.orderNum - o.getOrderNum();
    }
}
