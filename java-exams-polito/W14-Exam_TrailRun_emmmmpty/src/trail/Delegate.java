package trail;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
public class Delegate {
    private String ssn;
    private String name;
    private String surname;

    public Delegate(String ssn, String name, String surname) {
        this.ssn = ssn;
        this.name = name;
        this.surname = surname;
    }

    public String getSsn() {
        return ssn;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s", surname, name, ssn);
    }
}
