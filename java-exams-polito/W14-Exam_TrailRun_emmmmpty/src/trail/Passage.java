package trail;

/**
 * Created by <a href="mailto:zhao.s.long@gmail.com">S. Zhao</a>.
 */
public class Passage {
    private String  location;
    private int     bibNumber;
    private Long    time;
    private String  recordedBy;

    public Passage(String location, int bibNumber, Long time, String recordedBy) {
        this.location = location;
        this.bibNumber = bibNumber;
        this.time = time;
        this.recordedBy = recordedBy;
    }

    public String getLocation() {
        return location;
    }

    public int getBibNumber() {
        return bibNumber;
    }

    public Long getTime() {
        return time;
    }

    public String getRecordedBy() {
        return recordedBy;
    }
}