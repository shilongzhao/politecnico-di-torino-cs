package trail;

public class Runner {
    private int bibNumber;
    private String name;
    private String surname;

    public Runner(int bibNumber, String name, String surname) {
        this.bibNumber = bibNumber;
        this.name = name;
        this.surname = surname;
    }

    public int getBibNumber() {
        return bibNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
