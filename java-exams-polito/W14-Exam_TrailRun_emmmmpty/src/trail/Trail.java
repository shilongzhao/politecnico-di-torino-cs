package trail;

import java.util.*;
import java.util.stream.Collectors;

public class Trail {
    private Map<Integer, Runner> runners = new HashMap<>();
    private Map<String, Location> locations = new HashMap<>();
    private Map<String, Delegate> delegates = new HashMap<>();
    private Map<String, List<Delegate>> locationDelegates = new HashMap<>();
    private Map<String, List<Passage>> locationPassages = new HashMap<>();
    private Map<Integer, Passage> runnerLatestPassage = new HashMap<>();

    public int newRunner(String name, String surname){
        Runner r = new Runner(runners.size() + 1, name, surname);
        runners.put(runners.size() + 1, r);
        return r.getBibNumber();
    }
    
    public Runner getRunner(int bibNumber){
        return runners.get(bibNumber);
    }
    
    public Collection<Runner> getRunner(String surname){
        return runners.values().stream()
                .filter(runner -> runner.getSurname().equals(surname))
                .sorted(Comparator.comparing(Runner::getBibNumber))
                .collect(Collectors.toList());
    }
    
    public List<Runner> getRunners(){
        return runners.values().stream()
                .sorted(Comparator.comparing(Runner::getBibNumber))
                .collect(Collectors.toList());
    }

    public List<Runner> getRunnersByName(){
        return runners.values().stream()
                .sorted(Comparator.comparing(Runner::getName).thenComparing(Runner::getBibNumber))
                .collect(Collectors.toList());
    }
    
    public void addLocation(String location){
        Location l = new Location(locations.size(), location);
        locations.put(location, l);
    }
//    public void addLocation(String name, double lat, double lon, double elevation){
//        
//    }

    public Location getLocation(String location){
        return locations.get(location);
    }

    public List<Location> getPath(){
        return locations.values().stream()
                .sorted(Comparator.comparing(Location::getOrderNum))
                .collect(Collectors.toList());
    }
    
    public void newDelegate(String name, String surname, String id){
        Delegate d = new Delegate(id, name, surname);
        delegates.put(id, d);
    }

    public List<String> getDelegates(){
        return delegates.values().stream()
                .map(Delegate::toString)
                .sorted()
                .collect(Collectors.toList());
    }
    

    public void assignDelegate(String location, String delegate) throws TrailException {
       if (!locations.containsKey(location) || !delegates.containsKey(delegate)) {
           throw new TrailException();
       }
       Delegate d = delegates.get(delegate);
       if (locationDelegates.containsKey(location)) {
           locationDelegates.get(location).add(d);
       }
       else {
           List<Delegate> list = new ArrayList<>();
           list.add(d);
           locationDelegates.put(location, list);
       }


    }
 
    public List<String> getDelegates(String location){
        return locationDelegates.get(location).stream()
                .map(Delegate::toString)
                .sorted()
                .collect(Collectors.toList());
    }
    
    public long recordPassage(String delegate, String location, int bibNumber) throws TrailException {
        if (!locations.containsKey(location) ||
                !delegates.containsKey(delegate) ||
                !runners.containsKey(bibNumber)) {
            throw new TrailException();
        }
        Long currentTime = System.currentTimeMillis();
        Passage p = new Passage(location, bibNumber, currentTime, delegate);
        if (locationPassages.containsKey(location)) {
            locationPassages.get(location).add(p);
        }
        else {
            List<Passage> list = new ArrayList<>();
            list.add(p);
            locationPassages.put(location, list);
        }
        if (runnerLatestPassage.get(bibNumber) == null ||
                runnerLatestPassage.get(bibNumber).getTime() < currentTime) {
            runnerLatestPassage.put(bibNumber, p);
        }
        return currentTime;
    }
    
    public long getPassTime(String position, int bibNumber) throws TrailException {
        if (!locations.containsKey(position) || !runners.containsKey(bibNumber)) {
            throw new TrailException();
        }

        if (!locationPassages.containsKey(position)) {
            return -1;
        }

        List<Passage> passages = locationPassages.get(position);
        for (Passage p: passages) {
            if (p.getBibNumber() == bibNumber) {
                return p.getTime();
            }
        }

        return -1;
    }
    
    public List<Runner> getRanking(String location){
        if (!locationPassages.containsKey(location)) {
            return null;
        }
        return locationPassages.get(location).stream()
                .sorted(Comparator.comparing(Passage::getTime))
                .map(p -> runners.get(p.getBibNumber()))
                .collect(Collectors.toList());
    }

    public List<Runner> getRanking(){
        return runnerLatestPassage.values().stream()
                .sorted((p1, p2) -> locations.get(p2.getLocation()).compareTo(locations.get(p1.getLocation())))
                .map(p->runners.get(p.getBibNumber()))
                .collect(Collectors.toList());
    }


}
