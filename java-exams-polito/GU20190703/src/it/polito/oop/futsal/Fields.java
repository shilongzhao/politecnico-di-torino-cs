package it.polito.oop.futsal;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents a infrastructure with a set of playgrounds, it allows teams
 * to book, use, and  leave fields.
 *
 */
public class Fields {

	private String openingTime = "00:00";
	private String closingTime = "00:00";

	private Map<Integer, Field> fieldMap = new HashMap<>();
	private Map<Integer, Associate> associateMap = new HashMap<>();
	private Map<Integer, List<Reservation>> reservationMap = new HashMap<>();

    public static class Features {
        public final boolean indoor; // otherwise outdoor
        public final boolean heating;
        public final boolean ac;
        public Features(boolean i, boolean h, boolean a) {
            this.indoor=i; this.heating=h; this.ac = a;
        }

        public boolean satisfies(Features requirement) {
            if (requirement.indoor && !this.indoor) {
                return false;
            }
            if (requirement.heating && !this.heating) {
                return false;
            }
            if (requirement.ac && !this.ac) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "Features{" +
                    "indoor=" + indoor +
                    ", heating=" + heating +
                    ", ac=" + ac +
                    '}';
        }
    }
    
    public void defineFields(Features... features) throws FutsalException {
        for (Features f: features) {
            if ((!f.indoor) && (f.ac || f.heating)) throw new FutsalException();
            int id = fieldMap.size() + 1;
            Field field = new Field(id, f);
            fieldMap.put(id, field);
        }
    }
    
    public long countFields() {
        return fieldMap.size();
    }

    public long countIndoor() {
    	return fieldMap.values().stream().filter(field -> field.getFeatures().indoor).count();
    }
    
    public String getOpeningTime() {
        return openingTime;
    }
    
    public void setOpeningTime(String time) {
    	openingTime =time;
    }
    
    public String getClosingTime() {
        return closingTime;
    }
    
    public void setClosingTime(String time) {
    	closingTime = time;
    }

    public int newAssociate(String first, String last, String mobile) {
        int id = associateMap.size() + 1;
        Associate ass = new Associate(id, first, last, mobile);
        associateMap.put(id, ass);
    	return ass.getId();
    }
    
    public String getFirst(int partyId) throws FutsalException {
        if (!associateMap.containsKey(partyId)) throw new FutsalException();
        return associateMap.get(partyId).getName();
    }
    
    public String getLast(int associate) throws FutsalException {
    	if (!associateMap.containsKey(associate)) throw new FutsalException();
    	return associateMap.get(associate).getSurname();
    }
    
    public String getPhone(int associate) throws FutsalException {
        if (!associateMap.containsKey(associate)) throw new FutsalException();
        return associateMap.get(associate).getTelephoneNum();
    }
    
    public int countAssociates() {
    	return associateMap.size();
    }
    
    public void bookField(int field, int associate, String time) throws FutsalException {
        int timeDiff = timeStringToInteger(time) - timeStringToInteger(openingTime);
        if (!fieldMap.containsKey(field) || !associateMap.containsKey(associate) || timeDiff % 60 != 0) {
            throw new FutsalException();
        }

        Reservation r = new Reservation(field, associate, time);

        if (reservationMap.containsKey(field)) {
            reservationMap.get(field).add(r);
        } else {
            List<Reservation> reservations = new ArrayList<>();
            reservations.add(r);
            reservationMap.put(field, reservations);
        }
    }

    private int timeStringToInteger(String time) {
        String[] timing = time.split(":");
        String hour = timing[0];
        String min = timing[1];
        return Integer.valueOf(hour) * 60 + Integer.valueOf(min);
    }

    public boolean isBooked(int field, String time) {
        if (reservationMap.containsKey(field)) {
            return reservationMap.get(field).stream().anyMatch(r -> r.getTime().equals(time));
        }
        return false;
    }
    

    public int getOccupation(int field) {
        if (reservationMap.containsKey(field)) {
            return reservationMap.get(field).size();
        }
        return 0;
    }
    
    
    public List<FieldOption> findOptions(String time, Features required){
        return fieldMap.values().stream()
                .filter(field -> !isBooked(field.getId(), time))
                .filter(field -> field.getFeatures().satisfies(required))
                .map(field -> new FieldOptionImpl(field.getId(), getOccupation(field.getId())))
                .sorted((f1, f2) -> {
                    if (f1.getOccupation() == f2.getOccupation()) {
                        return f1.getField() - f2.getField();
                    }
                    // !!! unit test is different with the requirement !!!
                    return f1.getOccupation() - f2.getOccupation();
                })
                .collect(Collectors.toList());
    }
    
    public long countServedAssociates() {
        return reservationMap.values().stream()
                .flatMap(List::stream)
                .map(Reservation::getAssociateId)
                .distinct().count();
    }
    
    public Map<Integer, Long> fieldTurnover() {
        Map<Integer, Long> map = new HashMap<>();
        for (Integer k: fieldMap.keySet()) {
            List<Reservation> reservations = reservationMap.get(k);
            if (reservations == null) {
                map.put(k, 0L);
            } else {
                map.put(k, (long) reservations.size());
            }
        }
        return map;
    }
    
    public double occupation() {
        long timeBlock = countFields() * ((timeStringToInteger(closingTime) - timeStringToInteger(openingTime))/60);
        long num = reservationMap.values().stream().flatMap(List::stream).count();

        return num / (double) timeBlock;
    }
    
}
