package it.polito.oop.futsal;

public class Associate {

	private int id;
	private String name;
	private String surname;
	private String telephoneNum;

	public Associate(int id, String name, String surname, String telephoneNum) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.telephoneNum = telephoneNum;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getTelephoneNum() {
		return telephoneNum;
	}
}
