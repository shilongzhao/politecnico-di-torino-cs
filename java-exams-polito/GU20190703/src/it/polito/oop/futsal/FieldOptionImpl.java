package it.polito.oop.futsal;


public class FieldOptionImpl implements FieldOption {
    private int field;
    private int occupation;

    public FieldOptionImpl(int field, int occupation) {
        this.field = field;
        this.occupation = occupation;
    }

    @Override
    public int getField() {
        return field;
    }

    @Override
    public int getOccupation() {
        return occupation;
    }

    @Override
    public String toString() {
        return "FieldOptionImpl{" +
                "field=" + field +
                ", occupation=" + occupation +
                '}';
    }
}
