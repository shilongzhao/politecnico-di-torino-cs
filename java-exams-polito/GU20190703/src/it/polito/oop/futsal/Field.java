package it.polito.oop.futsal;

import it.polito.oop.futsal.Fields.Features;


public class Field {
    private int id;
    private Features features;

    public Field(int id, Features features) {
        this.id = id;
        this.features = features;
    }

    public int getId() {
        return id;
    }

    public Features getFeatures() {
        return features;
    }

    @Override
    public String toString() {
        return "Field{" +
                "id=" + id +
                ", features=" + features +
                '}';
    }
}
