package it.polito.oop.futsal;


public class Reservation {
    private int fieldId;
    private int associateId;
    private String time;

    public Reservation(int fieldId, int associateId, String time) {
        this.fieldId = fieldId;
        this.associateId = associateId;
        this.time = time;
    }

    public int getFieldId() {
        return fieldId;
    }

    public int getAssociateId() {
        return associateId;
    }

    public String getTime() {
        return time;
    }
}
