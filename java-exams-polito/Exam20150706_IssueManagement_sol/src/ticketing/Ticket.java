package ticketing;

/**
 * Class representing the ticket linked to an issue or malfunction.
 * 
 * The ticket is characterized by a severity and a state.
 */
public class Ticket {
    
    private int id;
    private User user; 
    private Component comp; 
    private String description;
    private Severity severity;
    private State state;
    private String solution;
    private User assignee;
    
    /**
     * Enumeration of possible severity levels for the tickets.
     * 
     * Note: the natural order corresponds to the order of declaration
     */
    public enum Severity { Blocking, Critical, Major, Minor, Cosmetic };
    
    /**
     * Enumeration of the possible valid states for a ticket
     */
    public static enum State { Open, Assigned, Closed }
    
    Ticket(int id, User user, Component comp, String description,
            Severity severity) throws TicketException {
        if(!user.getClasses().contains(IssueManager.UserClass.Reporter))
            throw new TicketException("User is not allowed to report issues");
        this.id=id;
        this.user = user;
        this.comp = comp;
        this.description = description;
        this.severity = severity;
        this.state = State.Open;
    }

    public int getId(){
        return id;
    }

    public String getDescription(){
        return description;
    }
    
    public String getAuthor(){
        return user.getName();
    }
    
    public String getComponent(){
        return comp.getPath();
    }
    

    public Severity getSeverity() {
        return severity;
    }

    public State getState(){
        return state;
    }
    
    public String getSolutionDescription() throws TicketException {
        if(state!=State.Closed) throw new TicketException("Cannot get solution for not closed ticket");
        return solution;
    }
    
    //--- Utility internal methods: they are used by IssueManager
    
    String getAssignee(){
        return assignee.getName();
    }
    

    void assignTo(User user) throws TicketException {
        if(this.state==State.Closed) 
            throw new TicketException("Cannot assing an already closed ticket");
        if(!user.getClasses().contains(IssueManager.UserClass.Maintainer)) 
            throw new TicketException("Cannot assing ticket to non maintainer");
        
        this.assignee = user;
        this.state=State.Assigned;
    }

    void close(String description) throws TicketException {
        if(this.state!=State.Assigned) 
            throw new TicketException("Cannot close a ticket that is not in assigne state");
        
        this.solution = description;
        this.state = State.Closed;
        assignee.incrementClosed();
    }
}
