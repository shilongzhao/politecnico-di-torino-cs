package ticketing;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ticketing.IssueManager.UserClass;

public class User {

    private String name;
    private Set<IssueManager.UserClass> classes;
    private int closedCount;

    public User(String name, Set<UserClass> classes) {
        this.name = name;
        this.classes = classes;
    }
    
    public User(String name, UserClass... classes) {
        this.name = name;
        this.classes = new HashSet<>(Arrays.asList(classes));
    }


    public String getName() {
        return name;
    }
    
    public Set<IssueManager.UserClass> getClasses() {
        return classes;
    }
    
    int countClosed(){
        return closedCount;
    }

    public void incrementClosed() {
        ++closedCount;
    }
    
}
