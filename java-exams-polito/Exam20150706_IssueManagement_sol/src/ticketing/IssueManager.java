package ticketing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class IssueManager {

    private Map<String,User> users = new HashMap<>();
    private Map<String,Component> components = new HashMap<>();
    private int ticketId;
    private List<Ticket> tickets = new ArrayList<>();
    
    /**
     * Eumeration of valid user classes
     */
    public static enum UserClass {
        /** user able to report an issue and create a corresponding ticket **/
        Reporter, 
        /** user that can be assigned to handle a ticket **/
        Maintainer }
    
    /**
     * Creates a new user
     * 
     * @param username name of the user
     * @param classes user classes
     * @throws TicketException if the username has already been created or if no user class has been specified
     */
    public void createUser(String username, UserClass... classes) throws TicketException {
        if(users.containsKey(username) || classes.length==0) throw new TicketException();
        users.put(username, new User(username,classes));
    }

    /**
     * Creates a new user
     * 
     * @param username name of the user
     * @param classes user classes
     * @throws TicketException if the username has already been created or if no user class has been specified
     */
   public void createUser(String username, Set<UserClass> classes) throws TicketException {
        if(users.containsKey(username) || classes.size()==0) throw new TicketException();
        users.put(username, new User(username,classes));
    }
    
   /**
    * Retrieves the user classes for a given user
    * 
    * @param username name of the user
    * @return the set of user classes the user belongs to
    */
    public Set<UserClass> getUserClasses(String username){
        if(!users.containsKey(username)) return null; // should never happen!
        return users.get(username).getClasses() ;
    }
    
    /**
     * Creates a new component
     * 
     * @param name unique name of the new component
     * @throws TicketException if a component with the same name already exists
     */
    public void defineComponent(String name) throws TicketException {
        String path = "/" + name;
        if(components.containsKey(path)) throw new TicketException("Duplicate component name");
        components.put(path,new Component(name));
    }
    
    /**
     * Creates a new sub-component as a child of an existing parent component
     * 
     * @param name unique name of the new component
     * @param parentPath path of the parent component
     * @throws TicketException if the the parent component does not exist or 
     *                          if a sub-component of the same parent exists with the same name
     */
    public void defineSubComponent(String name, String parentPath) throws TicketException {
        String path = parentPath + "/" + name;
        if(components.containsKey(path)) throw new TicketException("Duplicate component name");
        Component parent = components.get(parentPath);
        if(parent==null) throw new TicketException("Non existent parent");
        components.put(path,new Component(name,parent));
    }
    
    /**
     * Retrieves the sub-components of an existing component
     * 
     * @param path the path of the parent
     * @return set of children sub-components
     */
    public Set<String> getSubComponents(String path){
        
//        return components.keySet().stream()
//                .filter( p -> p.matches(path+"/[^/]+"))  // find immediate sub components paths 
//                .map( p -> p.replaceAll("^.*/", ""))  // remove leading part to extract simple name
//                .collect(Collectors.toSet());
        
        // OR
        Component parent = components.get(path);
        return components.values().stream()
                .filter(c ->  c.getParent()==parent)
                .map(Component::getName)
                .collect(Collectors.toSet())
                ;
    }

    /**
     * Retrieves the parent component name, or <code>null</code> if there is no parent
     * 
     * @param path the path of the parent
     * @return name of the parent
     */
    public String getParentComponent(String path){
        Component c = components.get(path);
        if(c==null) return null;
        Component p = c.getParent();
        return (p==null?null:p.getPath());
    }

    /**
     * Opens a new ticket to report an issue/malfunction
     * 
     * @param username name of the reporting user
     * @param componentPath path of the component or sub-component
     * @param description description of the malfunction
     * @param severity severity level
     * 
     * @return unique id of the new ticket
     * 
     * @throws TicketException if the user name is not valid, the path does not correspond to a defined component, 
     *                          or the user does not belong to the Reporter {@link IssueManager.UserClass}.
     */
   public int openTicket(String username, String componentPath, String description, Ticket.Severity severity) throws TicketException {
        User user = users.get(username);
        if(user==null) throw new TicketException("Invalid user name");
        Component comp = components.get(componentPath);
        if(comp==null) throw new TicketException("Invalid component");
        
//        if(!user.getClasses().contains(IssueManager.UserClass.Reporter)) 
//                        throw new TicketException("User is not allowed to report issues");
        
        // OR perform the check inside the constructor
        
        Ticket t = new Ticket(++ticketId,user,comp,description,severity);
        tickets.add(t);
        return ticketId;
    }
    
   /**
    * Returns a ticket object given its id
    * 
    * @param ticketId id of the tickets
    * @return the corresponding ticket object
    */
    public Ticket getTicket(int ticketId){
        return tickets.get(ticketId-1);
    }
    
    /**
     * Returns all the existing tickets sorted by severity
     * 
     * @return list of ticket objects
     */
   public List<Ticket> getAllTickets(){
        ArrayList<Ticket> res = new ArrayList<>(tickets);
        Collections.sort(res,Comparator.comparing(Ticket::getSeverity));
        return res;
    }
    
   /**
    * Assign a maintainer to an open ticket
    * 
    * @param ticketId  id of the ticket
    * @param username  name of the maintainer
    * @throws TicketException if the ticket is in state <i>Closed</i>, the ticket id or the username
    *                          are not valid, or the user does not belong to the <i>Maintainer</i> user class
    */
    public void assingTicket(int ticketId, String username) throws TicketException {
        
        if(ticketId<1 || ticketId>tickets.size()) throw new TicketException("Invalid ticket id");
        Ticket t = tickets.get(ticketId-1);
        User user = users.get(username);
        if(user == null) throw new TicketException("Invalid user name");
        
//        if(!user.getClasses().contains(IssueManager.UserClass.Maintainer)) 
//            throw new TicketException("User is not allowed to be a maintainer");
//      if(t.getState()==Ticket.State.Closed) 
//          throw new TicketException("Cannot close ticket not assigned");
        
        // OR perform the checks inside the method
      
        t.assignTo(user);
    }

    /**
     * Closes a ticket
     * 
     * @param ticketId id of the ticket
     * @param description description of how the issue was handled and solved
     * @throws TicketException if the ticket is not in state <i>Assigned</i>
     */
    public void closeTicket(int ticketId, String description) throws TicketException {
        if(ticketId<1 || ticketId>tickets.size()) throw new TicketException("Invalid ticket id");
        Ticket t = tickets.get(ticketId-1);
        
//        if(t.getState()!=Ticket.State.Assigned) 
//            throw new TicketException("Cannot close ticket not assigned");
        // or perform the check inside the close method

        t.close(description);
    }


    /**
     * returns a sorted map (keys sorted in natural order) with the number of  
     * tickets per Severity, considering only the tickets with the specific state.
     *  
     * @param state state of the tickets to be counted, all tickets are counted if <i>null</i>
     * @return a map with the severity and the corresponding count 
     */
    public SortedMap<Ticket.Severity,Long> countBySeverityOfState(Ticket.State state){
        
        Predicate<Ticket> which = t -> state==null||t.getState()==state;
        
        return tickets.stream()
                .filter(which)
                .collect(Collectors.groupingBy(Ticket::getSeverity,
                                               TreeMap::new,
                                               Collectors.counting())
                        )
                ;
    }

    
    /**
     * Find the top maintainers in terms of closed tickets.
     * 
     * The elements are strings formatted as <code>"username:###"</code> where <code>username</code> 
     * is the user name and <code>###</code> is the number of closed tickets. 
     * The list is sorter by descending number of closed tickets and then by username.

     * @return A list of strings with the top maintainers.
     */
    public List<String> topMaintainers(){
        
        return users.values().stream()
                    // only maintainers
                .filter( u -> u.getClasses().contains(UserClass.Maintainer) )
                    // sort them by descending closed tickets and then by user name
                .sorted( Comparator.comparingInt(User::countClosed).reversed()
                                    .thenComparing(User::getName) )
                    // map the users to strings
                .map( u -> u.getName() + ":" + u.countClosed())
                    // collect the strings into a list
                .collect(Collectors.toList())
        ;
        // OR
        
//        return tickets.stream()
//                .filter( t -> t.getState()==Ticket.State.Closed )
//                .collect(Collectors.groupingBy(Ticket::getAssignee,Collectors.counting()))
//                .entrySet().stream()
//                .sorted(Comparator.comparing((Map.Entry<String,Long> e) -> e.getValue(),Comparator.reverseOrder())
//                                  .thenComparing(Map.Entry::getKey))
//                .map(e->e.getKey() + ":" + e.getValue())
//                .collect(Collectors.toList())
//                ;
        
        
    }
}
