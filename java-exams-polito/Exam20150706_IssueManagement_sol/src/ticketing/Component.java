package ticketing;

public class Component {

    private String name;
    private Component parent;
    public Component(String name, Component parent) {
        this.name = name;
        this.parent = parent;
    }
    public Component(String name) {
        this.name = name;
        this.parent = null;
    }
    public String getName() {
        return name;
    }
    public Component getParent() {
        return parent;
    }
    public String getPath() {
        return (parent!=null?parent.getPath():"") + "/" + name;
    }
    
    
}
