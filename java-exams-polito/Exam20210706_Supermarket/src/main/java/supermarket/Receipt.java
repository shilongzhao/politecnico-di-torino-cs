package supermarket;

import java.util.ArrayList;
import java.util.List;

public class Receipt {
    private int code;
    private boolean closed = false;
    private int pointsCardCode = -1;
    private List<String> products = new ArrayList<>();
    private int redeemedPoints = 0;

    public Receipt(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public int getPointsCardCode() {
        return pointsCardCode;
    }

    public void setPointsCardCode(int pointsCardCode) {
        this.pointsCardCode = pointsCardCode;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public int getRedeemedPoints() {
        return redeemedPoints;
    }

    public void setRedeemedPoints(int redeemedPoints) {
        this.redeemedPoints = redeemedPoints;
    }
}
