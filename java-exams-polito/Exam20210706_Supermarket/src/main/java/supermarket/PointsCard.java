package supermarket;

public class PointsCard {
    private int id;
    private String customerName;
    private String dateBirth;
    private int redeemedPoints = 0;
    private int totalPoints = 0;

    public PointsCard(int id, String customerName, String dateBirth) {
        this.id = id;
        this.customerName = customerName;
        this.dateBirth = dateBirth;
    }

    public int getId() {
        return id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void addToRedeemedPoints(int redeemedPoints) {
        this.redeemedPoints += redeemedPoints;
    }

    public int getCurrentPoints() {
        return totalPoints - redeemedPoints;
    }

    public void addToTotalPoints(int amount) {
        totalPoints += amount;
    }

    public int getTotalPoints() {
        return totalPoints;
    }
}
