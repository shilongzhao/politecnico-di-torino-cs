package supermarket;

import java.time.ZonedDateTime;

public class Discount {
    private int percentage = 0;
    private String category;
    private String product;
    private ZonedDateTime dateTime;

    public Discount(int percentage, String category, String product, ZonedDateTime dateTime) {
        this.percentage = percentage;
        this.category = category;
        this.product = product;
        this.dateTime = dateTime;
    }

    public int getPercentage() {
        return percentage;
    }

    public String getCategory() {
        return category;
    }

    public String getProduct() {
        return product;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}
