package supermarket;

public class Product {
    private String name;
    private double price;
    private String category;

    public Product(String category, String name, double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }
}
