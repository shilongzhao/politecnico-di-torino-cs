package supermarket;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class Supermarket {

	private int nextCardId = 1000;
	private int nextPurchaseCode = 100;
	private int nextReceiptCode = 0;

	private Map<String, List<Discount>> categoryDiscountsMap = new HashMap<>();
	private Map<String, List<Discount>> productDiscountsMap = new HashMap<>();

	private Map<String, Set<Product>> categoryProductsMap = new HashMap<>();
	private Map<String, Product> productMap = new HashMap<>();

	private Map<String, PointsCard> cardMap = new HashMap<>();
	private Map<Integer, PointsCard> cardIdMap = new HashMap<>();

	private SortedMap<Integer, Integer> pointsToPercentageMap = new TreeMap<>();

	private Map<Integer, Purchase> purchaseMap = new HashMap<>();

	private Map<Integer, Receipt> receiptMap = new HashMap<>();
	//R1
	public int addProducts (String categoryName, String productNames, String productPrices) throws SMException {
		String[] names = productNames.split(",");
		String[] prices = productPrices.split(",");

		if (names.length != prices.length) throw new SMException("");
		if (categoryProductsMap.containsKey(categoryName)) throw new SMException("");
		for (String name: names) {
			if (productMap.containsKey(name)) throw new SMException("");
		}

		ZonedDateTime now = ZonedDateTime.now();
		Set<Product> productsOfCat = new HashSet<>();
		for (int i = 0; i < names.length; i++) {
			Product p = new Product(categoryName, names[i], Double.parseDouble(prices[i]));
			productMap.put(names[i], p);
			productsOfCat.add(p);

			List<Discount> productDiscounts = new ArrayList<>();
			productDiscounts.add(new Discount(0, null, names[i], now));
			productDiscountsMap.put(names[i], productDiscounts);
		}

		categoryProductsMap.put(categoryName, productsOfCat);

		List<Discount> categoryDiscounts = new ArrayList<>();
		categoryDiscounts.add(new Discount(0, categoryName, null, now));
		this.categoryDiscountsMap.put(categoryName, categoryDiscounts);

		return names.length;
	}

	public double getPrice (String productName) throws SMException {
		if (!productMap.containsKey(productName)) throw new SMException("");
		return productMap.get(productName).getPrice();
	}

	public SortedMap<String,String> mostExpensiveProductPerCategory () {
		SortedMap<String, String> map = new TreeMap<>();

		categoryProductsMap.forEach((k, s) -> {
			Product p = s.stream().max(Comparator.comparingDouble(Product::getPrice)).get();
			map.put(k, p.getName());
		});
		return map;
	}

	//R2
	public void setDiscount (String categoryName, int percentage) throws SMException {
		if (percentage < 0 || percentage > 40) throw new SMException("");
		if (!categoryProductsMap.containsKey(categoryName)) throw new SMException("");
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		categoryDiscountsMap.get(categoryName).add(new Discount(percentage, categoryName, null, ZonedDateTime.now()));
	}

	public void setDiscount (int percentage, String... productNames) throws SMException {
		for (String name: productNames) {
			if (!productMap.containsKey(name)) throw new SMException("");
		}

		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ZonedDateTime now = ZonedDateTime.now();
		for (String name: productNames) {
			productDiscountsMap.get(name).add(new Discount(percentage, null, name, now));
		}
	}

	public List<Integer> getDiscountHistoryForCategory(String categoryName) {
		return categoryDiscountsMap.get(categoryName).stream().
				map(Discount::getPercentage).collect(Collectors.toList());
	}

	public List<Integer> getDiscountHistoryForProduct(String productName) {
		List<Discount> allDiscounts = new ArrayList<>(productDiscountsMap.get(productName));
		Product p = productMap.get(productName);
		allDiscounts.addAll(categoryDiscountsMap.get(p.getCategory()));

		return allDiscounts.stream().sorted(Comparator.comparing(Discount::getDateTime))
				.map(Discount::getPercentage).distinct().collect(Collectors.toList());
	}

	//R3
	public int issuePointsCard (String name, String dateOfBirth) throws SMException {
		if (cardMap.containsKey(name + dateOfBirth)) throw new SMException("");
		PointsCard card = new PointsCard(nextCardId, name, dateOfBirth);
		cardMap.put(name + dateOfBirth, card);
		cardIdMap.put(card.getId(), card);
		nextCardId += 1;
		return card.getId();
	}



	public void fromPointsToDiscounts (String points, String discounts) throws SMException {
		String[] pointList = points.split(",");
		String[] discountList = discounts.split(",");
		if (pointList.length != discountList.length) throw new SMException("");
		for (int i = 0; i < pointList.length; i++) {
			pointsToPercentageMap.put(Integer.parseInt(pointList[i]), Integer.parseInt(discountList[i]));
		}
	}

	public SortedMap<Integer, Integer>  getMapPointsDiscounts() {
		return pointsToPercentageMap;
	}

	// it's an ABSOLUTE value! not a percentage!!!
	public int getDiscountFromPoints (int points) {
		return pointsToPercentageMap.getOrDefault(points, 0);
	}

	//R4

	public int getCurrentPoints (int pointsCardCode) throws SMException {
		if (!cardIdMap.containsKey(pointsCardCode)) throw new SMException("");
		return cardIdMap.get(pointsCardCode).getCurrentPoints();
	}

	public int getTotalPoints (int pointsCardCode) throws SMException {
		if (!cardIdMap.containsKey(pointsCardCode)) throw new SMException("");
		return cardIdMap.get(pointsCardCode).getTotalPoints();
	}

	public int addPurchase (int pointsCardCode, int pointsRedeemed, String ... productNames) throws SMException {
		if (getCurrentPoints(pointsCardCode) < pointsRedeemed) throw new SMException("");
		List<String> names = new ArrayList<>(Arrays.asList(productNames));
		Purchase purchase = new Purchase(nextPurchaseCode, pointsCardCode, pointsRedeemed, names);
		purchaseMap.put(nextPurchaseCode, purchase);

		double price = getPurchasePrice(purchase.getPurchaseCode());
		PointsCard card = cardIdMap.get(pointsCardCode);
		card.addToTotalPoints((int) Math.round(price));
		if (getDiscountFromPoints(pointsRedeemed) != 0) {
			card.addToRedeemedPoints(pointsRedeemed);
		}

		nextPurchaseCode += 1;
		return purchase.getPurchaseCode();
	}

	public double getPurchasePrice (int purchaseCode) throws SMException {
		if (!purchaseMap.containsKey(purchaseCode)) throw new SMException("");
		List<String> productList = purchaseMap.get(purchaseCode).getProducts();

		double total = productList.stream().map(productMap::get)
				.map(Product::getPrice)
				.reduce(0.0, Double::sum);

		double purchaseDiscount = getPurchaseDiscount(purchaseCode);
		return total - purchaseDiscount;
	}

	public double getPurchaseDiscount (int purchaseCode) throws SMException {
		if (!purchaseMap.containsKey(purchaseCode)) throw new SMException("");
		Purchase purchase = purchaseMap.get(purchaseCode);
		List<String> productList = purchase.getProducts();

		double discountByPoints = getDiscountFromPoints(purchase.getRedeemedPoints());

		double discountOfProdOrCategory = productList.stream().map(productMap::get)
				.map(p -> {
					List<Discount> discounts = new ArrayList<>(productDiscountsMap.get(p.getName()));
					discounts.addAll(categoryDiscountsMap.get(p.getCategory()));
					discounts.sort((a,b) -> b.getDateTime().compareTo(a.getDateTime()));
					return p.getPrice() * discounts.get(0).getPercentage() / 100.0;
				})
				.reduce(0.0, Double::sum);

		return (discountOfProdOrCategory + discountByPoints);
	}
	
	//R5

	public SortedMap<Integer, List<Integer>> pointsCardsPerTotalPoints () {
		TreeMap<Integer, List<Integer>> result = new TreeMap<>();
		Collection<PointsCard> pointsCards = cardIdMap.values().stream().filter(c -> c.getTotalPoints() != 0).collect(Collectors.toList());
		for (PointsCard card: pointsCards) {
			List<Integer> ids = result.getOrDefault(card.getTotalPoints(), new ArrayList<>());
			ids.add(card.getId());
			Collections.sort(ids);
			result.put(card.getTotalPoints(), ids);
		}
		return result;
	}


	public SortedMap<String, SortedSet<String>> customersPerCategory () {
		SortedMap<String, SortedSet<String>> result = new TreeMap<>();
		for (Purchase purchase: purchaseMap.values()) {
			PointsCard pointsCard = cardIdMap.get(purchase.getPointsCardCode());
			List<Product> products = purchase.getProducts().stream().map(productMap::get).collect(Collectors.toList());
			for (Product product: products) {
				SortedSet<String> customersOfCat = result.getOrDefault(product.getCategory(), new TreeSet<>());
				customersOfCat.add(pointsCard.getCustomerName());
				result.put(product.getCategory(), customersOfCat);
			}
		}
		return result;
	}

	public SortedMap<Integer, List<String>> productsPerDiscount() {
		SortedMap<Integer, List<String>> result = new TreeMap<>((a, b) -> b - a);

		for (Product p: productMap.values()) {
			List<Discount> discountsOfProd = new ArrayList<>(productDiscountsMap.get(p.getName()));
			discountsOfProd.addAll(categoryDiscountsMap.get(p.getCategory()));
			discountsOfProd = discountsOfProd.stream().filter(d -> d.getPercentage() != 0).collect(Collectors.toList());
			for (Discount discount: discountsOfProd) {
				List<String> productNames = result.getOrDefault(discount.getPercentage(), new ArrayList<>());

				if (!productNames.contains(p.getName()))
					productNames.add(p.getName());

				Collections.sort(productNames);
				result.put(discount.getPercentage(), productNames);
			}
		}

		return result;
	}


	// R6

	public int newReceipt() { // return code of new receipt
		Receipt r = new Receipt(nextReceiptCode);
		receiptMap.put(r.getCode(), r);
		nextReceiptCode += 1;
		return r.getCode();
	}

	public void receiptAddCard(int receiptCode, int pointCardCode )  throws SMException { // add the points card info to the receipt
		if (!receiptMap.containsKey(receiptCode)) throw new SMException("");
		Receipt receipt = receiptMap.get(receiptCode);
		if (receipt.isClosed()) throw new SMException("");
		if (!cardIdMap.containsKey(pointCardCode)) throw new SMException("");
		receipt.setPointsCardCode(pointCardCode);
	}

	public int receiptGetPoints(int receiptCode)  throws SMException { // return available points on points card if added before
		if (!receiptMap.containsKey(receiptCode)) throw new SMException("");
		Receipt receipt = receiptMap.get(receiptCode);
		if (receipt.getPointsCardCode() == -1) throw new SMException("");
		return cardIdMap.get(receipt.getPointsCardCode()).getCurrentPoints();
	}

	public void receiptAddProduct(int receiptCode, String product)  throws SMException { // add a new product to the receipt
		if (!receiptMap.containsKey(receiptCode)) throw new SMException("");
		Receipt receipt = receiptMap.get(receiptCode);
		if (receipt.isClosed()) throw new SMException("");
		if (!productMap.containsKey(product)) throw new SMException("");
		receipt.getProducts().add(product);
	}

	public double receiptGetTotal(int receiptCode)  throws SMException { // return current receipt code
		if (!receiptMap.containsKey(receiptCode)) throw new SMException("");
		Receipt receipt = receiptMap.get(receiptCode);
		List<String> productList = receipt.getProducts();

		double total = productList.stream().map(productMap::get)
				.map(Product::getPrice)
				.reduce(0.0, Double::sum);

		double discountOfProdOrCategory = productList.stream().map(productMap::get)
				.map(p -> {
					List<Discount> discounts = new ArrayList<>(productDiscountsMap.get(p.getName()));
					discounts.addAll(categoryDiscountsMap.get(p.getCategory()));
					discounts.sort((a,b) -> b.getDateTime().compareTo(a.getDateTime()));
					return p.getPrice() * discounts.get(0).getPercentage() / 100.0;
				})
				.reduce(0.0, Double::sum);

		return total - discountOfProdOrCategory;
	}

	public void receiptSetRedeem(int receiptCode, int points)  throws SMException { // sets the amount of points to be redeemed
		if (!receiptMap.containsKey(receiptCode)) throw new SMException("");
		Receipt receipt = receiptMap.get(receiptCode);
		PointsCard card = cardIdMap.get(receipt.getPointsCardCode());
		if (points > card.getCurrentPoints()) throw new SMException("");
		if (!pointsToPercentageMap.containsKey(points)) throw new SMException("");
		receipt.setRedeemedPoints(points);
		card.addToRedeemedPoints(points);
	}

	public int closeReceipt(int receiptCode)  throws SMException { // close the receipt and add the purchase (calls addPurchase() ) and return purchase code (could be the same as receipt code)
		if (!receiptMap.containsKey(receiptCode)) throw new SMException("");
		Receipt receipt = receiptMap.get(receiptCode);
		int pc = addPurchase(receipt.getPointsCardCode(), receipt.getRedeemedPoints(), receipt.getProducts().toArray(new String[0]));
		receipt.setClosed(true);
		return pc;
	}

}