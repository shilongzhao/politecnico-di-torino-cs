package supermarket;

import java.util.List;

public class Purchase {
    private int purchaseCode;
    private int pointsCardCode;
    private int redeemedPoints;
    private List<String> products;

    public Purchase(int purchaseCode, int pointsCardCode, int redeemedPoints, List<String> products) {
        this.purchaseCode = purchaseCode;
        this.pointsCardCode = pointsCardCode;
        this.redeemedPoints = redeemedPoints;
        this.products = products;
    }

    public int getPurchaseCode() {
        return purchaseCode;
    }

    public int getPointsCardCode() {
        return pointsCardCode;
    }

    public List<String> getProducts() {
        return products;
    }

    public int getRedeemedPoints() {
        return redeemedPoints;
    }
}
