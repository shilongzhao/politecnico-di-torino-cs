package it.polito.oop.vaccination;

public class Person {
	
	private String ssn;
	private int year;
	private String first;
	private String last;
	
	public Person(String first, String last, String ssn, int year) {
		this.first=first;
		this.last=last;
		this.ssn=ssn;
		this.year=year;
		}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	@Override
	public String toString() {
		return ssn + "," + last + "," + first + "," + year;
	}
	
	

}
