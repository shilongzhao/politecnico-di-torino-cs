package ticketing;

import java.util.ArrayList;
import java.util.List;

public class Utente {
    private  String nickname;
    private String name;
    private String email;
    private String password;
    private List<Ticket> tickets = new ArrayList<>();
    public Utente(String nickname, String name, String email, String password) {
        this.nickname = nickname;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getNickname(){
        return nickname;
    }

    public String getName(){
        return name;
    }
    
    public String getEmail(){
        return email;
    }
    
    public boolean authenticate(String pwd){
        return pwd.equals(password);
    }

    public void addTicket(Ticket t) {
        tickets.add(t);
    }

    public long numeroTicket(){
        return tickets.size();
    }
    
}
