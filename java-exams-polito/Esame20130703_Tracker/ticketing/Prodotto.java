package ticketing;


import java.util.ArrayList;
import java.util.List;

public class Prodotto {
    private static int COUNT = 1;
    private String nome;
    private String descrizione;
    private String codice;
    private List<Ticket> tickets = new ArrayList<>();
    public Prodotto(String nome, String descrizione) {
        this.nome = nome;
        this.descrizione = descrizione;
        this.codice = "P" + COUNT;
        COUNT++;
    }

    public String getNome(){
        return nome;
    }

    public String getDescrizione(){
        return descrizione;
    }

    public String getCodice(){
        return codice;
    }

    public void addTicket(Ticket t) {
        tickets.add(t);
    }
    public long numeroTicket(){
        return tickets.size();
    }
    
}
