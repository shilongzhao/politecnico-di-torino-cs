package ticketing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Ticket {
    private static long COUNT = 1;
    private long codice;
    private Utente creatore;
    private Prodotto prodotto;
    private String etichetta;
    private long timestamp;
    private Tracker tracker;
    private List<Commento> commenti = new ArrayList<>();

    public Ticket(Utente creatore, Prodotto prodotto, String etichetta, Tracker tracker) {
        this.creatore = creatore;
        this.prodotto = prodotto;
        this.etichetta = etichetta;
        this.codice = COUNT++;
        this.timestamp = System.currentTimeMillis();
        this.tracker = tracker;
    }

    public long getCodice(){
        return codice;
    }
    public Utente getCreatore() {
        return creatore;
    }
    public Prodotto getProdotto() {
        return prodotto;
    }
    public String getEtichetta() {
        return etichetta;
    }
    public long getTimestamp() {
        return timestamp;
    }
    
    public Commento nuovoCommento(String nick, String testo){
        Utente u = tracker.getUtente(nick);
        Commento c = new Commento(u, this, testo);
        commenti.add(c);
        return c;
    }


    public List<Commento> getCommenti(){
        return commenti.stream().sorted(Comparator.comparing(Commento::getTimestamp).reversed()).collect(Collectors.toList());
    }
}
