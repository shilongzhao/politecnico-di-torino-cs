package ticketing;

public class Commento {
    private Utente autore;
    private Ticket ticket;
    private String testso;
    private long timestamp;

    public Commento(Utente autore, Ticket ticket, String testso) {
        this.autore = autore;
        this.ticket = ticket;
        this.testso = testso;
        this.timestamp = System.currentTimeMillis();
    }

    public Utente getAutore() {
        return autore;
    }
    public Ticket getTicket() {
        return ticket;
    }
    public String getTesto() {
        return testso;
    }
    public long getTimestamp() {
        return timestamp;
    }
    
    
}
