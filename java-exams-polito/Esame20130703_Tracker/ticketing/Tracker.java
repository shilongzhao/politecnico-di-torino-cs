package ticketing;

import java.util.*;
import java.util.stream.Collectors;

public class Tracker {

    private String url;
    Map<String, Utente> utenti = new HashMap<>();
    Map<String, Prodotto> prodotti = new HashMap<>();
    Map<Long, Ticket> tickets = new HashMap();

    public Tracker(String url){
        this.url = url;
    }
    
    public String getURL(){
        return url;
    }
    
    public void nuovoUtente(String nick, String nome, String email, String pwd) throws InvalidInformationException {
        if (nick == null || email == null || pwd == null)
            throw new InvalidInformationException();
        utenti.put(nick, new Utente(nick, nome, email, pwd));
    }
    
    public Utente getUtente(String nick){
        return utenti.get(nick);
    }
    
    public Collection<Utente> getUtenti(){
        return utenti.values();
    }
    
    public String nuovoProdotto(String nome, String descrizione) throws InvalidInformationException {
        if (nome == null || descrizione == null)
            throw new InvalidInformationException();
        Prodotto p = new Prodotto(nome, descrizione);
        prodotti.put(p.getCodice(), p);
        return p.getCodice();
    }
    
    public Prodotto getProdotto(String code){
        return prodotti.get(code);
    }

    public Collection<Prodotto> getProdotti(){
        return prodotti.values();
    }
    
    public Ticket nuovoTicket(String code, String nick, String label){
        Ticket t = new Ticket(utenti.get(nick), prodotti.get(code), label, this);
        tickets.put(t.getCodice(), t);
        prodotti.get(code).addTicket(t);
        utenti.get(nick).addTicket(t);
        return t;
    }
    
    public Ticket getTicket(long code){
        return tickets.get(code);
    }
    
    public List<Ticket> getTickets(){
        return tickets.values().stream().
                sorted(Comparator.comparingLong(Ticket::getTimestamp).reversed()).
                collect(Collectors.toList());
    }
    
    public List<Prodotto> prodottiPerTicket(){
        return prodotti.values().stream().
                sorted(Comparator.comparingLong(Prodotto::numeroTicket).reversed()).
                collect(Collectors.toList());
    }
    
    public List<Utente> utentiPerTicket(){
        return utenti.values().stream().
                sorted(Comparator.comparingLong(Utente::numeroTicket).reversed()).
                collect(Collectors.toList());
    }
    
    
}
