package travels;

import java.util.*;
import java.util.stream.Collectors;

public class TravelHandling {
	// R1
	private List<String> customers = new ArrayList<>();
	private Map<String, List<String>> destinations = new HashMap<>(); // destinationName, list of operators
	private Map<String, List<String>> operators = new HashMap<>(); // operatorsName,destinationName
	private Map<String, String> proposals = new HashMap<>(); // proposal, destinationName
	private Map<String, List<String>> proposalUsers = new HashMap<>(); // proposalName, userNames
	private Map<String, List<String>> proposalOperators = new HashMap<>();// proposalName, Operators
	private Map<String, List<Quote>> quotes = new HashMap<>();//proposalName, quote
	private Map<String, Operator> operatorQuotes = new HashMap<>(); //operatorName, Operator

	public void addCustomers(String... userNames) {
		for (String s : userNames)
			customers.add(s);
	}

	public void addDestination(String destinationName, String... operatorNames)
			throws TravelException {
		if (destinations.containsKey(destinationName))
			throw new TravelException("Repeat destination name!");
		List<String> opNames = new ArrayList<>();
		for (String s : operatorNames) {
			opNames.add(s);
			if (!operators.containsKey(s)) {
				operators.put(s, new ArrayList<>());
				operators.get(s).add(destinationName);
			} else {
				operators.get(s).add(destinationName);
			}
		}
		destinations.put(destinationName, opNames);
	}

	public List<String> getDestinations(String operatorName) {
		return operators.get(operatorName).stream().sorted()
				.collect(Collectors.toList());
	}

	// R2
	public void addProposal(String name, String destinationName)
			throws TravelException {
		if (proposals.containsKey(name))
			throw new TravelException("Already has this proposal name!");
		if (!destinations.containsKey(destinationName))
			throw new TravelException("Does not exist this destination!");
		proposals.put(name, destinationName);
	}

	public List<String> setUsers(String proposalName, String... userNames) {
		List<String> result = new ArrayList<>();
		proposalUsers.put(proposalName, new ArrayList<>());
		for (String s : userNames) {
			if (!customers.contains(s))
				result.add(s);
			else
				proposalUsers.get(proposalName).add(s);
			
		}
		return result.stream().sorted().collect(Collectors.toList());
	}

	public SortedSet<String> getUsersOLD(String proposalName) {
		return null;
	}

	// R3
	public List<String> setOperators(String proposalName,
			String... operatorNames) {
		List<String> result = new ArrayList<>();
		proposalOperators.put(proposalName, new ArrayList<>());
		for (String s : operatorNames) {
			if ((!operators.containsKey(s))
					|| (!operators.get(s).contains(proposals.get(proposalName))))
				result.add(s);
			else
				proposalOperators.get(proposalName).add(s);
		}
		return result.stream().sorted().collect(Collectors.toList());
	}

	public SortedSet<String> getOperatorsOLD(String proposalName) {
		return null;
	}

	public void addQuote(String proposalName, String operatorName, int amount)
			throws TravelException {
		if (!proposalOperators.get(proposalName).contains(operatorName))
			throw new TravelException(
					"The operator does not associate with proposal");
		Quote q = new Quote(amount, proposalName, operatorName, proposals.get(proposalName));
		if(!quotes.containsKey(proposalName)){
			quotes.put(proposalName, new ArrayList<>());
			quotes.get(proposalName).add(q);
		}
		else {
			quotes.get(proposalName).add(q);
		}
		Operator o = new Operator(operatorName);
		if(!operatorQuotes.containsKey(operatorName))
			operatorQuotes.put(operatorName, o);
		else
			operatorQuotes.get(operatorName).addQuote();
	}

	public List<Quote> getQuotes(String proposalName) {
		return quotes.get(proposalName).stream()
				.sorted(Comparator.comparing(Quote::getOperatorName)).collect(Collectors.toList());
	}

	// R4
	public void makeChoice(String proposalName, String userName,
			String operatorName) throws TravelException {
		if(!proposalUsers.get(proposalName).contains(userName)) throw new TravelException("This user does not belong the proposal.");
		if(!quotes.containsKey(proposalName) ||
				quotes.get(proposalName).stream().noneMatch(q -> q.getOperatorName().equals(operatorName)))  throw new TravelException("This operator does not fit the proposal");
		for(Quote q : quotes.get(proposalName))
			if(q.getOperatorName().equals(operatorName)){
				q.addUsers(userName);
				break;
			}
	}

	public Quote getWinningQuote(String proposalName) {
		if(!quotes.containsKey(proposalName) )
			return null;
		return quotes.get(proposalName).stream()
				.sorted(Comparator.comparing(Quote::getNChoices).reversed().
						thenComparing((Quote::getAmount)) )
				.findFirst().get();
	}

	// R5
	public SortedMap<String, Integer> totalAmountOfQuotesPerDestination() {
		return quotes.values().stream().flatMap(q -> q.stream())
				.collect(Collectors.groupingBy(Quote::getDestination, TreeMap::new, Collectors.summingInt(Quote::getAmount))); 		
	}

	public SortedMap<Integer, List<String>> operatorsPerNumberOfQuotes() {
		 return operatorQuotes.values().stream().sorted(Comparator.comparing(Operator::getName))
				 .collect(Collectors.groupingBy(Operator::getNumQuote,() -> new TreeMap<>(Collections.reverseOrder()),
						 Collectors.mapping(Operator::getName, Collectors.toList())));
	}

	public SortedMap<String, Long> numberOfUsersPerDestination() {
		TreeMap<String, Long> result = new TreeMap<>();
		for(Map.Entry<String, List<String>> entry : proposalUsers.entrySet()){
			result.put(proposals.get(entry.getKey()), (long) entry.getValue().size());
		}
		return result;
	}
}

/*
 * 
 * public void setWinningQuote (String proposalName) { Proposal p =
 * proposals.get(proposalName); List<Quote> list = p.getQuotes(); if
 * (list.size() > 0) { Optional<Quote> quote = list.stream()
 * .max(comparing(Quote::getNUsers,naturalOrder())
 * .thenComparing(Quote::getAmount, reverseOrder()));
 * p.setWinningQuote(quote.get()); } }
 * 
 * public List<Quote> getUserQuotesOLD (String userName) { User user =
 * users.get(userName); return user.getQuotes(); }
 */