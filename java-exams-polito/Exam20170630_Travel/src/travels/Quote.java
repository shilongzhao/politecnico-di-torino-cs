package travels;

import java.util.ArrayList;
import java.util.List;


public class Quote {
	private int amount;
	private String proposalName;
	private String operatorName;
	private List<String> users = new ArrayList<>();
	private String destination;
	
	public Quote (int amount, String proposalName, String operatorName, String destination) {
		this.amount = amount;
		this.operatorName = operatorName;
		this.proposalName = proposalName;
		this.destination = destination;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public void addUsers(String user) {
		users.add(user);
	}
	
	public int getAmount() {
	    return amount;
	}
	
	public String getProposalName() {
	    return proposalName;
	}
	public String getOperatorName() {
        return operatorName;
        }
	public int getNChoices() {
        return users.size();
        }
		
}
