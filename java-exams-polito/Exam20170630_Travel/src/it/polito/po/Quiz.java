package it.polito.po;

public class Quiz {
	final static public String[] questions = {
	"What is specified in the middle section of a UML class?",
	"How does SVN resolve conflicts?",
	"What is the main limitation of the waterfall software development process?"
	};
	final static public String[][] options = {
	{
		"List of methods",
		"List of static attributes",
		"Name of the package",
		"List of static methods",
		"List of attributes"	},
	{
		"Copy-Modify-Merge",
		"Check-in/Check-out",
		"Lock-Unlock-Modify",
		"Modify-Commit-Unlock",
		"Lock-Modify-Unlock"	},
	{
		"User involvement and close collaboration are required throughout the development cycle",
		"It does not put enough emphasis on documentation",
		"It is not sufficiently formal",
		"Phases are arranged in a rigidly sequential order",
		"It puts too much emphasis on the development"	}
	};
	
	/**
	 * Return the index of the right answer(s) for the given question 
	 */
	public static int[] answer(int question){
		// TODO: answer the question
		
		switch(question){
			case 0: return null; // replace with your answers
			case 1: return null; // replace with your answers
			case 2: return null; // replace with your answers
		}
		return null; // means: "No answer"
	}

	/**
	 * When executed will show the answers you selected
	 */
	public static void main(String[] args){
		for(int q=0; q<questions.length; ++q){
			System.out.println("Question: " + questions[q]);
			int[] a = answer(q);
			if(a==null || a.length==0){
				System.out.println("<undefined>");
				continue;
			}
			System.out.println("Answer" + (a.length>1?"s":"") + ":" );
			for(int i=0; i<a.length; ++i){
				System.out.println(a[i] + " - " + options[q][a[i]]);
			}
		}
	}
}

