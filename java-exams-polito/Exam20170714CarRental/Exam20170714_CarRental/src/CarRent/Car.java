package CarRent;

public class Car {	
	String location;
	String model;
	double fare;
	boolean vacant;
	
	public Car(String location,String carModel,double fare)
	{
		this.vacant = true;
		this.location = location;
		this.model = carModel;
		this.fare = fare;
	}
	public String getLocation() {
		return location;
	}

	public String getModel() {
		return model;
	}

	public double getFare() {
		return fare;
	}
	
	
	public boolean isVacant() {
		return vacant;
	}
	
	public boolean isBooked() {
		return !vacant;
	}
	public void setVacant(boolean vacant) {
		this.vacant = vacant;
	}
}
