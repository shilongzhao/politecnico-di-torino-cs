package CarRent;

public class Booking {
	String customer;
	Car bookedCar;
	String pickUpLocation;
	String dropOffLocation;
	double price;
	public Booking(String customer, Car bookedCar, String pickUpLocation, String dropOffLocation, double price) {
		super();
		this.customer = customer;
		this.bookedCar = bookedCar;
		this.pickUpLocation = pickUpLocation;
		this.dropOffLocation = dropOffLocation;
		this.price = price;
	}
	
	@Override
	public String toString() {
		return String.format("%s:%s:%s:%s:%.1f", 
				customer, bookedCar.model, pickUpLocation, dropOffLocation, price);
	}
	
	public double getPrice() {
		return price;
	}
	
	public String getModel() {
		return bookedCar.getModel();
	}

}
