package CarRent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class CarRentalAgency {
	Map<String, List<Car>> locationCars = new HashMap<>();
	Map<String, Double> modelPrice = new HashMap<>(); 
	Map<String, Booking> customerBooking = new HashMap<>();
	List<Booking> bookingHistory = new ArrayList<>();
	
	public void addLocation(String location) throws InvalidValue {
		if (locationCars.containsKey(location)) {
			throw new InvalidValue();
		}
		List<Car> cars = new ArrayList<>();
		locationCars.put(location, cars);
	}

	public void addCar(String location, String model, double fare) throws InvalidValue {
		if (!locationCars.containsKey(location) || fare < 1.0) {
			throw new InvalidValue();
		}
		
		Car c = new Car(location, model, fare);
		locationCars.get(location).add(c);
		modelPrice.put(model, fare);
	}

	public Collection<String> getVacantCars(String location) throws InvalidValue {
		if (!locationCars.containsKey(location)) {
			throw new InvalidValue();
		}
		return locationCars.get(location).stream().filter(Car::isVacant).
				sorted(Comparator.comparing(Car::getModel)).map(Car::getModel).collect(Collectors.toList());
	}

	public double getFare(String model) throws InvalidValue {
		if (!modelPrice.containsKey(model)) {
			throw new InvalidValue();
		}
		return modelPrice.get(model);
	}

	public void addCustomer(String card) throws InvalidValue {
		if (customerBooking.containsKey(card)) {
			throw new InvalidValue();
		}
		customerBooking.put(card, null);
	}
	
	public double book(String card, String from, String to, String model,int days) throws InvalidValue {
		if (!customerBooking.containsKey(card) || !locationCars.containsKey(from) ||
				!locationCars.containsKey(to) || !modelPrice.containsKey(model)) {
			throw new InvalidValue();
		}
		if (customerBooking.get(card) != null) {
			return 0.0;
		}
		Car availableCar = null;
		for (Car c: locationCars.get(from)) {
			if (c.isVacant() && c.location.equals(from) && c.model.equals(model)) {
				availableCar = c;
			}
		}
		if (availableCar == null) {
			return 0.0;
		}
		
		double price = days * modelPrice.get(model);
		availableCar.setVacant(false);
		
		Booking b = new Booking(card, availableCar, from, to, price);
		customerBooking.put(card, b);
		return price;
	}

	public Collection<String> getBookedCars(String location) throws InvalidValue {
		if (!locationCars.containsKey(location)) {
			throw new InvalidValue();
		}
		return locationCars.get(location).stream().
				filter(Car::isBooked).sorted(Comparator.comparing(Car::getModel)).
				map(Car::getModel).collect(Collectors.toList());
	}

	public String pickUp(String card) throws InvalidValue {
		if (!customerBooking.containsKey(card)) {
			throw new InvalidValue();
		}
		Booking b = customerBooking.get(card);
		if (b == null) {
			return null;
		}
		
		locationCars.get(b.pickUpLocation).remove(b.bookedCar);
		return b.pickUpLocation;
	}
	
	public String giveBack(String card) throws InvalidValue {
		if (!customerBooking.containsKey(card)) {
			throw new InvalidValue();
		}
		Booking b = customerBooking.get(card);
		if (b == null) {
			return null;
		}
		
		b.bookedCar.setVacant(true);
		locationCars.get(b.dropOffLocation).add(b.bookedCar);
		bookingHistory.add(b);
		customerBooking.put(card, null);
		
		return b.toString();
	}

	public Collection<String> customerRentals(String card) throws InvalidValue {
		if (!customerBooking.containsKey(card)) {
			throw new InvalidValue();
		}
		return bookingHistory.stream().filter(booking -> booking.customer.equals(card)).
				sorted(Comparator.comparing(Booking::getPrice)).
				map(Booking::toString).collect(Collectors.toList()); 
	}

	public Collection<String> rentalsFrom(String location) throws InvalidValue {
		if (!locationCars.containsKey(location)){
			throw new InvalidValue();
		}
		return bookingHistory.stream().
				filter(booking -> booking.pickUpLocation.equals(location)).
				sorted(Comparator.comparing(Booking::getModel)).
				map(booking -> booking.bookedCar.model).collect(Collectors.toList());
	}

	public int modelRentals(String model) throws InvalidValue {
		if (!modelPrice.containsKey(model)) {
			throw new InvalidValue();
		}
		return bookingHistory.stream().
				filter(booking -> booking.bookedCar.model.equals(model)).
				collect(Collectors.counting()).intValue();
	}

	public double averageAmount(String model) throws InvalidValue {
		if (!modelPrice.containsKey(model)) {
			throw new InvalidValue();
		}
		int rentals = modelRentals(model);
		double income = 0;
		for (Booking b: bookingHistory) {
			if (b.bookedCar.model.equals(model)) {
				income+= b.price;
			}
		}
		return income/rentals;
	}
}
