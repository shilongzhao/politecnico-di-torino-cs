package it.polito.po;

public class Quiz {
	final static public String[] questions = {
	"What is specified in the top section of a UML class?",
	"What operation triggers SVN to increment the revision number?",
	"What can be demonstrated by a test?"
	};
	final static public String[][] options = {
	{
		"Name of the class",
		"List of attributes",
		"Name of the package",
		"Implementation details",
		"List of static methods"	},
	{
		"Lock",
		"Update",
		"Commit",
		"Merge",
		"Unlock"	},
	{
		"The absence of defects in the program",
		"The complexity of the program",
		"The correctness of the program",
		"The presence of defects in the program",
		"The maintainability of the program"	}
	};
	
	/**
	 * Return the index of the right answer(s) for the given question 
	 */
	public static int[] answer(int question){
		// TODO: answer the question
		
		switch(question){
			case 0: return null; // replace with your answers
			case 1: return null; // replace with your answers
			case 2: return null; // replace with your answers
		}
		return null; // means: "No answer"
	}

	/**
	 * When executed will show the answers you selected
	 */
	public static void main(String[] args){
		for(int q=0; q<questions.length; ++q){
			System.out.println("Question: " + questions[q]);
			int[] a = answer(q);
			if(a==null || a.length==0){
				System.out.println("<undefined>");
				continue;
			}
			System.out.println("Answer" + (a.length>1?"s":"") + ":" );
			for(int i=0; i<a.length; ++i){
				System.out.println(a[i] + " - " + options[q][a[i]]);
			}
		}
	}
}

