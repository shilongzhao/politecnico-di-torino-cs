package main;

import CarRent.CarRentalAgency;
import CarRent.InvalidValue;

public class MainClass {

	public static void main(String[] args) {
		CarRentalAgency cra = new CarRentalAgency();
		try{
			cra.addLocation("Torino1");
			cra.addLocation("Torino2");
			cra.addLocation("Milano1");
			cra.addLocation("Roma1");
			cra.addCar("Torino1", "Fiat500", 40.0);
			cra.addCar("Torino1", "FiatPanda", 50.0);
			cra.addCar("Torino1", "Fiat500", 45.0);
			cra.addCar("Milano1", "LanciaY", 65.0);
			System.out.println("1) Torino1 - disponibili: " + cra.getVacantCars("Torino1"));
	//		1) Torino1 - disponibili: [Fiat500, Fiat500, FiatPanda]
			System.out.println("2) tariffa Fiat500: " + cra.getFare("Fiat500"));
	//		2) tariffa Fiat500: 45.0
			cra.addCustomer("Z789");
			cra.addCustomer("X123");
			cra.addCustomer("Y456");
			System.out.println("3) prenotazione Fiat500 3 giorni - costo: "+cra.book("X123", "Torino1", "Milano1", "Fiat500", 3));
	//		3) prenotazione Fiat500 3 giorni - costo: 135.0
			System.out.println("4) prenotazione Fiat500 5 giorni - costo: "+cra.book("Y456", "Torino1", "Roma1", "Fiat500", 5));
	//		4) prenotazione Fiat500 5 giorni - costo: 225.0
			System.out.println("5) prenotazione Fiat500 2 giorni - costo: "+cra.book("Z789", "Torino1", "Torino2", "Fiat500", 2));
	//		5) prenotazione Fiat500 2 giorni - costo: 0.0
			System.out.println("6) prenotazione FiatPanda 4 giorni - costo: "+cra.book("X123", "Torino1", "Roma1", "FiatPanda", 4));
	//		6) prenotazione Fiat500 4 giorni - costo: 0.0
			System.out.println("7) Torino1 - disponibili: " + cra.getVacantCars("Torino1"));
	//		7) Torino1 - disponibili: [FiatPanda]
			System.out.println("8) Torino1 - prenotate: " + cra.getBookedCars("Torino1"));
	//		8) Torino1 - prenotate: [Fiat500, Fiat500]
			System.out.println("9) Milano1 - disponibili: " + cra.getVacantCars("Milano1"));
	//		9) Milano1 - disponibili: [LanciaY]
			String l = cra.pickUp("Y456");
			System.out.println("10) partenza di Y456 da "+l);
	//		10) partenza di Y456 da Torino1
			System.out.println("11) "+l+" - prenotate: "+cra.getBookedCars(l));
	//		11) Torino1 - prenotate: [Fiat500]
			System.out.println("12) pagamento-> "+cra.giveBack("Y456"));
	//		12) pagamento-> Y456:Fiat500:Torino1:Roma1:225.0
			System.out.println("13) Roma1 - disponibili: " + cra.getVacantCars("Roma1"));
	//		13) Roma1 - disponibili: [Fiat500]
			l = cra.pickUp("X123");
			System.out.println("14) partenza di X123 da "+l);
	//		14) partenza di X123 da Torino1
			System.out.println("15) "+l+" - prenotate: "+cra.getBookedCars(l));
	//		15) Torino1 - prenotate: []
			System.out.println("16) pagamento-> "+cra.giveBack("X123"));
	//		16) pagamento-> X123:Fiat500:Torino1:Milano1:135.0
			System.out.println("17) Milano1 - disponibili: " + cra.getVacantCars("Milano1"));			
	//		17) Milano1 - disponibili: [Fiat500, LanciaY]
			System.out.println("18) prenotazione LanciaY 2 giorni - costo: "+cra.book("X123", "Milano1", "Torino2", "LanciaY", 2));
	//		18) prenotazione LanciaY 2 giorni - costo: 130.0
			l = cra.pickUp("X123");
			System.out.println("19) partenza di X123 da "+l);
	//		19) partenza di X123 da Milano1
			System.out.println("20) pagamento-> "+cra.giveBack("X123"));
	//		20) pagamento-> X123:LanciaY:Milano1:Torino2:130.0
			System.out.println("21) pagamenti X123 -> "+cra.customerRentals("X123"));
	//		21) pagamenti X123 -> [X123:LanciaY:Milano1:Torino2:130.0, X123:Fiat500:Torino1:Milano1:135.0]
			System.out.println("22) auto partite da Torino1 -> "+cra.rentalsFrom("Torino1"));
	//		22) auto partite da Torino1 -> [Fiat500, Fiat500]
			System.out.println("23) Fiat500 noleggiate: " +cra.modelRentals("Fiat500"));
	//		23) Fiat500 noleggiate: 2
			System.out.println("24) ricavo medio da Fiat500: " +cra.averageAmount("Fiat500"));
	//		24) ricavo medio da Fiat500: 180.0
		}
		catch(InvalidValue e){
			e.printStackTrace();
		};
	}

}
