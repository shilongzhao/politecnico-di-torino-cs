package timetable;

import java.util.Date;

public class Book{
    String author;
    Date published;
    int copiesSold;
    String category;
    public Book(String author, String category) {
        this.author = author;
        this.category = category;
    }
    public String getAuthor() {
        return author;
    }

    public Date getPublished() {
        return published;
    }

    public int getCopiesSold() {
        return copiesSold;
    }

    public String getCategory() {
        return category;
    }
}
