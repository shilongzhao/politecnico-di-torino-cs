package timetable;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {
    public static <T, K extends Comparable<K>> Collector<T, ?, TreeMap<K, List<T>>>
    sortedGroupingBy(Function<T, K> function) {
        return Collectors.groupingBy(function,
                TreeMap::new, Collectors.toList());
    }

    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        books.add(new Book("alex", "aa"));
        books.add(new Book("bob", "bb"));
        books.add(new Book("clarice", "cc"));
        books.add(new Book("bob", "dd"));

        TreeMap<String, List<Book>> result = books.stream().collect(
                Collectors.groupingBy(Book::getAuthor, TreeMap::new, Collectors.toList()));

        Map<String, Set<Book>> r2 = books.stream().collect(
                Collectors.groupingBy(Book::getAuthor,  Collectors.toSet()));
        System.out.println(result.toString());
    }
}
