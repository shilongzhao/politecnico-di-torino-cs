package it.polito.oop.stocks;

import java.util.Collection;
import java.util.Map;

public class StockExchange {

	public int defineStocks(String... stocks) {
	    return -1;
	}

	public String getStock(String stockCode) throws StockException {
        return null;
	}

	public int registerUser(String name) {
        return -1;
	}

	public String getUser(int userId) throws UserException {
        return null;
	}

	public void addToBalance(int userId, double amount) throws UserException {
	}

	public double getBalance(int userId) throws UserException {
        return -1;
	}

	public void addToWallet(int userId, String stockCode, int quantity) throws UserException, StockException {
	}

	public Collection<String> getWallet(int userId) throws UserException {
        return null;
	}

	public void addAsk(int userId, String stockCode, int quantity, double minPrice)
			throws UserException, StockException, WalletException {
	}

	public Collection<String> getBook(String stockCode) throws StockException {
        return null;
	}

	public boolean executeBid(int userId, String stockCode, int quantity, double maxPrice)
			throws UserException, StockException, BalanceException {
		return false;
	}

	public long countActiveWallets() {
        return -1;
	}

	public long countAskedStocks() {
        return -1;
	}

	public Map<Integer, Long> stocksPerUsers() {
        return null;
	}

}
