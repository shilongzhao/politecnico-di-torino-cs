package travelPortal;

import java.util.HashSet;
import java.util.Set;

public class Agency {
    private String name;
    private Set<String> activities = new HashSet<>();

    public Agency(String name, Set<String> activities) {
        this.name = name;
        this.activities = activities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getActivities() {
        return activities;
    }

    public void setActivities(Set<String> activities) {
        this.activities = activities;
    }
}
