package travelPortal;

import java.util.*;


public class TravelPortal {
    private Set<String> activities = new TreeSet<>();
    private Map<String, Agency> agencyMap = new HashMap<>();
    private Map<String, Proposal> proposalMap = new HashMap<>();
    private Map<String, Participant> participantMap = new HashMap<>();
    private SortedMap<String, List<Integer>> userRatingsMap = new TreeMap<>();
    //R1
    public List<String> addActivityTypes(String... names) {
        activities.addAll(Arrays.asList(names));
        return new ArrayList<>(activities);
    }

    public int AddTravelAgency(String name, String... activityTypes) throws TPException {
        List<String> list = Arrays.asList(activityTypes);
        if (agencyMap.containsKey(name) || !activities.containsAll(list)) {
            throw new TPException("");
        }
        HashSet<String> set = new HashSet<>(list);
        Agency agency = new Agency(name, set);
        agencyMap.put(name, agency);
        return set.size();
    }

    public SortedMap<String, List<String>> getAgenciesForActivityTypes() {
        SortedMap<String, List<String>> result = new TreeMap<>();
        for (Agency agency: agencyMap.values()) {
            for (String activity: agency.getActivities()) {
                List<String> names = result.getOrDefault(activity, new ArrayList<>());
                names.add(agency.getName());
                result.put(activity, names);
            }
        }

        for (List<String> list: result.values()) {
            list.sort(String::compareTo);
        }
        return result;
    }

    //R2
    public int addProposal(String code, String agency, String destination, String period, int minNP, int maxNP, int price) throws TPException {
        if (proposalMap.containsKey(code)) throw new TPException("");
        if (!agencyMap.containsKey(agency)) throw new TPException("");
        String[] strings = period.split(":");
        Integer month = Integer.valueOf(strings[0]);
        String[] days = strings[1].split("-");
        Integer departDay = Integer.valueOf(days[0]);
        Integer returnDay = Integer.valueOf(days[1]);
        Proposal proposal = new Proposal(code, agency, destination, month, departDay, returnDay, minNP, maxNP, price);
        proposalMap.put(code, proposal);
        return returnDay - departDay;
    }

    public int addActivity(String code, String activityType, String description, int price) throws TPException {
        Proposal p = proposalMap.get(code);
        if (p == null) {
            throw new TPException("");
        }
        Agency agency = agencyMap.get(p.getAgency());
        if (!agency.getActivities().contains(activityType)) {
            throw new TPException("");
        }
        Activity activity = new Activity(code, activityType, description, price);
        p.addActivity(activity);

        // (a,b) -> a.compareTo(b) ---> String::compareTo
        //
        // a -> a.get --> Activity::get
        return p.getActivities().stream().mapToInt(Activity::getPrice).sum();
    }

    public int getProposalPrice(String code) throws TPException {
        if (!proposalMap.containsKey(code)) {
            throw new TPException("");
        }
        Proposal p = proposalMap.get(code);
        return p.getPrice() + p.getActivities().stream().mapToInt(Activity::getPrice).sum();
    }

    //R3
    public List<String> addParticipants(String code, String... names) throws TPException {
        Proposal proposal = proposalMap.get(code);
        if (proposal == null) {
            throw new TPException("");
        }
        int count = 0;
        for (String name: names) {
            Participant p = participantMap.getOrDefault(name, new Participant(name));
            if (p.canParticipate(proposal)) {
                count++;
            }
            participantMap.put(name, p);
        }

        if (count < proposal.getMin() || count > proposal.getMax()) {
            throw new TPException("");
        }

        List<String> acceptedNames = new ArrayList<>();
        for (String name: names) {
            Participant p = participantMap.get(name);
            if (p.canParticipate(proposal)) {
                p.addProposal(proposal);
                proposal.addParticipant(p);
                acceptedNames.add(name);
            }
        }

        return acceptedNames;
    }

    public int getIncome(String code) {
        Proposal p = proposalMap.get(code);

        try {
            return getProposalPrice(code) * p.getParticipants().size();
        } catch (TPException e) {
            return 0;
        }
    }

    //R4
    public String addRatings(String code, int... evaluations) throws TPException {
        Proposal p = proposalMap.get(code);
        if (evaluations.length != p.getParticipants().size()) {
            throw new TPException("");
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < evaluations.length; i++) {
            Participant participant = p.getParticipants().get(i);
            String username = participant.getName();
            if (i != 0) {
                builder.append(", ");
            }
            builder.append(username).append(":").append(evaluations[i]);

            List<Integer> rs = userRatingsMap.getOrDefault(username, new ArrayList<>());
            rs.add(evaluations[i]);
            userRatingsMap.put(username, rs);
        }

        return builder.toString();
    }

    public SortedMap<String, Integer> getTotalRatingsForParticipants() {
        TreeMap<String, Integer> result = new TreeMap<>();
        for (String u: userRatingsMap.keySet()) {
            int sum = userRatingsMap.get(u).stream().mapToInt(Integer::intValue).sum();
            result.put(u, sum);
        }
        return result;
    }

//R5

    public SortedMap<String, Integer> incomeForActivityTypes() {
        SortedMap<String, Integer> result = new TreeMap<>();

        for (Proposal proposal: proposalMap.values()) {
            List<Activity> activities = proposal.getActivities();
            for (Activity activity: activities) {
                Integer value = result.getOrDefault(activity.getActivityType(), 0);
                result.put(activity.getActivityType(), value + activity.getPrice());
            }
        }
        return result;
    }


    //lista partecipanti che partecipano allo stesso numero di proposte
    public SortedMap<Integer, List<String>> participantsWithSameNofProposals() {
        SortedMap<Integer, List<String>> map = new TreeMap<>();
        for (Participant p: participantMap.values()) {
            int num = p.getProposals().size();
            List<String> names = map.getOrDefault(num, new ArrayList<>());
            names.add(p.getName());
            map.put(num, names);
        }

        for (List<String> list: map.values()) {
            list.sort(String::compareTo);
        }
        return map;
    }
}


