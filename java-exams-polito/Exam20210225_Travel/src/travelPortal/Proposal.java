package travelPortal;

import java.util.ArrayList;
import java.util.List;

public class Proposal {
    private String code;
    private String agency;
    private String destination;
    private int month;
    private int departDay;
    private int returnDay;
    private int min;
    private int max;
    private int price;

    private List<Activity> activities = new ArrayList<>();

    private List<Participant> participants = new ArrayList<>();

    public Proposal(String code, String agency, String destination, int month, int departDay, int returnDay, int min, int max, int price) {
        this.code = code;
        this.agency = agency;
        this.destination = destination;
        this.month = month;
        this.departDay = departDay;
        this.returnDay = returnDay;
        this.min = min;
        this.max = max;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDepartDay() {
        return departDay;
    }

    public void setDepartDay(int departDay) {
        this.departDay = departDay;
    }

    public int getReturnDay() {
        return returnDay;
    }

    public void setReturnDay(int returnDay) {
        this.returnDay = returnDay;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public void addActivity(Activity activity) {
        activities.add(activity);
    }

    public boolean compatibleWith(Proposal other) {
        if (this.month != other.month) return true;
        return (this.departDay > other.returnDay || this.returnDay < other.departDay);
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public void addParticipant(Participant participant) {
        participants.add(participant);
    }
}
