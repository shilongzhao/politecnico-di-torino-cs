package travelPortal;

public class Activity {
    private String proposalCode;
    private String activityType;
    private String description;
    private int price;

    public Activity(String proposalCode, String activityType, String description, int price) {
        this.proposalCode = proposalCode;
        this.activityType = activityType;
        this.description = description;
        this.price = price;
    }

    public String getProposalCode() {
        return proposalCode;
    }

    public void setProposalCode(String proposalCode) {
        this.proposalCode = proposalCode;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
