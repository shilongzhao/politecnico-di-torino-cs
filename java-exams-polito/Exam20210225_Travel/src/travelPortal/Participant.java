package travelPortal;

import java.util.ArrayList;
import java.util.List;

public class Participant {
    private String name;
    private List<Proposal> proposals = new ArrayList<>();

    public Participant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Proposal> getProposals() {
        return proposals;
    }

    public void setProposals(List<Proposal> proposals) {
        this.proposals = proposals;
    }

    public boolean canParticipate(Proposal proposal) {
        for (Proposal p: proposals) {
            if (!p.compatibleWith(proposal)) {
                return false;
            }
        }
        return true;
    }

    public void addProposal(Proposal proposal) {

        this.proposals.add(proposal);
    }
}
