//
//  main.c
//  firstProject
//
//  Created by Shilong Zhao on 04/08/2017.
//  Copyright © 2017 Shilong Zhao. All rights reserved.
//

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct {
    int remaining_units;
    int num_refills;
    int active_consumers;
    pthread_mutex_t lock;
    pthread_cond_t  can_refill;
    pthread_cond_t  can_consume;
} pump_t;

typedef struct {
    pump_t *pump;
    int    id;
    int    garden_area;
    int    lifetime_days;
    int    served;
} gardener_t;

gardener_t *gardeners;
int N;
int max_area(gardener_t *array) {
    int x = 0;
    for (int i = 0; i < N; i++) {
        if (!array[i].served && array[i].garden_area > x) {
            x = array[i].garden_area;
        }
    }
    return x;
}

int min_area(gardener_t *array) {
    int x = 1000; //INT_MAX
    for (int i = 0; i < N; i++) {
        if (!array[i].served && array[i].garden_area < x) {
            x = array[i].garden_area;
        }
    }
    return x;
}

void *producer(void *arg) {
    pump_t *p = (pump_t *) arg;
    pthread_mutex_lock(&(p->lock));
    while (1) {
        while(p->remaining_units > max_area(gardeners) && p->active_consumers > 0) {
            pthread_cond_wait(&(p->can_refill), &(p->lock));
        }
        if (p->active_consumers == 0) {
            printf("all consumers fed, returning...\n");
            break;
        }
        printf("refilling...\n");
        // sleep(2);
        p->remaining_units = 12;
        p->num_refills += 1;
        pthread_cond_broadcast(&(p->can_consume));
        printf("waking up consumers ...\n");
    }
    pthread_mutex_unlock(&(p->lock));
    
    return arg;
}

void *consumer(void *arg) {
    gardener_t *g = (gardener_t *) arg;
    pump_t *p = g->pump;
    pthread_mutex_lock(&(p->lock));
    printf("consumer created: [id = %d, area = %d, lifetime = %d days]\n", g->id, g->garden_area, g->lifetime_days);
    
    while (p->remaining_units < g->garden_area) {
        pthread_cond_wait(&(p->can_consume), &(p->lock));
    }
    p->remaining_units -= g->garden_area;
    p->active_consumers -= 1;
    g->served = 1;
    printf("consumer fed: [id = %d, area = %d, lifetime = %d days]", g->id, g->garden_area, g->lifetime_days);
    printf("(pump %p [remaining_units = %d, active_consumers = %d])\n", p, p->remaining_units, p->active_consumers);
    if (p->remaining_units < min_area(gardeners) || p->active_consumers == 0) {
        pthread_cond_signal(&(p->can_refill));
    }
    pthread_mutex_unlock(&(p->lock));
    return arg;
}

int main(int argc, const char * argv[]) {
    N = atoi(argv[1]); // number of gardeners
    pump_t pump = {
        .remaining_units = 0,
        .num_refills = 0,
        .active_consumers = N,
        .lock = PTHREAD_MUTEX_INITIALIZER,
        .can_refill = PTHREAD_COND_INITIALIZER,
        .can_consume = PTHREAD_COND_INITIALIZER
    };
    gardeners = malloc(sizeof(gardener_t) * N);
    
    pthread_t *consumer_threads = malloc(sizeof(pthread_t) * N);
    pthread_t producer_thread;
    srand(time(NULL));
    for (int i = 0; i < N; i++) {
        gardeners[i].pump = &pump;
        gardeners[i].id = i;
        gardeners[i].garden_area = 5 + rand() % 8;
        gardeners[i].lifetime_days = 1 + rand() % 10;
        gardeners[i].served = 0;
        pthread_create(&consumer_threads[i], NULL, consumer, &gardeners[i]);
    }
    pthread_create(&producer_thread, NULL, producer, &pump);
    
    for (int i = 0; i < N; i++) {
        pthread_join(consumer_threads[i], NULL);
    }
    pthread_join(producer_thread, NULL);
    
    free(gardeners);
    free(consumer_threads);
    return 0;
}
