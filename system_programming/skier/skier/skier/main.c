//
//  main.c
//  skier
//
//  Created by Shilong Zhao on 10/02/2018.
//  Copyright © 2018 Shilong Zhao. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <pthread.h>

typedef struct {
    sem_t *sem_doors;
    sem_t *sem_track;
} door_t;

typedef struct {
    int id; // skie ID
    int passages; // remaining passes
} skier_t;

door_t *door;

/**
 * each skier runs
 */
void *run(void *arg) {
    sleep(2); // reaching gate 2s
    skier_t *s= (skier_t *)arg;
    while (s->passages > 0) {
        printf("Skier %d reached the door\n", s->id);
        if (sem_wait(door->sem_doors) == -1) {
            exit(-1);
        }
        printf("Skier %d-%d entered the door\n", s->id, s->passages);
        sleep(2); // passing the door needs 2s
        s->passages -= 1;
        if (sem_post(door->sem_doors) == -1) {
            exit(-1);
        }
        
        if (sem_wait(door->sem_track) == -1) {
            exit(-1);
        }
        printf("Skier %d dropping slope\n", s->id);
        sleep(1 + rand() % 20); // needs random time to reach end
        if (sem_post(door->sem_track) == -1) {
            exit(-1);
        }
        printf("Skier %d returning to top\n", s->id);
        sleep(1 + rand() % 4);
        
    }
    printf("Skier %d returning home\n", s->id);
    return arg;
}

int main(int argc, const char * argv[]) {
    srand((unsigned int) time(NULL));
    
    door = malloc(sizeof(door_t));
    // create semaphore for 2 doors
    door->sem_doors = sem_open("sem_doors", O_CREAT | O_EXCL, 0x0777, 2);
    if (door->sem_doors == SEM_FAILED) {
        perror("sem_open doors ");
        exit(-1);
    }
    // create semaphore for track, maximum 5 people on track
    door->sem_track = sem_open("sem_track", O_CREAT | O_EXCL, 0x0777, 5);
    if (door->sem_track == SEM_FAILED) {
        perror("sem_open track");
        exit(-1);
    }
    // generating people
    pthread_t threads[100];
    skier_t   skiers[100];
    int count = 0;
    
    for (int i = 0; i < 3; i++) {
        int np = rand() % 3 + 1;
        for (int j = 0; j < np; j++) {
            int passages = rand() % 3 + 1;
            skiers[count].id = count;
            skiers[count].passages = passages;
            pthread_create(&threads[count], NULL, run, &skiers[count]);
            count++;
        }
    }
    
    for (int i = 0; i < count; i++) {
        pthread_join(threads[i], NULL);
    }
    
    sem_close(door->sem_doors);
    sem_close(door->sem_track);
    sem_unlink("sem_doors");
    sem_unlink("sem_track");
    
    return 0;
}

