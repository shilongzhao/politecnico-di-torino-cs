/**
 * @author sabrina.x1993@gmail.com.
 * @modified by webmaster@dreamhack.it
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

/**
 * 20130719
 * */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define R 10
#define C 40

int main(int argc,char *argv[]) {
    if(argc!=2){
        fprintf(stderr,"wrong arguments\n") ;
        exit(-1);
    }

    FILE *fmap=fopen(argv[1],"r");
    if(fmap==NULL){
        fprintf(stderr,"fill open filed");
        exit(-2);
    }

    char map[R][C+1];
    int line = 0;
    while(fscanf(fmap,"%s",map[line])==1) {
        line += 1;
    }
    int r,c;
    while(1){
        printf("insert the coordinate(r c): \n");
        scanf("%d %d", &r,&c);
        if(r==-1&&c==-1){
            break;
        }

        if(map[r][c]=='#') {
            printf("you are on the island ,don't need swim!\n");
            continue;
        }

        int count[4]={INT_MAX,INT_MAX,INT_MAX,INT_MAX};
        if(map[r][c]=='.'){
            int i;
            for( i=c;i<C;i++){
                if(map[r][i]=='#'){
                    count[0]=i-c;
                    break;
                }
            }

            for(i=c;i>0;i--){
                if(map[r][i]=='#'){
                    count[1]=c-i;
                    break;
                }
            }

            int j;
            for(j=r;j<R;j++){
                if(map[j][c]=='#'){
                    count[2]=r-j;
                    break;
                }
            }
            for(j=r;j>0;j--){
                if(map[j][c]=='#'){
                    count[3]=j-r;
                    break;
                }
            }

            int min=count[0], index = 0;
            for(int m=1;m<4;m++){
                if(min>count[m]){
                    index =m;
                    min=count[m];
                }
            }

            char directions[4][100] = {"right", "left", "down", "up"};

            if (min == INT_MAX) {
                printf("no island in any direction.\n");
            }
            else {
                printf("the nearest island is in the %s direction\n", directions[index]);
            }

        }
    }

    return 0;
}
