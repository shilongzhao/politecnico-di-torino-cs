//
// Created by zhaos on 1/13/2020.
//

#include <stdio.h>
int main(int argc, char *argv[]) {
    char s[3][10] = {"abc",
                     "12345",
                     "#$%^&"};

    printf("line 0 is %s\n", s[0]);
    printf("line 2 column 2 character is %c\n", s[2][2]);

    for (int i = 0; i < 3; i++) {
        printf("line %d is %s\n",i, s[i]);
    }

    printf("\n----------------------------\n");

    for (int i = 0; i < argc; i++) {
        printf("argv %d is %s\n", i, argv[i]);
    }


    char map[10][11] = {
            "***###****",
            "***###****",
            "**********",
            "***###****",
            "***###****",
            "**********",
            "***###****",
            "***###****",
            "**********",
            "**********"
    };










}