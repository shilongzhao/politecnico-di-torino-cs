//
// Created by zhaos on 1/15/2020.
//

#include <stdio.h>
#include <float.h>

int main(int argc, char *argv[]) {
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("file open failed\n");
        return -1;
    }

    int age;
    float score;

    int sum_age = 0;
    float max = FLT_MIN, min = FLT_MAX;

    int count = 0;
    while(fscanf(fp, "%*s %d %f", &age, &score) != EOF) {
        sum_age += age;
        if (score > max) {
            max = score;
        }
        if (score < min) {
            min = score;
        }
        count++;
    }

    fclose(fp);
    printf("average age is %.2f, max score is %.2f, min score is %.2f\n",
            sum_age/(float) count, max, min);
    return 0;
}