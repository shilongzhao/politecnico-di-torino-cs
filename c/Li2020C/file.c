//
// Created by zhaos on 1/15/2020.
//

#include <stdio.h>
#include <float.h>
#define N 5

int main(int argc, char *argv[]) {
    int ages[N];
    float scores[N];

    FILE *fp = fopen("C:\\Users\\zhaos\\CLionProjects\\Li2020C\\students.dat", "r");
    if (fp == NULL) {
        printf("file open failed\n");
        return -1;
    }

    for (int i = 0; i < N; i++) {
        fscanf(fp, "%*s %d %f", &ages[i], &scores[i]);
    }

    fclose(fp);

    float sum_age = 0;
    for (int i = 0; i < N; i++) {
        sum_age += ages[i];
    }
    printf("average age is %.2f\n", sum_age/(float) N);

    float max = FLT_MIN, min = FLT_MAX;
    for (int i = 0; i < N; i++) {
        if (scores[i] > max) {
            max = scores[i];
        }

        if (scores[i] < min) {
            min = scores[i];
        }
    }

    printf("max score is %.2f, min score is %.2f", max, min);
}