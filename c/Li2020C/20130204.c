//
// Created by zhaos on 1/20/2020.
//

#include <stdio.h>
#include <string.h>

#define M 6
#define N 7
int main(int argc, char *argv[]) {
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("file open failed\n");
        return -1;
    }

    char m[M][N + 1];
    int row = 0;
    while (fscanf(fp, "%s", m[row]) != EOF) {
//        printf("====read line %s====\n", m[row]);
        row++;
    }

//    char m[M][N + 2];
//    while(fgets(m[row], 9, fp) != NULL) {
//        m[row][N] = '\0';
//        printf("====read line %s====\n", m[row]);
//        row++;
//    }
    fclose(fp);

    if (strcmp(argv[2], "-a") == 0) {
        char c = 0;
        if (strcmp(argv[3], "red") == 0) {
            c = 'r';
        }
        else {
            c = 'y';
        }

        int win = 0;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (m[i][j] != c) {
                    continue;
                }

                // right
                int right = 1;
                for (int d = 0; d <= 3; d++) {
                    if (j + d >= N || m[i][j+d] != c) {
                        right = 0;
                        break;
                    }
                }

                int down = 1;
                for (int d = 0; d <=3; d++) {
                    if (i + d >= M || m[i+d][j] != c) {
                        down = 0;
                        break;
                    }
                }

                int left_down = 1;
                for (int d = 0; d <=3; d++) {
                    if (i + d >= M || j - d < 0 || m[i+d][j-d] != c) {
                        left_down = 0;
                        break;
                    }
                }

                int right_down = 1;
                for (int d = 0; d <= 3; d++) {
                    if (i + d >= M || j + d >= N || m[i+d][j+d] != c) {
                        right_down = 0;
                        break;
                    }
                }

                if (right == 1 || down == 1 || left_down == 1 || right_down == 1) {
                    win = 1;
                    break;
                }
            }
            if (win == 1) {
                break;
            }
        }
        if (win == 1) {
            printf("%s wins!\n", argv[3]);
        }
        else {
            printf("%s does not win!\n", argv[3]);
        }

    }

    if (strcmp(argv[2], "-b") == 0) {

    }

    return 0;

}