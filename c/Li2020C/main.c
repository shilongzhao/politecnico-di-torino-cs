#include <stdio.h> //standard input output

int main() {
    int a, b;
    scanf("%d %d", &a, &b);

    printf("Hello, %d %d!\n", a, b);

    float c = 1.23;
    char d = 'm';

    /**
     * arithmetic operators: + - * / %
     */
     printf("a%%b = %d\n", a % b);
    printf("a/b = %.3f\n", a/(float) b);
    /**
     * relation operator: >, < , >=, <=, ==, !=
     */
    if (a > b) {
        printf("a > b\n");
    }
    /**
     * logic operator: &&, ||, !
     */
     if (! (a > b)) {
         printf("a is not greater than b\n");
     }

     if ((a > b) && (d == 'm')) {
         printf("xxxxx\n");
     }
     /**
      * self increment/decrement, ++, --
      */
      // a == 37
     int x = a++; // int x = ++a;
     /**
      * assignment operator, =, +=, -=, *=, /=, %=,
      */
      a += 2;// a = a + 2;
      a %= 3; // a = a % 3;
      /**
       * bitwise operator.....
       */

    return 0;
}