//
// Created by zhaos on 1/13/2020.
//
#include <stdio.h>

int main(int argc, char *argv[]) {
    int m[10][10] = {
            {1,1,1,1,1,1,1,0,0,0},
            {1,1,1,1,1,1,1,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,1,1,1,1,0,0,0,0},
            {0,0,1,1,1,1,0,0,0,0},
            {0,0,1,1,1,1,0,0,0,0},
            {0,0,0,0,0,0,0,1,1,1},
            {0,0,0,0,0,0,0,1,1,1},
            {0,0,0,0,0,0,0,1,1,1},
    };

    int x, y;
    scanf("%d %d", &x, &y);

    if (m[x][y] == 0) {
        printf("I am on the sea!\n");
        return 0;
    }
    printf("I am on the land!\n");
    
    int u = 0, d = 0, l = 0, r = 0;
    
    while (x - u >= 0 && m[x - u][y] == 1) {
        u++;
    }
    
    while (x + d < 10 && m[x + d][y] == 1) {
        d++;
    }
    
    while (y - l >= 0 && m[x][y - l] == 1) {
        l++;
    }
    
    while (y + r < 10 && m[x][y + r] == 1) {
        r++;
    }
    
    int area = (u + d - 1) * (l + r - 1);
    
    printf("area of island is %d\n", area);

    return 0;
}