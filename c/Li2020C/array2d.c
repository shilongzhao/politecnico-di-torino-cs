//
// Created by zhaos on 1/7/2020.
//

#include <stdio.h>
#include <limits.h>

int is_left_upper(int m[][10], int x, int y) {
    if (m[x][y] == 0) {
        return 0;
    }
    // m[x][y] == 1
    if (x == 0 && y == 0) {
        return 1;
    }
    else if (x == 0 && y != 0) {
        if (m[x][y-1] == 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else if (x != 0 && y == 0) {
        if (m[x-1][y] == 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        if (m[x-1][y] == 0 && m[x][y-1] == 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
}
int main(int argc, char *argv[]) {

    int m[10][10] = {
            {1,1,1,1,1,1,1,0,0,0},
            {1,1,1,1,1,1,1,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,0,0,0,0,0,0,0,0},
            {0,0,1,1,1,1,0,0,0,0},
            {0,0,1,1,1,1,0,0,0,0},
            {0,0,1,1,1,1,0,0,0,0},
            {0,0,0,0,0,0,0,1,1,1},
            {0,0,0,0,0,0,0,1,1,1},
            {0,0,0,0,0,0,0,1,1,1},
    };
    
    int max_area = 0;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            if (is_left_upper(m, i, j)) {
                int height = 0;
                int width = 0;

                while ( i + height < 10 && m[i + height][j] == 1) {
                    height++;
                }

                while (j + width < 10 && m[i][j + width] == 1) {
                    width++;
                }

                int area = height * width;
                if (area > max_area) {
                    max_area = area;
                }
            }
        }
    }

    printf("max area is %d\n", max_area);


    int a[3][3] = {34,22,12,37,51,36,17,48,49};
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    // sum
    int all_sum = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            all_sum += a[i][j];
        }
    }
    printf("all sum = %d \n", all_sum);
    // row sum
    for (int i = 0; i < 3; i++) {
        int row_sum = 0;
        for (int j = 0; j < 3; j++) {
            row_sum += a[i][j];
        }
        printf("row %d sum = %d \n", i, row_sum);
    }
    // col sum

    // max
    int max = INT_MIN;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (max < a[i][j]) {
                max = a[i][j];
            }
        }
    }
    printf("max is %d\n", max);
    // row max

    // col max

}