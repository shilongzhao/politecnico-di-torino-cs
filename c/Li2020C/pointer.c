//
// Created by zhaos on 1/29/2020.
//
#include <stdio.h>
int fun(int n1, int *n2, int n3[]) {
    int a;
    a = n1 + (*n2);
    n1++;
    (*n2)--;
    n3[0]++;
    n3[1]--;
    return a;
}

int main() {
    int a = 0, b = 0;
    int c[2] = {0};
    // a = 0, b = -1, c[0] = 1, f = 0;
    printf("a=%d, b=%d, c0=%d, f=%d\n", a, b, c[0], fun(a, &b, c));
}