//
// Created by zhaos on 1/13/2020.
//

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    // > calculator.exe / 12 23 23 34
    // > calculator.exe + 12 23 44 45

    int sum = 0;
    for (int i = 1; i < argc; i++) {
        sum += atoi(argv[i]);
    }

    printf("sum is %d\n", sum);

}