//
// Created by zhaos on 1/20/2020.
//

#include <stdio.h>
#include <float.h>

#define NUM 7
int main(int argc, char *argv[]) {
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("file open failed\n");
        return -1;
    }

    float m[NUM][NUM] = {};

    int p, q;
    while (fscanf(fp, "%d %d", &p, &q) != EOF) {
        for (int i = p - 2; i <= p + 2; i++) {
            for (int j = q - 2; j <= q + 2; j++) {
                if (i >= 0 && j >= 0 && j < NUM && i < NUM) {
                    m[i][j] += 0.2;
                }
            }
        }

        for (int i = p - 1; i <= p + 1; i++) {
            for (int j = q - 1; j <= q + 1; j++) {
                if (i >= 0 && j >= 0 && j < NUM && i < NUM) {
                    m[i][j] += 0.3;
                }
            }
        }

        m[p][q] += 0.5;
    }

    float max = FLT_MIN;
    int max_i = -1, max_j = -1;

    for (int i = 0; i < NUM; i++) {
        for (int j = 0; j < NUM; j++) {
            if (m[i][j] > max) {
                max = m[i][j];
                max_i = i;
                max_j = j;
            }
        }
    }

    printf("the brightest spot is (%d,%d)\n", max_i, max_j);
}