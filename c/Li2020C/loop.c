//
// Created by zhaos on 1/2/2020.
//

#include <stdio.h>

int main(int argc, char *argv[]) {

    int sum = 0;
    for (int x = 2; x <= 100; x++) {
        int flag = 1;
        for (int i = 2; i <= x - 1; i++) {
            if (x % i == 0) {
                flag = 0;
                break;
            }
        }
        if (flag == 1) {
            printf("%d is a prime\n", x);
            sum += x;
        }
    }

    printf("sum of all primes 1..100 = %d\n", sum);
}