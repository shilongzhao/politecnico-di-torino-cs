//
// Created by zhaos on 1/7/2020.
//

#include <stdio.h>
#include <ctype.h>

int is_alpha_num(int c) {
    if (c >= 97 && c <= 122) {
        return 1;
    }
    if (c >= 'A' && c <= 'Z') {
        return 1;
    }
    if (c >= '0' && c <= '9') {
        return 1;
    }
    return 0;
}

int to_upper(int c) {
    return c - 32 ; // c + 'B' - 'b';
}

int str_len(char s[]) {
    return 0;
}

int str_cmp(char s1[], char s2[]) {
    return 0;
}


int main(int argc, char *argv[]) {
    int i = to_upper('b');
    printf("%c\n", i);
}

