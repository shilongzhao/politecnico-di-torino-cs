//
// Created by ZhaoShilong on 15/02/2017.
//

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("wrong argc\n");
        return -1;
    }
    int L = atoi(argv[1]);
    float limits[8][2];
    FILE *flimits = fopen("limits.txt", "r");
    if (flimits == NULL) {
        printf("fail open file limits.txt\n");
        return -1;
    }
    int i = 0;
    while(fscanf(flimits, "%f %f", &limits[i][0], &limits[i][1]) == 2) {
        i++;
    }
    if (!feof(flimits)) {
        printf("wrong format limits.txt\n");
        return -1;
    }
    fclose(flimits);

    FILE *fm = fopen("measurements.txt", "r");
    if (fm == NULL) {
        printf("fail open measurements.txt\n");
        return -1;
    }
    float m[4][8];
    i = 0;
    while(1) {
        for (int j = 0; j < 8; j++) {
            int r = fscanf(fm, "%f", &m[i%4][j]);
            if (r != 1)  {
                if (!feof(fm)) {
                    printf("format error measurements.txt\n");
                    return -1;
                }
                fclose(fm);
                return 0;
            }
        }
        i++;
        if (i < 4) continue;
        float average[8];
        for(int p = 0; p < 8; p++) {
            float sum = 0;
            for (int q = 0; q < 4; q++) {
                sum += m[q][p];
            }
            average[p] = sum/4;
        }

        int abnormal = 0;
        for (int p = 0; p < 8; p++) {
            if (average[p] < limits[p][0] || average[p] > limits[p][1]) {
                abnormal++;
            }
        }
        if (abnormal <= L) {
            printf("normal measurements line %d\n", i);
        }
    }
}









