#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int month[12] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335};

int main(int argc, char *argv[]) {
    if(argc != 2) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fd = fopen("london.dat", "r");
    FILE *fa = fopen("new_york.dat", "r");

    if (!fd || !fa) {
        fprintf(stderr, "file open failure\n");
        exit(-1);
    }

    char vascelli[500][20];
    int  depart[500];
    int  arrival[500];

    for(int i = 0; i < 500; i++) {
        depart[i] = arrival[i] = -1;
    }

    int  dd,mm;
    int  count = 0;
    while(fscanf(fd, "%s %d-%d", vascelli[count], &dd, &mm) ==3) {
        depart[count] = dd + month[mm-1];
        count += 1;
    }

    char this[20];
    while(fscanf(fa, "%s %d-%d", this, &dd, &mm) == 3) {
        int index = -1;
        for (int i = 0; i < count; i++) {
            if(strcmp(vascelli[i], this) == 0) {
                index = i;
                break;
            }
        }
        if (index != -1)
          arrival[index] = dd + month[mm-1];
    }

    int date = atoi(argv[1]);
    int flag = 0;
    for (int i = 0; i < count; i++) {
        if(date < arrival[i] && date > depart[i]) {
            flag = 1;
            printf("%s\n", vascelli[i]);
        }
    }
    if (flag == 0) {
        printf("nessun veliero in mare in giorno %d\n", date);
    }

    fclose(fd);
    fclose(fa);
    return 0;
}
