//
// Created by ZhaoShilong on 01/09/2018.
//

#include <stdio.h>
#include <math.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("wrong arguments!\n");
        return -1;
    }
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("error while opening file\n");
        return -1;
    }

    int info[100][6] = {};
    int size = 0;
    int id, route, x, y, t;
    while (fscanf(fp, "%d %d %d %d %d", &id, &route, &x, &y, &t) == 5) {
        int flag = 0;
        int i;
        for (i = 0; i < size; i++) {
            if (info[i][0] == id) {
                flag = 1;
                break;
            }
        }
        // if bus id has not appeared before
        if (flag == 0) {
            info[size][0] = id;
            info[size][1] = x;
            info[size][2] = y;
            info[size][4] = t;
            size = size + 1;
        }
        else { // bus id already recorded in info[i][]
            double d = sqrt( (x - info[i][1]) * (x - info[i][1])
                    + (y - info[i][2]) * (y - info[i][2]) );
            info[i][3] += (int) round(d);
            info[i][1] = x;
            info[i][2] = y;
            info[i][5] = t;
        }
    }

    if (strcmp(argv[2], "-d") == 0) {
        for (int i = 0; i < size; i++) {
            int t1s = info[i][4] % 60;
            int t1m = info[i][4]/60 % 60;
            int t1h = info[i][4]/3600 % 60;
            int t2s = info[i][5] % 60;
            int t2m = info[i][5]/60 % 60;
            int t2h = info[i][5]/3600 % 60;
            printf("%d:%d m, - First check: %02d:%02d:%02d – Last check: %02d:%02d:%02d\n",
                    info[i][0], info[i][3], t1h, t1m, t1s, t2h, t2m, t2s);
        }
    }

    if (strcmp(argv[2], "-v") == 0) {
        for (int i = 0; i < size; i++) {
            printf("%d: %.3f km/h\n", info[i][0], info[i][3] * 3.6 /  (float) (info[i][5] - info[i][4]) );
        }
    }

}