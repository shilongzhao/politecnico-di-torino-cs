/**
 * @author webmaster@dreamhack.it
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */
 
/***
 * 20130204
 * Usage: crossword puzzle.txt words.txt
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 50


int main(int argc, char *argv[]) {

    if (argc != 3) {
        fprintf(stderr, "usage: prog_name puzzle_file word_file\n");
        exit(-1);
    }

    FILE *fp = fopen(argv[1], "r");
    FILE *fw = fopen(argv[2], "r");

    if (fp == NULL || fw == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    char matrix[N][N+1];

    int n = 0;
    while(fgets(matrix[n], N+1, fp) != NULL) {
        n++;
    }
    // now n equals the number of lines in quiz matrix
    // it is usefull when search south

    char word[N+1];
    char directions[4][2] = {"E", "W", "S", "N"};
    while(fscanf(fw, "%s", word) == 1) {

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < strlen(matrix[i]); j++) {


                // if first char matches, start search
                if (matrix[i][j] == word[0]) {
                    
                    int matched[4] = {1, 1, 1, 1};

                    //search east
                    for (int x = 0; word[x] != '\0'; x++) {
                        if (word[x] != matrix[i][j + x] || j + x > strlen(matrix[i])) {
                            matched[0] = 0;
                            break;
                        }
                    }

                    //west
                    for (int x = 0; word[x] != '\0'; x++) {
                        if (word[x] != matrix[i][j - x] || j - x < 0) {
                            matched[1] = 0;
                            break;
                        }
                    }

                    // south
                    for (int x = 0; word[x] != '\0'; x++) {
                        if (word[x] != matrix[i + x][j] || i + x >= n) { // notice the range of i+x here
                            matched[2] = 0;
                            break;
                        }
                    }

                    // north
                    for (int x = 0; word[x] != '\0'; x++) {
                        if (word[x] != matrix[i - x][j] || i - x < 0) {
                            matched[3] = 0;
                            break;
                        }
                    }

                    for(int p = 0; p < 4; p++) {
                        if (matched[p] == 1) {
                            printf("%s found at %d, %d (%s)\n", word, i, j, directions[p]);
                        }
                    }
                }
                
            }

        }
    }

    return 0;
}