/**
 * @author yueyvonne1233@hotmail.com.
 * @modified by webmaster@dreamhack.it
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

int main(int argc,char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fd = fopen(argv[1], "r");
    if (fd == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    char goodname[20];
    printf("input goodname: ");
    scanf("%s", goodname);

    int max_d = 0;
    int max_m = 0;
    int max_y = 0;
    int sum = 0;
    int count = 0;

    float quintals;
    float income;

    char this_good[20];
    int d, m, y;
    int kg;

    while (fscanf(fd, "%s %d-%d-%d %d", this_good, &d, &m, &y, &kg) == 5) {


        if (strcmp(this_good, goodname) != 0 || kg < 0)  continue;

        count += 1;
        sum += kg;
        if (y > max_y) {
            max_y = y;
            max_m = m;
            max_d = d;
        }
        if (y == max_y && m > max_m) {
            max_m = m;
            max_d = d;
        }

        if (y == max_y && m == max_m && d > max_d) {
            max_d = d;
        }

    }

    if (!feof(fd)) {
        fprintf(stderr, "file format error\n");
        exit(-1);
    }

    quintals = sum/100.0;
    income = sum * 0.1 + count * 10;


    printf("%.2f of loaded bananas\n", quintals);
    printf("%d-%d-%d of last loading operation\n", max_d, max_m, max_y);
    printf("%.2f from loading operations on bananas\n", income);

    fclose(fd);
    return 0;

}
