#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 30

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "wrong arguments, usage: rm_seq NUM\n");
        exit(-1);
    }

    int limit = atoi(argv[1]);

    FILE *fp = fopen("numbers.dat", "r");
    if (fp == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    int num[MAX_LEN];
    int count = 0;
    while(fscanf(fp, "%d", &num[count]) == 1) {
        count += 1;
    }


    // the start of sequence, index of
    int seq_start = 0;

    // length of the sequence
    int seq_count = 1;
    for (int i = 1; i < count; i++) {

        if (num[i] == num[i-1]) {
            seq_count += 1;
            continue;
        }

        // there are two possible situations when a sequence ends
        // 1. num[i] != num[i-1]
        if (num[i] != num[i-1]) {
            if (seq_count >= limit) {
                for (int j = seq_start; j < i; j++) {
                    num[j] = -1;
                }
            }
            seq_start = i;
            seq_count = 1;
        }

        // 2. when num reaches the last number
        if (i == count-1) {
            if (seq_count >= limit) {
                for (int j = seq_start; j <= count-1; j++) {
                    num[j] = -1;
                }
            }
        }

    }
    fclose(fp);

    // NOTICE: in the question, it should write back
    // to the original file numbers.txt. which means the numbers.dat
    // has to be reopened with write mode.
    // Here, for conveniences, the result is written to another file.
    FILE *fout = fopen("result.txt", "w");
    for(int i = 0; i < count; i++) {
        if (num[i] != -1)
            fprintf(fout, "%d\n", num[i]);
    }

    fclose(fout);

    return 0;
}