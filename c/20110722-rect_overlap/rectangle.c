/**
 * @author webmaster@dreamhack.it
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

 /**
 * 20110722
 * Usage: rectangle input.txt
 */

#include <stdio.h>
#include <stdlib.h>
#define N 10


int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fin = fopen(argv[1], "r");
    if (fin == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    int matrix[N+1][N+1] = {};

    int x1, y1, x2, y2;
    while(fscanf(fin, "%d %d %d %d", &x1, &y1, &x2, &y2) == 4) {
        if (x1 > x2 || y1 > y2 || x1 < 0 ||
                x1 > N || x2 < 0 || x2 > N ||
                y1 < 0 || y1 > N || y2 < 0 || y2 > N) {
            fprintf(stderr, "Error in format of file\n");
            exit(-1);
        }

        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                if (matrix[i][j] == 0) {
                    matrix[i][j] = 1;
                }
                else {
                    printf("The rectangles have (at least) one overlap\n");
                    fclose(fin);
                    return 0;
                }
            }
        }
    }

    printf("no overlapping rectangles\n");
    fclose(fin);
    return 0;
}