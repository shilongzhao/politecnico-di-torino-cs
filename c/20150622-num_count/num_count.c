#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 500

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fp1 = fopen(argv[1], "r");
    FILE *fp2 = fopen(argv[2], "r");
    if (fp1 == NULL || fp2 == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    int num[MAX_LEN];
    int count = 0;
    while(fscanf(fp1, "%d", &num[count]) == 1) {
        count++;
    }
    if (!feof(fp1)) {
        fprintf(stderr, "file format error\n");
        exit(-1);
    }
    fclose(fp1);

    int this;
    while(fscanf(fp2, "%d", &this) == 1) {
        int freq = 0;
        for (int i = 0; i < count; i++) {
            if (this == num[i]) {
                freq += 1;
            }
        }

        if (freq != 0) {
            printf("%d: %d\n", this, freq);
        }
    }
    if(!feof(fp2)) {
        fprintf(stderr, "file %s format error\n", argv[2]);
        exit(-1);
    }

    fclose(fp2);
    return 0;
}