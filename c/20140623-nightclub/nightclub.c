#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define  N    7
int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr,"file %s open failed", argv[1]);
        exit(-1);
    }


    int r, c;
    float mat[N][N] = {0};

    while(fscanf(fp, "%d %d", &r, &c) == 2) {

        for (int i = -2; i <=2 ; i++) {
            for(int j = -2; j <= 2; j++) {

                int nr = i + r;
                int nc = j + c;
                if (nr < 0 || nc < 0 || nr >= N || nc >= N) {
                    continue;
                }

                if (i == 0 && j == 0) {
                    mat[r][c] += 1.0;
                }
                else if ( abs(i) == 2 || abs(j) == 2 ) {
                    mat[nr][nc] += 0.2;
                }
                else {
                    mat[nr][nc] += 0.5;
                }

            }
        }
    }

    float max = mat[0][0];
    int max_x = 0;
    int max_y = 0;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (mat[i][j] > max) {
                max = mat[i][j];
                max_x = i;
                max_y = j;
            }
            printf("%.1f ", mat[i][j]);
        }
        printf("\n");
    }
    printf("max = %.1f at %d %d \n", max, max_x, max_y);

}
