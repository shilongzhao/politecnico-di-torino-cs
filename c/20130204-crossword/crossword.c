/***
 * 20130204
 * Usage: crossword puzzle.txt words.txt
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 50


int main(int argc, char *argv[]) {

    if (argc != 3) {
        fprintf(stderr, "usage: prog_name puzzle_file word_file\n");
        exit(-1);
    }

    FILE *fp = fopen(argv[1], "r");
    FILE *fw = fopen(argv[2], "r");

    if (fp == NULL || fw == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    char matrix[N][N+1];

    int n = 0;
    while(fgets(matrix[n], N+1, fp) != NULL) {
        n++;
    }
    // now n equals the size of matrix;

    char word[N+1];
    while(fscanf(fw, "%s", word) == 1) {
        int matched = 0;

        for (int i = 0; i < n; i ++) {
            for (int j = 0; j < strlen(matrix[0]); j++) {
                int p;
                // Est
                for (p = 0; p < strlen(word); p++) { // no need to judge overflow boundary
                    if (word[p] != matrix[i][j+p])
                        break;
                }
                if (p == strlen(word)) {
                    printf("%s found at row %d, col %d (E)\n", word, i, j);
                    matched = 1;
                }
                // Ovest
                for (p = 0; p < strlen(word); p++) {
                    if (j - p < 0 || word[p] != matrix[i][j-p])
                        break;
                }
                if (p == strlen(word)) {
                    printf("%s found at row %d, col %d (W)\n", word, i, j);
                    matched = 1;
                }

                // Nord
                for (p = 0; p < strlen(word); p++) {
                    if (i - p < 0 || word[p] != matrix[i-p][j])
                        break;
                }
                if (p == strlen(word)){
                    printf("%s found at row %d, col %d (N)\n", word, i, j);
                    matched = 1;
                }
                // Sud
                for (p = 0; p < strlen(word); p++) {
                    if (i + p >= n || word[p] != matrix[i+p][j])
                        break;
                }
                if (p== strlen(word)) {
                    printf("%s found at row %d, col %d (S)\n", word, i, j);
                    matched = 1;
                }

            }
        }
        if (matched == 0) {
            printf("%s is not found\n", word);
        }
    }
    return 0;

}
