#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MROW 6
#define MCOL 8

int check_win(char matrix[][MCOL], int line, int color);

void win_move(char matrix[][MCOL], int line, int color);

int main(int argc, char *argv[]) {
    if (argc != 4) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    char matrix[MROW][MCOL];

    int line = 0;
    while(fscanf(fp, "%s", matrix[line]) == 1) {
        line += 1;
    }
    if(!feof(fp)) {
        fprintf(stderr, "file format error\n");
        exit(-1);
    }
    fclose(fp);

    int color = (strcmp(argv[3], "red") == 0) ? 'r':'y';

    printf("color = %c\n", color);
    int win = check_win(matrix, line, color);

//    if (strcmp(argv[2], "-a") == 0) {

        if (!win) {
            printf("selected player not win\n");
        }
        else {
            printf("selected player is the winner\n");
        }
//    }


    if (strcmp(argv[2], "-b") == 0 ) {
        win_move(matrix, line, color);
    }

    return 0;

}

int check_win(char matrix[][MCOL], int line, int color) {

    for(int i = 0; i < line; i++) {
        for(int j = 0; j < strlen(matrix[i]); j++) {
            int len[4] = {};

            if (matrix[i][j] != color)
                continue;

            // east, == west
            for (int d = j; d < strlen(matrix[i]); d++) {
                if (matrix[i][d] != color) {
                    len[0] = d - j;
                    break;
                }
            }

            // south, == north
            for (int d = i; d < line; d++) {
                if(matrix[d][j] != color) {
                    len[1] = d - i;
                    break;
                }
            }

            // diagonal 45
            for (int d1 = i, d2 = j; d1 > 0 && d2 < strlen(matrix[i]); d1--, d2++) {
                if (matrix[d1][d2] != color) {
                    len[2] = d2 - j;
                    break;
                }
            }

            // diagonal 135
            for (int d1 = i, d2 = j; d1 < line && d2 < strlen(matrix[i]); d1++, d2++) {
                if (matrix[d1][d2] != color) {
                    len[3] = d1 - i;
                    break;
                }
            }

            for (int p = 0; p < 4; p++) {
//                printf("(%d, %d) dir:%d len:%d\n",i, j, p, len[p]);
                if (len[i] >=4) {
                    return 1;
                }
            }
        }
    }
    return 0;

}

void win_move(char matrix[][MCOL], int line, int color) {

    int can_win = 0;
    for(int i = 0; i < line; i++) {
        for(int j = 0; j < strlen(matrix[i]); j++) {

            if (matrix[i][j] != color)
                continue;

            // east
            for (int d = j; d < strlen(matrix[i]); d++) {
                if (matrix[i][d] != color) {
                    if( d - j == 3 && matrix[i][d] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", i, d);
                    }
                    else
                        break;
                }
            }

            // west
            for (int d = j; d > 0 ; d--) {
                if (matrix[i][d] != color) {
                    if( j-d == 3 && matrix[i][d] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", i, d);
                    }
                    else
                        break;
                }
            }

            // south
            for (int d = i; d < line; d++) {
                if(matrix[d][j] != color) {
                    if( d - i == 3 && matrix[d][j] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", d, j);
                    }
                    else
                        break;
                }
            }

            //north
            for (int d = i; d > 0; d--) {
                if(matrix[d][j] != color) {
                    if( i-d == 3 && matrix[d][j] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", d, j);
                    }
                    else
                        break;
                }
            }

            // diagonal 45
            for (int d1 = i, d2 = j; d1 > 0 && d2 < strlen(matrix[i]); d1--, d2++) {
                if (matrix[d1][d2] != color) {
                    if (d2 - j == 3 && matrix[d1][d2] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", d1, d2);
                    }
                    else
                        break;
                }
            }
            //diagonal -135
            for (int d1 = i, d2 = j; d1 < line && d2 > 0; d1++, d2--) {
                if (matrix[d1][d2] != color) {
                    if (j-d2 == 3 && matrix[d1][d2] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", d1, d2);
                    }
                    else
                        break;
                }
            }

            // diagonal -45
            for (int d1 = i, d2 = j; d1 < line && d2 < strlen(matrix[i]); d1++, d2++) {
                if (matrix[d1][d2] != color) {
                    if (d2 - j == 3 && matrix[d1][d2] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", d1, d2);
                    }
                    else
                        break;
                }
            }
            // 135
            for (int d1 = i, d2 = j; d1 > 0  && d2 > 0 ; d1--, d2--) {
                if (matrix[d1][d2] != color) {
                    if ( j - d2 == 3 && matrix[d1][d2] == 'o') {
                        can_win = 1;
                        printf("win move: put at (%d, %d)", d1, d2);
                    }
                    else
                        break;
                }
            }

        }
    }
    if (!can_win)
        printf("no win move found for selected player\n");

    return;
}
