
/**
 * @author yueyvonne1233@hotmail.com.
 * @modified by webmaster@dreamhack.it
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

#include <stdio.h>
#include <stdlib.h>
#define R 10
#define C 40

int main(int argc,char *argv[]){
    if (argc!=2){
        fprintf(stderr,"wroneg arguments\n");
        exit(-1);
    }
    FILE *fmap=fopen(argv[1],"r");
    if(fmap==NULL){
        fprintf(stderr,"file format error\n");
        exit(-1);
    }
    char map[R][C+1];
    int c = 0;
    while(fscanf(fmap,"%s",map[c])==1){
        c++;
    }
    int a,b;
    while(1){
        printf("Insert the coordinate(R C):\n");
        scanf("%d %d",&a,&b);
        if(a==-1&&b==-1){
            break;
        }

        if(map[a][b]=='#'){
            printf("the point is on the island\n");
        }
        if(map[a][b]=='.'){
            printf("the point is in the sea\n");

            int count[4]={0,0,0,0};
            int i;
            //->right
            for(i=b;i<=C;i++){
                if(map[a][i]=='.') {
                    count[0]++;
                }
                if(map[a][i]=='#') {
                    break;
                }
            }
            if(i>C){
                count[0] = 100;
            }

            //left<-
            for(i=b;i>=0;i--){
                if(map[a][i]=='.') {
                    count[1]++;
                }
                if(map[a][i]=='#'){
                    break;
                }
            }
            if(i<0){
                count[1]=100;
            }
            int j;
            // down
            for(j=a;j<=R;j++){
                if(map[j][b]=='.') {
                    count[2]++;
                }
                if(map[j][b]=='#'){
                    break;
                }
            }
            if(j>R){
                count[2]=100;
            }

            // up
            for(j=a;j>=0;j--){
                if(map[j][b]=='.') {
                    count[3]++;
                }
                if(map[j][b]=='#'){
                    break;
                }
            }
            if(j<0){
                count[3]=100;
            }

            char directions[4][100] = {"right", "left", "down", "up"};

            int min = count[0], min_index = 0;

            for(int m = 0; m < 4; m ++) {
                if (count[m] < min) {
                    min = count[m];
                    min_index = m;
                }
            }

            if (min == 100) {
                printf("no island in any direction\n");
            }
            else {
                printf("nearest island in %s direction, distance %d\n", directions[min_index], min);
            }
        }

    }

    return 0;
}
