//
// Created by ZhaoShilong on 01/09/2018.
//

#include <stdio.h>
#include <math.h>
#include <float.h>

#define M 2
#define N 9

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("wrong arguments\n");
        return -1;
    }
    FILE *ftr = fopen(argv[1], "r");
    if (ftr == NULL) {
        printf("fail opening train file");
        return -1;
    }
    FILE *fts = fopen(argv[2], "r");
    if (fts == NULL) {
        printf("fail opening test file");
        return -1;
    }

    int label[N];
    float feature[N][M];
    char t[10];
    float x, y;
    int r = 0, l;
    while (fscanf(ftr, "%s %f %f %d", t, &x, &y, &l ) == 4){
        feature[r][0] = x;
        feature[r][1] = y;
        label[r] = l;
    }

    char o[10];
    int nc = 0;
    int total = 0;
    while (fscanf(fts, "%s %f %f %d", o, &x, &y, &l ) == 4) {
        float d = FLT_MAX;
        int p;
        for (int i = 0; i < N; i++) {
            float dd = sqrt(pow(x - feature[i][0], 2) + pow(y - feature[i][1], 2));
            if (dd < d) {
                d = dd;
                p = i;
            }
        }
        total++;
        if (l == label[p]) {
            nc++;
        }
        printf("Object %s: predicted label %d – actual label %d\n", o, label[p], l );
    }
    printf("Accuracy is %.2f\n", nc/(float)total);
}