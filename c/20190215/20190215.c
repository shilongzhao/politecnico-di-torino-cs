//
// Created by zhaos on 1/18/2020.
//

#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {

    FILE *f1 = fopen(argv[1], "r");
    if (f1 == NULL) {
        fprintf(stderr, "file open failed f1\n");
        return -1;
    }

    FILE *f2 = fopen(argv[3], "r");
    if (f2 == NULL) {
        fprintf(stderr, "file open failed f2");
        return -1;
    }

    // store information from file 2
    char scodes[50][12];
    char desc[50][102];
    int ns = 0;
    while (fgets(scodes[ns], 100, f2) != NULL) {
        fgets(desc[ns], 200, f2);

        scodes[ns][10] = '\0';
        desc[ns][100] = '\0';

//        printf("\n =======GOT scode = %s, desc = %s ===== \n", scodes[ns], desc[ns]);
        ns++;
    }

    fclose(f2);

    // read and process file1
    char hcode[10];
    char k1[21], k2[21], k3[21];
    char scode[12];

    int count[50] = {};
    char first[50][9] = {};

    while(fgets(hcode, 100, f1) != NULL) {
        hcode[8] = '\0';
        fscanf(f1, "%s %s %s\n", k1, k2, k3);
        fgets(scode, 200, f1);
        scode[10] = '\0';

//        printf("\n------- hcode: %s, kw: %s %s %s, scode: %s --------\n", hcode, k1, k2, k3, scode);
        // first
        if (strcmp(argv[2], k1) == 0 ||
            strcmp(argv[2], k2) == 0 ||
            strcmp(argv[2], k3) == 0) {

            int flag = -1;
            for (int i = 0; i < ns; i++) {
                if (strcmp(scodes[i], scode) == 0) {
                    flag = i;
                }
            }

            if (flag == -1) {
                printf("%s %s software description not available\n", hcode, scode);
            }
            else {
                printf("%s %s %s\n", hcode, scode, desc[flag]);
            }
        }
        // second
        for (int i = 0; i < ns; i++) {
            if (strcmp(scodes[i], scode) == 0) {
                count[i] += 1;
                if (strlen(first[i]) == 0) {
                    strcpy(first[i], hcode);
                }
            }
        }
    }

    for (int i = 0; i < ns; i++) {
        if (count[i] > 0) {
            printf("%s is used by %d devices, first device is %s\n",
                    scodes[i], count[i], first[i]);
        }
        else {
            printf("%s is not used\n", scodes[i]);
        }
    }
    return 0;
}