//
// Created by ZhaoShilong on 08/09/2018.
//

#define N 5

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 5) {
        return -1;
    }
    FILE *fa = fopen(argv[1], "r");
    if (fa == NULL) {
        return -1;
    }
    float m[N][N];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            fscanf(fa, "%f", &m[i][j]);
        }
    }

    fclose(fa);

    FILE *fb = fopen(argv[2], "r");
    if (fb == NULL) {
        return -1;
    }

    int rows[150];
    int cols[150];
    float weights[150];
    int r = 0;
    while (fscanf(fb, "%d %d %f", &rows[r], &cols[r], &weights[r]) == 3) {
        r++;
    }
    fclose(fb);

    int MAX_P = atoi(argv[3]);
    int MAX_W = atoi(argv[4]);
    int dive = 1;
    int remaining = MAX_W;
    for (int i = 0; i < r; i++) {
        int x = rows[i];
        int y = cols[i];
        if (x < 0 || x >= N || y < 0 || y >= N) {
            printf("Not found in map: %d %d\n",x, y);
            continue;
        }

        if (m[x][y] > MAX_P) {
            printf("excessive pressure: %d %d pressure %.2f\n", x, y, m[x][y]);
            continue;
        }

        if (weights[i] > MAX_W) {
            printf("excessive weight %d %d\n", x, y);
            continue;
        }

        if (remaining >= weights[i]) {
            remaining -= weights[i];
            printf("Dive %d: %d %d\n", dive, x, y);
        }
        else {
            remaining = MAX_W;
            dive = dive + 1;
            remaining -= weights[i];
            printf("Dive %d: %d %d\n", dive, x, y);
        }
    }
}
