INTRODUCTION TO CS, THEORY PART

1. DEC->HEX->OCT->BIN

	positive integer : SM == 1C == 2C
	
	negative integer: 
	S&M: 
	1's complement : 
	2's complement :  

	rules to detect overflow: 
		- If the sum of two positive numbers yields a negative result, the sum has overflowed.
		- If the sum of two negative numbers yields a positive result, the sum has overflowed.
		- Otherwise, the sum has not overflowed.

2. Image size: pixels * byte per pixel 
	file size: 
	structure size: 

3. CPU: 
	Computer Instruction Cycle: Fetch, Decode, Execute,  (Store) 

	Computer architecture: Input/Output Device, Memory(RAM, Hard Disk) , CPU
	
	CPU Architecture:  Clock, ALU, Registers, Control Unit, Flags

		Clock: The clock synchronizes the internal operations of the CPU with other system components. 
			On each tick or pulse another operation can be started; 

		Control Unit: According to the provided program, and the state of all the units, 
				CU schedules the operations to be executed, and issues the corresponding instructions. 
				CU coordinates the input and output devices of a computer system. 
				CU coordinates the fetching of program code from main memory to the CPU and 
				CU directs the operation of the other processor components by providing timing and control signals.
		
		————— Control Unit tells ALU what operations to perform (+, - , *, /, …) ———

		ALU: The Arithmetic Logic Unit or the ALU is a digital circuit that performs arithmetic (addition, subtraction, multiplication, division) and logical operations AND, OR, NOT. 
			It takes values in registers and performs any of the multitude of operations the CPU is capable of.

		
		———- ALU loads data from and store results to Registers ————
		Registers: A small amount of fast storage which is part of the CPU
			
			Program Counter:  an incrementing counter that keeps track of the memory address of which instruction is to be executed next...
			Instruction Register:  a temporary holding ground for the instruction that has just been fetched from memory
			Accumulator: Used to store results of calculations
	
		

		Word size : The number of bits of information that a processor can process at one time. 32bit/64bits

		BUS: A collection of wires through which data is transmitted from one part of a computer to another. 
			the bus speed is also defined by its frequency (expressed in Hz), the number of data packets sent or received per second

		Bus Size: The size of a bus, known as its width, is important because it determines how much data can be transmitted at one time. 
			  The number of bits of information a bus can carry at one time (the number of wires making up a bus) .

		Data Bus: 
			data bus transfers actual data

		Control Bus:  
			Transports orders and synchronisation signals coming from the control unit (CU) and traveling to all other hardware components.
		
		Address Bus:
			Address bus transfers information about where the data should go.



4. Boolean Algebra:
	Truth table: 
	Logic gate: AND, OR, NOT
	
5. Compiler, Linker, Loader:
	Compiler: A compiler is a program that reads source code files and translates code into an object file (*.o files)
	Linker: A linker is a program that combines one or more object files and possible some library code into either some executable, some library or a list of error messages.
	Loader: A loader is a program A loader reads the executable code into memory, does some address translation and tries to run the program resulting in a running program or an error message (or both).

6. Libraries: 
	C Library: A Library provides macros (e.g. NULL), type definitions (e.g. int, float, double), 
			and functions (e.g. atoi(), fopen(), fscanf() ) for tasks like string handling (in <string.h>), 
			mathematical computations (in <math.h>), input/output processing, 
			memory allocation and several other operating system services.
		One uses these definitions, functions by including the corresponding header files.

	Header File (string.h, stdio.h ….): Header file is the interface to C Libraries. 
		
		A library can be used by 1. including its header file and 2. link it to the executable file. 

7. Memories: 
	Primary Memory(main memory, internal memory, memory): 
		1. directly connects to CPU by data bus, address bus
		2. volatile 
	Secondary storage(external memory, auxiliary storage):
		1. NOT directly connects to CPU
		2. non-volatile, does not lose data when computer is powered off. 
		3. much slower than main memory when read & write data

