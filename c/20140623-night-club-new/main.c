/**
 * @author: webmaster@dreamhack.it
 * it's free to distribute this program under the condition that
 * the program is not modified and
 * the author information is not modified
 */
#include <stdio.h>
#include <stdlib.h>

#define NUM 7
#define legal_range(x) ((x) >= 0 && (x) < NUM)

static float LUCE[5][5] = {
        {0.2, 0.2, 0.2, 0.2, 0.2},
        {0.2, 0.5, 0.5, 0.5, 0.2},
        {0.2, 0.5, 1.0, 0.5, 0.2},
        {0.2, 0.5, 0.5, 0.5, 0.2},
        {0.2, 0.2, 0.2, 0.2, 0.2}
};

int main(int argc, char *argv[]) {
    if(argc != 2) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        fprintf(stderr, "error while opening file\n");
        exit(-1);
    }

    int r, c;
    float mat[NUM][NUM] = {0};

    while(fscanf(fp, "%d %d", &r, &c) == 2) {
        for (int p = r - 2; p <= r + 2; p++) {
            for (int q = c - 2; q <= c + 2; q++) {
                if (legal_range(p) && legal_range(q)) {
                    mat[p][q] += LUCE[p - (r - 2)][q - (c - 2)];
                }
            }
        }
        // for (int i = -2; i <= 2; i++) {
        //     for (int j = -2; j <=2; j++) {
        //         if (legal_range(r+i) && legal_range(c+j)) {
        //             mat[r+i][c+j] += LUCE[2+i][2+j];
        //         }
        //     }
        // }
    }

    float max = 0;
    for (int i = 0; i < NUM; i++) {
        for (int j = 0; j < NUM; j++) {
            if (mat[i][j] > max) {
                max = mat[i][j];
                r = i;
                c = j;
            }
        }
    }

    printf("max = %.1f at coordinate (%d, %d)", max, r, c);

}