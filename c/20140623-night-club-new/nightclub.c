void nightclub(float **mat, int n, int r, int c) {
	for (int i = r - 2; i <= r + 2; i++) {
		for (int j = c - 2; j <= c + 2; j++) {
			
			if (i < 0 || i >= n || j < 0 || j >= n) {
				continue;
			}

			if (i == r && j == c) {
				mat[i][j] = 1;
			}
			else if (i <= r + 1 && i >= r - 1 && j <= c + 1 && j>= c - 1){
				mat[i][j] = 0.5;
			}
			else {
				mat[i][j] = 0.2;
			}
		}
	}	
}