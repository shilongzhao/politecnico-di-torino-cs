/**
 * programmed by webmaster@dreamhack.it
 * you are free to distribute it,
 * on the condition that
 * 1. author information is included
 * 2. and the program is not modified.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include<string.h>

int main(int argc,char *argv[]){
     if(argc!=2){
        fprintf(stderr,"wrong arguments\n");
        exit(-1);
     }
     FILE *fp1=fopen(argv[1],"r");
     if (fp1==NULL){
        fprintf(stderr,"file open failed\n");
        exit(-1);
     }
     char max_name[15];
     float max_average=0;
     char name[15];
     while(fscanf(fp1,"%s",name)==1){
        printf("%s\n", name);
        int score=0;
        int sum=0;
        int count=0;
        float average;
        while(fscanf(fp1,"%d",&score)==1){
            printf("%d\n", score);
            if(score!=-1){
                sum+=score;
                count+=1;
            }
            else
                break;
        }
        average=sum/(float)count;
        if(average>max_average){
            max_average=average;
            strcpy(max_name,name);
        }

     }

    printf("max_average of %s is %.2f\n",max_name,max_average);
    if(!feof(fp1)){
        fprintf(stderr,"file format error\n");
        exit(-1);
    }
    fclose(fp1);
    return 0;
}
