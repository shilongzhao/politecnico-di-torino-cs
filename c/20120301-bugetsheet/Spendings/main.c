/**
 * @author webmaster@dreamhack.it.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */


#include <stdio.h>
#include <stdlib.h>
#define MAXLEN 50
int main(int argc,char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "wrong arguments!\n");
        exit(-1);
    }

    FILE *fin = fopen(argv[1], "a+");
    FILE *fout = fopen(argv[2], "a+");
    FILE *ft = fopen("TODAY.TXT", "r");
    if (fin == NULL || fout == NULL || ft == NULL) {
        fprintf(stderr, "file open failed\n");
        exit(-1);
    }

    int today;
    fscanf(ft, "%d", &today);
    char io[MAXLEN];
    float amount;
    char motive[MAXLEN];

    /**
     * The %c conversion specifier won't automatically skip any leading whitespace,
     * so if there's a stray newline in the input stream (from a previous entry, for example)
     * the scanf call will consume it immediately.
     * One way around the problem is to put a blank space before the conversion specifier in the format string:
     * scanf(" %c", &c);
     * The blank in the format string tells scanf to skip leading whitespace,
     * and the first non-whitespace character will be read with the %c conversion specifier.
     */


    while(fscanf(ft, "%s %f %s", io, &amount, motive) == 3) {
        if (io[0] == 'I') {
            fprintf(fin, "%d %.2f %s\n", today, amount, motive);
        }

        if (io[0] == 'O') {
            fprintf(fout, "%d %.2f %s\n", today, amount, motive);
        }
    }

    if (!feof(ft)) {
        fprintf(stderr, "file TODAY.TXT has wrong format\n");
        exit(-1);
    }

    rewind(fin);
    rewind(fout);

    float total_in = 0;
    float income = 0;
    while(fscanf(fin, "%*d %f %*s", &income) == 1) {
        total_in += income;
    }
    if (!feof(fin)) {
        fprintf(stderr, "file %s has wrong format\n", argv[1]);
        exit(-1);
    }

    float total_out = 0;
    float out_going = 0;
    while(fscanf(fout, "%*d %f %*s", &out_going) == 1) {
        total_out += out_going;
    }
    if (!feof(fout)) {
        fprintf(stderr, "file %s has wrong format\n", argv[2]);
        exit(-1);
    }

    printf("Balance is %.2f\n", total_in-total_out);
    fclose(fout);
    fclose(fin);
    fclose(ft);

    return 0;

}