//
// Created by ZhaoShilong on 2019-08-20.
//

#include <stdio.h>
#include <stdlib.h>

#define N 6
int main(int argc, char *argv[]) {
    if (argc != 4) {
        return -1;
    }
    
    int x = atoi(argv[1]) - 1;
    int y = atoi(argv[2]) - 1;
    
    int M = atoi(argv[3]); 
    
    int m1[N][N];
    int m2[N][N];
    
    FILE *f1 = fopen("/Users/zhaoshilong/ClionProjects/C201907/class12/map1.txt", "r");
    if (f1 == NULL) {
        return -1;
    }
    FILE *f2 = fopen("/Users/zhaoshilong/ClionProjects/C201907/class12/map2.txt", "r");
    if (f2 == NULL) {
        return -2;
    }
    
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            fscanf(f1, "%d", &m1[i][j]);
            fscanf(f2, "%d", &m2[i][j]);
        }
    }
    
    int flag = 1;
    
    int diff = m2[x][y] - m1[x][y];
    for (int i = x - M/2; i <= x + M/2; i++) {
        for (int j = y - M/2; j <= y + M/2; j++) {
            if (i < 0 || i >= N || j < 0 || j >= N) {
                continue;
            }
            
            if (m2[i][j] - m1[i][j] != diff) {
                flag = 0;
            }
        }
    }
    
    if (flag == 1) {
        printf("TECTONIC SHIFT");
    } 
    else {

        for (int i = x - M/2; i <= x + M/2; i++) {
            for (int j = y - M/2; j <= y + M/2; j++) {
                if (i < 0 || i >= N || j < 0 || j >= N) {
                    continue;
                }
                if (m2[i][j] != m1[i][j]) {
                    printf("%d, %d: %.2f%%\n", i+1, j+1, (m2[i][j] - m1[i][j]) * 100.0 / m1[i][j]);
                }
            }
        }
        
    }
    
    
    
    fclose(f1);
    fclose(f2);
    
}