//
// Created by ZhaoShilong on 2019-08-20.
//

#include <stdio.h>

#define NUM 7

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return -1;
    }
    FILE *fp = fopen(argv[1], "r");
    
    float roof[NUM][NUM] = {};
    
    int x, y; 
    
    float max_damage = 0;
    while (fscanf(fp, "%d %d", &x, &y) == 2) { // x = 0 y = 0 
        
        for (int i = x - 2; i <= x + 2; i++) {
            for (int j = y - 2; j <= y + 2; j++) {
                if (i < 0 || i >= NUM || j < 0 || j >= NUM) {
                    continue;
                }
                
                roof[i][j] += 0.2;
                
                if (roof[i][j] > max_damage) {
                    max_damage = roof[i][j];
                }
            }
        }
        
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i < 0 || i >= NUM || j < 0 || j >= NUM) {
                    continue;
                }
                
                roof[i][j] += 0.3;
                
                if (roof[i][j] > max_damage) {
                    max_damage = roof[i][j];
                }
            }
        }

        roof[x][y] += 0.5;
        
        if (roof[x][y] > max_damage) {
            max_damage = roof[x][y];
        }

    }
    
    fclose(fp);
    
    printf("The final condition of the roof: \n");
    for (int i = 0; i < NUM; i++) {
        for (int j = 0; j < NUM; j++) {
            printf("%.1f ", roof[i][j]);
        }
        printf("\n");
    }
    
    printf("Coordinate of the square with most serious deformation: ");
    
    for (int i = 0; i < NUM; i++) {
        for (int j = 0; j < NUM; j++) {
            if (roof[i][j] == max_damage) {
                printf("(%d %d) ", i, j);
            }
        }
    }
    
}