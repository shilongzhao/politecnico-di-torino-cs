//
// Created by ZhaoShilong on 2019-08-13.
//


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

typedef struct hotel_t hotel_t;

struct hotel_t {
    char name[41];
    char area[6];
    float price;
    int booked;
};


int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("wrong arguments!\n");
        return -1;
    }

    FILE *f = fopen(argv[1], "r");
    if (f == NULL) {
        printf("file open failed\n");
        return -1;
    }

    hotel_t a[40];

    int n = 0;
    while ( fscanf(f, "%s %s %f", a[n].name, a[n].area, &(a[n].price) ) == 3) {
        n++;
        a[n].booked = 0;
    }


    fclose(f);

    while (1) {
        printf("Input your request (1 book – with area, 2 print, 3 exit):\n");
        int x;
        scanf("%d" , &x);
        if (x == 3) {
            break;
        }
        else if (x == 2) {
            for (int i = 0; i < n; i++) {
                if (a[i].booked == 0) {
                    printf("%s %s %.2f\n", a[i].name, a[i].area, a[i].price);
                }
            }
        } else {
            char area[6];
            scanf("%s", area);

            int best_hotel_index = -1;
            float best_price = FLT_MAX;
            for (int i = 0; i < n; i++) {
                if ( (a[i].booked == 0) && (strcmp(a[i].area, area) == 0)) {
                    if (a[i].price < best_price) {
                        best_hotel_index = i;
                        best_price = a[i].price;
                    }
                }
            }

            if (best_hotel_index == -1) {
                printf("There is no hotel as requested\n");
            }
            else {
                a[best_hotel_index].booked = 1;
                printf("%s %s %.2f\n", a[best_hotel_index].name, a[best_hotel_index].area, a[best_hotel_index].price);
            }
        }

    }
}