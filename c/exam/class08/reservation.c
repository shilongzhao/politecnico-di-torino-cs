//
// Created by ZhaoShilong on 2019-08-11.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("wrong arguments!\n");
        return -1;
    }

    FILE *f = fopen(argv[1], "r");
    if (f == NULL) {
        printf("file open failed\n");
        return -1;
    }

    char names[40][41];
    char areas[40][6];
    float prices[40];

    int booked[40] = {};

    int n = 0;
    while ( fscanf(f, "%s %s %f", names[n], areas[n], &prices[n]) == 3) {
        n++;
    }

    fclose(f);

    while (1) {
        printf("Input your request (1 book – with area, 2 print, 3 exit):\n");
        int x;
        scanf("%d" , &x);
        if (x == 3) {
            break;
        }
        else if (x == 2) {
            for (int i = 0; i < n; i++) {
                if (booked[i] == 0) {
                    printf("%s %s %.2f\n", names[i], areas[i], prices[i]);
                }
            }
        } else {
            char area[6];
            scanf("%s", area);

            int best_hotel_index = -1;
            float best_price = FLT_MAX;
            for (int i = 0; i < n; i++) {
                if ( (booked[i] == 0) && (strcmp(areas[i], area) == 0)) {
                    if (prices[i] < best_price) {
                        best_hotel_index = i;
                        best_price = prices[i];
                    }
                }
            }

            if (best_hotel_index == -1) {
                printf("There is no hotel as requested\n");
            }
            else {
                booked[best_hotel_index] = 1;
                printf("%s %s %.2f\n", names[best_hotel_index], areas[best_hotel_index], prices[best_hotel_index]);
            }
        }

    }

}