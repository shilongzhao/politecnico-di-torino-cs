//
// Created by ZhaoShilong on 2019-08-11.
//

#include <stdio.h>

int main(int argc, char *argv[]) {
    FILE *f = fopen("/Users/zhaoshilong/ClionProjects/C201907/class08/students.txt", "w");
    if (f == NULL) {
        printf("file open failed\n");
        return -1;
    }
    
    fprintf(f, "%s %s %d-%d-%d %d", "Alex", "CS", 2018, 9, 1, 20);
    
    fclose(f);
    
    return 0;
}