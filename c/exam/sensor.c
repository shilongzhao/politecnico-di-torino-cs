//
// Created by ZhaoShilong on 2019-08-13.
//

#include <stdio.h>

int main(int argc, char *argv[]) {
    if (argc != 1) {
        printf("wrong arguments!\n");
        return -1;
    }

    FILE *f1 = fopen("/Users/zhaoshilong/ClionProjects/C201907/class09/first.txt", "r");
    if (f1 == NULL) {
        return -2;
    }

    FILE *f2 = fopen("/Users/zhaoshilong/ClionProjects/C201907/class09/second.txt", "r");
    if (f2 == NULL) {
        return -3;
    }

    FILE *f3 = fopen("/Users/zhaoshilong/ClionProjects/C201907/class09/burton.txt", "w");
    if (f3 == NULL) {
        return -4;
    }


    int h1, m1, s1;
    float v1;
    fscanf(f1, "%d:%d:%d %f", &h1, &m1, &s1, &v1);

    int h2, m2, s2;
    float v2;
    fscanf(f2, "%d:%d:%d %f", &h2, &m2, &s2, &v2);


    int t1 = h1 * 3600 + m1 * 60 + s1;
    int t2 = h2 * 3600 + m2 * 60 + s2;

    while (1) {
        while (t1 < t2) {
            fprintf(f3, "%02d:%02d:%02d %.2f\n", h1, m1, s1, v1);

            if (fscanf(f1, "%d:%d:%d %f", &h1, &m1, &s1, &v1) != 4) {
                break;
            }

            t1 = h1 * 3600 + m1 * 60 + s1;
        }

        while (t1 > t2) {
            fprintf(f3, "%02d:%02d:%02d %.2f\n", h2, m2, s2, v2);

            if (fscanf(f2, "%d:%d:%d %f", &h2, &m2, &s2, &v2) != 4) {
                break;
            }

            t2 = h2 * 3600 + m2 * 60 + s2;
        }

        while (t1 == t2) {
            fprintf(f3, "%02d:%02d:%02d %.2f\n", h2, m2, s2, (v1 + v2) / 2);

            if (fscanf(f1, "%d:%d:%d %f", &h1, &m1, &s1, &v1) != 4) {
                break;
            }
            t1 = h1 * 3600 + m1 * 60 + s1;

            if (fscanf(f2, "%d:%d:%d %f", &h2, &m2, &s2, &v2) != 4) {
                break;
            }
            t2 = h2 * 3600 + m2 * 60 + s2;
        }

        if (feof(f1) || feof(f2)) {
            break;
        }
    }

    if (feof(f1)) {
        while (fscanf(f2, "%d:%d:%d %f", &h2, &m2, &s2, &v2) == 4) {
            fprintf(f3, "%02d:%02d:%02d %.2f\n", h2, m2, s2, v2);
        }
    }

    if (feof(f2)) {
        while (fscanf(f1, "%d:%d:%d %f", &h1, &m1, &s1, &v1) == 4) {
            fprintf(f3, "%02d:%02d:%02d %.2f\n", h1, m1, s1, v1);
        }
    }


    fclose(f1);
    fclose(f2);
    fclose(f3);
}