//
// Created by ZhaoShilong on 2019-08-15.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int cal_age(int d, int m, int y, int bd, int bm, int by);

int is_leap_year(int y);

int get_index(char id[], char ids[][6], int N);

int get_days(int d, int m, int y);

int main(int argc, char *argv[]) {

    FILE *f1 = fopen("/Users/zhaoshilong/ClionProjects/C201907/class10/visits.txt", "r");
    if (f1 == NULL) {
        return -1;
    }

    FILE *f2 = fopen("/Users/zhaoshilong/ClionProjects/C201907/class10/prices.txt", "r");
    if (f2 == NULL) {
        return -2;
    }

    float prices[3];
    int n = 0;
    while(fscanf(f2, "%f", &prices[n]) == 1) {
        n++;
    }
    fclose(f2);


    int YEAR = atoi(argv[1]);


    if (strcmp("S", argv[2]) == 0) {

        int num_visits = 0;
        float income = 0;
        int total_minutes = 0;

        int d, m, y;
        int bd, bm, by;
        int eh, em;
        int lh, lm;

        while(fscanf(f1, "%*s %d/%d/%d %d:%d:%*d %d:%d:%*d %d/%d/%d",
                &d, &m, &y, &eh, &em, &lh, &lm, &bd, &bm, &by) == 10) {
            if (y == YEAR) {
                num_visits += 1;

                int age = cal_age(d, m, y, bd, bm, by);
                if (age <= 10) {
                    income += prices[0];
                }
                else if (age <= 17) {
                    income += prices[1];
                }
                else {
                    income += prices[2];
                }

                total_minutes += lh * 60 + lm - (eh * 60 + em);
            }
        }

        printf("Total number of visits of %d is %d\n", YEAR, num_visits);
        printf("Total income of %d is %.2f euro\n", YEAR, income);
        printf("Average visiting time of %d is %d minutes\n", YEAR, total_minutes/num_visits);
    }

    if (argv[2][0] == 'G') {
        int N = argc - 3;

        int last_visit_day[10] = {}; // 最多10个账号的上一次的访问日期 (整数, 从 1月1号 到 上一次访问日期 的天数 get_days)
        char ids[10][6];
        int suspicious[10] = {};

        for (int i = 3; i < argc; i++) {
            strcpy(ids[i - 3], argv[i]);
        }

        char id[6];
        int d, m, y;


        while(fscanf(f1, "%s %d/%d/%d %*d:%*d:%*d %*d:%*d:%*d %*d/%*d/%*d", id, &d, &m, &y) == 4) {
            if (y == YEAR) {
                int index = get_index(id, ids, N);
                if (index != -1) {
                    int day = get_days(d, m, y);

                    if (day - last_visit_day[index] <= 1) {
                        suspicious[index] = 1;
                    }

                    last_visit_day[index] = day;
                }
            }
        }

        int count = 0;
        for (int i = 0; i < 10; i++) {
            if (suspicious[i]) {
                printf("suspected account: %s\n", ids[i]);
                count++;
            }
        }

        printf("%d people visited the museum more than once in less than two days", count);

    }

    fclose(f1);

}

int get_index(char id[], char ids[][6], int N) {
    for (int i = 0; i < N; i++) {
        if (strcmp(ids[i], id) == 0) {
            return i;
        }
    }
    return -1;
}

int cal_age(int d, int m, int y, int bd, int bm, int by) {
    if (m > bm) {
        return y - by;
    }

    if (m == bm && d > bd) {
        return y - by;
    }

    return y - by - 1;
}

int get_days(int d, int m, int y) {
    int dd = d;
    if (is_leap_year(y)) {
        int days[13] = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (int i = 0; i < m; i++) {
            dd += days[i];
        }
    }
    else {
        int days[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (int i = 0; i < m; i++) {
            dd += days[i];
        }
    }
    return dd;
}


int is_leap_year(int y) {
    if (y % 100 == 0 && y % 400 == 0) {
        return 1;
    }

    if (y % 100 != 0 && y % 4 == 0) {
        return 1;
    }

    return 0;
}