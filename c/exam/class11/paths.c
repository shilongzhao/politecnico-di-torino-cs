//
// Created by ZhaoShilong on 2019-08-18.
//

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define N 11
#define M 4

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return -1;
    }

    FILE *fp = fopen("/Users/zhaoshilong/ClionProjects/C201907/class11/map.txt", "r");
    if (fp == NULL) {
        return -2;
    }

    char map[N][N + 1];
    for (int i = 0; i < N; i++) {
        fscanf(fp, "%s", map[i]);
    }
    fclose(fp);

    int total = 0;

    int x = -1, y = -1;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {

            if ( isdigit(map[i][j]) ) {

                if (map[i][j] == argv[1][0]) {
                    x = i;
                    y = j;
                }

                total += 1;
            }

            if ( map[i][j] == '+'){
                total += 1;
            }

        }
    }

    // x, y
    int len = 0;
    while (1) {
        len += 1;

        map[x][y] = 'X';

        if (x - 1 >= 0 && map[x-1][y] == '+') {
            x = x - 1;
        }
        else if (x + 1 < N && map[x + 1][y] == '+') {
            x = x + 1;
        }
        else if (y - 1 >= 0 && map[x][y - 1] == '+') {
            y = y - 1;
        }
        else if (y + 1 < N && map[x][y + 1] == '+') {
            y = y + 1;
        }
        else {
            break;
        }
    }

    printf("path %s length is %d\n", argv[1], len);
    printf("total length = %d, average length = %.2f", total, total/ (float) M);

}