//
// Created by ZhaoShilong on 2019-08-18.
//

#include <stdio.h>
#include <string.h>
#include <limits.h>

#define N 3

int main(int argc, char *argv[]) {
    if (argc != 3) {
        return -1;
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        return -2;
    }
    
    FILE *fp2 = fopen(argv[2], "w");
    if (fp2 == NULL) {
        return -3;
    }

    char m[2 * N][102];
    
    for (int i = 0; i < 2 * N; i++) {
        fgets(m[i], 102, fp);
    }
    
    fclose(fp);
    
    int M = strlen(m[0]) - 1;
    
    int max = INT_MIN;
    int pos = -1;
    
    for (int j = 0; j < M; j ++) {
        int count = 0;
        
        for (int i = 0; i < N; i++) {
            if (m[i][j] == 'X') {
                count += 1;
            }
        }
        
        for (int i = N; i < 2 * N; i++) {
            if (m[i][j] == 'X') {
                count -= 1;
            }
        }
        
        if (count > max) {
            max = count;
            pos = j;
        }
        
        fprintf(fp2, "%d ", count);
    }
    
    printf("max value = %d, position = %d\n", max, pos + 1);
    fclose(fp2);
}