#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char*argv[])
{
    if(argc!=4){
        fprintf(stderr,"wrong arguement.\n");
        return -1;
    }
    FILE*album;
    int year;
    char artist[30+1];
    char title[50+1];
    if(strcmp(argv[2],"-p")==0){
        album=fopen(argv[1],"r");
        if(album==NULL){
            fprintf(stderr,"File open failed.\n");
            return-2;
        }
        int i=0;
        int distinct=0;
        int temp[50];
        while(fscanf(album,"%d %s %s",&year,artist,title)==3){
            int selectedyear;
            selectedyear=atoi(argv[3]);
            int flag=0;
            if(year<selectedyear){
                printf("%d %s %s\n",year,artist,title);
                for(i=0;i<distinct;i++){
                    if(temp[i]==year){
                        flag=1;
                        break;
                    }
                }
                if(flag==0){
                    temp[distinct]==year;
                    distinct++;
                }
            }
        }
        printf("Distinct years:%d\n",distinct);
    }
    if(strcmp(argv[2],"-t")==0){
        album=fopen(argv[1],"r");
        if(album==NULL){
            fprintf(stderr,"File open failed.\n");
            return -2;
        }
        while(fscanf(album,"%d %s %s",&year,artist,title)==3){
            if(strcmp(argv[3],title)==0){
                printf("%d %s\n",year,artist);
            }
        }
    }
    if(!feof(album)){
        fprintf(stderr,"File has wrong format.\n");
        return -3;
    }
    fclose(album);

    return 0;
}
