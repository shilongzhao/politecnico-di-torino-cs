/**
* given matrix m, filled with 'o' (empty), 'y' and 'r'
* with r rows and c columns
* find if there is any line with at least 4
* identical chars.
*/
int connect4(char **m, int r, int c) {
  for (int i = 0; i < r; i++) {
    for (int j = 0; j < c; j++) {
      // line
        int b=1;
        for(int x=0;x<4;x++){
          if(m[i][j]==m[i][j+x]){
            continue;

          }
          else{
            b=0;
          }
        }
        if(b==1){
          return 1;
        }
        // column
         b=1;
         for(int x=0;x<4;x++){
           if(m[i][j]==m[i+x][j]){
             continue;
           }
           else{
             b=0;
           }
         }
         if(b==1){
           return 1;
         }
         //xiezhe
         b=1;
         for(int x=0;x<4;x++){
           if (i + x >= r || j+x >= c || m[i][j] != m[i+x][j+x]) {
             b = 0;
           }
          //  if(m[i][j]==m[i+x][j+x]){
          //    continue;
          //  }
          //  else{
          //    b=0;
          //  }

         }
         if(b==1){
           return 1;
         }
         //xiezhe youshang
         b=1;
         for(int x=0;x<4;x++){
           if (i-x < 0 || j + x >= c || m[i][j] != m[i-x][j+x]) {
             b = 0;
           }
          // if( m[i][j]==m[i-x][j+x]){
          //   continue;
          //
          // }
          // else{
          //   b=0;
          // }
         }
        if(b==1){
          return 1;
        }
        else{
          return 0;
        }
    }
  }
}
