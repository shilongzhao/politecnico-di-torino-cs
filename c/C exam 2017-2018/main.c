#include <stdio.h>
#include <stdlib.h>

/**
 * @author admin@szhao.org
 */

void invert_sequence(int *v1, int n, int *v2) {
    int i = 0, j = 0;
    while (i < n) {

        j += 1;
        // increase j until non-increasing, or end of array (j == n)
        while (v1[j] > v1[j - 1] && j < n) j++;

        for (int p = i, q = j - 1; p < j; p++, q--) {
            v2[p] = v1[q];
        }

        i = j;
    }
}

typedef struct node node;
struct node {
    int k;
    node *next;
};

void int2list(int n, struct node **head) {
    while (n != 0) {
        int k = n % 10; // get the least important digit

        node *t = malloc(sizeof(node));
        t->k = k;
        t->next = *head;
        *head = t;

        n = n / 10; // shift right (remove the least important digit)
    }
}
int main() {

    int a1[10] = {1,2,3,4,5,0,12,13,14,2};
    int a2[10] = {};

    printf("\n============= testing 1 =============\n");
    invert_sequence(a1, 10, a2);
    for (int i = 0; i < 10; i++) {
        printf("%d ", a2[i]);
    }

    printf("\n============= testing 2 =============\n");
    node *head = NULL;
    int2list(1357, &head);
    for (node *p = head; p != NULL; p = p->next) {
        printf("%d ", p->k);
    }


    return 0;
}