/**
 * @author sabrina.x1993@gmail.com.
 * One may distribute this program freely under the condition that
 * 1. the author information MUST be included
 * 2. the program MUST NOT be modified.
 */

/**
 * 20110919
 * */

#include <stdio.h>
#include <stdlib.h>
#define N 4
int main(int argc,char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "wrong arguments\n");
        exit(-1);
    }

    FILE *fmap = fopen(argv[1], "r");
    if (fmap == NULL) {
        fprintf(stderr, "fill open filed\n");
        exit(-1);
    }
    int map[N][N];
    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            if(fscanf(fmap,"%d",&map[i][j])!=1){
                fprintf(stderr,"error!\n");
            }
        }
    }
    int x1, y1, x2, y2;
    printf("please iput 4 numbers\n");
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    if (x1> x2 || y1>y2 || x1 > N || y1 > N || x2 > N || y2 > N || x1 < 0 || y1 < 0 || x2 < 0 || y2 < 0){
        fprintf(stderr, "File format error");
        exit(-1);
    }
    int max=0,min=3000;
    int a[5]={0,0,0,0,0};
    for(int x=x1;x<=x2;x++) {
        for (int y = y1; y <= y2; y++) {
            a[0]++;

            if (max < map[y][x]) {
                max = map[y][x];
            }
            if (min >= map[y][x]&& map[y][x]!=0) {
                min = map[y][x];
            }
            if (map[y][x] == 0) {
                a[1]++;
            }
            if (0 < map[y][x] && map[y][x] < 200) {
                a[2]++;
            }
            if (200 <= map[y][x] && map[y][x] <= 600) {
                a[3]++;
            }
            if (map[y][x] > 600) {
                a[4]++;
            }
        }
    }
    printf("maxmum altitude is %d\n",max);
    printf("minmum altitude is %d\n",min)  ;
    printf("Percentage of sea : %.2f%%\n",a[1]/(float)a[0]*100);
    printf("Percentage of plain areas :%.2f%%\n",a[2]/(float)a[0]*100);
    printf("Percentage of hills:%.2f%%\n",a[3]/(float)a[0]*100);
    printf("Percentage of mountains:%.2f%%\n",a[4]/(float)a[0]*100);
    fclose(fmap);

    return 0;
}